using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class OperateOneAward : MonoBehaviour
{
    public string OneAward;
    public GameObject OneInputField;
    public GameObject TextDisplayOneAward;
    [SerializeField] Button btnNext;

    private void Awake()
    {

        if (PlayerPrefs.HasKey("OneAward"))
        {
            OneAward = PlayerPrefs.GetString("OneAward");
            TextDisplayOneAward.GetComponent<Text>().text = OneAward;
        }

    }
    
    public void GoToTwoSceneAwards()
    {
        
        OneAward = PlayerPrefs.GetString("OneAward");
        OneAward = OneInputField.GetComponent<Text>().text;
        TextDisplayOneAward.GetComponent<Text>().text = OneAward;
        PlayerPrefs.SetString("OneAward", OneAward);
        PlayerPrefs.Save();
        SceneManager.LoadScene(3);
    }
}
