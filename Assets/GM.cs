using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GM : MonoBehaviour
{
    public static GM instance;
     string OneAward;
     string TwoAward;
     string ThreeAward;

    [SerializeField] Text txtScore;
    [SerializeField] Text DisplayOneAward;
    [SerializeField] Text DisplayTwoAward;
    [SerializeField] Text DisplayThreeAward;
    [SerializeField] Text TextError;
    
    
    private void Awake()
    {
        TextError.GetComponent<Text>().enabled = false;
        if (instance == null)
        {
            instance = this;
        }
        

        if ((PlayerPrefs.HasKey("OneAward")&&PlayerPrefs.HasKey("TwoAward"))&& PlayerPrefs.HasKey("ThreeAward"))
        {
            OneAward = PlayerPrefs.GetString("OneAward");
            TwoAward = PlayerPrefs.GetString("TwoAward");
            ThreeAward = PlayerPrefs.GetString("ThreeAward");
            
            DisplayOneAward.text = OneAward;
            DisplayTwoAward.text = TwoAward;
            DisplayThreeAward.text = ThreeAward;

        }
        int Score = PlayerPrefs.GetInt("SavePoints");
        txtScore.text = Score.ToString();

    }
    public void ChangePointsOneAward()
    {
        MinusPointsOneAward(1000);
    }
    public void ChangePointsTwoAward()
    {
        MinusPointsTwoAward(2000);
    }
    public void ChangePointsThreeAward()
    {
        MinusPointsThreeAward(3000);
    }
    void MinusPointsOneAward(int Point)
    {
        int Score = PlayerPrefs.GetInt("SavePoints");
        if (Score < Point)
        {
            TextError.GetComponent<Text>().enabled = true;
        }
        else
        {
            DisplayScore(1000);
        }
    }
    void MinusPointsTwoAward(int Point)
    {
        int Score = PlayerPrefs.GetInt("SavePoints");
        if (Score > Point)
        {
            DisplayScore(2000);
        }
        else
        {
            TextError.GetComponent<Text>().enabled = true;
        }
    }
    void MinusPointsThreeAward(int Point)
    {
        int Score = PlayerPrefs.GetInt("SavePoints");
        if (Score > Point)
        {
            DisplayScore(3000);
        }
        else
        {
            TextError.GetComponent<Text>().enabled = true;
        }
       
    }
    void DisplayScore(int Point)
    {
        int Score = PlayerPrefs.GetInt("SavePoints");
        int Points = Score - Point;
        PlayerPrefs.SetInt("SavePoints", Points);
        PlayerPrefs.Save();
        txtScore.text = Points.ToString();
    }

}
