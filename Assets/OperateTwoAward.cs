using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class OperateTwoAward : MonoBehaviour
{
    public string TwoAward;
    public GameObject TwoInputField;
    public GameObject TextDisplayTwoAward;

    private void Awake()
    {

        if (PlayerPrefs.HasKey("TwoAward"))
        {
            TwoAward = PlayerPrefs.GetString("TwoAward");
            TextDisplayTwoAward.GetComponent<Text>().text = TwoAward;
        }

    }
    public void GoToThreeSceneAwards()
    {
        TwoAward = PlayerPrefs.GetString("TwoAward");
        TwoAward = TwoInputField.GetComponent<Text>().text;
        TextDisplayTwoAward.GetComponent<Text>().text = TwoAward;
        PlayerPrefs.SetString("TwoAward", TwoAward);
        PlayerPrefs.Save();
        SceneManager.LoadScene(4);
    }
}
