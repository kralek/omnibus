using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MoveScenes : MonoBehaviour
{

    public void ResetPlayer()
    {
        PlayerPrefs.DeleteKey("OneAward");
        PlayerPrefs.DeleteKey("PIN1");
        PlayerPrefs.DeleteKey("PIN2");
        PlayerPrefs.SetInt("SavePoints", 5);

    }
    public void GoToGame()
    {
        SceneManager.LoadScene(1);
    }
    public void GoToMenu()
    {
        SceneManager.LoadScene(0);
    }
    public void GoToLeng()
    {
        SceneManager.LoadScene(6);
    }
    public void GoToSettings()
    {
        if (!PlayerPrefs.HasKey("PIN1"))
        {
            SceneManager.LoadScene(7);
        }
        else
        {
            SceneManager.LoadScene(9);
        }
        
    }
    public void GoToAwardsNew()
    {
        
            SceneManager.LoadScene(2);
        
        
            //SceneManager.LoadScene(2);
        
    }
    public void GoToAwards()
    {
        if ((PlayerPrefs.HasKey("OneAward"))&& (PlayerPrefs.HasKey("TwoAward"))&& (PlayerPrefs.HasKey("ThreeAward")))
        {
            SceneManager.LoadScene(5);
        }
        else
        {
            SceneManager.LoadScene(2);
        }
           
    }
    public void Exit()
    {
        Application.Quit();
    }
    public void ChangePIN()
    {
        SceneManager.LoadScene(7);
    }

}


