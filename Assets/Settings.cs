using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Settings : MonoBehaviour
{
    string PIN1;
    string PIN2;
    public GameObject PIN1InputField;
    public GameObject PIN2InputField;
    public GameObject TextDisplayPIN1;
    public GameObject TextDisplayPIN2;
    [SerializeField] Text TextError;

    private void Awake()
    {
        TextError.GetComponent<Text>().enabled = false;
    }
    public void CheckPIN()
    {
        
        PIN1 = PIN1InputField.GetComponent<Text>().text;
        PIN2 = PIN2InputField.GetComponent<Text>().text;
        TextDisplayPIN1.GetComponent<Text>().text = PIN1;
        TextDisplayPIN2.GetComponent<Text>().text = PIN2;

        if (PIN1 == "" ||  PIN2 == "" || PIN1 != PIN2)
        {
            TextError.color = Color.red;
            TextError.GetComponent<Text>().enabled = true;
        }
        else
        {
            PlayerPrefs.SetString("PIN1", PIN1);
            PlayerPrefs.SetString("PIN2", PIN2);
            SceneManager.LoadScene(8);
            
        }
    }
}
