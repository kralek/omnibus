using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class OperateThreeAward : MonoBehaviour
{
    public string ThreeAward;
    public GameObject ThreeInputField;
    public GameObject TextDisplayThreeAward;

    private void Awake()
    {

        if (PlayerPrefs.HasKey("ThreeAward"))
        {
            ThreeAward = PlayerPrefs.GetString("ThreeAward");
            TextDisplayThreeAward.GetComponent<Text>().text = ThreeAward;
        }

    }
    public void GoToSceneAwards()
    {
        ThreeAward = PlayerPrefs.GetString("ThreeAward");
        ThreeAward = ThreeInputField.GetComponent<Text>().text;
        TextDisplayThreeAward.GetComponent<Text>().text = ThreeAward;
        PlayerPrefs.SetString("ThreeAward", ThreeAward);
        PlayerPrefs.Save();
        SceneManager.LoadScene(5);
    }
}
