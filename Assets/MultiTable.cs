using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class MultiTable : MonoBehaviour
{
    public static MultiTable instance;
    AudioSource audios;
    [SerializeField] AudioClip[] Fileaudios;
    [SerializeField] Button btnStartGame, btnStopGame;
    [SerializeField] GameObject Sounds, MuteSound;
    [SerializeField] Text txtScore;
    [SerializeField] Text txtTimer;
    [SerializeField] Text[] Buttons;
    [SerializeField] Text txtNum1, txtNum2;
    Text txtAnswer1, txtAnswer2, txtAnswer3, txtResult;
    public List<int> answers = new List<int>();
    float timeRemaining = 5f;
    bool isStartGame = false;
    public int Points, num1, num2, answer0, answer1, answer2, answer3, index;
    //int result;


    void Awake()
    {
        MuteSound.gameObject.SetActive(false);
        audios = GetComponent<AudioSource>();
        if (instance == null)
        {
            instance = this;
        }

        isStartGame = false;
        Points = PlayerPrefs.GetInt("SavePoints");
        txtScore.text = Points.ToString();
        txtTimer.text = timeRemaining.ToString();

    }
    void Update()
    {
        if (isStartGame)
        {
            Timer();
        }
    }
    void Timer()
    {
        if (timeRemaining > 0)
        {
            timeRemaining -= Time.deltaTime;
            txtTimer.text = timeRemaining.ToString("f0");
        }
        else if (timeRemaining <= 0)
        {
            //PlaySounds(2);
            MinusPoints();
            ResetTimer();
            RemonteList();
            RandomMathOper();
        }
    }

    void RandomMathOper()
    {
        int num1 = Random.Range(0, 11);
        int num2 = Random.Range(0, 11);

        txtNum1.text = num1.ToString();
        txtNum2.text = num2.ToString();

        int result = num1 * num2;

        int answer1 = Random.Range(0, 101);
        int answer2 = Random.Range(0, 101);
        int answer3 = Random.Range(0, 101);

        answers.Add(result);
        answers.Add(answer1);
        answers.Add(answer2);
        answers.Add(answer3);

        int rand = Random.Range(0, 4);

        Buttons[rand].GetComponentInChildren<Text>().text = answers[0].ToString();

        if (rand == 0)
        {
            Buttons[rand + 1].GetComponentInChildren<Text>().text = answers[1].ToString();
            Buttons[rand + 2].GetComponentInChildren<Text>().text = answers[2].ToString();
            Buttons[rand + 3].GetComponentInChildren<Text>().text = answers[3].ToString();
        }
        else if (rand == 1)
        {
            Buttons[rand - 1].GetComponentInChildren<Text>().text = answers[2].ToString();
            Buttons[rand + 1].GetComponentInChildren<Text>().text = answers[1].ToString();
            Buttons[rand + 2].GetComponentInChildren<Text>().text = answers[3].ToString();
        }
        else if (rand == 2)
        {
            Buttons[rand - 2].GetComponentInChildren<Text>().text = answers[3].ToString();
            Buttons[rand - 1].GetComponentInChildren<Text>().text = answers[1].ToString();
            Buttons[rand + 1].GetComponentInChildren<Text>().text = answers[2].ToString();
        }
        else
        {
            Buttons[rand - 3].GetComponentInChildren<Text>().text = answers[1].ToString();
            Buttons[rand - 2].GetComponentInChildren<Text>().text = answers[2].ToString();
            Buttons[rand - 1].GetComponentInChildren<Text>().text = answers[3].ToString();
        }

    }
    void OnVibration()
    {
        Handheld.Vibrate();
    }
    public void MuteSounds()
    {
        MuteSound.gameObject.SetActive(true);
        audios.mute = true;
    }
    public void OnSounds()
    {
        MuteSound.gameObject.SetActive(false);
        audios.mute = false;
    }

    void PlaySounds(int IndexClip)
    {
        audios.clip = Fileaudios[IndexClip];
        audios.Play();
    }
    public void RemonteList()
    {
        answers.Clear();
    }

    public void AddPoints()
    {
        Points++;
        txtScore.text = Points.ToString();
    }
    void MinusPoints()
    {
        Points--;
        txtScore.text = Points.ToString();
    }
    public void StopGame()
    {
        isStartGame = false;
        btnStopGame.gameObject.SetActive(false);
        btnStartGame.gameObject.SetActive(true);
        btnStartGame.interactable = true;
        PlayerPrefs.SetInt("SavePoints", Points);
        PlayerPrefs.Save();
        SceneManager.LoadScene(1);
    }
    public void StartGame()
    {
        RandomMathOper();
        txtTimer.text = timeRemaining.ToString();
        isStartGame = true;
        btnStartGame.gameObject.SetActive(false);
        btnStopGame.gameObject.SetActive(true);
        //btnStartGame.interactable = false;

    }
    void ResetTimer()
    {
        timeRemaining = 5f;
        txtTimer.text = timeRemaining.ToString();
    }

    public void CheckBtnZero()
    {
        string answer0 = GameObject.FindGameObjectWithTag("btn0").GetComponentInChildren<Text>().text;

        if (answers[0].ToString() == answer0.ToString())
        {
            PlaySounds(1);
            AddPoints();
            ResetTimer();
            RemonteList();
            RandomMathOper();
        }
       
        else
        {
            PlaySounds(0);
            OnVibration();
            MinusPoints();
            ResetTimer();
            RemonteList();
            RandomMathOper();
        }

    }
    public void CheckBtnOne()
    {
        string answer1 = GameObject.FindGameObjectWithTag("btn1").GetComponentInChildren<Text>().text;


        if (answers[0].ToString() == answer1.ToString())
        {
            PlaySounds(1);
            AddPoints();
            ResetTimer();
            RemonteList();
            RandomMathOper();
        }

        else
        {
            PlaySounds(0);
            OnVibration();
            MinusPoints();
            ResetTimer();
            RemonteList();
            RandomMathOper();
        }

    }
    public void CheckBtnTwo()
    {
        string answer2 = GameObject.FindGameObjectWithTag("btn2").GetComponentInChildren<Text>().text;


        if (answers[0].ToString() == answer2.ToString())
        {
            PlaySounds(1);
            AddPoints();
            ResetTimer();
            RemonteList();
            RandomMathOper();
        }

        else
        {
            PlaySounds(0);
            OnVibration();
            MinusPoints();
            ResetTimer();
            RemonteList();
            RandomMathOper();
        }

    }
    public void CheckBtnThree()
    {
        string answer3 = GameObject.FindGameObjectWithTag("btn3").GetComponentInChildren<Text>().text;


        if (answers[0].ToString() == answer3.ToString())
        {
            PlaySounds(1);
            AddPoints();
            ResetTimer();
            RemonteList();
            RandomMathOper();
        }

        else
        {
            PlaySounds(0);
            OnVibration();
            MinusPoints();
            ResetTimer();
            RemonteList();
            RandomMathOper();
        }

    }

}
