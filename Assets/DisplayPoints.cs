using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayPoints : MonoBehaviour
{
    [SerializeField] Text txtScore;

    private void Awake()
    {
        int Score = PlayerPrefs.GetInt("SavePoints");
        txtScore.text = Score.ToString();

    }
}
