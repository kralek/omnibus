using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EnterPIN : MonoBehaviour
{
    string SetPIN1;
    string SetPIN;
    public GameObject InputField;
    public GameObject TextDisplay;
    [SerializeField] Text TextError;

    private void Awake()
    {
        TextError.GetComponent<Text>().enabled = false;

        if (PlayerPrefs.HasKey("PIN1"))
        {
            SetPIN1 = PlayerPrefs.GetString("PIN1");
        }

    }
    public void CheckPINOn()
    {
        //string Error = GameObject.FindGameObjectWithTag("Error").GetComponent<Text>().text;

        SetPIN = InputField.GetComponent<Text>().text;
        TextDisplay.GetComponent<Text>().text = SetPIN;
        if (SetPIN == SetPIN1)
        {
            SceneManager.LoadScene(8);
        }
        else
        {
            TextError.color = Color.red;
            TextError.GetComponent<Text>().enabled = true;
        }
    }
}
    
