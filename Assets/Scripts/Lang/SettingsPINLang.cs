﻿using UnityEngine;
using UnityEngine.UI;


public class SettingsPINLang : MonoBehaviour
{
    [SerializeField] Text Text1, Text2, Text3, Text4, Text5;
    string[] pl = { "USTAW PIN","WPISZ PIN", "POTWIERDŹ PIN","WPROWADZONE PINY SĄ NIEZGODNE","ZAPISZ" };
    string[] en = { "SET PIN", "ENTER PIN", "CHANGE PIN", "THE ENTERED PINS ARE INCOMPATIBLE", "SAVE" };
    string[] de = { "PIN EINSTELLEN", "PIN EINGEBEN", "PIN BESTÄTIGEN", "Die eingegebenen Pins sind nicht kompatibel", "SPEICHERN" };
    string[] fe = { "SET PIN", "ENTRER PIN", "CONFIRMER LE NIP", "LES BROCHES ENTRÉES SONT INCOMPATIBLES", "SAUVEGARDER" };
    string[] es = { "ESTABLECER PIN", "INGRESE SU PIN", "CONFIRMAR PIN", "LOS PINES INTRODUCIDOS SON INCOMPATIBLES", "SALVAR" };
    string[] ru = { "УСТАНОВИТЬ ПИН-код", "ВВЕДИТЕ ПИН-код", "ПОДТВЕРДИТЬ PIN", "Введенные PIN-коды несовместимы", "СПАСТИ" };

    private void Awake()
    {
        if (Application.systemLanguage == SystemLanguage.Polish)
        {
            setLang(pl, 0);
        }
        else if (Application.systemLanguage == SystemLanguage.German)
        {
            setLang(de, 0);
        }
        else if (Application.systemLanguage == SystemLanguage.French)
        {
            setLang(fe, 0);
        }
        else if (Application.systemLanguage == SystemLanguage.Spanish)
        {
            setLang(es, 0);
        }
        else if (Application.systemLanguage == SystemLanguage.Russian)
        {
            setLang(ru, 0);
        }
        else
        {
            setLang(en, 0);
        }
    }
    public void setLang(string[] Leng, int Index)
    {
        Text1.text = Leng[Index].ToString();
        Text2.text = (Leng[Index + 1]).ToString();
        Text3.text = (Leng[Index + 2]).ToString();
        Text4.text = (Leng[Index + 3]).ToString();
        Text5.text = (Leng[Index + 4]).ToString();
    }
}
