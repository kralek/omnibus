﻿using UnityEngine;
using UnityEngine.UI;


public class GameLang : MonoBehaviour
{
    [SerializeField] Text Score, Start, Stop, Menu;
    string[] pl = { "Zdobyte punkty", "START", "STOP", "MENU" };
    string[] en = { "Score", "START", "STOP", "MENU" };
    string[] de = { "Punkte erhalten", "START", "STOP", "MENU" };
    string[] fe = { "Points gagnés", "START", "STOP", "MENU" };
    string[] es = { "Puntos ganados", "START", "STOP", "MENU" };
    string[] ru = { "Заработанные баллы", "НАЧНИТЕ", "ОСТАНОВКА", "МЕНЮ" };

    private void Awake()
    {
        if (Application.systemLanguage == SystemLanguage.Polish)
        {
            setLang(pl, 0);
        }
        else if (Application.systemLanguage == SystemLanguage.German)
        {
            setLang(de, 0);
        }
        else if (Application.systemLanguage == SystemLanguage.French)
        {
            setLang(fe, 0);
        }
        else if (Application.systemLanguage == SystemLanguage.Spanish)
        {
            setLang(es, 0);
        }
        else if (Application.systemLanguage == SystemLanguage.Russian)
        {
            setLang(ru, 0);
        }
        else
        {
            setLang(en, 0);
        }
    }
    public void setLang(string[] Leng, int Index)
    {
        Score.text = Leng[Index].ToString();
        Start.text = (Leng[Index + 1]).ToString();
        Stop.text = (Leng[Index + 2]).ToString();
        Menu.text = (Leng[Index + 3]).ToString();
    }
}
