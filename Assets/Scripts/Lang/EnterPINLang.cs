﻿using UnityEngine;
using UnityEngine.UI;


public class EnterPINLang : MonoBehaviour
{
    public static EnterPINLang instance;
    [SerializeField] Text Text1, Text2, Text3;
    string[] pl = { "WPROWADŹ PIN", "NIEPOPRAWNY PIN", "OK" };
    string[] en = { "ENTER PIN", "INCORRECT PIN", "OK" };
    string[] de = { "PIN EINGEBEN", "FALSCHE PIN", "OK"};
    string[] fe = { "ENTRER PIN", "PIN INCORRECT", "OK"};
    string[] es = { "INGRESE SU PIN", "PIN INCORRECTO", "OK"};
    string[] ru = { "ВВЕДИТЕ ПИН-код", "НЕПРАВИЛЬНЫЙ ПИН", "OK"};

    private void Awake()
    {

        if (Application.systemLanguage == SystemLanguage.Polish)
        {
            setLang(pl, 0);
        }
        else if (Application.systemLanguage == SystemLanguage.German)
        {
            setLang(de, 0);
        }
        else if (Application.systemLanguage == SystemLanguage.French)
        {
            setLang(fe, 0);
        }
        else if (Application.systemLanguage == SystemLanguage.Spanish)
        {
            setLang(es, 0);
        }
        else if (Application.systemLanguage == SystemLanguage.Russian)
        {
            setLang(ru, 0);
        }
        else
        {
            setLang(en, 0);
        }
    }
    void setLang(string[] Leng, int Index)
    {
        Text1.text = Leng[Index].ToString();
        Text2.text = (Leng[Index + 1]).ToString();
        Text3.text = (Leng[Index + 2]).ToString();

    }
   
}
