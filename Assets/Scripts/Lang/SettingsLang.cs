﻿using UnityEngine;
using UnityEngine.UI;


public class SettingsLang : MonoBehaviour
{
    [SerializeField] Text Text1, Text2, Text3, Text4;
    string[] pl = { "USTAWIENIA", "ZMIEŃ NAGRODY", "ZMIEŃ PIN", "POWRÓT DO MENU" };
    string[] en = { "SETTINGS", "CHANGE THE REWARDS", "CHANGE PIN", "GO TO MENU" };
    string[] de = { "DIE EINSTELLUNGEN", "ÄNDERN SIE DIE BELOHNUNGEN", "ZURÜCK ZUM MENÜ" };
    string[] fe = { "RÉGLAGES", "CHANGER LES RÉCOMPENSES", "CHANGER LE PIN", "RETOUR AU MENU" };
    string[] es = { "AJUSTES", "CAMBIAR LAS RECOMPENSAS", "CAMBIAR PIN", "VOLVER AL MENÚ" };
    string[] ru = { "НАСТРОЙКИ", "ИЗМЕНИТЬ НАГРАДЫ", "ИЗМЕНИТЬ ПИН", "НАЗАД К МЕНЮ" };

    private void Awake()
    {
        if (Application.systemLanguage == SystemLanguage.Polish)
        {
            setLang(pl, 0);
        }
        else if (Application.systemLanguage == SystemLanguage.German)
        {
            setLang(de, 0);
        }
        else if (Application.systemLanguage == SystemLanguage.French)
        {
            setLang(fe, 0);
        }
        else if (Application.systemLanguage == SystemLanguage.Spanish)
        {
            setLang(es, 0);
        }
        else if (Application.systemLanguage == SystemLanguage.Russian)
        {
            setLang(ru, 0);
        }
        else
        {
            setLang(en, 0);
        }
    }
    public void setLang(string[] Leng, int Index)
    {
        Text1.text = Leng[Index].ToString();
        Text2.text = (Leng[Index + 1]).ToString();
        Text3.text = (Leng[Index + 2]).ToString();
    }
}
