﻿using UnityEngine;
using UnityEngine.UI;


public class OneAwardLang : MonoBehaviour
{
    [SerializeField] Text Text1, Text2, Text3, Text4;
    string[] pl = { "Dodaj pierwszą nagrodę", "Nagroda za 1000p", "Wpisz pierwszą nagrodę. np.Pizza", "NEXT" };
    string[] en = { "Add the first prize", "Reward for 1000p", "Enter the first award. e.g. Pizza", "NEXT" };
    string[] de = { "Fügen Sie den ersten Preis hinzu", "Belohnung für 1000p", "Geben Sie den ersten Preis ein. z.B. Pizza", "NEXT" };
    string[] fe = { "Ajouter le premier prix", "Récompense pour 1000p", "Entrez le premier prix. p. ex. pizza", "NEXT" };
    string[] es = { "Agrega el primer premio", "Recompensa por 1000p", "Ingrese el primer premio. p. ej., pizza", "NEXT" };
    string[] ru = { "Добавить первый приз", "Приз за 1000p", "Войдите в первый приз. например, пицца", "СЛЕДУЮЩИЙ" };

    private void Awake()
    {

        if (Application.systemLanguage == SystemLanguage.Polish)
        {
            setLang(pl, 0);
        }
        else if (Application.systemLanguage == SystemLanguage.German)
        {
            setLang(de, 0);
        }
        else if (Application.systemLanguage == SystemLanguage.French)
        {
            setLang(fe, 0);
        }
        else if (Application.systemLanguage == SystemLanguage.Spanish)
        {
            setLang(es, 0);
        }
        else if (Application.systemLanguage == SystemLanguage.Russian)
        {
            setLang(ru, 0);
        }
        else
        {
            setLang(en, 0);
        }
    }
    public void setLang(string[] Leng, int Index)
    {
        Text1.text = Leng[Index].ToString();
        Text2.text = (Leng[Index + 1]).ToString();
        Text3.text = (Leng[Index + 2]).ToString();
        Text4.text = (Leng[Index + 3]).ToString();
    }
}
