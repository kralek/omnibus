﻿using UnityEngine;
using UnityEngine.UI;


public class ThreeAwardLang : MonoBehaviour
{
    [SerializeField] Text Text1, Text2, Text3, Text4;
    string[] pl = { "Dodaj trzecią nagrodę", "Nagroda za 3000p", "Wpisz trzecią nagrodę. np.Dodatkowa godzina grania na konsoli", "ZAPISZ" };
    string[] en = { "Add a third prize", "Reward for 3000p", "Enter the third award. e.g. An extra hour of playing on the console", "SAVE" };
    string[] de = { "Fügen Sie einen dritten Preis hinzu", "Belohnung für 3000p", "Geben Sie den dritten Preis ein. B. eine zusätzliche Stunde auf der Konsole spielen", "SPEICHERN" };
    string[] fe = { "Ajouter un troisième prix", "Récompense pour 3000p", "Entrez le troisième prix. par exemple une heure supplémentaire de lecture sur la console", "SAUVEGARDER" };
    string[] es = { "Agrega un tercer premio", "Recompensa por 3000p", "Ingrese el tercer premio. por ejemplo, una hora extra de juego en la consola", "SALVAR" };
    string[] ru = { "Добавить третий приз", "Награда за 3000p", "Получите третий приз. например, дополнительный час игры на консоли", "СПАСТИ" };

    private void Awake()
    {

        if (Application.systemLanguage == SystemLanguage.Polish)
        {
            setLang(pl, 0);
        }
        else if (Application.systemLanguage == SystemLanguage.German)
        {
            setLang(de, 0);
        }
        else if (Application.systemLanguage == SystemLanguage.French)
        {
            setLang(fe, 0);
        }
        else if (Application.systemLanguage == SystemLanguage.Spanish)
        {
            setLang(es, 0);
        }
        else if (Application.systemLanguage == SystemLanguage.Russian)
        {
            setLang(ru, 0);
        }
        else
        {
            setLang(en, 0);
        }
    }
    public void setLang(string[] Leng, int Index)
    {
        Text1.text = Leng[Index].ToString();
        Text2.text = (Leng[Index + 1]).ToString();
        Text3.text = (Leng[Index + 2]).ToString();
        Text4.text = (Leng[Index + 3]).ToString();
    }
}
