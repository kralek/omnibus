﻿using UnityEngine;
using UnityEngine.UI;


public class GameMenuLang : MonoBehaviour
{
    [SerializeField] Text Score, Text1, Text2, Text3;
    [SerializeField] Image Flag;
    [SerializeField] Sprite[] Flags;
    string[] pl = { "Zdobyte punkty", "Tabliczka mnożenia","Sklep","Wyjście"};
    string[] en = { "Score", "Multiplication table","Store","Exit"};
    string[] de = { "Punkte erhalten", "Multiplikationstabelle", "Geschäft", "Ausgang" };
    string[] fe = { "Points gagnés", "Table de multiplication", "Magasin", "Sortir" };
    string[] es = { "Puntos ganados", "Tabla de multiplicación", "Tienda", "Salida" };
    string[] ru = { "Заработанные баллы", "Таблица умножения", "Магазин", "Выход" };

    private void Awake()
    {

        if (Application.systemLanguage == SystemLanguage.Polish)
        {
            Flag.sprite = Flags[1];
            setLang(pl, 0);
        }
        else if (Application.systemLanguage == SystemLanguage.German)
        {
            Flag.sprite = Flags[2];
            setLang(de, 0);
        }
        else if (Application.systemLanguage == SystemLanguage.French)
        {
            Flag.sprite = Flags[3];
            setLang(fe, 0);
        }
        else if (Application.systemLanguage == SystemLanguage.Spanish)
        {
            Flag.sprite = Flags[4];
            setLang(es, 0);
        }
        else if (Application.systemLanguage == SystemLanguage.Russian)
        {
            Flag.sprite = Flags[5];
            setLang(ru, 0);
        }
        else
        {
            Flag.sprite = Flags[0];
            setLang(en, 0);
        }
    }
   public void setLang(string[] Leng, int Index)
    {
        Score.text = Leng[Index].ToString();
        Text1.text = (Leng[Index+1]).ToString();
        Text2.text = (Leng[Index+2]).ToString();
        Text3.text = (Leng[Index+3]).ToString();
    }
}
