﻿using UnityEngine;
using UnityEngine.UI;


public class AwardsMenuLang : MonoBehaviour
{
    [SerializeField] Text Text1, Text2, Text3, Text4, Text5, Text6;
    string[] pl = { "Zdobyte punkty", "WYMIEŃ", "WYMIEŃ", "WYMIEŃ", "POWRÓT DO MENU", "Brak wystarczającej ilości punktów!" };
    string[] en = { "Score", "REPLACE", "REPLACE", "REPLACE", "BACK TO MENU", "Not enough points!" };
    string[] de = { "Punkte erhalten", "ERSETZEN", "ERSETZEN", "ERSETZEN", "ZURÜCK ZUM MENÜ", "Nicht genügend Punkte!" };
    string[] fe = { "Points gagnés", "REMPLACER", "REMPLACER", "REMPLACER", "RETOUR AU MENU", "Pas assez de points!" };
    string[] es = { "Puntos ganados", "REEMPLAZAR", "REEMPLAZAR", "REEMPLAZAR", "VOLVER AL MENÚ", "No hay suficientes puntos!" };
    string[] ru = { "Заработанные баллы", "ЗАМЕНЯТЬ", "ЗАМЕНЯТЬ", "ЗАМЕНЯТЬ", "НАЗАД К МЕНЮ", "Мало очков!" };

    private void Awake()
    {
        
        if (Application.systemLanguage == SystemLanguage.Polish)
        {
            setLang(pl, 0);
        }
        else if (Application.systemLanguage == SystemLanguage.German)
        {
            setLang(de, 0);
        }
        else if (Application.systemLanguage == SystemLanguage.French)
        {
            setLang(fe, 0);
        }
        else if (Application.systemLanguage == SystemLanguage.Spanish)
        {
            setLang(es, 0);
        }
        else if (Application.systemLanguage == SystemLanguage.Russian)
        {
            setLang(ru, 0);
        }
        else
        {
            setLang(en, 0);
        }
    }
    public void setLang(string[] Leng, int Index)
    {
        Text1.text = Leng[Index].ToString();
        Text2.text = (Leng[Index + 1]).ToString();
        Text3.text = (Leng[Index + 2]).ToString();
        Text4.text = (Leng[Index + 3]).ToString();
        Text5.text = (Leng[Index + 4]).ToString();
        Text6.text = (Leng[Index + 5]).ToString();
       
    }
}
