﻿using UnityEngine;
using UnityEngine.UI;


public class TwoAwardLang : MonoBehaviour
{
    [SerializeField] Text Text1, Text2, Text3, Text4;
    string[] pl = { "Dodaj drugą nagrodę", "Nagroda za 2000p", "Wpisz drugą nagrodę. np.Wyście do kina", "NEXT" };
    string[] en = { "Add a second award", "Reward for 2000p", "Enter the second award. e.g. Going to the cinema", "NEXT" };
    string[] de = { "Fügen Sie einen zweiten Preis hinzu", "Auszeichnung für 2000p", "Geben Sie den zweiten Preis ein. B. ins Kino gehen", "NEXT" };
    string[] fe = { "Ajouter un deuxième prix", "Prix ​​pour 2000p", "Entrez le deuxième prix. par exemple aller au cinéma", "NEXT" };
    string[] es = { "Agrega un segundo premio", "Premio por 2000p", "Ingrese el segundo premio. por ejemplo, ir al cine", "NEXT" };
    string[] ru = { "Добавить второй приз", "Премия за 2000p", "Получите второй приз. например, пойти в кино", "NEXT" };

    private void Awake()
    {

        if (Application.systemLanguage == SystemLanguage.Polish)
        {
            setLang(pl, 0);
        }
        else if (Application.systemLanguage == SystemLanguage.German)
        {
            setLang(de, 0);
        }
        else if (Application.systemLanguage == SystemLanguage.French)
        {
            setLang(fe, 0);
        }
        else if (Application.systemLanguage == SystemLanguage.Spanish)
        {
            setLang(es, 0);
        }
        else if (Application.systemLanguage == SystemLanguage.Russian)
        {
            setLang(ru, 0);
        }
        else
        {
            setLang(en, 0);
        }
    }
    public void setLang(string[] Leng, int Index)
    {
        Text1.text = Leng[Index].ToString();
        Text2.text = (Leng[Index + 1]).ToString();
        Text3.text = (Leng[Index + 2]).ToString();
        Text4.text = (Leng[Index + 3]).ToString();
    }
}
