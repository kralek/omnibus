﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void UnityEngine.Advertisements.Advertisement::.cctor()
extern void Advertisement__cctor_mB44256768A89BD38E6BA522B4C178350A1983B74 (void);
// 0x00000002 System.Boolean UnityEngine.Advertisements.Advertisement::get_isInitialized()
extern void Advertisement_get_isInitialized_mB504A790B305C52734533C91982678CA45EA9A4F (void);
// 0x00000003 System.Boolean UnityEngine.Advertisements.Advertisement::get_isSupported()
extern void Advertisement_get_isSupported_m137C1841EE86BF502BD7EB1D9889078B2387CF8E (void);
// 0x00000004 System.Boolean UnityEngine.Advertisements.Advertisement::get_debugMode()
extern void Advertisement_get_debugMode_m6E32EAAA7633FA61F2312396E5DBA7367840080A (void);
// 0x00000005 System.Void UnityEngine.Advertisements.Advertisement::set_debugMode(System.Boolean)
extern void Advertisement_set_debugMode_mA64998E948E522B05755757BDF7279866AD52067 (void);
// 0x00000006 System.String UnityEngine.Advertisements.Advertisement::get_version()
extern void Advertisement_get_version_m6574BA0BE4B94AA51E1E92D207BE3294C8E48FF1 (void);
// 0x00000007 System.Boolean UnityEngine.Advertisements.Advertisement::get_isShowing()
extern void Advertisement_get_isShowing_m004D5F97E151927DD0637A9967FAD7E704932123 (void);
// 0x00000008 System.Void UnityEngine.Advertisements.Advertisement::Initialize(System.String)
extern void Advertisement_Initialize_m4B4A6CA19B11047279C327CDF240F1E7223C39F2 (void);
// 0x00000009 System.Void UnityEngine.Advertisements.Advertisement::Initialize(System.String,System.Boolean)
extern void Advertisement_Initialize_m50E5DCFAC318D93B37B03C454C0B6648428A109F (void);
// 0x0000000A System.Void UnityEngine.Advertisements.Advertisement::Initialize(System.String,System.Boolean,System.Boolean)
extern void Advertisement_Initialize_mDC6992FA08E4C26BEF880B14E644F71DE9C9804D (void);
// 0x0000000B System.Void UnityEngine.Advertisements.Advertisement::Initialize(System.String,System.Boolean,System.Boolean,UnityEngine.Advertisements.IUnityAdsInitializationListener)
extern void Advertisement_Initialize_m8E5F0FE27AA821BC18C08CA9B0DFCD795EF0EB3E (void);
// 0x0000000C System.Boolean UnityEngine.Advertisements.Advertisement::IsReady()
extern void Advertisement_IsReady_m4F30149C74D2F243D9DE8F95F4864B241236D016 (void);
// 0x0000000D System.Boolean UnityEngine.Advertisements.Advertisement::IsReady(System.String)
extern void Advertisement_IsReady_m7880E5F097F352E81CED7DFA42B48F150B0EC682 (void);
// 0x0000000E System.Void UnityEngine.Advertisements.Advertisement::Load(System.String)
extern void Advertisement_Load_m0C9D79444FCD24319D033DCB6B305B8AF15FA2E5 (void);
// 0x0000000F System.Void UnityEngine.Advertisements.Advertisement::Load(System.String,UnityEngine.Advertisements.IUnityAdsLoadListener)
extern void Advertisement_Load_m2A2737F1D9BDBF23912614A7B8BAA7EC837CF1A8 (void);
// 0x00000010 System.Void UnityEngine.Advertisements.Advertisement::Show()
extern void Advertisement_Show_m1FC1AE09403CE9104EDBCC088773B531C44C0C85 (void);
// 0x00000011 System.Void UnityEngine.Advertisements.Advertisement::Show(UnityEngine.Advertisements.ShowOptions)
extern void Advertisement_Show_m75759E89438020A5888F5083A67D4B00A428BAB5 (void);
// 0x00000012 System.Void UnityEngine.Advertisements.Advertisement::Show(System.String)
extern void Advertisement_Show_m8B3BB4A04BD6822D5B0A8CE6E675E6CDEDFDCEED (void);
// 0x00000013 System.Void UnityEngine.Advertisements.Advertisement::Show(System.String,UnityEngine.Advertisements.ShowOptions)
extern void Advertisement_Show_m542E0BE5F1952599326798CFF080DE113AA93D8C (void);
// 0x00000014 System.Void UnityEngine.Advertisements.Advertisement::Show(System.String,UnityEngine.Advertisements.IUnityAdsShowListener)
extern void Advertisement_Show_mC45B5B2EE78CAC1D790803BD9337FEF4472F8F33 (void);
// 0x00000015 System.Void UnityEngine.Advertisements.Advertisement::Show(System.String,UnityEngine.Advertisements.ShowOptions,UnityEngine.Advertisements.IUnityAdsShowListener)
extern void Advertisement_Show_m927377540F2FBAA7072449D8C011A7252A78FA3A (void);
// 0x00000016 System.Void UnityEngine.Advertisements.Advertisement::SetMetaData(UnityEngine.Advertisements.MetaData)
extern void Advertisement_SetMetaData_mA9AA97AF50809D3FC117D28E80E24A1A31031F28 (void);
// 0x00000017 System.Void UnityEngine.Advertisements.Advertisement::AddListener(UnityEngine.Advertisements.IUnityAdsListener)
extern void Advertisement_AddListener_m1548B00420180351DF863FBB372C0264AAE49C44 (void);
// 0x00000018 System.Void UnityEngine.Advertisements.Advertisement::RemoveListener(UnityEngine.Advertisements.IUnityAdsListener)
extern void Advertisement_RemoveListener_m408A2E9C2793DC9FDC6806514CA7619A62B9D792 (void);
// 0x00000019 UnityEngine.Advertisements.PlacementState UnityEngine.Advertisements.Advertisement::GetPlacementState()
extern void Advertisement_GetPlacementState_mB65B8E93250DE669EA3B46F54A39C0D2D0132648 (void);
// 0x0000001A UnityEngine.Advertisements.PlacementState UnityEngine.Advertisements.Advertisement::GetPlacementState(System.String)
extern void Advertisement_GetPlacementState_mE0B4AC310CDAFAF4D14A04992BB26220253EFF5B (void);
// 0x0000001B UnityEngine.Advertisements.Platform.IPlatform UnityEngine.Advertisements.Advertisement::CreatePlatform()
extern void Advertisement_CreatePlatform_mC1CDD4759D65156D6F201E06DDD979696B05D4E3 (void);
// 0x0000001C System.Boolean UnityEngine.Advertisements.Advertisement::IsSupported()
extern void Advertisement_IsSupported_m251D8427493C493098367A60067F458251BD62DF (void);
// 0x0000001D UnityEngine.Advertisements.Utilities.IUnityLifecycleManager UnityEngine.Advertisements.Banner::get_UnityLifecycleManager()
extern void Banner_get_UnityLifecycleManager_mF54F9B5DA7BC9F2937A66D5699D8DA858001F410 (void);
// 0x0000001E System.Boolean UnityEngine.Advertisements.Banner::get_IsLoaded()
extern void Banner_get_IsLoaded_m5E13400241DEFF428441C8FA624F53180D7D2436 (void);
// 0x0000001F System.Boolean UnityEngine.Advertisements.Banner::get_ShowAfterLoad()
extern void Banner_get_ShowAfterLoad_m18167D1F22BB9C243CAB3F43630C407EBF73DF92 (void);
// 0x00000020 System.Void UnityEngine.Advertisements.Banner::set_ShowAfterLoad(System.Boolean)
extern void Banner_set_ShowAfterLoad_m25D9F9574C9405A6E5C3B17943CECFE8D0E672D0 (void);
// 0x00000021 System.Void UnityEngine.Advertisements.Banner::.ctor(UnityEngine.Advertisements.INativeBanner,UnityEngine.Advertisements.Utilities.IUnityLifecycleManager)
extern void Banner__ctor_mAAB66E5B48DA4D19C92F7010E26267D320753F28 (void);
// 0x00000022 System.Void UnityEngine.Advertisements.Banner::Load(System.String,UnityEngine.Advertisements.BannerLoadOptions)
extern void Banner_Load_m02EF12198664BAFA0C0F2D2EB10653C816A63E1E (void);
// 0x00000023 System.Void UnityEngine.Advertisements.Banner::Show(System.String,UnityEngine.Advertisements.BannerOptions)
extern void Banner_Show_mDF4B91C253C1B26884B07F008037A2E02544F99E (void);
// 0x00000024 System.Void UnityEngine.Advertisements.Banner::Hide(System.Boolean)
extern void Banner_Hide_m7C66B6BB7207D92974D1704E57BF087EA1E44601 (void);
// 0x00000025 System.Void UnityEngine.Advertisements.Banner::SetPosition(UnityEngine.Advertisements.BannerPosition)
extern void Banner_SetPosition_m8892F4A92814ADD9C870F75074C03C15880B6504 (void);
// 0x00000026 System.Void UnityEngine.Advertisements.Banner::UnityAdsBannerDidShow(System.String,UnityEngine.Advertisements.BannerOptions)
extern void Banner_UnityAdsBannerDidShow_mD8BE33941D2EFB7BABDA2A76E49DC8A076F67615 (void);
// 0x00000027 System.Void UnityEngine.Advertisements.Banner::UnityAdsBannerDidHide(System.String,UnityEngine.Advertisements.BannerOptions)
extern void Banner_UnityAdsBannerDidHide_mC653817DB4091D2E1D17C2C2DC8F925100E95A96 (void);
// 0x00000028 System.Void UnityEngine.Advertisements.Banner::UnityAdsBannerClick(System.String,UnityEngine.Advertisements.BannerOptions)
extern void Banner_UnityAdsBannerClick_m10D3293D7D833DB6015C29F618155C4D827DC82A (void);
// 0x00000029 System.Void UnityEngine.Advertisements.Banner::UnityAdsBannerDidLoad(System.String,UnityEngine.Advertisements.BannerLoadOptions)
extern void Banner_UnityAdsBannerDidLoad_mD75000EB978B1FE76D8FB67F6774A670EFB353D8 (void);
// 0x0000002A System.Void UnityEngine.Advertisements.Banner::UnityAdsBannerDidError(System.String,UnityEngine.Advertisements.BannerLoadOptions)
extern void Banner_UnityAdsBannerDidError_m6D321E25A913EC317789D61630D67CFFD77787B9 (void);
// 0x0000002B UnityEngine.Advertisements.BannerLoadOptions/LoadCallback UnityEngine.Advertisements.BannerLoadOptions::get_loadCallback()
extern void BannerLoadOptions_get_loadCallback_mF742FFBF21191B852CCA2DDB31C8E0D850C7A2E2 (void);
// 0x0000002C System.Void UnityEngine.Advertisements.BannerLoadOptions::set_loadCallback(UnityEngine.Advertisements.BannerLoadOptions/LoadCallback)
extern void BannerLoadOptions_set_loadCallback_m9D3C4E78D67CEC16AE816BF69B65A6423A6969B2 (void);
// 0x0000002D UnityEngine.Advertisements.BannerLoadOptions/ErrorCallback UnityEngine.Advertisements.BannerLoadOptions::get_errorCallback()
extern void BannerLoadOptions_get_errorCallback_m7710B7894CAA75F3A117F2A766F24588AF1A6EFD (void);
// 0x0000002E System.Void UnityEngine.Advertisements.BannerLoadOptions::set_errorCallback(UnityEngine.Advertisements.BannerLoadOptions/ErrorCallback)
extern void BannerLoadOptions_set_errorCallback_m5069D00E410DF3BEDFF097BF40F3B3476EF88B95 (void);
// 0x0000002F System.Void UnityEngine.Advertisements.BannerLoadOptions::.ctor()
extern void BannerLoadOptions__ctor_m4D5D26302DC12B8F2BA55508FDFDE6A152051DAD (void);
// 0x00000030 UnityEngine.Advertisements.BannerOptions/BannerCallback UnityEngine.Advertisements.BannerOptions::get_showCallback()
extern void BannerOptions_get_showCallback_mDC81918C6D7C62040F55455F6E4A824E17FF8F2D (void);
// 0x00000031 System.Void UnityEngine.Advertisements.BannerOptions::set_showCallback(UnityEngine.Advertisements.BannerOptions/BannerCallback)
extern void BannerOptions_set_showCallback_mA9D8C200D021B29B0B974636F94CE652FED761FD (void);
// 0x00000032 UnityEngine.Advertisements.BannerOptions/BannerCallback UnityEngine.Advertisements.BannerOptions::get_hideCallback()
extern void BannerOptions_get_hideCallback_m84DCD124F4FA98C19E43073B0269A9E7C61DB956 (void);
// 0x00000033 System.Void UnityEngine.Advertisements.BannerOptions::set_hideCallback(UnityEngine.Advertisements.BannerOptions/BannerCallback)
extern void BannerOptions_set_hideCallback_mBD46ECBE1865B8B388C0E96B8CAD031A059CAEBB (void);
// 0x00000034 UnityEngine.Advertisements.BannerOptions/BannerCallback UnityEngine.Advertisements.BannerOptions::get_clickCallback()
extern void BannerOptions_get_clickCallback_m6B317492111A800F9260E1AE3460D8976B8FFFEA (void);
// 0x00000035 System.Void UnityEngine.Advertisements.BannerOptions::set_clickCallback(UnityEngine.Advertisements.BannerOptions/BannerCallback)
extern void BannerOptions_set_clickCallback_m2ECE4E61C720E9F065E86579B0F0D92122F319D2 (void);
// 0x00000036 System.Void UnityEngine.Advertisements.BannerOptions::.ctor()
extern void BannerOptions__ctor_m7B5E6180AA634C069E053C7E13F1544C50B01131 (void);
// 0x00000037 System.Boolean UnityEngine.Advertisements.Configuration::get_enabled()
extern void Configuration_get_enabled_m03B11BE0F38C5260711D0465911DC29CFC2793CA (void);
// 0x00000038 System.String UnityEngine.Advertisements.Configuration::get_defaultPlacement()
extern void Configuration_get_defaultPlacement_mE20D0F03E31BD1A9FD19BA5DFC7A8A87579F0B0F (void);
// 0x00000039 System.Collections.Generic.Dictionary`2<System.String,System.Boolean> UnityEngine.Advertisements.Configuration::get_placements()
extern void Configuration_get_placements_m1B6CEB298ABFC85E077B538BC1B277DFB0828EBE (void);
// 0x0000003A System.Void UnityEngine.Advertisements.Configuration::.ctor(System.String)
extern void Configuration__ctor_m09EE5351C61B65B64FA1B5AFC53BD2579BA6615C (void);
// 0x0000003B UnityEngine.Advertisements.Utilities.IUnityLifecycleManager UnityEngine.Advertisements.IBanner::get_UnityLifecycleManager()
// 0x0000003C System.Boolean UnityEngine.Advertisements.IBanner::get_IsLoaded()
// 0x0000003D System.Boolean UnityEngine.Advertisements.IBanner::get_ShowAfterLoad()
// 0x0000003E System.Void UnityEngine.Advertisements.IBanner::set_ShowAfterLoad(System.Boolean)
// 0x0000003F System.Void UnityEngine.Advertisements.IBanner::Load(System.String,UnityEngine.Advertisements.BannerLoadOptions)
// 0x00000040 System.Void UnityEngine.Advertisements.IBanner::Show(System.String,UnityEngine.Advertisements.BannerOptions)
// 0x00000041 System.Void UnityEngine.Advertisements.IBanner::Hide(System.Boolean)
// 0x00000042 System.Void UnityEngine.Advertisements.IBanner::SetPosition(UnityEngine.Advertisements.BannerPosition)
// 0x00000043 System.Void UnityEngine.Advertisements.IBanner::UnityAdsBannerDidShow(System.String,UnityEngine.Advertisements.BannerOptions)
// 0x00000044 System.Void UnityEngine.Advertisements.IBanner::UnityAdsBannerDidHide(System.String,UnityEngine.Advertisements.BannerOptions)
// 0x00000045 System.Void UnityEngine.Advertisements.IBanner::UnityAdsBannerClick(System.String,UnityEngine.Advertisements.BannerOptions)
// 0x00000046 System.Void UnityEngine.Advertisements.IBanner::UnityAdsBannerDidLoad(System.String,UnityEngine.Advertisements.BannerLoadOptions)
// 0x00000047 System.Void UnityEngine.Advertisements.IBanner::UnityAdsBannerDidError(System.String,UnityEngine.Advertisements.BannerLoadOptions)
// 0x00000048 System.Boolean UnityEngine.Advertisements.INativeBanner::get_IsLoaded()
// 0x00000049 System.Void UnityEngine.Advertisements.INativeBanner::SetupBanner(UnityEngine.Advertisements.IBanner)
// 0x0000004A System.Void UnityEngine.Advertisements.INativeBanner::Load(System.String,UnityEngine.Advertisements.BannerLoadOptions)
// 0x0000004B System.Void UnityEngine.Advertisements.INativeBanner::Show(System.String,UnityEngine.Advertisements.BannerOptions)
// 0x0000004C System.Void UnityEngine.Advertisements.INativeBanner::Hide(System.Boolean)
// 0x0000004D System.Void UnityEngine.Advertisements.INativeBanner::SetPosition(UnityEngine.Advertisements.BannerPosition)
// 0x0000004E System.Void UnityEngine.Advertisements.IUnityAdsListener::OnUnityAdsReady(System.String)
// 0x0000004F System.Void UnityEngine.Advertisements.IUnityAdsListener::OnUnityAdsDidError(System.String)
// 0x00000050 System.Void UnityEngine.Advertisements.IUnityAdsListener::OnUnityAdsDidStart(System.String)
// 0x00000051 System.Void UnityEngine.Advertisements.IUnityAdsListener::OnUnityAdsDidFinish(System.String,UnityEngine.Advertisements.ShowResult)
// 0x00000052 System.Void UnityEngine.Advertisements.IUnityAdsInitializationListener::OnInitializationComplete()
// 0x00000053 System.Void UnityEngine.Advertisements.IUnityAdsInitializationListener::OnInitializationFailed(UnityEngine.Advertisements.UnityAdsInitializationError,System.String)
// 0x00000054 System.Void UnityEngine.Advertisements.IUnityAdsLoadListener::OnUnityAdsAdLoaded(System.String)
// 0x00000055 System.Void UnityEngine.Advertisements.IUnityAdsLoadListener::OnUnityAdsFailedToLoad(System.String,UnityEngine.Advertisements.UnityAdsLoadError,System.String)
// 0x00000056 System.Void UnityEngine.Advertisements.IUnityAdsShowListener::OnUnityAdsShowFailure(System.String,UnityEngine.Advertisements.UnityAdsShowError,System.String)
// 0x00000057 System.Void UnityEngine.Advertisements.IUnityAdsShowListener::OnUnityAdsShowStart(System.String)
// 0x00000058 System.Void UnityEngine.Advertisements.IUnityAdsShowListener::OnUnityAdsShowClick(System.String)
// 0x00000059 System.Void UnityEngine.Advertisements.IUnityAdsShowListener::OnUnityAdsShowComplete(System.String,UnityEngine.Advertisements.UnityAdsShowCompletionState)
// 0x0000005A System.String UnityEngine.Advertisements.MetaData::get_category()
extern void MetaData_get_category_mBEB012D8CC3DBD56EEC5E14C875C34C946EAC3BB (void);
// 0x0000005B System.Void UnityEngine.Advertisements.MetaData::set_category(System.String)
extern void MetaData_set_category_m1DF58928A9830B1E889C59E5B935FE4928C8C784 (void);
// 0x0000005C System.Void UnityEngine.Advertisements.MetaData::.ctor(System.String)
extern void MetaData__ctor_m6EF2DA52AAD0C124284ED96227F52ABCDE73C87C (void);
// 0x0000005D System.Void UnityEngine.Advertisements.MetaData::Set(System.String,System.Object)
extern void MetaData_Set_mF1B807C8D6D380084618F0C6FAA3A9C21F9DD16C (void);
// 0x0000005E System.Object UnityEngine.Advertisements.MetaData::Get(System.String)
extern void MetaData_Get_m83F3F32F0D2EF2E3AC76A08563BD215287588B1B (void);
// 0x0000005F System.Collections.Generic.IDictionary`2<System.String,System.Object> UnityEngine.Advertisements.MetaData::Values()
extern void MetaData_Values_m23DFE4EA7EA2884125E84D9A73B1A5F55663C3BA (void);
// 0x00000060 System.String UnityEngine.Advertisements.MetaData::ToJSON()
extern void MetaData_ToJSON_m249EDA63A72895084104FBE66D86462FEE33F948 (void);
// 0x00000061 System.Void UnityEngine.Advertisements.AndroidInitializationListener::.ctor(UnityEngine.Advertisements.Platform.IPlatform,UnityEngine.Advertisements.IUnityAdsInitializationListener)
extern void AndroidInitializationListener__ctor_mAE6F05EC1B9F314BB1DEFFDA210E9A69DD466038 (void);
// 0x00000062 System.Void UnityEngine.Advertisements.AndroidInitializationListener::onInitializationComplete()
extern void AndroidInitializationListener_onInitializationComplete_mCF14A6CFB4E6983434E25C2CA0AA49A6E645DC7C (void);
// 0x00000063 System.Void UnityEngine.Advertisements.AndroidInitializationListener::onInitializationFailed(UnityEngine.AndroidJavaObject,System.String)
extern void AndroidInitializationListener_onInitializationFailed_mB27DFE58C465F18C0F0EDE001A63CD6FC9C5D779 (void);
// 0x00000064 System.Void UnityEngine.Advertisements.AndroidLoadListener::.ctor(UnityEngine.Advertisements.Platform.IPlatform,UnityEngine.Advertisements.IUnityAdsLoadListener)
extern void AndroidLoadListener__ctor_m329BA4CFC453CFBD1AAA2D8F919C977114B3F983 (void);
// 0x00000065 System.Void UnityEngine.Advertisements.AndroidLoadListener::onUnityAdsAdLoaded(System.String)
extern void AndroidLoadListener_onUnityAdsAdLoaded_m8AB01365523818E228F57CE987081EB3B1B9EACA (void);
// 0x00000066 System.Void UnityEngine.Advertisements.AndroidLoadListener::onUnityAdsFailedToLoad(System.String,UnityEngine.AndroidJavaObject,System.String)
extern void AndroidLoadListener_onUnityAdsFailedToLoad_m98E454C3615083F0DCDAEA653CE7472D7031B3D7 (void);
// 0x00000067 System.Void UnityEngine.Advertisements.AndroidShowListener::.ctor(UnityEngine.Advertisements.Platform.IPlatform,UnityEngine.Advertisements.IUnityAdsShowListener)
extern void AndroidShowListener__ctor_m78A228D8BBB182AA5BDDEE6A784D15BD5629ABBF (void);
// 0x00000068 System.Void UnityEngine.Advertisements.AndroidShowListener::onUnityAdsShowFailure(System.String,UnityEngine.AndroidJavaObject,System.String)
extern void AndroidShowListener_onUnityAdsShowFailure_m1ECA3172457D1839C43FD6D914A63480DF36C821 (void);
// 0x00000069 System.Void UnityEngine.Advertisements.AndroidShowListener::onUnityAdsShowStart(System.String)
extern void AndroidShowListener_onUnityAdsShowStart_mF79CE9BA0FEF9B29848FE0886AD90728EDA4A625 (void);
// 0x0000006A System.Void UnityEngine.Advertisements.AndroidShowListener::onUnityAdsShowClick(System.String)
extern void AndroidShowListener_onUnityAdsShowClick_mD89F26EF7574FF4659F4DD3783B25D0B14F6EE40 (void);
// 0x0000006B System.Void UnityEngine.Advertisements.AndroidShowListener::onUnityAdsShowComplete(System.String,UnityEngine.AndroidJavaObject)
extern void AndroidShowListener_onUnityAdsShowComplete_m1675B4E37B07777ED894121B299405E5A9F550FE (void);
// 0x0000006C System.Void UnityEngine.Advertisements.INativePlatform::SetupPlatform(UnityEngine.Advertisements.Platform.IPlatform)
// 0x0000006D System.Void UnityEngine.Advertisements.INativePlatform::Initialize(System.String,System.Boolean,System.Boolean,UnityEngine.Advertisements.IUnityAdsInitializationListener)
// 0x0000006E System.Void UnityEngine.Advertisements.INativePlatform::Load(System.String,UnityEngine.Advertisements.IUnityAdsLoadListener)
// 0x0000006F System.Void UnityEngine.Advertisements.INativePlatform::Show(System.String,UnityEngine.Advertisements.IUnityAdsShowListener)
// 0x00000070 System.Void UnityEngine.Advertisements.INativePlatform::SetMetaData(UnityEngine.Advertisements.MetaData)
// 0x00000071 System.Boolean UnityEngine.Advertisements.INativePlatform::GetDebugMode()
// 0x00000072 System.Void UnityEngine.Advertisements.INativePlatform::SetDebugMode(System.Boolean)
// 0x00000073 System.String UnityEngine.Advertisements.INativePlatform::GetVersion()
// 0x00000074 System.Boolean UnityEngine.Advertisements.INativePlatform::IsInitialized()
// 0x00000075 System.Boolean UnityEngine.Advertisements.INativePlatform::IsReady(System.String)
// 0x00000076 UnityEngine.Advertisements.PlacementState UnityEngine.Advertisements.INativePlatform::GetPlacementState(System.String)
// 0x00000077 System.Action`1<UnityEngine.Advertisements.ShowResult> UnityEngine.Advertisements.ShowOptions::get_resultCallback()
extern void ShowOptions_get_resultCallback_m3927DDD6E2EF6219A02421B4C56D6D5F5F059DB8 (void);
// 0x00000078 System.Void UnityEngine.Advertisements.ShowOptions::set_resultCallback(System.Action`1<UnityEngine.Advertisements.ShowResult>)
extern void ShowOptions_set_resultCallback_mC6C40AE153D30E9F4BB9E904CC15E23D1735E6B3 (void);
// 0x00000079 System.String UnityEngine.Advertisements.ShowOptions::get_gamerSid()
extern void ShowOptions_get_gamerSid_m778D4145EEE07AA52F4F14062AE02CF8B5E78A40 (void);
// 0x0000007A System.Void UnityEngine.Advertisements.ShowOptions::set_gamerSid(System.String)
extern void ShowOptions_set_gamerSid_m22AFB748A6E131A3534825E583420AE42A303172 (void);
// 0x0000007B System.Void UnityEngine.Advertisements.ShowOptions::.ctor()
extern void ShowOptions__ctor_mE9F7F751F6BE70F4A7B1F3949E9B9B54DE1EB6FE (void);
// 0x0000007C System.Void UnityEngine.Advertisements.Utilities.ApplicationQuit::add_OnApplicationQuitEventHandler(UnityEngine.Events.UnityAction)
extern void ApplicationQuit_add_OnApplicationQuitEventHandler_m11D5EBA7CE43A2ECC034302EC29D640970C8739B (void);
// 0x0000007D System.Void UnityEngine.Advertisements.Utilities.ApplicationQuit::remove_OnApplicationQuitEventHandler(UnityEngine.Events.UnityAction)
extern void ApplicationQuit_remove_OnApplicationQuitEventHandler_m1C5525879D2483EE251E682B44882A2814770D51 (void);
// 0x0000007E System.Void UnityEngine.Advertisements.Utilities.ApplicationQuit::OnApplicationQuit()
extern void ApplicationQuit_OnApplicationQuit_mADE194EF6C6CB9C6D830A8715893E85E8294AF34 (void);
// 0x0000007F System.Void UnityEngine.Advertisements.Utilities.ApplicationQuit::.ctor()
extern void ApplicationQuit__ctor_m7FF706AC8A858505C029739ED28613AEE0D67363 (void);
// 0x00000080 System.Void UnityEngine.Advertisements.Utilities.CoroutineExecutor::Update()
extern void CoroutineExecutor_Update_mBDE991E82D05D5A0BBB50918A558D90152508C2C (void);
// 0x00000081 System.Void UnityEngine.Advertisements.Utilities.CoroutineExecutor::.ctor()
extern void CoroutineExecutor__ctor_mAD7016CBCA79F3304EB1C5948255E87E44A7422A (void);
// 0x00000082 UnityEngine.Advertisements.ShowResult UnityEngine.Advertisements.Utilities.EnumUtilities::GetShowResultsFromCompletionState(UnityEngine.Advertisements.UnityAdsShowCompletionState)
extern void EnumUtilities_GetShowResultsFromCompletionState_mDCF7522190C670BFCDD677A6AFA7B7C98B273D74 (void);
// 0x00000083 T UnityEngine.Advertisements.Utilities.EnumUtilities::GetEnumFromAndroidJavaObject(UnityEngine.AndroidJavaObject,T)
// 0x00000084 UnityEngine.Coroutine UnityEngine.Advertisements.Utilities.IUnityLifecycleManager::StartCoroutine(System.Collections.IEnumerator)
// 0x00000085 System.Void UnityEngine.Advertisements.Utilities.IUnityLifecycleManager::Post(System.Action)
// 0x00000086 System.Void UnityEngine.Advertisements.Utilities.IUnityLifecycleManager::SetOnApplicationQuitCallback(UnityEngine.Events.UnityAction)
// 0x00000087 System.Object UnityEngine.Advertisements.Utilities.Json::Deserialize(System.String)
extern void Json_Deserialize_m76115855BAC9BE9588C192A488D3DAC15CD8BF6C (void);
// 0x00000088 System.String UnityEngine.Advertisements.Utilities.Json::Serialize(System.Object)
extern void Json_Serialize_m1B1229A14A448280851ACD5195F0EA976F903EC6 (void);
// 0x00000089 System.Void UnityEngine.Advertisements.Utilities.UnityLifecycleManager::.ctor()
extern void UnityLifecycleManager__ctor_mD95AC377DC9E3E99821DB9FC9BBA241579AEFEB2 (void);
// 0x0000008A System.Void UnityEngine.Advertisements.Utilities.UnityLifecycleManager::Initialize()
extern void UnityLifecycleManager_Initialize_mD73B907AD8F2E0F0B2F7720FDA4C00B533182DC6 (void);
// 0x0000008B UnityEngine.Coroutine UnityEngine.Advertisements.Utilities.UnityLifecycleManager::StartCoroutine(System.Collections.IEnumerator)
extern void UnityLifecycleManager_StartCoroutine_m3C0B3C37F7BABBB621FC5B3FE7176D8CE58E8CE9 (void);
// 0x0000008C System.Void UnityEngine.Advertisements.Utilities.UnityLifecycleManager::Post(System.Action)
extern void UnityLifecycleManager_Post_mD922A2AB5F40500B93E6732F27EDBB855A70D82D (void);
// 0x0000008D System.Void UnityEngine.Advertisements.Utilities.UnityLifecycleManager::Dispose()
extern void UnityLifecycleManager_Dispose_m2E15066A520646FED13DF2AAE6BCE8049BC9F746 (void);
// 0x0000008E System.Void UnityEngine.Advertisements.Utilities.UnityLifecycleManager::SetOnApplicationQuitCallback(UnityEngine.Events.UnityAction)
extern void UnityLifecycleManager_SetOnApplicationQuitCallback_m3F4658AC36BED9AC5AA3AFBAAF01C3FFE4ABFB3F (void);
// 0x0000008F System.Void UnityEngine.Advertisements.Purchasing.IPurchase::Initialize(UnityEngine.Advertisements.Purchasing.IPurchasingEventSender)
// 0x00000090 System.Void UnityEngine.Advertisements.Purchasing.IPurchase::SendEvent(System.String)
// 0x00000091 System.Void UnityEngine.Advertisements.Purchasing.IPurchase::onPurchasingCommand(System.String)
// 0x00000092 System.Void UnityEngine.Advertisements.Purchasing.IPurchase::onGetPurchasingVersion()
// 0x00000093 System.Void UnityEngine.Advertisements.Purchasing.IPurchase::onGetProductCatalog()
// 0x00000094 System.Void UnityEngine.Advertisements.Purchasing.IPurchase::onInitializePurchasing()
// 0x00000095 System.Void UnityEngine.Advertisements.Purchasing.IPurchasingEventSender::SendPurchasingEvent(System.String)
// 0x00000096 System.Void UnityEngine.Advertisements.Purchasing.Purchase::onPurchasingCommand(System.String)
extern void Purchase_onPurchasingCommand_mCCB94AFCA2E8C369F16E0D1C8E12C40BA6A08818 (void);
// 0x00000097 System.Void UnityEngine.Advertisements.Purchasing.Purchase::onGetPurchasingVersion()
extern void Purchase_onGetPurchasingVersion_m3069B940592B0841B13AB1873EA79D07F9284D98 (void);
// 0x00000098 System.Void UnityEngine.Advertisements.Purchasing.Purchase::onGetProductCatalog()
extern void Purchase_onGetProductCatalog_m3B6B1FE855B05DBEA625BEFD93624F6B90F1E68D (void);
// 0x00000099 System.Void UnityEngine.Advertisements.Purchasing.Purchase::onInitializePurchasing()
extern void Purchase_onInitializePurchasing_m1CA49D6B7098E0ABC34D9C5068C6B4FF40959525 (void);
// 0x0000009A System.Void UnityEngine.Advertisements.Purchasing.Purchase::SendEvent(System.String)
extern void Purchase_SendEvent_m3C79B682C8D2C031D44746202F61AB5BEA6C0F22 (void);
// 0x0000009B System.Void UnityEngine.Advertisements.Purchasing.Purchase::Initialize(UnityEngine.Advertisements.Purchasing.IPurchasingEventSender)
extern void Purchase_Initialize_mAC442010AFA23D482F2CDF7144B2BDA793D5F92B (void);
// 0x0000009C System.Void UnityEngine.Advertisements.Purchasing.Purchase::.ctor()
extern void Purchase__ctor_mBD4CD713E8E9167FA4FD2024B651C9BBB3A9E7A7 (void);
// 0x0000009D System.Boolean UnityEngine.Advertisements.Purchasing.Purchasing::Initialize(UnityEngine.Advertisements.Purchasing.IPurchasingEventSender)
extern void Purchasing_Initialize_m14E011B5AE0C0D41490676A551DD364245EBF0EC (void);
// 0x0000009E System.Boolean UnityEngine.Advertisements.Purchasing.Purchasing::InitiatePurchasingCommand(System.String)
extern void Purchasing_InitiatePurchasingCommand_m0B076A546123334CADFE0E805E7125BBE081D4B3 (void);
// 0x0000009F System.String UnityEngine.Advertisements.Purchasing.Purchasing::GetPurchasingCatalog()
extern void Purchasing_GetPurchasingCatalog_mC2BEAD1D96CFB58D7778AEBB4A43A9A6D9DD0CD9 (void);
// 0x000000A0 System.String UnityEngine.Advertisements.Purchasing.Purchasing::GetPromoVersion()
extern void Purchasing_GetPromoVersion_mDB84F17B70F76C5B00D7E2062B26BB92AF4304BA (void);
// 0x000000A1 System.Boolean UnityEngine.Advertisements.Purchasing.Purchasing::SendEvent(System.String)
extern void Purchasing_SendEvent_m8274AD7B80C0853660EA40ADB6FBF2A0B12A7950 (void);
// 0x000000A2 System.Void UnityEngine.Advertisements.Purchasing.Purchasing::.cctor()
extern void Purchasing__cctor_m616808EB3A5A2F1577EBF1F684D7080F82B17292 (void);
// 0x000000A3 System.Void UnityEngine.Advertisements.Platform.IPlatform::add_OnStart(System.EventHandler`1<UnityEngine.Advertisements.Events.StartEventArgs>)
// 0x000000A4 System.Void UnityEngine.Advertisements.Platform.IPlatform::remove_OnStart(System.EventHandler`1<UnityEngine.Advertisements.Events.StartEventArgs>)
// 0x000000A5 System.Void UnityEngine.Advertisements.Platform.IPlatform::add_OnFinish(System.EventHandler`1<UnityEngine.Advertisements.Events.FinishEventArgs>)
// 0x000000A6 System.Void UnityEngine.Advertisements.Platform.IPlatform::remove_OnFinish(System.EventHandler`1<UnityEngine.Advertisements.Events.FinishEventArgs>)
// 0x000000A7 UnityEngine.Advertisements.IBanner UnityEngine.Advertisements.Platform.IPlatform::get_Banner()
// 0x000000A8 UnityEngine.Advertisements.Utilities.IUnityLifecycleManager UnityEngine.Advertisements.Platform.IPlatform::get_UnityLifecycleManager()
// 0x000000A9 UnityEngine.Advertisements.INativePlatform UnityEngine.Advertisements.Platform.IPlatform::get_NativePlatform()
// 0x000000AA System.Boolean UnityEngine.Advertisements.Platform.IPlatform::get_IsInitialized()
// 0x000000AB System.Boolean UnityEngine.Advertisements.Platform.IPlatform::get_IsShowing()
// 0x000000AC System.String UnityEngine.Advertisements.Platform.IPlatform::get_Version()
// 0x000000AD System.Boolean UnityEngine.Advertisements.Platform.IPlatform::get_DebugMode()
// 0x000000AE System.Void UnityEngine.Advertisements.Platform.IPlatform::set_DebugMode(System.Boolean)
// 0x000000AF System.Collections.Generic.HashSet`1<UnityEngine.Advertisements.IUnityAdsListener> UnityEngine.Advertisements.Platform.IPlatform::get_Listeners()
// 0x000000B0 System.Void UnityEngine.Advertisements.Platform.IPlatform::Initialize(System.String,System.Boolean,System.Boolean,UnityEngine.Advertisements.IUnityAdsInitializationListener)
// 0x000000B1 System.Void UnityEngine.Advertisements.Platform.IPlatform::Load(System.String,UnityEngine.Advertisements.IUnityAdsLoadListener)
// 0x000000B2 System.Void UnityEngine.Advertisements.Platform.IPlatform::Show(System.String,UnityEngine.Advertisements.ShowOptions,UnityEngine.Advertisements.IUnityAdsShowListener)
// 0x000000B3 System.Void UnityEngine.Advertisements.Platform.IPlatform::AddListener(UnityEngine.Advertisements.IUnityAdsListener)
// 0x000000B4 System.Void UnityEngine.Advertisements.Platform.IPlatform::RemoveListener(UnityEngine.Advertisements.IUnityAdsListener)
// 0x000000B5 System.Boolean UnityEngine.Advertisements.Platform.IPlatform::IsReady(System.String)
// 0x000000B6 UnityEngine.Advertisements.PlacementState UnityEngine.Advertisements.Platform.IPlatform::GetPlacementState(System.String)
// 0x000000B7 System.Void UnityEngine.Advertisements.Platform.IPlatform::SetMetaData(UnityEngine.Advertisements.MetaData)
// 0x000000B8 System.Void UnityEngine.Advertisements.Platform.IPlatform::UnityAdsReady(System.String)
// 0x000000B9 System.Void UnityEngine.Advertisements.Platform.IPlatform::UnityAdsDidError(System.String)
// 0x000000BA System.Void UnityEngine.Advertisements.Platform.IPlatform::UnityAdsDidStart(System.String)
// 0x000000BB System.Void UnityEngine.Advertisements.Platform.IPlatform::UnityAdsDidFinish(System.String,UnityEngine.Advertisements.ShowResult)
// 0x000000BC System.Void UnityEngine.Advertisements.Platform.Platform::add_OnStart(System.EventHandler`1<UnityEngine.Advertisements.Events.StartEventArgs>)
extern void Platform_add_OnStart_mA5EE80229E95F2DF71502EAB1143E5D89EAD018D (void);
// 0x000000BD System.Void UnityEngine.Advertisements.Platform.Platform::remove_OnStart(System.EventHandler`1<UnityEngine.Advertisements.Events.StartEventArgs>)
extern void Platform_remove_OnStart_m44D267A6FFEEEE0BF6C7BFDDA355AC683A0641A8 (void);
// 0x000000BE System.Void UnityEngine.Advertisements.Platform.Platform::add_OnFinish(System.EventHandler`1<UnityEngine.Advertisements.Events.FinishEventArgs>)
extern void Platform_add_OnFinish_mAAFDFF3ACA72652DEDC17B5651C2EF169A8D7F4F (void);
// 0x000000BF System.Void UnityEngine.Advertisements.Platform.Platform::remove_OnFinish(System.EventHandler`1<UnityEngine.Advertisements.Events.FinishEventArgs>)
extern void Platform_remove_OnFinish_mBF555D3C995D7A9180CAA6B1761874640E0FD255 (void);
// 0x000000C0 UnityEngine.Advertisements.IBanner UnityEngine.Advertisements.Platform.Platform::get_Banner()
extern void Platform_get_Banner_m17B6B2160CCB2F2368C06524104DA55639C4B9FA (void);
// 0x000000C1 UnityEngine.Advertisements.Utilities.IUnityLifecycleManager UnityEngine.Advertisements.Platform.Platform::get_UnityLifecycleManager()
extern void Platform_get_UnityLifecycleManager_m6E8BDFA3313C3476DA8519F9E759A6C2599F61CC (void);
// 0x000000C2 UnityEngine.Advertisements.INativePlatform UnityEngine.Advertisements.Platform.Platform::get_NativePlatform()
extern void Platform_get_NativePlatform_m055DE92A6397C9BFB2A729F9294AFA99B002F8C2 (void);
// 0x000000C3 System.Boolean UnityEngine.Advertisements.Platform.Platform::get_IsInitialized()
extern void Platform_get_IsInitialized_mD7116C3FCF92EED195075AE7499A67C728A201B7 (void);
// 0x000000C4 System.Boolean UnityEngine.Advertisements.Platform.Platform::get_IsShowing()
extern void Platform_get_IsShowing_mD7D4DB0DCE81A7100F990CF75DEB9597D5CDEC12 (void);
// 0x000000C5 System.Void UnityEngine.Advertisements.Platform.Platform::set_IsShowing(System.Boolean)
extern void Platform_set_IsShowing_m1575EFA862EA969C6E44160AF66846DEF009A2BF (void);
// 0x000000C6 System.String UnityEngine.Advertisements.Platform.Platform::get_Version()
extern void Platform_get_Version_mC8539FE79E8D9DAFB8A56995873C6F760E4F4D7D (void);
// 0x000000C7 System.Boolean UnityEngine.Advertisements.Platform.Platform::get_DebugMode()
extern void Platform_get_DebugMode_m763567976C7FDE0681BC141DDCDF5ADD1315B057 (void);
// 0x000000C8 System.Void UnityEngine.Advertisements.Platform.Platform::set_DebugMode(System.Boolean)
extern void Platform_set_DebugMode_m7BA41C5CF5636C427D6373602E81618739E37D2C (void);
// 0x000000C9 System.Collections.Generic.HashSet`1<UnityEngine.Advertisements.IUnityAdsListener> UnityEngine.Advertisements.Platform.Platform::get_Listeners()
extern void Platform_get_Listeners_m04B40907B7A972048637D4537674D7ACB6089F41 (void);
// 0x000000CA System.Void UnityEngine.Advertisements.Platform.Platform::.ctor(UnityEngine.Advertisements.INativePlatform,UnityEngine.Advertisements.IBanner,UnityEngine.Advertisements.Utilities.IUnityLifecycleManager)
extern void Platform__ctor_mAC2FF35E92BDB79828A05B296D7FB07CFF9C6F36 (void);
// 0x000000CB System.Void UnityEngine.Advertisements.Platform.Platform::Initialize(System.String,System.Boolean,System.Boolean,UnityEngine.Advertisements.IUnityAdsInitializationListener)
extern void Platform_Initialize_mE0D86443F52FBB8F9B43DBF399DE10C226B7E184 (void);
// 0x000000CC System.Void UnityEngine.Advertisements.Platform.Platform::Load(System.String)
extern void Platform_Load_m5F63716E5B2174B9C3B4AA71E0CB8D975CE34485 (void);
// 0x000000CD System.Void UnityEngine.Advertisements.Platform.Platform::Load(System.String,UnityEngine.Advertisements.IUnityAdsLoadListener)
extern void Platform_Load_m52FBC7EAA62CB8714E764E262C8143C5ECB0A0DA (void);
// 0x000000CE System.Void UnityEngine.Advertisements.Platform.Platform::Show(System.String,UnityEngine.Advertisements.ShowOptions,UnityEngine.Advertisements.IUnityAdsShowListener)
extern void Platform_Show_mD45AAE3C87D4D25C60C45B415A74C4AFBC9BFA1F (void);
// 0x000000CF System.Void UnityEngine.Advertisements.Platform.Platform::AddListener(UnityEngine.Advertisements.IUnityAdsListener)
extern void Platform_AddListener_m0E5E496A62CBA5D60CACB226AAF364CDBFAE9BC9 (void);
// 0x000000D0 System.Void UnityEngine.Advertisements.Platform.Platform::RemoveListener(UnityEngine.Advertisements.IUnityAdsListener)
extern void Platform_RemoveListener_m66CB33820F4A0A5B9DEF3194EFECA0E26EA655AD (void);
// 0x000000D1 System.Boolean UnityEngine.Advertisements.Platform.Platform::IsReady(System.String)
extern void Platform_IsReady_mB094B73DE8225C33AE267B7088FF8A733F3861C8 (void);
// 0x000000D2 UnityEngine.Advertisements.PlacementState UnityEngine.Advertisements.Platform.Platform::GetPlacementState(System.String)
extern void Platform_GetPlacementState_mA61C07F97BE90F7F48B6F6BBA0C192D71C16F3F8 (void);
// 0x000000D3 System.Void UnityEngine.Advertisements.Platform.Platform::SetMetaData(UnityEngine.Advertisements.MetaData)
extern void Platform_SetMetaData_mE3B49CE0F3F4450ADCC90AC98B53E313192A39A7 (void);
// 0x000000D4 System.Void UnityEngine.Advertisements.Platform.Platform::UnityAdsReady(System.String)
extern void Platform_UnityAdsReady_m27010E4F25709F174EDEED43C5DD346839BC2B91 (void);
// 0x000000D5 System.Void UnityEngine.Advertisements.Platform.Platform::UnityAdsDidError(System.String)
extern void Platform_UnityAdsDidError_mF8EA8A52F16A9017774A30732F824ADD5F9EC19C (void);
// 0x000000D6 System.Void UnityEngine.Advertisements.Platform.Platform::UnityAdsDidStart(System.String)
extern void Platform_UnityAdsDidStart_m21E24FF84CED1830B1B0AFE1E6359564601D0EBC (void);
// 0x000000D7 System.Void UnityEngine.Advertisements.Platform.Platform::UnityAdsDidFinish(System.String,UnityEngine.Advertisements.ShowResult)
extern void Platform_UnityAdsDidFinish_m4661B61B0BD4C0DCE9DCADA4B094C24CDABC35E8 (void);
// 0x000000D8 System.Collections.Generic.HashSet`1<UnityEngine.Advertisements.IUnityAdsListener> UnityEngine.Advertisements.Platform.Platform::GetClonedHashSet(System.Collections.Generic.HashSet`1<UnityEngine.Advertisements.IUnityAdsListener>)
extern void Platform_GetClonedHashSet_m646ACA8E270FA8DCA83D924DE430CFF9C8E3D324 (void);
// 0x000000D9 System.Void UnityEngine.Advertisements.Platform.Platform::<Initialize>b__30_0(System.Object,UnityEngine.Advertisements.Events.StartEventArgs)
extern void Platform_U3CInitializeU3Eb__30_0_m4F14311AF1F837C76F9598C0898151D55622CF7E (void);
// 0x000000DA System.Void UnityEngine.Advertisements.Platform.Platform::<Initialize>b__30_1(System.Object,UnityEngine.Advertisements.Events.FinishEventArgs)
extern void Platform_U3CInitializeU3Eb__30_1_m2FEC023987B9CFA5A49272DD7E6B9E4844CACDCF (void);
// 0x000000DB System.Boolean UnityEngine.Advertisements.Platform.Unsupported.UnsupportedBanner::get_IsLoaded()
extern void UnsupportedBanner_get_IsLoaded_mD3A1CE2F7F0CEA24C7604E761666965EFBBEB442 (void);
// 0x000000DC System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedBanner::SetupBanner(UnityEngine.Advertisements.IBanner)
extern void UnsupportedBanner_SetupBanner_mAF0EBC53100492D01C56F63DC4FDE13A0710B774 (void);
// 0x000000DD System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedBanner::Load(System.String,UnityEngine.Advertisements.BannerLoadOptions)
extern void UnsupportedBanner_Load_mDDBC9C23F8E43EFE02C3DBEA31E8503AFC4D6E8C (void);
// 0x000000DE System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedBanner::Show(System.String,UnityEngine.Advertisements.BannerOptions)
extern void UnsupportedBanner_Show_mDB70F8A3986418BFE8F8C9085E23A1701871161C (void);
// 0x000000DF System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedBanner::Hide(System.Boolean)
extern void UnsupportedBanner_Hide_m446314938DF6A0357E8D9F57ACDBB728725067F7 (void);
// 0x000000E0 System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedBanner::SetPosition(UnityEngine.Advertisements.BannerPosition)
extern void UnsupportedBanner_SetPosition_mF76D1BCE99527D849B20566BFB034C1CBA5B42FE (void);
// 0x000000E1 System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedBanner::.ctor()
extern void UnsupportedBanner__ctor_m2305236218B2386B02DA678ED001FF4FAE2D2787 (void);
// 0x000000E2 System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::SetupPlatform(UnityEngine.Advertisements.Platform.IPlatform)
extern void UnsupportedPlatform_SetupPlatform_m22984B9713B00083B85A33D3CF4A5B165E76D2F7 (void);
// 0x000000E3 System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::Initialize(System.String,System.Boolean,System.Boolean,UnityEngine.Advertisements.IUnityAdsInitializationListener)
extern void UnsupportedPlatform_Initialize_mF9C00E63D068D1E5604BEC91620D1FEC74333413 (void);
// 0x000000E4 System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::Load(System.String,UnityEngine.Advertisements.IUnityAdsLoadListener)
extern void UnsupportedPlatform_Load_mBD55F5399A17E003DCE5FC857412561513D7339D (void);
// 0x000000E5 System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::Show(System.String,UnityEngine.Advertisements.IUnityAdsShowListener)
extern void UnsupportedPlatform_Show_mCE2A60CDF44A03F7B18E1BEC4DD2E6BF8D8A669C (void);
// 0x000000E6 System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::SetMetaData(UnityEngine.Advertisements.MetaData)
extern void UnsupportedPlatform_SetMetaData_m80A5421CC456CCBCD6F05B243D9DD95B8A749D01 (void);
// 0x000000E7 System.Boolean UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::GetDebugMode()
extern void UnsupportedPlatform_GetDebugMode_m3908FE5E695799E32262440044A17232E6DB6E0B (void);
// 0x000000E8 System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::SetDebugMode(System.Boolean)
extern void UnsupportedPlatform_SetDebugMode_m383A19A4EE64823129E7389A6F9C1B5E7F91EB48 (void);
// 0x000000E9 System.String UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::GetVersion()
extern void UnsupportedPlatform_GetVersion_mE54D4A6DF5D19DF303A2BA5F8B97DB2B80C7C40C (void);
// 0x000000EA System.Boolean UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::IsInitialized()
extern void UnsupportedPlatform_IsInitialized_mFB76F9E1BAFCE627B07E6667873CF1F4E696D13E (void);
// 0x000000EB System.Boolean UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::IsReady(System.String)
extern void UnsupportedPlatform_IsReady_mEBA734BC05174630AF0270A448A76267FA347794 (void);
// 0x000000EC UnityEngine.Advertisements.PlacementState UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::GetPlacementState(System.String)
extern void UnsupportedPlatform_GetPlacementState_m4F06A841BD83A043D03A6068270457D3B145ABFF (void);
// 0x000000ED System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::.ctor()
extern void UnsupportedPlatform__ctor_m4DDFDBCFE5CCC6D97BD1E9101E88807FAD3390A6 (void);
// 0x000000EE System.Void UnityEngine.Advertisements.Platform.Editor.BannerPlaceholder::Awake()
extern void BannerPlaceholder_Awake_m103F5F462A094619FBE7F10B8B8EFDBC2BE1C83B (void);
// 0x000000EF System.Void UnityEngine.Advertisements.Platform.Editor.BannerPlaceholder::OnGUI()
extern void BannerPlaceholder_OnGUI_m544F919AB3F2EE7835A609AC83742C6B7861E173 (void);
// 0x000000F0 System.Void UnityEngine.Advertisements.Platform.Editor.BannerPlaceholder::ShowBanner(UnityEngine.Advertisements.BannerPosition,UnityEngine.Advertisements.BannerOptions)
extern void BannerPlaceholder_ShowBanner_m69226A7A2E420A019BE8FEA1C2F03B0393DA2C69 (void);
// 0x000000F1 System.Void UnityEngine.Advertisements.Platform.Editor.BannerPlaceholder::HideBanner()
extern void BannerPlaceholder_HideBanner_mB1AC5F23AA8332647D0EFB3461FFA6E940CD19A4 (void);
// 0x000000F2 UnityEngine.Texture2D UnityEngine.Advertisements.Platform.Editor.BannerPlaceholder::BackgroundTexture(System.Int32,System.Int32,UnityEngine.Color)
extern void BannerPlaceholder_BackgroundTexture_m7FA16D1C2F769BC1BEB753C135362B08A9EC5606 (void);
// 0x000000F3 UnityEngine.Rect UnityEngine.Advertisements.Platform.Editor.BannerPlaceholder::GetBannerRect(UnityEngine.Advertisements.BannerPosition)
extern void BannerPlaceholder_GetBannerRect_m92EF5CC1CCE2E5A5781CE17F1C1C4B91D9EE021A (void);
// 0x000000F4 System.Void UnityEngine.Advertisements.Platform.Editor.BannerPlaceholder::.ctor()
extern void BannerPlaceholder__ctor_m7AFD4CCC06EF529C0467A5AD511D233B15726E90 (void);
// 0x000000F5 System.Boolean UnityEngine.Advertisements.Platform.Android.AndroidBanner::get_IsLoaded()
extern void AndroidBanner_get_IsLoaded_m1090D7F14C00A444B7F905BEB0ECFDAC9E3D2721 (void);
// 0x000000F6 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::.ctor()
extern void AndroidBanner__ctor_mEFC2C531C4E5A77327120C2509408E0240FC111B (void);
// 0x000000F7 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::SetupBanner(UnityEngine.Advertisements.IBanner)
extern void AndroidBanner_SetupBanner_m7A722CA69652B7A2F0908FB01E9B04DF2F033B9F (void);
// 0x000000F8 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::Load(System.String,UnityEngine.Advertisements.BannerLoadOptions)
extern void AndroidBanner_Load_m3BA946237BC3E25BB91992BABF4FA06BD26FA06C (void);
// 0x000000F9 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::Show(System.String,UnityEngine.Advertisements.BannerOptions)
extern void AndroidBanner_Show_mE7BBBD0DECF98645A727D7C42ACE31B931A23CE7 (void);
// 0x000000FA System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::Hide(System.Boolean)
extern void AndroidBanner_Hide_m63A3B7CC005F053B60858B768E7C5E60E8259BD9 (void);
// 0x000000FB System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::SetPosition(UnityEngine.Advertisements.BannerPosition)
extern void AndroidBanner_SetPosition_m69DFAAF571A7DFD9261876EAA116C2DF06932586 (void);
// 0x000000FC System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::onUnityBannerShow(System.String)
extern void AndroidBanner_onUnityBannerShow_mE685C1F80E303BB80CD72395CB4912F2F416106C (void);
// 0x000000FD System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::onUnityBannerHide(System.String)
extern void AndroidBanner_onUnityBannerHide_mB098E9359B8366A35E3B6C30A4389F43080AE016 (void);
// 0x000000FE System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::onUnityBannerLoaded(System.String,UnityEngine.AndroidJavaObject)
extern void AndroidBanner_onUnityBannerLoaded_m096EAEC3FE07949B5D8D1A0BAA87F95F3C6D1A4C (void);
// 0x000000FF System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::onUnityBannerUnloaded(System.String)
extern void AndroidBanner_onUnityBannerUnloaded_m3B4D9FCD2C1FBDD3AB411616797093B2EE182DBE (void);
// 0x00000100 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::onUnityBannerClick(System.String)
extern void AndroidBanner_onUnityBannerClick_m37EAA4F3784D85FA358406F15F9BFA14515158E0 (void);
// 0x00000101 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::onUnityBannerError(System.String)
extern void AndroidBanner_onUnityBannerError_m828266783A11EC1C42E92557DC60FD51F79817BF (void);
// 0x00000102 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::<Hide>b__13_0()
extern void AndroidBanner_U3CHideU3Eb__13_0_m757B84BBA3EAC22EA037984375E4D74BB8C0C392 (void);
// 0x00000103 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::<Hide>b__13_1()
extern void AndroidBanner_U3CHideU3Eb__13_1_mCCC885F5E3422A1B18721462501A1CE3D84F9A57 (void);
// 0x00000104 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::<onUnityBannerLoaded>b__17_1()
extern void AndroidBanner_U3ConUnityBannerLoadedU3Eb__17_1_m6826646D632CC76023CE29D6A2743D0B828712CD (void);
// 0x00000105 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::<onUnityBannerLoaded>b__17_0()
extern void AndroidBanner_U3ConUnityBannerLoadedU3Eb__17_0_m2472E032DFC18A109FC71320B1DD1209B4B855D5 (void);
// 0x00000106 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::<onUnityBannerClick>b__19_0()
extern void AndroidBanner_U3ConUnityBannerClickU3Eb__19_0_mDC301E5E41214CCFCDA947F919C62281570C0CBA (void);
// 0x00000107 System.Void UnityEngine.Advertisements.Platform.Android.AndroidPlatform::.ctor()
extern void AndroidPlatform__ctor_mC0F30F0051F74F6EB82B75AEE221C4E8E276AFBB (void);
// 0x00000108 System.Void UnityEngine.Advertisements.Platform.Android.AndroidPlatform::SetupPlatform(UnityEngine.Advertisements.Platform.IPlatform)
extern void AndroidPlatform_SetupPlatform_m3E37CE5B5FB6F75D735D1C6B81D9C471A8DD8789 (void);
// 0x00000109 System.Void UnityEngine.Advertisements.Platform.Android.AndroidPlatform::Initialize(System.String,System.Boolean,System.Boolean,UnityEngine.Advertisements.IUnityAdsInitializationListener)
extern void AndroidPlatform_Initialize_m3CEA4813A7341719103F9427211663C6FC9C8FB5 (void);
// 0x0000010A System.Void UnityEngine.Advertisements.Platform.Android.AndroidPlatform::Load(System.String,UnityEngine.Advertisements.IUnityAdsLoadListener)
extern void AndroidPlatform_Load_m08BC441FD44F432B834FAD50EFB18DAFC8CBCCD7 (void);
// 0x0000010B System.Void UnityEngine.Advertisements.Platform.Android.AndroidPlatform::Show(System.String,UnityEngine.Advertisements.IUnityAdsShowListener)
extern void AndroidPlatform_Show_mAE3198D5C993293848A01F5AFCF1C2DA7AA8B3BD (void);
// 0x0000010C System.Void UnityEngine.Advertisements.Platform.Android.AndroidPlatform::SetMetaData(UnityEngine.Advertisements.MetaData)
extern void AndroidPlatform_SetMetaData_mE6528096B96E18E6F4088C2297440FE4D8241A95 (void);
// 0x0000010D System.Boolean UnityEngine.Advertisements.Platform.Android.AndroidPlatform::GetDebugMode()
extern void AndroidPlatform_GetDebugMode_m12C47DA56D6107866567D04955A6C066B5441421 (void);
// 0x0000010E System.Void UnityEngine.Advertisements.Platform.Android.AndroidPlatform::SetDebugMode(System.Boolean)
extern void AndroidPlatform_SetDebugMode_mEA48D6DD4DF54DF91843D434F9E3D2D180798323 (void);
// 0x0000010F System.String UnityEngine.Advertisements.Platform.Android.AndroidPlatform::GetVersion()
extern void AndroidPlatform_GetVersion_mC4FBF42109EF450D2F04A5439DE715AB1E559977 (void);
// 0x00000110 System.Boolean UnityEngine.Advertisements.Platform.Android.AndroidPlatform::IsInitialized()
extern void AndroidPlatform_IsInitialized_mFBC9096CB7EAEE5739F85695481B5C0FDF8037C2 (void);
// 0x00000111 System.Boolean UnityEngine.Advertisements.Platform.Android.AndroidPlatform::IsReady(System.String)
extern void AndroidPlatform_IsReady_m325868E1025DC4E7C6B94D50581645C668F5F7FF (void);
// 0x00000112 System.Void UnityEngine.Advertisements.Platform.Android.AndroidPlatform::RemoveListener()
extern void AndroidPlatform_RemoveListener_mF4FEFBC4D00DE55040239A2DDA95E4B8DA7C6BB3 (void);
// 0x00000113 UnityEngine.Advertisements.PlacementState UnityEngine.Advertisements.Platform.Android.AndroidPlatform::GetPlacementState(System.String)
extern void AndroidPlatform_GetPlacementState_m6EF901639291B3CC96B201CE2995868540393BC3 (void);
// 0x00000114 UnityEngine.AndroidJavaObject UnityEngine.Advertisements.Platform.Android.AndroidPlatform::GetCurrentAndroidActivity()
extern void AndroidPlatform_GetCurrentAndroidActivity_mA35240AE0C6D4CD2D53F97184FB5E7673E9FD6EC (void);
// 0x00000115 System.Void UnityEngine.Advertisements.Platform.Android.AndroidPlatform::UnityEngine.Advertisements.Purchasing.IPurchasingEventSender.SendPurchasingEvent(System.String)
extern void AndroidPlatform_UnityEngine_Advertisements_Purchasing_IPurchasingEventSender_SendPurchasingEvent_m8FC35C2E29174CF548316F3E83AA75F6BF74B3B1 (void);
// 0x00000116 UnityEngine.AndroidJavaObject UnityEngine.Advertisements.Platform.Android.BannerBundle::get_bannerView()
extern void BannerBundle_get_bannerView_m494FB7D5596B778F81D08E411FBAAEF1D44B35F5 (void);
// 0x00000117 System.String UnityEngine.Advertisements.Platform.Android.BannerBundle::get_bannerPlacementId()
extern void BannerBundle_get_bannerPlacementId_m4E41B08F882B7E40EDA642DB23CE704A2604DE2D (void);
// 0x00000118 System.Void UnityEngine.Advertisements.Platform.Android.BannerBundle::.ctor(System.String,UnityEngine.AndroidJavaObject)
extern void BannerBundle__ctor_m1619218BCCA4FCD13708A3236FFA341712D871B8 (void);
// 0x00000119 System.String UnityEngine.Advertisements.Events.FinishEventArgs::get_placementId()
extern void FinishEventArgs_get_placementId_mC6F1BC10E5AFAD091FFC195A6D54EF279406C1C7 (void);
// 0x0000011A UnityEngine.Advertisements.ShowResult UnityEngine.Advertisements.Events.FinishEventArgs::get_showResult()
extern void FinishEventArgs_get_showResult_m26C0D63F84FC858B04B3A788E633FA1F35AB93F7 (void);
// 0x0000011B System.Void UnityEngine.Advertisements.Events.FinishEventArgs::.ctor(System.String,UnityEngine.Advertisements.ShowResult)
extern void FinishEventArgs__ctor_m22E9FC5FC756FA1F6893629C16C7B7B5C77F5219 (void);
// 0x0000011C System.String UnityEngine.Advertisements.Events.StartEventArgs::get_placementId()
extern void StartEventArgs_get_placementId_m9469A02B1AF7DCFC3BF0FDC63C867163827F4519 (void);
// 0x0000011D System.Void UnityEngine.Advertisements.Events.StartEventArgs::.ctor(System.String)
extern void StartEventArgs__ctor_mCB7FE6F57A53D5BBEB50ED07DBB158A8E112868C (void);
// 0x0000011E System.Void UnityEngine.Advertisements.Advertisement/Banner::Load()
extern void Banner_Load_m06A48D7352F2E23A7AD41CF41409BB0946DD9A00 (void);
// 0x0000011F System.Void UnityEngine.Advertisements.Advertisement/Banner::Load(UnityEngine.Advertisements.BannerLoadOptions)
extern void Banner_Load_mB519531E433DB53EA86E74A2757DFF857B0E5CB6 (void);
// 0x00000120 System.Void UnityEngine.Advertisements.Advertisement/Banner::Load(System.String)
extern void Banner_Load_mC922C4D831834C3649203DA96E78E5971CF40210 (void);
// 0x00000121 System.Void UnityEngine.Advertisements.Advertisement/Banner::Load(System.String,UnityEngine.Advertisements.BannerLoadOptions)
extern void Banner_Load_m000224EDA0BF2FF61EDE66E167148ACB8C294579 (void);
// 0x00000122 System.Void UnityEngine.Advertisements.Advertisement/Banner::Show()
extern void Banner_Show_mF34A748E029451235C4EB3C2EC0B1E8E128AEE96 (void);
// 0x00000123 System.Void UnityEngine.Advertisements.Advertisement/Banner::Show(UnityEngine.Advertisements.BannerOptions)
extern void Banner_Show_m40A12D094C9EAA6AF725784589EA3228F0EF2520 (void);
// 0x00000124 System.Void UnityEngine.Advertisements.Advertisement/Banner::Show(System.String)
extern void Banner_Show_m3C42E55C8F73C12F90BF9AB9D5A293CA7368906E (void);
// 0x00000125 System.Void UnityEngine.Advertisements.Advertisement/Banner::Show(System.String,UnityEngine.Advertisements.BannerOptions)
extern void Banner_Show_m73BE208CABEE908A5F4DAC936686A31A715EBCDB (void);
// 0x00000126 System.Void UnityEngine.Advertisements.Advertisement/Banner::Hide(System.Boolean)
extern void Banner_Hide_mA828D623AAD961B197A2338FD1DF21D4D5D0C067 (void);
// 0x00000127 System.Void UnityEngine.Advertisements.Advertisement/Banner::SetPosition(UnityEngine.Advertisements.BannerPosition)
extern void Banner_SetPosition_mB03D6FE7CBC2DA088649E8EC1DD89D352E201043 (void);
// 0x00000128 System.Boolean UnityEngine.Advertisements.Advertisement/Banner::get_isLoaded()
extern void Banner_get_isLoaded_m35305E62DC54136201C80DD82F6614D579F46875 (void);
// 0x00000129 System.Void UnityEngine.Advertisements.Advertisement/<>c__DisplayClass32_0::.ctor()
extern void U3CU3Ec__DisplayClass32_0__ctor_mFB01650D49D562E550A97CEF5AE47AD75A9B7DD7 (void);
// 0x0000012A System.Void UnityEngine.Advertisements.Advertisement/<>c__DisplayClass32_0::<CreatePlatform>b__0()
extern void U3CU3Ec__DisplayClass32_0_U3CCreatePlatformU3Eb__0_mD0920F22572DB42E8D4918971E2DCA0BEA61C605 (void);
// 0x0000012B System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass15_0::.ctor()
extern void U3CU3Ec__DisplayClass15_0__ctor_m67A9A93608F4F936AA6F42CE50957BC4E2A9B7C4 (void);
// 0x0000012C System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass15_0::<UnityAdsBannerDidShow>b__0()
extern void U3CU3Ec__DisplayClass15_0_U3CUnityAdsBannerDidShowU3Eb__0_m290D12AF0E2C5E24291DF62E5A32F4EDFFF55D59 (void);
// 0x0000012D System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass16_0::.ctor()
extern void U3CU3Ec__DisplayClass16_0__ctor_m598B85B717722C83DA7A15A08816325E22B8D0AB (void);
// 0x0000012E System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass16_0::<UnityAdsBannerDidHide>b__0()
extern void U3CU3Ec__DisplayClass16_0_U3CUnityAdsBannerDidHideU3Eb__0_mD693250A23E026FC2968DB1093357A9EF75887F2 (void);
// 0x0000012F System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass17_0::.ctor()
extern void U3CU3Ec__DisplayClass17_0__ctor_mD441B8465D66F3AB740A34B65BBC82DAE3CA0289 (void);
// 0x00000130 System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass17_0::<UnityAdsBannerClick>b__0()
extern void U3CU3Ec__DisplayClass17_0_U3CUnityAdsBannerClickU3Eb__0_m71D54A12369066DDE53612B11C16A4DD5CBA633A (void);
// 0x00000131 System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass18_0::.ctor()
extern void U3CU3Ec__DisplayClass18_0__ctor_mCF0DFDB73C55CE62D99AB2F0C50D512B21A4DA73 (void);
// 0x00000132 System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass18_0::<UnityAdsBannerDidLoad>b__0()
extern void U3CU3Ec__DisplayClass18_0_U3CUnityAdsBannerDidLoadU3Eb__0_m757FA977A4DB6BC2C878A072214A0261E64114DD (void);
// 0x00000133 System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass19_0::.ctor()
extern void U3CU3Ec__DisplayClass19_0__ctor_m2E5BCC22AE13F767CC10CEEF8A6C9378EFD141B6 (void);
// 0x00000134 System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass19_0::<UnityAdsBannerDidError>b__0()
extern void U3CU3Ec__DisplayClass19_0_U3CUnityAdsBannerDidErrorU3Eb__0_m858AD48AEDD9C616E3385F73D8072347190CDA44 (void);
// 0x00000135 System.Void UnityEngine.Advertisements.BannerLoadOptions/LoadCallback::.ctor(System.Object,System.IntPtr)
extern void LoadCallback__ctor_m149C4CA2674F63DC85F26C60CF4BE852C5974D60 (void);
// 0x00000136 System.Void UnityEngine.Advertisements.BannerLoadOptions/LoadCallback::Invoke()
extern void LoadCallback_Invoke_m5CCDF2F93BF4603344CE9A164AC93709559F99D9 (void);
// 0x00000137 System.IAsyncResult UnityEngine.Advertisements.BannerLoadOptions/LoadCallback::BeginInvoke(System.AsyncCallback,System.Object)
extern void LoadCallback_BeginInvoke_m26CBBDD499B77472B762CCEBEAD5A80F539B3DAC (void);
// 0x00000138 System.Void UnityEngine.Advertisements.BannerLoadOptions/LoadCallback::EndInvoke(System.IAsyncResult)
extern void LoadCallback_EndInvoke_m82BE135E79EB3C0026A2B37FE3DD0ADF94A75845 (void);
// 0x00000139 System.Void UnityEngine.Advertisements.BannerLoadOptions/ErrorCallback::.ctor(System.Object,System.IntPtr)
extern void ErrorCallback__ctor_m8C594084F860F0AC9D416B339E78D69649DFA7F1 (void);
// 0x0000013A System.Void UnityEngine.Advertisements.BannerLoadOptions/ErrorCallback::Invoke(System.String)
extern void ErrorCallback_Invoke_mC151F8EE2510FBE8A39EEBAED2A4737AC53155F7 (void);
// 0x0000013B System.IAsyncResult UnityEngine.Advertisements.BannerLoadOptions/ErrorCallback::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void ErrorCallback_BeginInvoke_mAA70F10967DC875DF3B654EE625BB7FB9F01D6B4 (void);
// 0x0000013C System.Void UnityEngine.Advertisements.BannerLoadOptions/ErrorCallback::EndInvoke(System.IAsyncResult)
extern void ErrorCallback_EndInvoke_m5FF48332DD1390B74C606AD6867C78AE54BE6E81 (void);
// 0x0000013D System.Void UnityEngine.Advertisements.BannerOptions/BannerCallback::.ctor(System.Object,System.IntPtr)
extern void BannerCallback__ctor_m0944126600B12308BCFD070EAF6CC81009B5C83F (void);
// 0x0000013E System.Void UnityEngine.Advertisements.BannerOptions/BannerCallback::Invoke()
extern void BannerCallback_Invoke_mC67383B48C2D4BBC88D1D3D283D8F7117F6AC984 (void);
// 0x0000013F System.IAsyncResult UnityEngine.Advertisements.BannerOptions/BannerCallback::BeginInvoke(System.AsyncCallback,System.Object)
extern void BannerCallback_BeginInvoke_m56ED5ED5DDAF529F6969CEF1C43D423829F38D96 (void);
// 0x00000140 System.Void UnityEngine.Advertisements.BannerOptions/BannerCallback::EndInvoke(System.IAsyncResult)
extern void BannerCallback_EndInvoke_m501FDA627ADE21E44683064A94CCD8C5193A4EA5 (void);
// 0x00000141 System.Boolean UnityEngine.Advertisements.Utilities.Json/Parser::IsWordBreak(System.Char)
extern void Parser_IsWordBreak_mEA531542CF6B0595B87B30359F4810D8C4AF3CCE (void);
// 0x00000142 System.Void UnityEngine.Advertisements.Utilities.Json/Parser::.ctor(System.String)
extern void Parser__ctor_m1A2111A83BDF4FAE0D99D96B9A6B73BD62308F13 (void);
// 0x00000143 System.Object UnityEngine.Advertisements.Utilities.Json/Parser::Parse(System.String)
extern void Parser_Parse_mC705E4F81C5A57EA58D18EEDAE2BDFF9F3986DCC (void);
// 0x00000144 System.Void UnityEngine.Advertisements.Utilities.Json/Parser::Dispose()
extern void Parser_Dispose_m7820AD20ADF545D1312B2913EE76CB629AB36E50 (void);
// 0x00000145 System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Advertisements.Utilities.Json/Parser::ParseObject()
extern void Parser_ParseObject_m58B999F87989D85F18FB78A5C9BACC52396258B6 (void);
// 0x00000146 System.Collections.Generic.List`1<System.Object> UnityEngine.Advertisements.Utilities.Json/Parser::ParseArray()
extern void Parser_ParseArray_m87775DD7390CB08B60A5A7A5DD005BFFED58A22A (void);
// 0x00000147 System.Object UnityEngine.Advertisements.Utilities.Json/Parser::ParseValue()
extern void Parser_ParseValue_m4D47F7AEDA32907EAE7299EC747DA6DEA0C43B42 (void);
// 0x00000148 System.Object UnityEngine.Advertisements.Utilities.Json/Parser::ParseByToken(UnityEngine.Advertisements.Utilities.Json/Parser/TOKEN)
extern void Parser_ParseByToken_mDA0A4BB8C64060809DA9C419726C315F102F9C4C (void);
// 0x00000149 System.String UnityEngine.Advertisements.Utilities.Json/Parser::ParseString()
extern void Parser_ParseString_m7CF0134829A15287809F252B5E7D708C4F737430 (void);
// 0x0000014A System.Object UnityEngine.Advertisements.Utilities.Json/Parser::ParseNumber()
extern void Parser_ParseNumber_mDE40D9D8524DD6AEF3D4DB3B9039C05B68656056 (void);
// 0x0000014B System.Void UnityEngine.Advertisements.Utilities.Json/Parser::EatWhitespace()
extern void Parser_EatWhitespace_mDD81C0893F358C5F2890CDCB99FFEB7D3612D0FB (void);
// 0x0000014C System.Char UnityEngine.Advertisements.Utilities.Json/Parser::get_PeekChar()
extern void Parser_get_PeekChar_m94BF1DEC531D3A6F09FE92A154B9C02148E79068 (void);
// 0x0000014D System.Char UnityEngine.Advertisements.Utilities.Json/Parser::get_NextChar()
extern void Parser_get_NextChar_m32AB048CC54E50139E92FD17D561580A6AC9ACB1 (void);
// 0x0000014E System.String UnityEngine.Advertisements.Utilities.Json/Parser::get_NextWord()
extern void Parser_get_NextWord_mE66A01EBC62B417384B25B796FDB56CB7A022429 (void);
// 0x0000014F UnityEngine.Advertisements.Utilities.Json/Parser/TOKEN UnityEngine.Advertisements.Utilities.Json/Parser::get_NextToken()
extern void Parser_get_NextToken_mF80ADF81AB1B385ACE09B4E216C3005E8F004E1A (void);
// 0x00000150 System.Void UnityEngine.Advertisements.Utilities.Json/Serializer::.ctor()
extern void Serializer__ctor_m3203CD70FEF4DFC38424D9E29109A98604F6A8CA (void);
// 0x00000151 System.String UnityEngine.Advertisements.Utilities.Json/Serializer::Serialize(System.Object)
extern void Serializer_Serialize_mB274C6B492226DB15C8552D3ABFF2F26F7C19BAD (void);
// 0x00000152 System.Void UnityEngine.Advertisements.Utilities.Json/Serializer::SerializeValue(System.Object)
extern void Serializer_SerializeValue_m05069E2615454E585C7D6E94CE5AD40D7883BC20 (void);
// 0x00000153 System.Void UnityEngine.Advertisements.Utilities.Json/Serializer::SerializeObject(System.Collections.IDictionary)
extern void Serializer_SerializeObject_m244A448784F9792E207C3F5ABBD690F1DD367D97 (void);
// 0x00000154 System.Void UnityEngine.Advertisements.Utilities.Json/Serializer::SerializeArray(System.Collections.IList)
extern void Serializer_SerializeArray_mEF16068F947E3CA46FDDE359E1871A15A6FEEA2C (void);
// 0x00000155 System.Void UnityEngine.Advertisements.Utilities.Json/Serializer::SerializeString(System.String)
extern void Serializer_SerializeString_m53F7333FF3673BC1EC02FB3EFEC439A11240175B (void);
// 0x00000156 System.Void UnityEngine.Advertisements.Utilities.Json/Serializer::SerializeOther(System.Object)
extern void Serializer_SerializeOther_m69C647BED1C71CFA73CC89B52D8B4600E5D6330D (void);
// 0x00000157 System.Void UnityEngine.Advertisements.Platform.Platform/<>c__DisplayClass33_0::.ctor()
extern void U3CU3Ec__DisplayClass33_0__ctor_m455D237D767665AE14BE4B82352A2308A32B4BB8 (void);
// 0x00000158 System.Void UnityEngine.Advertisements.Platform.Platform/<>c__DisplayClass33_0::<Show>b__0(System.Object,UnityEngine.Advertisements.Events.FinishEventArgs)
extern void U3CU3Ec__DisplayClass33_0_U3CShowU3Eb__0_m050B2616328D1A64A058F83A83591F346CAA9E84 (void);
// 0x00000159 System.Void UnityEngine.Advertisements.Platform.Platform/<>c__DisplayClass39_0::.ctor()
extern void U3CU3Ec__DisplayClass39_0__ctor_mAF019D1DE85234AE7450F5B06ADEB3B61A8015CA (void);
// 0x0000015A System.Void UnityEngine.Advertisements.Platform.Platform/<>c__DisplayClass39_0::<UnityAdsReady>b__0()
extern void U3CU3Ec__DisplayClass39_0_U3CUnityAdsReadyU3Eb__0_mE10152E14DE66C4BA4C9CCF1000BA687AA8BFB5B (void);
// 0x0000015B System.Void UnityEngine.Advertisements.Platform.Platform/<>c__DisplayClass40_0::.ctor()
extern void U3CU3Ec__DisplayClass40_0__ctor_m207B4303A22E409C0AC160FECDC048D8372FB705 (void);
// 0x0000015C System.Void UnityEngine.Advertisements.Platform.Platform/<>c__DisplayClass40_0::<UnityAdsDidError>b__0()
extern void U3CU3Ec__DisplayClass40_0_U3CUnityAdsDidErrorU3Eb__0_m9FD88CE43B7C31FF2EEC4DB9D6A5E8C8FF66F5F1 (void);
// 0x0000015D System.Void UnityEngine.Advertisements.Platform.Platform/<>c__DisplayClass41_0::.ctor()
extern void U3CU3Ec__DisplayClass41_0__ctor_m65CC8D05E090548EDAB02E3D300092A26FAEC982 (void);
// 0x0000015E System.Void UnityEngine.Advertisements.Platform.Platform/<>c__DisplayClass41_0::<UnityAdsDidStart>b__0()
extern void U3CU3Ec__DisplayClass41_0_U3CUnityAdsDidStartU3Eb__0_m4540296447426B4EB390F86A71FB982C7B94E638 (void);
// 0x0000015F System.Void UnityEngine.Advertisements.Platform.Platform/<>c__DisplayClass42_0::.ctor()
extern void U3CU3Ec__DisplayClass42_0__ctor_mB375F170A2D2D542DAFCCB20440D1F7CF0DE67EA (void);
// 0x00000160 System.Void UnityEngine.Advertisements.Platform.Platform/<>c__DisplayClass42_0::<UnityAdsDidFinish>b__0()
extern void U3CU3Ec__DisplayClass42_0_U3CUnityAdsDidFinishU3Eb__0_mB61C98F322502254470E0D4CDB36E6E02652A1C9 (void);
// 0x00000161 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner/<>c__DisplayClass11_0::.ctor()
extern void U3CU3Ec__DisplayClass11_0__ctor_m762DE44D1512CB4805DC8A29EF9C6528796CF5CD (void);
// 0x00000162 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner/<>c__DisplayClass11_0::<Load>b__0()
extern void U3CU3Ec__DisplayClass11_0_U3CLoadU3Eb__0_m9920C93CCA34B774014E199AAE65CC7C4B055757 (void);
// 0x00000163 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner/<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_m58EE4C928EF5FC73C913609507FDEA5AF3341F81 (void);
// 0x00000164 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner/<>c__DisplayClass12_0::<Show>b__0()
extern void U3CU3Ec__DisplayClass12_0_U3CShowU3Eb__0_m271FCA57067860092B7BC2D4CE9475AF0F95C6C8 (void);
// 0x00000165 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner/<>c__DisplayClass12_0::<Show>b__1()
extern void U3CU3Ec__DisplayClass12_0_U3CShowU3Eb__1_m5DDA628C1D9F00A543BAE39AFD48383B3BED59BD (void);
// 0x00000166 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner/<>c__DisplayClass20_0::.ctor()
extern void U3CU3Ec__DisplayClass20_0__ctor_m51046C9EFFA4F39A1EA8BF75C71D92FB24CD0845 (void);
// 0x00000167 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner/<>c__DisplayClass20_0::<onUnityBannerError>b__0()
extern void U3CU3Ec__DisplayClass20_0_U3ConUnityBannerErrorU3Eb__0_mBBCFAF34F5D9CC8ACC5C149156925A3FC6A05E06 (void);
static Il2CppMethodPointer s_methodPointers[359] = 
{
	Advertisement__cctor_mB44256768A89BD38E6BA522B4C178350A1983B74,
	Advertisement_get_isInitialized_mB504A790B305C52734533C91982678CA45EA9A4F,
	Advertisement_get_isSupported_m137C1841EE86BF502BD7EB1D9889078B2387CF8E,
	Advertisement_get_debugMode_m6E32EAAA7633FA61F2312396E5DBA7367840080A,
	Advertisement_set_debugMode_mA64998E948E522B05755757BDF7279866AD52067,
	Advertisement_get_version_m6574BA0BE4B94AA51E1E92D207BE3294C8E48FF1,
	Advertisement_get_isShowing_m004D5F97E151927DD0637A9967FAD7E704932123,
	Advertisement_Initialize_m4B4A6CA19B11047279C327CDF240F1E7223C39F2,
	Advertisement_Initialize_m50E5DCFAC318D93B37B03C454C0B6648428A109F,
	Advertisement_Initialize_mDC6992FA08E4C26BEF880B14E644F71DE9C9804D,
	Advertisement_Initialize_m8E5F0FE27AA821BC18C08CA9B0DFCD795EF0EB3E,
	Advertisement_IsReady_m4F30149C74D2F243D9DE8F95F4864B241236D016,
	Advertisement_IsReady_m7880E5F097F352E81CED7DFA42B48F150B0EC682,
	Advertisement_Load_m0C9D79444FCD24319D033DCB6B305B8AF15FA2E5,
	Advertisement_Load_m2A2737F1D9BDBF23912614A7B8BAA7EC837CF1A8,
	Advertisement_Show_m1FC1AE09403CE9104EDBCC088773B531C44C0C85,
	Advertisement_Show_m75759E89438020A5888F5083A67D4B00A428BAB5,
	Advertisement_Show_m8B3BB4A04BD6822D5B0A8CE6E675E6CDEDFDCEED,
	Advertisement_Show_m542E0BE5F1952599326798CFF080DE113AA93D8C,
	Advertisement_Show_mC45B5B2EE78CAC1D790803BD9337FEF4472F8F33,
	Advertisement_Show_m927377540F2FBAA7072449D8C011A7252A78FA3A,
	Advertisement_SetMetaData_mA9AA97AF50809D3FC117D28E80E24A1A31031F28,
	Advertisement_AddListener_m1548B00420180351DF863FBB372C0264AAE49C44,
	Advertisement_RemoveListener_m408A2E9C2793DC9FDC6806514CA7619A62B9D792,
	Advertisement_GetPlacementState_mB65B8E93250DE669EA3B46F54A39C0D2D0132648,
	Advertisement_GetPlacementState_mE0B4AC310CDAFAF4D14A04992BB26220253EFF5B,
	Advertisement_CreatePlatform_mC1CDD4759D65156D6F201E06DDD979696B05D4E3,
	Advertisement_IsSupported_m251D8427493C493098367A60067F458251BD62DF,
	Banner_get_UnityLifecycleManager_mF54F9B5DA7BC9F2937A66D5699D8DA858001F410,
	Banner_get_IsLoaded_m5E13400241DEFF428441C8FA624F53180D7D2436,
	Banner_get_ShowAfterLoad_m18167D1F22BB9C243CAB3F43630C407EBF73DF92,
	Banner_set_ShowAfterLoad_m25D9F9574C9405A6E5C3B17943CECFE8D0E672D0,
	Banner__ctor_mAAB66E5B48DA4D19C92F7010E26267D320753F28,
	Banner_Load_m02EF12198664BAFA0C0F2D2EB10653C816A63E1E,
	Banner_Show_mDF4B91C253C1B26884B07F008037A2E02544F99E,
	Banner_Hide_m7C66B6BB7207D92974D1704E57BF087EA1E44601,
	Banner_SetPosition_m8892F4A92814ADD9C870F75074C03C15880B6504,
	Banner_UnityAdsBannerDidShow_mD8BE33941D2EFB7BABDA2A76E49DC8A076F67615,
	Banner_UnityAdsBannerDidHide_mC653817DB4091D2E1D17C2C2DC8F925100E95A96,
	Banner_UnityAdsBannerClick_m10D3293D7D833DB6015C29F618155C4D827DC82A,
	Banner_UnityAdsBannerDidLoad_mD75000EB978B1FE76D8FB67F6774A670EFB353D8,
	Banner_UnityAdsBannerDidError_m6D321E25A913EC317789D61630D67CFFD77787B9,
	BannerLoadOptions_get_loadCallback_mF742FFBF21191B852CCA2DDB31C8E0D850C7A2E2,
	BannerLoadOptions_set_loadCallback_m9D3C4E78D67CEC16AE816BF69B65A6423A6969B2,
	BannerLoadOptions_get_errorCallback_m7710B7894CAA75F3A117F2A766F24588AF1A6EFD,
	BannerLoadOptions_set_errorCallback_m5069D00E410DF3BEDFF097BF40F3B3476EF88B95,
	BannerLoadOptions__ctor_m4D5D26302DC12B8F2BA55508FDFDE6A152051DAD,
	BannerOptions_get_showCallback_mDC81918C6D7C62040F55455F6E4A824E17FF8F2D,
	BannerOptions_set_showCallback_mA9D8C200D021B29B0B974636F94CE652FED761FD,
	BannerOptions_get_hideCallback_m84DCD124F4FA98C19E43073B0269A9E7C61DB956,
	BannerOptions_set_hideCallback_mBD46ECBE1865B8B388C0E96B8CAD031A059CAEBB,
	BannerOptions_get_clickCallback_m6B317492111A800F9260E1AE3460D8976B8FFFEA,
	BannerOptions_set_clickCallback_m2ECE4E61C720E9F065E86579B0F0D92122F319D2,
	BannerOptions__ctor_m7B5E6180AA634C069E053C7E13F1544C50B01131,
	Configuration_get_enabled_m03B11BE0F38C5260711D0465911DC29CFC2793CA,
	Configuration_get_defaultPlacement_mE20D0F03E31BD1A9FD19BA5DFC7A8A87579F0B0F,
	Configuration_get_placements_m1B6CEB298ABFC85E077B538BC1B277DFB0828EBE,
	Configuration__ctor_m09EE5351C61B65B64FA1B5AFC53BD2579BA6615C,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MetaData_get_category_mBEB012D8CC3DBD56EEC5E14C875C34C946EAC3BB,
	MetaData_set_category_m1DF58928A9830B1E889C59E5B935FE4928C8C784,
	MetaData__ctor_m6EF2DA52AAD0C124284ED96227F52ABCDE73C87C,
	MetaData_Set_mF1B807C8D6D380084618F0C6FAA3A9C21F9DD16C,
	MetaData_Get_m83F3F32F0D2EF2E3AC76A08563BD215287588B1B,
	MetaData_Values_m23DFE4EA7EA2884125E84D9A73B1A5F55663C3BA,
	MetaData_ToJSON_m249EDA63A72895084104FBE66D86462FEE33F948,
	AndroidInitializationListener__ctor_mAE6F05EC1B9F314BB1DEFFDA210E9A69DD466038,
	AndroidInitializationListener_onInitializationComplete_mCF14A6CFB4E6983434E25C2CA0AA49A6E645DC7C,
	AndroidInitializationListener_onInitializationFailed_mB27DFE58C465F18C0F0EDE001A63CD6FC9C5D779,
	AndroidLoadListener__ctor_m329BA4CFC453CFBD1AAA2D8F919C977114B3F983,
	AndroidLoadListener_onUnityAdsAdLoaded_m8AB01365523818E228F57CE987081EB3B1B9EACA,
	AndroidLoadListener_onUnityAdsFailedToLoad_m98E454C3615083F0DCDAEA653CE7472D7031B3D7,
	AndroidShowListener__ctor_m78A228D8BBB182AA5BDDEE6A784D15BD5629ABBF,
	AndroidShowListener_onUnityAdsShowFailure_m1ECA3172457D1839C43FD6D914A63480DF36C821,
	AndroidShowListener_onUnityAdsShowStart_mF79CE9BA0FEF9B29848FE0886AD90728EDA4A625,
	AndroidShowListener_onUnityAdsShowClick_mD89F26EF7574FF4659F4DD3783B25D0B14F6EE40,
	AndroidShowListener_onUnityAdsShowComplete_m1675B4E37B07777ED894121B299405E5A9F550FE,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ShowOptions_get_resultCallback_m3927DDD6E2EF6219A02421B4C56D6D5F5F059DB8,
	ShowOptions_set_resultCallback_mC6C40AE153D30E9F4BB9E904CC15E23D1735E6B3,
	ShowOptions_get_gamerSid_m778D4145EEE07AA52F4F14062AE02CF8B5E78A40,
	ShowOptions_set_gamerSid_m22AFB748A6E131A3534825E583420AE42A303172,
	ShowOptions__ctor_mE9F7F751F6BE70F4A7B1F3949E9B9B54DE1EB6FE,
	ApplicationQuit_add_OnApplicationQuitEventHandler_m11D5EBA7CE43A2ECC034302EC29D640970C8739B,
	ApplicationQuit_remove_OnApplicationQuitEventHandler_m1C5525879D2483EE251E682B44882A2814770D51,
	ApplicationQuit_OnApplicationQuit_mADE194EF6C6CB9C6D830A8715893E85E8294AF34,
	ApplicationQuit__ctor_m7FF706AC8A858505C029739ED28613AEE0D67363,
	CoroutineExecutor_Update_mBDE991E82D05D5A0BBB50918A558D90152508C2C,
	CoroutineExecutor__ctor_mAD7016CBCA79F3304EB1C5948255E87E44A7422A,
	EnumUtilities_GetShowResultsFromCompletionState_mDCF7522190C670BFCDD677A6AFA7B7C98B273D74,
	NULL,
	NULL,
	NULL,
	NULL,
	Json_Deserialize_m76115855BAC9BE9588C192A488D3DAC15CD8BF6C,
	Json_Serialize_m1B1229A14A448280851ACD5195F0EA976F903EC6,
	UnityLifecycleManager__ctor_mD95AC377DC9E3E99821DB9FC9BBA241579AEFEB2,
	UnityLifecycleManager_Initialize_mD73B907AD8F2E0F0B2F7720FDA4C00B533182DC6,
	UnityLifecycleManager_StartCoroutine_m3C0B3C37F7BABBB621FC5B3FE7176D8CE58E8CE9,
	UnityLifecycleManager_Post_mD922A2AB5F40500B93E6732F27EDBB855A70D82D,
	UnityLifecycleManager_Dispose_m2E15066A520646FED13DF2AAE6BCE8049BC9F746,
	UnityLifecycleManager_SetOnApplicationQuitCallback_m3F4658AC36BED9AC5AA3AFBAAF01C3FFE4ABFB3F,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Purchase_onPurchasingCommand_mCCB94AFCA2E8C369F16E0D1C8E12C40BA6A08818,
	Purchase_onGetPurchasingVersion_m3069B940592B0841B13AB1873EA79D07F9284D98,
	Purchase_onGetProductCatalog_m3B6B1FE855B05DBEA625BEFD93624F6B90F1E68D,
	Purchase_onInitializePurchasing_m1CA49D6B7098E0ABC34D9C5068C6B4FF40959525,
	Purchase_SendEvent_m3C79B682C8D2C031D44746202F61AB5BEA6C0F22,
	Purchase_Initialize_mAC442010AFA23D482F2CDF7144B2BDA793D5F92B,
	Purchase__ctor_mBD4CD713E8E9167FA4FD2024B651C9BBB3A9E7A7,
	Purchasing_Initialize_m14E011B5AE0C0D41490676A551DD364245EBF0EC,
	Purchasing_InitiatePurchasingCommand_m0B076A546123334CADFE0E805E7125BBE081D4B3,
	Purchasing_GetPurchasingCatalog_mC2BEAD1D96CFB58D7778AEBB4A43A9A6D9DD0CD9,
	Purchasing_GetPromoVersion_mDB84F17B70F76C5B00D7E2062B26BB92AF4304BA,
	Purchasing_SendEvent_m8274AD7B80C0853660EA40ADB6FBF2A0B12A7950,
	Purchasing__cctor_m616808EB3A5A2F1577EBF1F684D7080F82B17292,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Platform_add_OnStart_mA5EE80229E95F2DF71502EAB1143E5D89EAD018D,
	Platform_remove_OnStart_m44D267A6FFEEEE0BF6C7BFDDA355AC683A0641A8,
	Platform_add_OnFinish_mAAFDFF3ACA72652DEDC17B5651C2EF169A8D7F4F,
	Platform_remove_OnFinish_mBF555D3C995D7A9180CAA6B1761874640E0FD255,
	Platform_get_Banner_m17B6B2160CCB2F2368C06524104DA55639C4B9FA,
	Platform_get_UnityLifecycleManager_m6E8BDFA3313C3476DA8519F9E759A6C2599F61CC,
	Platform_get_NativePlatform_m055DE92A6397C9BFB2A729F9294AFA99B002F8C2,
	Platform_get_IsInitialized_mD7116C3FCF92EED195075AE7499A67C728A201B7,
	Platform_get_IsShowing_mD7D4DB0DCE81A7100F990CF75DEB9597D5CDEC12,
	Platform_set_IsShowing_m1575EFA862EA969C6E44160AF66846DEF009A2BF,
	Platform_get_Version_mC8539FE79E8D9DAFB8A56995873C6F760E4F4D7D,
	Platform_get_DebugMode_m763567976C7FDE0681BC141DDCDF5ADD1315B057,
	Platform_set_DebugMode_m7BA41C5CF5636C427D6373602E81618739E37D2C,
	Platform_get_Listeners_m04B40907B7A972048637D4537674D7ACB6089F41,
	Platform__ctor_mAC2FF35E92BDB79828A05B296D7FB07CFF9C6F36,
	Platform_Initialize_mE0D86443F52FBB8F9B43DBF399DE10C226B7E184,
	Platform_Load_m5F63716E5B2174B9C3B4AA71E0CB8D975CE34485,
	Platform_Load_m52FBC7EAA62CB8714E764E262C8143C5ECB0A0DA,
	Platform_Show_mD45AAE3C87D4D25C60C45B415A74C4AFBC9BFA1F,
	Platform_AddListener_m0E5E496A62CBA5D60CACB226AAF364CDBFAE9BC9,
	Platform_RemoveListener_m66CB33820F4A0A5B9DEF3194EFECA0E26EA655AD,
	Platform_IsReady_mB094B73DE8225C33AE267B7088FF8A733F3861C8,
	Platform_GetPlacementState_mA61C07F97BE90F7F48B6F6BBA0C192D71C16F3F8,
	Platform_SetMetaData_mE3B49CE0F3F4450ADCC90AC98B53E313192A39A7,
	Platform_UnityAdsReady_m27010E4F25709F174EDEED43C5DD346839BC2B91,
	Platform_UnityAdsDidError_mF8EA8A52F16A9017774A30732F824ADD5F9EC19C,
	Platform_UnityAdsDidStart_m21E24FF84CED1830B1B0AFE1E6359564601D0EBC,
	Platform_UnityAdsDidFinish_m4661B61B0BD4C0DCE9DCADA4B094C24CDABC35E8,
	Platform_GetClonedHashSet_m646ACA8E270FA8DCA83D924DE430CFF9C8E3D324,
	Platform_U3CInitializeU3Eb__30_0_m4F14311AF1F837C76F9598C0898151D55622CF7E,
	Platform_U3CInitializeU3Eb__30_1_m2FEC023987B9CFA5A49272DD7E6B9E4844CACDCF,
	UnsupportedBanner_get_IsLoaded_mD3A1CE2F7F0CEA24C7604E761666965EFBBEB442,
	UnsupportedBanner_SetupBanner_mAF0EBC53100492D01C56F63DC4FDE13A0710B774,
	UnsupportedBanner_Load_mDDBC9C23F8E43EFE02C3DBEA31E8503AFC4D6E8C,
	UnsupportedBanner_Show_mDB70F8A3986418BFE8F8C9085E23A1701871161C,
	UnsupportedBanner_Hide_m446314938DF6A0357E8D9F57ACDBB728725067F7,
	UnsupportedBanner_SetPosition_mF76D1BCE99527D849B20566BFB034C1CBA5B42FE,
	UnsupportedBanner__ctor_m2305236218B2386B02DA678ED001FF4FAE2D2787,
	UnsupportedPlatform_SetupPlatform_m22984B9713B00083B85A33D3CF4A5B165E76D2F7,
	UnsupportedPlatform_Initialize_mF9C00E63D068D1E5604BEC91620D1FEC74333413,
	UnsupportedPlatform_Load_mBD55F5399A17E003DCE5FC857412561513D7339D,
	UnsupportedPlatform_Show_mCE2A60CDF44A03F7B18E1BEC4DD2E6BF8D8A669C,
	UnsupportedPlatform_SetMetaData_m80A5421CC456CCBCD6F05B243D9DD95B8A749D01,
	UnsupportedPlatform_GetDebugMode_m3908FE5E695799E32262440044A17232E6DB6E0B,
	UnsupportedPlatform_SetDebugMode_m383A19A4EE64823129E7389A6F9C1B5E7F91EB48,
	UnsupportedPlatform_GetVersion_mE54D4A6DF5D19DF303A2BA5F8B97DB2B80C7C40C,
	UnsupportedPlatform_IsInitialized_mFB76F9E1BAFCE627B07E6667873CF1F4E696D13E,
	UnsupportedPlatform_IsReady_mEBA734BC05174630AF0270A448A76267FA347794,
	UnsupportedPlatform_GetPlacementState_m4F06A841BD83A043D03A6068270457D3B145ABFF,
	UnsupportedPlatform__ctor_m4DDFDBCFE5CCC6D97BD1E9101E88807FAD3390A6,
	BannerPlaceholder_Awake_m103F5F462A094619FBE7F10B8B8EFDBC2BE1C83B,
	BannerPlaceholder_OnGUI_m544F919AB3F2EE7835A609AC83742C6B7861E173,
	BannerPlaceholder_ShowBanner_m69226A7A2E420A019BE8FEA1C2F03B0393DA2C69,
	BannerPlaceholder_HideBanner_mB1AC5F23AA8332647D0EFB3461FFA6E940CD19A4,
	BannerPlaceholder_BackgroundTexture_m7FA16D1C2F769BC1BEB753C135362B08A9EC5606,
	BannerPlaceholder_GetBannerRect_m92EF5CC1CCE2E5A5781CE17F1C1C4B91D9EE021A,
	BannerPlaceholder__ctor_m7AFD4CCC06EF529C0467A5AD511D233B15726E90,
	AndroidBanner_get_IsLoaded_m1090D7F14C00A444B7F905BEB0ECFDAC9E3D2721,
	AndroidBanner__ctor_mEFC2C531C4E5A77327120C2509408E0240FC111B,
	AndroidBanner_SetupBanner_m7A722CA69652B7A2F0908FB01E9B04DF2F033B9F,
	AndroidBanner_Load_m3BA946237BC3E25BB91992BABF4FA06BD26FA06C,
	AndroidBanner_Show_mE7BBBD0DECF98645A727D7C42ACE31B931A23CE7,
	AndroidBanner_Hide_m63A3B7CC005F053B60858B768E7C5E60E8259BD9,
	AndroidBanner_SetPosition_m69DFAAF571A7DFD9261876EAA116C2DF06932586,
	AndroidBanner_onUnityBannerShow_mE685C1F80E303BB80CD72395CB4912F2F416106C,
	AndroidBanner_onUnityBannerHide_mB098E9359B8366A35E3B6C30A4389F43080AE016,
	AndroidBanner_onUnityBannerLoaded_m096EAEC3FE07949B5D8D1A0BAA87F95F3C6D1A4C,
	AndroidBanner_onUnityBannerUnloaded_m3B4D9FCD2C1FBDD3AB411616797093B2EE182DBE,
	AndroidBanner_onUnityBannerClick_m37EAA4F3784D85FA358406F15F9BFA14515158E0,
	AndroidBanner_onUnityBannerError_m828266783A11EC1C42E92557DC60FD51F79817BF,
	AndroidBanner_U3CHideU3Eb__13_0_m757B84BBA3EAC22EA037984375E4D74BB8C0C392,
	AndroidBanner_U3CHideU3Eb__13_1_mCCC885F5E3422A1B18721462501A1CE3D84F9A57,
	AndroidBanner_U3ConUnityBannerLoadedU3Eb__17_1_m6826646D632CC76023CE29D6A2743D0B828712CD,
	AndroidBanner_U3ConUnityBannerLoadedU3Eb__17_0_m2472E032DFC18A109FC71320B1DD1209B4B855D5,
	AndroidBanner_U3ConUnityBannerClickU3Eb__19_0_mDC301E5E41214CCFCDA947F919C62281570C0CBA,
	AndroidPlatform__ctor_mC0F30F0051F74F6EB82B75AEE221C4E8E276AFBB,
	AndroidPlatform_SetupPlatform_m3E37CE5B5FB6F75D735D1C6B81D9C471A8DD8789,
	AndroidPlatform_Initialize_m3CEA4813A7341719103F9427211663C6FC9C8FB5,
	AndroidPlatform_Load_m08BC441FD44F432B834FAD50EFB18DAFC8CBCCD7,
	AndroidPlatform_Show_mAE3198D5C993293848A01F5AFCF1C2DA7AA8B3BD,
	AndroidPlatform_SetMetaData_mE6528096B96E18E6F4088C2297440FE4D8241A95,
	AndroidPlatform_GetDebugMode_m12C47DA56D6107866567D04955A6C066B5441421,
	AndroidPlatform_SetDebugMode_mEA48D6DD4DF54DF91843D434F9E3D2D180798323,
	AndroidPlatform_GetVersion_mC4FBF42109EF450D2F04A5439DE715AB1E559977,
	AndroidPlatform_IsInitialized_mFBC9096CB7EAEE5739F85695481B5C0FDF8037C2,
	AndroidPlatform_IsReady_m325868E1025DC4E7C6B94D50581645C668F5F7FF,
	AndroidPlatform_RemoveListener_mF4FEFBC4D00DE55040239A2DDA95E4B8DA7C6BB3,
	AndroidPlatform_GetPlacementState_m6EF901639291B3CC96B201CE2995868540393BC3,
	AndroidPlatform_GetCurrentAndroidActivity_mA35240AE0C6D4CD2D53F97184FB5E7673E9FD6EC,
	AndroidPlatform_UnityEngine_Advertisements_Purchasing_IPurchasingEventSender_SendPurchasingEvent_m8FC35C2E29174CF548316F3E83AA75F6BF74B3B1,
	BannerBundle_get_bannerView_m494FB7D5596B778F81D08E411FBAAEF1D44B35F5,
	BannerBundle_get_bannerPlacementId_m4E41B08F882B7E40EDA642DB23CE704A2604DE2D,
	BannerBundle__ctor_m1619218BCCA4FCD13708A3236FFA341712D871B8,
	FinishEventArgs_get_placementId_mC6F1BC10E5AFAD091FFC195A6D54EF279406C1C7,
	FinishEventArgs_get_showResult_m26C0D63F84FC858B04B3A788E633FA1F35AB93F7,
	FinishEventArgs__ctor_m22E9FC5FC756FA1F6893629C16C7B7B5C77F5219,
	StartEventArgs_get_placementId_m9469A02B1AF7DCFC3BF0FDC63C867163827F4519,
	StartEventArgs__ctor_mCB7FE6F57A53D5BBEB50ED07DBB158A8E112868C,
	Banner_Load_m06A48D7352F2E23A7AD41CF41409BB0946DD9A00,
	Banner_Load_mB519531E433DB53EA86E74A2757DFF857B0E5CB6,
	Banner_Load_mC922C4D831834C3649203DA96E78E5971CF40210,
	Banner_Load_m000224EDA0BF2FF61EDE66E167148ACB8C294579,
	Banner_Show_mF34A748E029451235C4EB3C2EC0B1E8E128AEE96,
	Banner_Show_m40A12D094C9EAA6AF725784589EA3228F0EF2520,
	Banner_Show_m3C42E55C8F73C12F90BF9AB9D5A293CA7368906E,
	Banner_Show_m73BE208CABEE908A5F4DAC936686A31A715EBCDB,
	Banner_Hide_mA828D623AAD961B197A2338FD1DF21D4D5D0C067,
	Banner_SetPosition_mB03D6FE7CBC2DA088649E8EC1DD89D352E201043,
	Banner_get_isLoaded_m35305E62DC54136201C80DD82F6614D579F46875,
	U3CU3Ec__DisplayClass32_0__ctor_mFB01650D49D562E550A97CEF5AE47AD75A9B7DD7,
	U3CU3Ec__DisplayClass32_0_U3CCreatePlatformU3Eb__0_mD0920F22572DB42E8D4918971E2DCA0BEA61C605,
	U3CU3Ec__DisplayClass15_0__ctor_m67A9A93608F4F936AA6F42CE50957BC4E2A9B7C4,
	U3CU3Ec__DisplayClass15_0_U3CUnityAdsBannerDidShowU3Eb__0_m290D12AF0E2C5E24291DF62E5A32F4EDFFF55D59,
	U3CU3Ec__DisplayClass16_0__ctor_m598B85B717722C83DA7A15A08816325E22B8D0AB,
	U3CU3Ec__DisplayClass16_0_U3CUnityAdsBannerDidHideU3Eb__0_mD693250A23E026FC2968DB1093357A9EF75887F2,
	U3CU3Ec__DisplayClass17_0__ctor_mD441B8465D66F3AB740A34B65BBC82DAE3CA0289,
	U3CU3Ec__DisplayClass17_0_U3CUnityAdsBannerClickU3Eb__0_m71D54A12369066DDE53612B11C16A4DD5CBA633A,
	U3CU3Ec__DisplayClass18_0__ctor_mCF0DFDB73C55CE62D99AB2F0C50D512B21A4DA73,
	U3CU3Ec__DisplayClass18_0_U3CUnityAdsBannerDidLoadU3Eb__0_m757FA977A4DB6BC2C878A072214A0261E64114DD,
	U3CU3Ec__DisplayClass19_0__ctor_m2E5BCC22AE13F767CC10CEEF8A6C9378EFD141B6,
	U3CU3Ec__DisplayClass19_0_U3CUnityAdsBannerDidErrorU3Eb__0_m858AD48AEDD9C616E3385F73D8072347190CDA44,
	LoadCallback__ctor_m149C4CA2674F63DC85F26C60CF4BE852C5974D60,
	LoadCallback_Invoke_m5CCDF2F93BF4603344CE9A164AC93709559F99D9,
	LoadCallback_BeginInvoke_m26CBBDD499B77472B762CCEBEAD5A80F539B3DAC,
	LoadCallback_EndInvoke_m82BE135E79EB3C0026A2B37FE3DD0ADF94A75845,
	ErrorCallback__ctor_m8C594084F860F0AC9D416B339E78D69649DFA7F1,
	ErrorCallback_Invoke_mC151F8EE2510FBE8A39EEBAED2A4737AC53155F7,
	ErrorCallback_BeginInvoke_mAA70F10967DC875DF3B654EE625BB7FB9F01D6B4,
	ErrorCallback_EndInvoke_m5FF48332DD1390B74C606AD6867C78AE54BE6E81,
	BannerCallback__ctor_m0944126600B12308BCFD070EAF6CC81009B5C83F,
	BannerCallback_Invoke_mC67383B48C2D4BBC88D1D3D283D8F7117F6AC984,
	BannerCallback_BeginInvoke_m56ED5ED5DDAF529F6969CEF1C43D423829F38D96,
	BannerCallback_EndInvoke_m501FDA627ADE21E44683064A94CCD8C5193A4EA5,
	Parser_IsWordBreak_mEA531542CF6B0595B87B30359F4810D8C4AF3CCE,
	Parser__ctor_m1A2111A83BDF4FAE0D99D96B9A6B73BD62308F13,
	Parser_Parse_mC705E4F81C5A57EA58D18EEDAE2BDFF9F3986DCC,
	Parser_Dispose_m7820AD20ADF545D1312B2913EE76CB629AB36E50,
	Parser_ParseObject_m58B999F87989D85F18FB78A5C9BACC52396258B6,
	Parser_ParseArray_m87775DD7390CB08B60A5A7A5DD005BFFED58A22A,
	Parser_ParseValue_m4D47F7AEDA32907EAE7299EC747DA6DEA0C43B42,
	Parser_ParseByToken_mDA0A4BB8C64060809DA9C419726C315F102F9C4C,
	Parser_ParseString_m7CF0134829A15287809F252B5E7D708C4F737430,
	Parser_ParseNumber_mDE40D9D8524DD6AEF3D4DB3B9039C05B68656056,
	Parser_EatWhitespace_mDD81C0893F358C5F2890CDCB99FFEB7D3612D0FB,
	Parser_get_PeekChar_m94BF1DEC531D3A6F09FE92A154B9C02148E79068,
	Parser_get_NextChar_m32AB048CC54E50139E92FD17D561580A6AC9ACB1,
	Parser_get_NextWord_mE66A01EBC62B417384B25B796FDB56CB7A022429,
	Parser_get_NextToken_mF80ADF81AB1B385ACE09B4E216C3005E8F004E1A,
	Serializer__ctor_m3203CD70FEF4DFC38424D9E29109A98604F6A8CA,
	Serializer_Serialize_mB274C6B492226DB15C8552D3ABFF2F26F7C19BAD,
	Serializer_SerializeValue_m05069E2615454E585C7D6E94CE5AD40D7883BC20,
	Serializer_SerializeObject_m244A448784F9792E207C3F5ABBD690F1DD367D97,
	Serializer_SerializeArray_mEF16068F947E3CA46FDDE359E1871A15A6FEEA2C,
	Serializer_SerializeString_m53F7333FF3673BC1EC02FB3EFEC439A11240175B,
	Serializer_SerializeOther_m69C647BED1C71CFA73CC89B52D8B4600E5D6330D,
	U3CU3Ec__DisplayClass33_0__ctor_m455D237D767665AE14BE4B82352A2308A32B4BB8,
	U3CU3Ec__DisplayClass33_0_U3CShowU3Eb__0_m050B2616328D1A64A058F83A83591F346CAA9E84,
	U3CU3Ec__DisplayClass39_0__ctor_mAF019D1DE85234AE7450F5B06ADEB3B61A8015CA,
	U3CU3Ec__DisplayClass39_0_U3CUnityAdsReadyU3Eb__0_mE10152E14DE66C4BA4C9CCF1000BA687AA8BFB5B,
	U3CU3Ec__DisplayClass40_0__ctor_m207B4303A22E409C0AC160FECDC048D8372FB705,
	U3CU3Ec__DisplayClass40_0_U3CUnityAdsDidErrorU3Eb__0_m9FD88CE43B7C31FF2EEC4DB9D6A5E8C8FF66F5F1,
	U3CU3Ec__DisplayClass41_0__ctor_m65CC8D05E090548EDAB02E3D300092A26FAEC982,
	U3CU3Ec__DisplayClass41_0_U3CUnityAdsDidStartU3Eb__0_m4540296447426B4EB390F86A71FB982C7B94E638,
	U3CU3Ec__DisplayClass42_0__ctor_mB375F170A2D2D542DAFCCB20440D1F7CF0DE67EA,
	U3CU3Ec__DisplayClass42_0_U3CUnityAdsDidFinishU3Eb__0_mB61C98F322502254470E0D4CDB36E6E02652A1C9,
	U3CU3Ec__DisplayClass11_0__ctor_m762DE44D1512CB4805DC8A29EF9C6528796CF5CD,
	U3CU3Ec__DisplayClass11_0_U3CLoadU3Eb__0_m9920C93CCA34B774014E199AAE65CC7C4B055757,
	U3CU3Ec__DisplayClass12_0__ctor_m58EE4C928EF5FC73C913609507FDEA5AF3341F81,
	U3CU3Ec__DisplayClass12_0_U3CShowU3Eb__0_m271FCA57067860092B7BC2D4CE9475AF0F95C6C8,
	U3CU3Ec__DisplayClass12_0_U3CShowU3Eb__1_m5DDA628C1D9F00A543BAE39AFD48383B3BED59BD,
	U3CU3Ec__DisplayClass20_0__ctor_m51046C9EFFA4F39A1EA8BF75C71D92FB24CD0845,
	U3CU3Ec__DisplayClass20_0_U3ConUnityBannerErrorU3Eb__0_mBBCFAF34F5D9CC8ACC5C149156925A3FC6A05E06,
};
static const int32_t s_InvokerIndices[359] = 
{
	1988,
	1982,
	1982,
	1982,
	1956,
	1976,
	1982,
	1954,
	1807,
	1609,
	1443,
	1982,
	1919,
	1954,
	1805,
	1988,
	1954,
	1954,
	1805,
	1805,
	1606,
	1954,
	1954,
	1954,
	1971,
	1863,
	1976,
	1982,
	1126,
	1145,
	1145,
	1007,
	648,
	648,
	648,
	1007,
	982,
	648,
	648,
	648,
	648,
	648,
	1126,
	991,
	1126,
	991,
	1157,
	1126,
	991,
	1126,
	991,
	1126,
	991,
	1157,
	1145,
	1126,
	1126,
	991,
	1126,
	1145,
	1145,
	1007,
	648,
	648,
	1007,
	982,
	648,
	648,
	648,
	648,
	648,
	1145,
	991,
	648,
	648,
	1007,
	982,
	991,
	991,
	991,
	645,
	1157,
	606,
	991,
	410,
	410,
	991,
	991,
	645,
	1126,
	991,
	991,
	648,
	798,
	1126,
	1126,
	648,
	1157,
	648,
	648,
	991,
	417,
	648,
	417,
	991,
	991,
	648,
	991,
	276,
	648,
	648,
	991,
	1145,
	1007,
	1126,
	1145,
	878,
	751,
	1126,
	991,
	1126,
	991,
	1157,
	991,
	991,
	1157,
	1157,
	1157,
	1157,
	1859,
	-1,
	798,
	991,
	991,
	1898,
	1898,
	1157,
	1157,
	798,
	991,
	1157,
	991,
	991,
	991,
	991,
	1157,
	1157,
	1157,
	991,
	991,
	1157,
	1157,
	1157,
	991,
	991,
	1157,
	1919,
	1919,
	1976,
	1976,
	1919,
	1988,
	991,
	991,
	991,
	991,
	1126,
	1126,
	1126,
	1145,
	1145,
	1126,
	1145,
	1007,
	1126,
	276,
	648,
	417,
	991,
	991,
	878,
	751,
	991,
	991,
	991,
	991,
	645,
	991,
	991,
	991,
	991,
	1126,
	1126,
	1126,
	1145,
	1145,
	1007,
	1126,
	1145,
	1007,
	1126,
	417,
	276,
	991,
	648,
	417,
	991,
	991,
	878,
	751,
	991,
	991,
	991,
	991,
	645,
	1898,
	648,
	648,
	1145,
	991,
	648,
	648,
	1007,
	982,
	1157,
	991,
	276,
	648,
	648,
	991,
	1145,
	1007,
	1126,
	1145,
	878,
	751,
	1157,
	1157,
	1157,
	606,
	1157,
	1501,
	1909,
	1157,
	1145,
	1157,
	991,
	648,
	648,
	1007,
	982,
	991,
	991,
	648,
	991,
	991,
	991,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	991,
	276,
	648,
	648,
	991,
	1145,
	1007,
	1126,
	1145,
	878,
	1157,
	751,
	1976,
	991,
	1126,
	1126,
	648,
	1126,
	1115,
	645,
	1126,
	991,
	1988,
	1954,
	1954,
	1805,
	1988,
	1954,
	1954,
	1805,
	1956,
	1952,
	1982,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	647,
	1157,
	494,
	991,
	647,
	991,
	329,
	991,
	647,
	1157,
	494,
	991,
	1915,
	991,
	1898,
	1157,
	1126,
	1126,
	1126,
	795,
	1126,
	1126,
	1157,
	1114,
	1114,
	1126,
	1115,
	1157,
	1898,
	991,
	991,
	991,
	991,
	991,
	1157,
	648,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
};
static const Il2CppTokenRangePair s_rgctxIndices[1] = 
{
	{ 0x06000083, { 0, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[2] = 
{
	{ (Il2CppRGCTXDataType)1, 87 },
	{ (Il2CppRGCTXDataType)2, 87 },
};
extern const CustomAttributesCacheGenerator g_UnityEngine_Advertisements_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_UnityEngine_Advertisements_CodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_Advertisements_CodeGenModule = 
{
	"UnityEngine.Advertisements.dll",
	359,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	1,
	s_rgctxIndices,
	2,
	s_rgctxValues,
	NULL,
	g_UnityEngine_Advertisements_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
