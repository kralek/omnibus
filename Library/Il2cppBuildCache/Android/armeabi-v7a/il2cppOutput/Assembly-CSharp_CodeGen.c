﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void AdsBanner::Start()
extern void AdsBanner_Start_m502AF0C94AF9C7230FDD68144603C7FF06421843 (void);
// 0x00000002 System.Collections.IEnumerator AdsBanner::ShowBannerWhenInitialized()
extern void AdsBanner_ShowBannerWhenInitialized_mDE78FF5506E38562FFEF9B19F241C8DEF33A03AF (void);
// 0x00000003 System.Void AdsBanner::.ctor()
extern void AdsBanner__ctor_m9D31B9C76AABA5141CE89CEB1D1BDD1081D85716 (void);
// 0x00000004 System.Void DisplayPoints::Awake()
extern void DisplayPoints_Awake_mF7D76B9012B2867C3010C0DD04EEEA19A23E0FB6 (void);
// 0x00000005 System.Void DisplayPoints::.ctor()
extern void DisplayPoints__ctor_mFC4D6E5BBA97990603BBABF884C5DEE78BA8DBEA (void);
// 0x00000006 System.Void EnterPIN::Awake()
extern void EnterPIN_Awake_mC760E9A64B19736731A1000D8B9658A5BED5CFFD (void);
// 0x00000007 System.Void EnterPIN::CheckPINOn()
extern void EnterPIN_CheckPINOn_m60B9688B15BB696B8392F919F17433876D4F3CB4 (void);
// 0x00000008 System.Void EnterPIN::.ctor()
extern void EnterPIN__ctor_mD170EF96023CA0F52FD50240FF3BD621227F3BFD (void);
// 0x00000009 System.Void GM::Awake()
extern void GM_Awake_m58CF3D467404FD735D23405BED07D28919FFD4A8 (void);
// 0x0000000A System.Void GM::ChangePointsOneAward()
extern void GM_ChangePointsOneAward_m3215E6D6B990385199DF89BD488B22F4F11CCE1A (void);
// 0x0000000B System.Void GM::ChangePointsTwoAward()
extern void GM_ChangePointsTwoAward_m5AEE14D3E762C6EE9BD2211432A4C1CEF83DD606 (void);
// 0x0000000C System.Void GM::ChangePointsThreeAward()
extern void GM_ChangePointsThreeAward_m6796CE768ED25A769EC0193975C13BB80CA21682 (void);
// 0x0000000D System.Void GM::MinusPointsOneAward(System.Int32)
extern void GM_MinusPointsOneAward_m49EE269CBFE173553961DC4B1C10C71F1C76FE8E (void);
// 0x0000000E System.Void GM::MinusPointsTwoAward(System.Int32)
extern void GM_MinusPointsTwoAward_mE225AE18889C36C657B21FA781C3910CABF7D646 (void);
// 0x0000000F System.Void GM::MinusPointsThreeAward(System.Int32)
extern void GM_MinusPointsThreeAward_m9B88BC9C158273893CF784A7C0146A8526DF72D7 (void);
// 0x00000010 System.Void GM::DisplayScore(System.Int32)
extern void GM_DisplayScore_m9D1B41034079EE6F05346F3DFA4F69D031516C3D (void);
// 0x00000011 System.Void GM::.ctor()
extern void GM__ctor_mEB1FBC9DA28AE63BEA0C4EDE9261034A5ED4932F (void);
// 0x00000012 System.Void MainMenu::Start()
extern void MainMenu_Start_m3B552CE289B1D7E5343961C8461C484EA61DB621 (void);
// 0x00000013 System.Void MainMenu::Update()
extern void MainMenu_Update_m2DE1F2570AFBF09401821F2F89779639CD0BBC76 (void);
// 0x00000014 System.Void MainMenu::.ctor()
extern void MainMenu__ctor_m4D77CEC8F91682A2D9492AE815F89B178BF9717D (void);
// 0x00000015 System.Void MoveScenes::ResetPlayer()
extern void MoveScenes_ResetPlayer_m4DD51BA8E2F6512E076ADF834248E979BBECDD59 (void);
// 0x00000016 System.Void MoveScenes::GoToGame()
extern void MoveScenes_GoToGame_m9E78314FA9457A016EBE6BE4B3BF4C7BA389FBFA (void);
// 0x00000017 System.Void MoveScenes::GoToMenu()
extern void MoveScenes_GoToMenu_m16E09AE612DDF55500163AC9EB55C3B533C207CA (void);
// 0x00000018 System.Void MoveScenes::GoToLeng()
extern void MoveScenes_GoToLeng_m155008EC70257E0A33D13FDC54DDBE3335783176 (void);
// 0x00000019 System.Void MoveScenes::GoToSettings()
extern void MoveScenes_GoToSettings_m0997DD73155D917CD357EA3601D2124ACE72D7DD (void);
// 0x0000001A System.Void MoveScenes::GoToAwardsNew()
extern void MoveScenes_GoToAwardsNew_mDAABC2F77881CE5FBC7608F853413D621771357E (void);
// 0x0000001B System.Void MoveScenes::GoToAwards()
extern void MoveScenes_GoToAwards_m8B4511572CEFF307714385C813C4CCFB67E6E7CB (void);
// 0x0000001C System.Void MoveScenes::Exit()
extern void MoveScenes_Exit_m25FCCD9471FB39841AE8BFA5794841886A1A5677 (void);
// 0x0000001D System.Void MoveScenes::ChangePIN()
extern void MoveScenes_ChangePIN_m81FA7929492E38C20ABBE3CFF6F1A20A9BD85FE4 (void);
// 0x0000001E System.Void MoveScenes::.ctor()
extern void MoveScenes__ctor_m7FF737B394D13C1AF24F3DA8145F6C987E554168 (void);
// 0x0000001F System.Void MultiTable::Awake()
extern void MultiTable_Awake_mD4ACC8EEC340DEED112FD459D2219102221A8F47 (void);
// 0x00000020 System.Void MultiTable::Update()
extern void MultiTable_Update_m321C153556B65CD94A61E9C6DD70091F0CD04E3D (void);
// 0x00000021 System.Void MultiTable::Timer()
extern void MultiTable_Timer_mE67424E0638FE200B3E4917ACC7716A4DA7FAA21 (void);
// 0x00000022 System.Void MultiTable::RandomMathOper()
extern void MultiTable_RandomMathOper_m648924AB98EC575E4E02B2E91B3DB4DC70404E20 (void);
// 0x00000023 System.Void MultiTable::OnVibration()
extern void MultiTable_OnVibration_m33FE08D7C4D2249464CB79DE79E9B630A64EB4B2 (void);
// 0x00000024 System.Void MultiTable::MuteSounds()
extern void MultiTable_MuteSounds_mBBC5CEED95E798A9A3E0A0BA22B854FE232B50BC (void);
// 0x00000025 System.Void MultiTable::OnSounds()
extern void MultiTable_OnSounds_m51705407D34611622E9AE2FDAD02AC1B4EA4EA23 (void);
// 0x00000026 System.Void MultiTable::PlaySounds(System.Int32)
extern void MultiTable_PlaySounds_m5D0B5AB0E188B53FC4714F7BE49A960C6A5E5659 (void);
// 0x00000027 System.Void MultiTable::RemonteList()
extern void MultiTable_RemonteList_mCE61DCD2CD42ABEF80CCB1653A5160E4CE98842C (void);
// 0x00000028 System.Void MultiTable::AddPoints()
extern void MultiTable_AddPoints_mCF0996F6154238A8000EEA211B659076B565B233 (void);
// 0x00000029 System.Void MultiTable::MinusPoints()
extern void MultiTable_MinusPoints_m1CAB936C4BB41304C0F84FFF740BDB8C90F67574 (void);
// 0x0000002A System.Void MultiTable::StopGame()
extern void MultiTable_StopGame_mDC42C69626996D613140DF940005822E9395CCCB (void);
// 0x0000002B System.Void MultiTable::StartGame()
extern void MultiTable_StartGame_m530950033857B8D5459D26A44F6B06DD60BF137F (void);
// 0x0000002C System.Void MultiTable::ResetTimer()
extern void MultiTable_ResetTimer_m3EDBEC0DABDB2C44DB50E1BA0494C599CD43882B (void);
// 0x0000002D System.Void MultiTable::CheckBtnZero()
extern void MultiTable_CheckBtnZero_m87E1A39FF161E5691B43DD3411E15CBA45D997DD (void);
// 0x0000002E System.Void MultiTable::CheckBtnOne()
extern void MultiTable_CheckBtnOne_m21B5A5564BCB5815C171453F21C436AFBFCB7644 (void);
// 0x0000002F System.Void MultiTable::CheckBtnTwo()
extern void MultiTable_CheckBtnTwo_m27A5A4575EE3408DEFAA05A848B081B4762F7ADC (void);
// 0x00000030 System.Void MultiTable::CheckBtnThree()
extern void MultiTable_CheckBtnThree_mE92B07328B3CB4241BAC7EFAE9BBA7AB0F869442 (void);
// 0x00000031 System.Void MultiTable::.ctor()
extern void MultiTable__ctor_mC74F963BA5AD318F968DC7C7FDED3CC73963E724 (void);
// 0x00000032 System.Void OperateOneAward::Awake()
extern void OperateOneAward_Awake_m0743FF891291370783D4078645D1F71BB76FFFA7 (void);
// 0x00000033 System.Void OperateOneAward::GoToTwoSceneAwards()
extern void OperateOneAward_GoToTwoSceneAwards_mA847ADAE8A1141CE1A8C9189089E9B8EB39246FD (void);
// 0x00000034 System.Void OperateOneAward::.ctor()
extern void OperateOneAward__ctor_m369F55CDB2FD39015CB82205FD13D2439A433D7D (void);
// 0x00000035 System.Void OperateThreeAward::Awake()
extern void OperateThreeAward_Awake_mD63E1DC17E3ECB9E7EAAF23A07925FFE090AB41B (void);
// 0x00000036 System.Void OperateThreeAward::GoToSceneAwards()
extern void OperateThreeAward_GoToSceneAwards_m4165BD3BD54B69C55386F4A98B5A5DF3C4A58209 (void);
// 0x00000037 System.Void OperateThreeAward::.ctor()
extern void OperateThreeAward__ctor_mEFF96D2D36F84D3E1E71725167F1235035D575DF (void);
// 0x00000038 System.Void OperateTwoAward::Awake()
extern void OperateTwoAward_Awake_m477F7E004001CC13273D6E49CF8CC2A10852F606 (void);
// 0x00000039 System.Void OperateTwoAward::GoToThreeSceneAwards()
extern void OperateTwoAward_GoToThreeSceneAwards_m9A31B66CF6B81ED057BE9FF44D6CD33AE36610E3 (void);
// 0x0000003A System.Void OperateTwoAward::.ctor()
extern void OperateTwoAward__ctor_m857F2C283020A1B6BD5D26A572D2743F683D7E73 (void);
// 0x0000003B System.Void AwardsMenuLang::Awake()
extern void AwardsMenuLang_Awake_m09CB9F36A702E34B62BD59FC6FCD66C362F8EF49 (void);
// 0x0000003C System.Void AwardsMenuLang::setLang(System.String[],System.Int32)
extern void AwardsMenuLang_setLang_mD1FBBD124E2B37C1506481BFE485E3C7C0813D07 (void);
// 0x0000003D System.Void AwardsMenuLang::.ctor()
extern void AwardsMenuLang__ctor_mC09878DD4D109FBFA839BECBF168D2533787441A (void);
// 0x0000003E System.Void EnterPINLang::Awake()
extern void EnterPINLang_Awake_m6389AE7D5503D098139FC906407E0E10E87CE3EE (void);
// 0x0000003F System.Void EnterPINLang::setLang(System.String[],System.Int32)
extern void EnterPINLang_setLang_m20C95F886AFB7F533F7B37F09F749D4F0D3BA165 (void);
// 0x00000040 System.Void EnterPINLang::.ctor()
extern void EnterPINLang__ctor_mD5FA4442D1A7B53C968E7A3E161F5CE0F8992B51 (void);
// 0x00000041 System.Void GameLang::Awake()
extern void GameLang_Awake_m88182EB0C0AE4A78DB88095F9D2B16A2237CEC5E (void);
// 0x00000042 System.Void GameLang::setLang(System.String[],System.Int32)
extern void GameLang_setLang_mDB511190EC4318D87062125C2656C1EE9B83F8E9 (void);
// 0x00000043 System.Void GameLang::.ctor()
extern void GameLang__ctor_m144185E266545F58DB685C555BDC78E98E4C4AB3 (void);
// 0x00000044 System.Void GameMenuLang::Awake()
extern void GameMenuLang_Awake_mE914B67055DF65ECD53A98153DDCFDA0C03640E9 (void);
// 0x00000045 System.Void GameMenuLang::setLang(System.String[],System.Int32)
extern void GameMenuLang_setLang_mA20C12C7B1D825EFD990246302FD9401A98217B9 (void);
// 0x00000046 System.Void GameMenuLang::.ctor()
extern void GameMenuLang__ctor_mCE79D49DE21611DF2C790014AC58D13EC1BC06B1 (void);
// 0x00000047 System.Void OneAwardLang::Awake()
extern void OneAwardLang_Awake_mCFE94B300C3AD48B4287E6FEBE5A86F861545762 (void);
// 0x00000048 System.Void OneAwardLang::setLang(System.String[],System.Int32)
extern void OneAwardLang_setLang_m71BF319A0CFF2967CBFB7ECC05F7CF70B82182F4 (void);
// 0x00000049 System.Void OneAwardLang::.ctor()
extern void OneAwardLang__ctor_m7109BA9634E6974ADB66C1EBBF2FDE22589897C2 (void);
// 0x0000004A System.Void SettingsLang::Awake()
extern void SettingsLang_Awake_m4651456ED167577745ECA89FB1F3F4FF06CC7C68 (void);
// 0x0000004B System.Void SettingsLang::setLang(System.String[],System.Int32)
extern void SettingsLang_setLang_mF661C7627049B8FCB2F4535F048F85C94279B563 (void);
// 0x0000004C System.Void SettingsLang::.ctor()
extern void SettingsLang__ctor_m02AF41BCD4C92388A17F0117F6E42F6813513C4A (void);
// 0x0000004D System.Void SettingsPINLang::Awake()
extern void SettingsPINLang_Awake_m382EFB07AAC18FE069FC4C64B0572986FD6CC6DD (void);
// 0x0000004E System.Void SettingsPINLang::setLang(System.String[],System.Int32)
extern void SettingsPINLang_setLang_m2B1B554C9BAFFD7DED5FCBBC253966C47935A989 (void);
// 0x0000004F System.Void SettingsPINLang::.ctor()
extern void SettingsPINLang__ctor_m76902D98A8B491489AA91ED7D0F4D3042A39A4CD (void);
// 0x00000050 System.Void ThreeAwardLang::Awake()
extern void ThreeAwardLang_Awake_mFE9E4E41EF1A0F665F145E4E610E64AE18E16FD5 (void);
// 0x00000051 System.Void ThreeAwardLang::setLang(System.String[],System.Int32)
extern void ThreeAwardLang_setLang_mB6B0D1EA2E97E5BF6AF62349692D278055A9322A (void);
// 0x00000052 System.Void ThreeAwardLang::.ctor()
extern void ThreeAwardLang__ctor_m5EC5F4A4055221D27A5D67F3214A701918D42566 (void);
// 0x00000053 System.Void TwoAwardLang::Awake()
extern void TwoAwardLang_Awake_mDCE3F5388A42670FC8C22230DD373326F14E3CF0 (void);
// 0x00000054 System.Void TwoAwardLang::setLang(System.String[],System.Int32)
extern void TwoAwardLang_setLang_mF511A6231AF485C0208F0D6B801569C4B0C20C6D (void);
// 0x00000055 System.Void TwoAwardLang::.ctor()
extern void TwoAwardLang__ctor_mCF71BEEEE0C2FEA7CD21261F0DD5E30BCE849C78 (void);
// 0x00000056 System.Void Settings::Awake()
extern void Settings_Awake_mBA445DDD366FCA51F1223575885A10ABDF812A21 (void);
// 0x00000057 System.Void Settings::CheckPIN()
extern void Settings_CheckPIN_m9240031C817801CBB76A55EF476BABEED6960A4B (void);
// 0x00000058 System.Void Settings::.ctor()
extern void Settings__ctor_m2E00E4F8E213532A3051E24E75CE65368C0BB661 (void);
// 0x00000059 System.Void AdsBanner/<ShowBannerWhenInitialized>d__4::.ctor(System.Int32)
extern void U3CShowBannerWhenInitializedU3Ed__4__ctor_m8DCA04DCF4BE1E4A624C3E15F9D1D9A9A68E35BE (void);
// 0x0000005A System.Void AdsBanner/<ShowBannerWhenInitialized>d__4::System.IDisposable.Dispose()
extern void U3CShowBannerWhenInitializedU3Ed__4_System_IDisposable_Dispose_mD55CAADBA6A2A4D90D2D9ED73ED3204E52ED6443 (void);
// 0x0000005B System.Boolean AdsBanner/<ShowBannerWhenInitialized>d__4::MoveNext()
extern void U3CShowBannerWhenInitializedU3Ed__4_MoveNext_m9E5EBEBE43E8B1ABE5D9C19983995D650E154AEA (void);
// 0x0000005C System.Object AdsBanner/<ShowBannerWhenInitialized>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShowBannerWhenInitializedU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m85649261B8CBA0ADFC4624AF146BD1F735BDD6F3 (void);
// 0x0000005D System.Void AdsBanner/<ShowBannerWhenInitialized>d__4::System.Collections.IEnumerator.Reset()
extern void U3CShowBannerWhenInitializedU3Ed__4_System_Collections_IEnumerator_Reset_mDDAA23D5AE0F0E33040D431742D59BEE06E7CB2B (void);
// 0x0000005E System.Object AdsBanner/<ShowBannerWhenInitialized>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CShowBannerWhenInitializedU3Ed__4_System_Collections_IEnumerator_get_Current_m9DD5569CEF9113FAA0420420EA3015085FEA420A (void);
static Il2CppMethodPointer s_methodPointers[94] = 
{
	AdsBanner_Start_m502AF0C94AF9C7230FDD68144603C7FF06421843,
	AdsBanner_ShowBannerWhenInitialized_mDE78FF5506E38562FFEF9B19F241C8DEF33A03AF,
	AdsBanner__ctor_m9D31B9C76AABA5141CE89CEB1D1BDD1081D85716,
	DisplayPoints_Awake_mF7D76B9012B2867C3010C0DD04EEEA19A23E0FB6,
	DisplayPoints__ctor_mFC4D6E5BBA97990603BBABF884C5DEE78BA8DBEA,
	EnterPIN_Awake_mC760E9A64B19736731A1000D8B9658A5BED5CFFD,
	EnterPIN_CheckPINOn_m60B9688B15BB696B8392F919F17433876D4F3CB4,
	EnterPIN__ctor_mD170EF96023CA0F52FD50240FF3BD621227F3BFD,
	GM_Awake_m58CF3D467404FD735D23405BED07D28919FFD4A8,
	GM_ChangePointsOneAward_m3215E6D6B990385199DF89BD488B22F4F11CCE1A,
	GM_ChangePointsTwoAward_m5AEE14D3E762C6EE9BD2211432A4C1CEF83DD606,
	GM_ChangePointsThreeAward_m6796CE768ED25A769EC0193975C13BB80CA21682,
	GM_MinusPointsOneAward_m49EE269CBFE173553961DC4B1C10C71F1C76FE8E,
	GM_MinusPointsTwoAward_mE225AE18889C36C657B21FA781C3910CABF7D646,
	GM_MinusPointsThreeAward_m9B88BC9C158273893CF784A7C0146A8526DF72D7,
	GM_DisplayScore_m9D1B41034079EE6F05346F3DFA4F69D031516C3D,
	GM__ctor_mEB1FBC9DA28AE63BEA0C4EDE9261034A5ED4932F,
	MainMenu_Start_m3B552CE289B1D7E5343961C8461C484EA61DB621,
	MainMenu_Update_m2DE1F2570AFBF09401821F2F89779639CD0BBC76,
	MainMenu__ctor_m4D77CEC8F91682A2D9492AE815F89B178BF9717D,
	MoveScenes_ResetPlayer_m4DD51BA8E2F6512E076ADF834248E979BBECDD59,
	MoveScenes_GoToGame_m9E78314FA9457A016EBE6BE4B3BF4C7BA389FBFA,
	MoveScenes_GoToMenu_m16E09AE612DDF55500163AC9EB55C3B533C207CA,
	MoveScenes_GoToLeng_m155008EC70257E0A33D13FDC54DDBE3335783176,
	MoveScenes_GoToSettings_m0997DD73155D917CD357EA3601D2124ACE72D7DD,
	MoveScenes_GoToAwardsNew_mDAABC2F77881CE5FBC7608F853413D621771357E,
	MoveScenes_GoToAwards_m8B4511572CEFF307714385C813C4CCFB67E6E7CB,
	MoveScenes_Exit_m25FCCD9471FB39841AE8BFA5794841886A1A5677,
	MoveScenes_ChangePIN_m81FA7929492E38C20ABBE3CFF6F1A20A9BD85FE4,
	MoveScenes__ctor_m7FF737B394D13C1AF24F3DA8145F6C987E554168,
	MultiTable_Awake_mD4ACC8EEC340DEED112FD459D2219102221A8F47,
	MultiTable_Update_m321C153556B65CD94A61E9C6DD70091F0CD04E3D,
	MultiTable_Timer_mE67424E0638FE200B3E4917ACC7716A4DA7FAA21,
	MultiTable_RandomMathOper_m648924AB98EC575E4E02B2E91B3DB4DC70404E20,
	MultiTable_OnVibration_m33FE08D7C4D2249464CB79DE79E9B630A64EB4B2,
	MultiTable_MuteSounds_mBBC5CEED95E798A9A3E0A0BA22B854FE232B50BC,
	MultiTable_OnSounds_m51705407D34611622E9AE2FDAD02AC1B4EA4EA23,
	MultiTable_PlaySounds_m5D0B5AB0E188B53FC4714F7BE49A960C6A5E5659,
	MultiTable_RemonteList_mCE61DCD2CD42ABEF80CCB1653A5160E4CE98842C,
	MultiTable_AddPoints_mCF0996F6154238A8000EEA211B659076B565B233,
	MultiTable_MinusPoints_m1CAB936C4BB41304C0F84FFF740BDB8C90F67574,
	MultiTable_StopGame_mDC42C69626996D613140DF940005822E9395CCCB,
	MultiTable_StartGame_m530950033857B8D5459D26A44F6B06DD60BF137F,
	MultiTable_ResetTimer_m3EDBEC0DABDB2C44DB50E1BA0494C599CD43882B,
	MultiTable_CheckBtnZero_m87E1A39FF161E5691B43DD3411E15CBA45D997DD,
	MultiTable_CheckBtnOne_m21B5A5564BCB5815C171453F21C436AFBFCB7644,
	MultiTable_CheckBtnTwo_m27A5A4575EE3408DEFAA05A848B081B4762F7ADC,
	MultiTable_CheckBtnThree_mE92B07328B3CB4241BAC7EFAE9BBA7AB0F869442,
	MultiTable__ctor_mC74F963BA5AD318F968DC7C7FDED3CC73963E724,
	OperateOneAward_Awake_m0743FF891291370783D4078645D1F71BB76FFFA7,
	OperateOneAward_GoToTwoSceneAwards_mA847ADAE8A1141CE1A8C9189089E9B8EB39246FD,
	OperateOneAward__ctor_m369F55CDB2FD39015CB82205FD13D2439A433D7D,
	OperateThreeAward_Awake_mD63E1DC17E3ECB9E7EAAF23A07925FFE090AB41B,
	OperateThreeAward_GoToSceneAwards_m4165BD3BD54B69C55386F4A98B5A5DF3C4A58209,
	OperateThreeAward__ctor_mEFF96D2D36F84D3E1E71725167F1235035D575DF,
	OperateTwoAward_Awake_m477F7E004001CC13273D6E49CF8CC2A10852F606,
	OperateTwoAward_GoToThreeSceneAwards_m9A31B66CF6B81ED057BE9FF44D6CD33AE36610E3,
	OperateTwoAward__ctor_m857F2C283020A1B6BD5D26A572D2743F683D7E73,
	AwardsMenuLang_Awake_m09CB9F36A702E34B62BD59FC6FCD66C362F8EF49,
	AwardsMenuLang_setLang_mD1FBBD124E2B37C1506481BFE485E3C7C0813D07,
	AwardsMenuLang__ctor_mC09878DD4D109FBFA839BECBF168D2533787441A,
	EnterPINLang_Awake_m6389AE7D5503D098139FC906407E0E10E87CE3EE,
	EnterPINLang_setLang_m20C95F886AFB7F533F7B37F09F749D4F0D3BA165,
	EnterPINLang__ctor_mD5FA4442D1A7B53C968E7A3E161F5CE0F8992B51,
	GameLang_Awake_m88182EB0C0AE4A78DB88095F9D2B16A2237CEC5E,
	GameLang_setLang_mDB511190EC4318D87062125C2656C1EE9B83F8E9,
	GameLang__ctor_m144185E266545F58DB685C555BDC78E98E4C4AB3,
	GameMenuLang_Awake_mE914B67055DF65ECD53A98153DDCFDA0C03640E9,
	GameMenuLang_setLang_mA20C12C7B1D825EFD990246302FD9401A98217B9,
	GameMenuLang__ctor_mCE79D49DE21611DF2C790014AC58D13EC1BC06B1,
	OneAwardLang_Awake_mCFE94B300C3AD48B4287E6FEBE5A86F861545762,
	OneAwardLang_setLang_m71BF319A0CFF2967CBFB7ECC05F7CF70B82182F4,
	OneAwardLang__ctor_m7109BA9634E6974ADB66C1EBBF2FDE22589897C2,
	SettingsLang_Awake_m4651456ED167577745ECA89FB1F3F4FF06CC7C68,
	SettingsLang_setLang_mF661C7627049B8FCB2F4535F048F85C94279B563,
	SettingsLang__ctor_m02AF41BCD4C92388A17F0117F6E42F6813513C4A,
	SettingsPINLang_Awake_m382EFB07AAC18FE069FC4C64B0572986FD6CC6DD,
	SettingsPINLang_setLang_m2B1B554C9BAFFD7DED5FCBBC253966C47935A989,
	SettingsPINLang__ctor_m76902D98A8B491489AA91ED7D0F4D3042A39A4CD,
	ThreeAwardLang_Awake_mFE9E4E41EF1A0F665F145E4E610E64AE18E16FD5,
	ThreeAwardLang_setLang_mB6B0D1EA2E97E5BF6AF62349692D278055A9322A,
	ThreeAwardLang__ctor_m5EC5F4A4055221D27A5D67F3214A701918D42566,
	TwoAwardLang_Awake_mDCE3F5388A42670FC8C22230DD373326F14E3CF0,
	TwoAwardLang_setLang_mF511A6231AF485C0208F0D6B801569C4B0C20C6D,
	TwoAwardLang__ctor_mCF71BEEEE0C2FEA7CD21261F0DD5E30BCE849C78,
	Settings_Awake_mBA445DDD366FCA51F1223575885A10ABDF812A21,
	Settings_CheckPIN_m9240031C817801CBB76A55EF476BABEED6960A4B,
	Settings__ctor_m2E00E4F8E213532A3051E24E75CE65368C0BB661,
	U3CShowBannerWhenInitializedU3Ed__4__ctor_m8DCA04DCF4BE1E4A624C3E15F9D1D9A9A68E35BE,
	U3CShowBannerWhenInitializedU3Ed__4_System_IDisposable_Dispose_mD55CAADBA6A2A4D90D2D9ED73ED3204E52ED6443,
	U3CShowBannerWhenInitializedU3Ed__4_MoveNext_m9E5EBEBE43E8B1ABE5D9C19983995D650E154AEA,
	U3CShowBannerWhenInitializedU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m85649261B8CBA0ADFC4624AF146BD1F735BDD6F3,
	U3CShowBannerWhenInitializedU3Ed__4_System_Collections_IEnumerator_Reset_mDDAA23D5AE0F0E33040D431742D59BEE06E7CB2B,
	U3CShowBannerWhenInitializedU3Ed__4_System_Collections_IEnumerator_get_Current_m9DD5569CEF9113FAA0420420EA3015085FEA420A,
};
static const int32_t s_InvokerIndices[94] = 
{
	1157,
	1126,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	982,
	982,
	982,
	982,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	982,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	1157,
	645,
	1157,
	1157,
	645,
	1157,
	1157,
	645,
	1157,
	1157,
	645,
	1157,
	1157,
	645,
	1157,
	1157,
	645,
	1157,
	1157,
	645,
	1157,
	1157,
	645,
	1157,
	1157,
	645,
	1157,
	1157,
	1157,
	1157,
	982,
	1157,
	1145,
	1126,
	1157,
	1126,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	94,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
