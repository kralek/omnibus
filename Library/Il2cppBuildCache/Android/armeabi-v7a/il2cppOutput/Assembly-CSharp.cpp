﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t34AA4AF4E7352129CA58045901530E41445AC16D;
// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_t815A476B0A21E183042059E705F9E505478CD8AE;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3;
// UnityEngine.AudioClip[]
struct AudioClipU5BU5D_t9BA3E7C4B62164BA2E054F3A82CD6CE6EB273CBE;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
// System.IntPtr[]
struct IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6;
// UnityEngine.UI.Selectable[]
struct SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
// UnityEngine.UI.Text[]
struct TextU5BU5D_t16DD1967B137EC602803C77DBB246B05B3D0275F;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;
// AdsBanner
struct AdsBanner_tE70BD583B6D94ED4239305BA678D6CD5152F38DE;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11;
// UnityEngine.AudioClip
struct AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE;
// UnityEngine.AudioSource
struct AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B;
// AwardsMenuLang
struct AwardsMenuLang_t7929C416FB74BDCC2DC4FA1F061FAE951E4F56EE;
// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9;
// UnityEngine.UI.Button
struct Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D;
// UnityEngine.Canvas
struct Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E;
// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684;
// UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7;
// DisplayPoints
struct DisplayPoints_tD2C77305910E0F73D81CED0D2E01A289C08B740E;
// EnterPIN
struct EnterPIN_t48ADD764A3FB09C507FD777FB1A570714C54C2A0;
// EnterPINLang
struct EnterPINLang_t7DB4141FE4619EA8936999CB62CFD95077E1B3D5;
// UnityEngine.UI.FontData
struct FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738;
// GM
struct GM_tFC30914A710042CAFEA7CF10E2AD01C9A529CEF0;
// GameLang
struct GameLang_t88B393D27387CFB1391819269B4FD02205959915;
// GameMenuLang
struct GameMenuLang_t49BEAA2ABAFC0A899B3346BEF7C050C4212D467C;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// UnityEngine.UI.Graphic
struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24;
// System.Collections.IDictionary
struct IDictionary_t99871C56B8EC2452AC5C4CF3831695E617B89D3A;
// System.Collections.IEnumerator
struct IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105;
// UnityEngine.UI.Image
struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C;
// MainMenu
struct MainMenu_tEB11F5A993C42E93B585FBB65C9E92EC91C5707C;
// UnityEngine.Material
struct Material_t8927C00353A72755313F046D0CE85178AE8218EE;
// UnityEngine.Mesh
struct Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A;
// MoveScenes
struct MoveScenes_t757DBF243D1177806463417C842AF5884F8C7C17;
// MultiTable
struct MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206;
// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// OneAwardLang
struct OneAwardLang_t98E602972882C2F997D3285FD94AE10CA89D1CB0;
// OperateOneAward
struct OperateOneAward_t30BCE36800FCC2888072B07ED15106D80175AB80;
// OperateThreeAward
struct OperateThreeAward_t647790567BA6B786988084DF0A6C9E4E6606BC17;
// OperateTwoAward
struct OperateTwoAward_t2378CD21BAB017639BC691A4F64BEE3B42DC528D;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15;
// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F;
// UnityEngine.UI.Selectable
struct Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD;
// Settings
struct Settings_t9AEAFFBA64B4FC9ECF724519DF460F19962F4495;
// SettingsLang
struct SettingsLang_tEC296FA53D4D04D71C9F9F8E0316113CC0DA7B62;
// SettingsPINLang
struct SettingsPINLang_tDF04F4EF7D0C7ACE1CEFBCB9722593B175BF93C7;
// UnityEngine.Sprite
struct Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9;
// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1;
// UnityEngine.TextGenerator
struct TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70;
// UnityEngine.Texture2D
struct Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF;
// ThreeAwardLang
struct ThreeAwardLang_tDDE2CB79F32FB3C2ACA78937569B61DFE012EF26;
// TwoAwardLang
struct TwoAwardLang_tA136F34FD58DCE4FC78BAEBF81A33966AB84DF8C;
// UnityEngine.Events.UnityAction
struct UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099;
// UnityEngine.UI.VertexHelper
struct VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013;
// AdsBanner/<ShowBannerWhenInitialized>d__4
struct U3CShowBannerWhenInitializedU3Ed__4_t49F7781D8F81FA49B995C576008FC45EC56C152B;
// UnityEngine.AudioClip/PCMReaderCallback
struct PCMReaderCallback_t9CA1437D36509A9FAC5EDD8FF2BC3259C24D0E0B;
// UnityEngine.AudioClip/PCMSetPositionCallback
struct PCMSetPositionCallback_tBDD99E7C0697687F1E7B06CDD5DE444A3709CF4C;
// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4;

IL2CPP_EXTERN_C RuntimeClass* Advertisement_t049C3D5BE2CA5B376B1AEC9E88408AD0B2494D84_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GM_tFC30914A710042CAFEA7CF10E2AD01C9A529CEF0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CShowBannerWhenInitializedU3Ed__4_t49F7781D8F81FA49B995C576008FC45EC56C152B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral00CC5BFA1E037DB47EBF07DAAEFFCD993088112B;
IL2CPP_EXTERN_C String_t* _stringLiteral021DFDEE24FD0D1AA24E28572F9B12B32ED7470C;
IL2CPP_EXTERN_C String_t* _stringLiteral035A0EFF495FDDF533CE351CD95BCE3D200A3FF8;
IL2CPP_EXTERN_C String_t* _stringLiteral04D9F7A73DAB5872453B01CA9006EA04E5D4AF1C;
IL2CPP_EXTERN_C String_t* _stringLiteral1297894ED887548D0C5F68D62D8FEE6DD439DCAF;
IL2CPP_EXTERN_C String_t* _stringLiteral1378668D80AB356B40A2895FBA87A6ED36D93325;
IL2CPP_EXTERN_C String_t* _stringLiteral1764427109D31651865A98B70E1AE56C4E9DA044;
IL2CPP_EXTERN_C String_t* _stringLiteral18545C029F8273DF3E13B4D472D6E44246691335;
IL2CPP_EXTERN_C String_t* _stringLiteral1AE348EAFA097AB898941EAFE912D711A407DA10;
IL2CPP_EXTERN_C String_t* _stringLiteral1B4B0C8D886688ABA7D856B296240347916533C8;
IL2CPP_EXTERN_C String_t* _stringLiteral1BE450AF172FA36120A25FDEE8A898E902B2AD73;
IL2CPP_EXTERN_C String_t* _stringLiteral1DB4937A6FF134F494853D495F9912C69401B8D3;
IL2CPP_EXTERN_C String_t* _stringLiteral1E00E5176988AB5EB370C9793515E4691A169055;
IL2CPP_EXTERN_C String_t* _stringLiteral24F81026436D7EF86549AA058D3F541EED7A6974;
IL2CPP_EXTERN_C String_t* _stringLiteral24FF8FA7BBD419257374764826B5F3463CB2BF7A;
IL2CPP_EXTERN_C String_t* _stringLiteral252402BDDD47746D72180BEB27ACE12A14C28952;
IL2CPP_EXTERN_C String_t* _stringLiteral292E99589E6C7C2BF893F091E364EBBD4F55F82A;
IL2CPP_EXTERN_C String_t* _stringLiteral29AE05BA7260E645D628C1A847FF86E97893857C;
IL2CPP_EXTERN_C String_t* _stringLiteral29D2BAAB6C1DADC3F02DBC611FE1F33D1316C96E;
IL2CPP_EXTERN_C String_t* _stringLiteral2A9CFA9E1CCC77FD6688571E47F9B7FB42108B32;
IL2CPP_EXTERN_C String_t* _stringLiteral2CBE4FC4E0A773099AE34DEE978B5DFFFB73E9BA;
IL2CPP_EXTERN_C String_t* _stringLiteral2D04724A9557A62A9E96AFFEAB1585D081CF98B5;
IL2CPP_EXTERN_C String_t* _stringLiteral2D189F33BC75731D52B3B0FEE15F38DDA0D3705F;
IL2CPP_EXTERN_C String_t* _stringLiteral2D1B3A3874C4103AEC900F038C6433A5A97FDC93;
IL2CPP_EXTERN_C String_t* _stringLiteral305609DE0BD9A461509B7BA6EAF308D9CE516DB1;
IL2CPP_EXTERN_C String_t* _stringLiteral31595E973ADCB725F8CD1C8D0BE95AC5C5F177F0;
IL2CPP_EXTERN_C String_t* _stringLiteral31A1366D20C9DFB407937096ACBB38DBCB891917;
IL2CPP_EXTERN_C String_t* _stringLiteral329914E10919B589345B44E12B94F381C92412C3;
IL2CPP_EXTERN_C String_t* _stringLiteral336B029D04847DC4F36985E1B89D1A0B11D299A3;
IL2CPP_EXTERN_C String_t* _stringLiteral347A115193887F78FC1467972A3DE60D336FCB4E;
IL2CPP_EXTERN_C String_t* _stringLiteral36A3B7109E60582E8F76195C95C760FB2B03D883;
IL2CPP_EXTERN_C String_t* _stringLiteral37675749D5041A6ED53FAA3D96E0A53C91F68B5F;
IL2CPP_EXTERN_C String_t* _stringLiteral39994065AEF5B81B168A2450D5CB126C9CA8D3EE;
IL2CPP_EXTERN_C String_t* _stringLiteral3B7950727169090BB5C52667A23D97449FCA4DB0;
IL2CPP_EXTERN_C String_t* _stringLiteral3B9BFD67DABF0DD11C843581037D06D2DD85B75E;
IL2CPP_EXTERN_C String_t* _stringLiteral3C96E1BE89CC1EF02D4EC7F1C799E7A61AD171E2;
IL2CPP_EXTERN_C String_t* _stringLiteral3CA684424B0F3C717BBC6239F51361B70485D4A1;
IL2CPP_EXTERN_C String_t* _stringLiteral4096248D5E896770D10C3198F6CA42764F6065B5;
IL2CPP_EXTERN_C String_t* _stringLiteral4314D0BCBBB70D26FE2C01FA02E57C8C10120C29;
IL2CPP_EXTERN_C String_t* _stringLiteral440ADF0B63952180E519C4C8C2B9B099EEF2FA6A;
IL2CPP_EXTERN_C String_t* _stringLiteral459128DFEC7AEB59A3931020D880E9BB40929AC4;
IL2CPP_EXTERN_C String_t* _stringLiteral473A5415DB67A3D81A9EFFA2683F7A5AABDDB7E1;
IL2CPP_EXTERN_C String_t* _stringLiteral47782EC37002A09A7750757091F216DF542806E0;
IL2CPP_EXTERN_C String_t* _stringLiteral4A474656ED5A1176BC10BE769CABB0A46172AFD3;
IL2CPP_EXTERN_C String_t* _stringLiteral4B2FFD21A87B4EF130038DE3D2FFF5078BB872AD;
IL2CPP_EXTERN_C String_t* _stringLiteral4D0A51C14A2331C361C3CC21880E4CB63FEAFB6A;
IL2CPP_EXTERN_C String_t* _stringLiteral4F16F1667495C8DAA5142C398B5AFD69A30C4871;
IL2CPP_EXTERN_C String_t* _stringLiteral4F96043EB67DE62B0944AC570CD83249C52E7DB7;
IL2CPP_EXTERN_C String_t* _stringLiteral508746C9AC40E3F9CF297A5FBE2CDCEA56348D2E;
IL2CPP_EXTERN_C String_t* _stringLiteral5186B0598411520C99562D6615A80942FE52EF2A;
IL2CPP_EXTERN_C String_t* _stringLiteral518A22A9F69C395D395396210805148FC85CAC3F;
IL2CPP_EXTERN_C String_t* _stringLiteral52D443779C3B4BD379537021CDDE739770D7F448;
IL2CPP_EXTERN_C String_t* _stringLiteral530B175B7F6011707568F6CCAAA957BCEF5E641E;
IL2CPP_EXTERN_C String_t* _stringLiteral5768436A9385EC30851400A543BFEE4F42D779D4;
IL2CPP_EXTERN_C String_t* _stringLiteral5A9E55471716F3AEEA71B91E41E453A38C9CBE7F;
IL2CPP_EXTERN_C String_t* _stringLiteral5AC7460DE1EBD9758F16E22EFCD6C591008ED7E5;
IL2CPP_EXTERN_C String_t* _stringLiteral5C8680FB0D26638685FA8EC75DF5799F2B3FD582;
IL2CPP_EXTERN_C String_t* _stringLiteral5ED838701824663CF4628D94757507C98B26C5ED;
IL2CPP_EXTERN_C String_t* _stringLiteral5FA2A08D6D10D5421B774A13D4C4288064837A89;
IL2CPP_EXTERN_C String_t* _stringLiteral62F9586420589E0E9C123EA0FB9858B4A9ACCFC5;
IL2CPP_EXTERN_C String_t* _stringLiteral6371A78739DD9B8B6C667985882072A75525C424;
IL2CPP_EXTERN_C String_t* _stringLiteral639AB154A61332B41DDAE6F8231F56224448A1E2;
IL2CPP_EXTERN_C String_t* _stringLiteral653A457CFCABD3E7BD6C53A71BFDDFB52801E206;
IL2CPP_EXTERN_C String_t* _stringLiteral66AE76C32C3B18489B51BA40CF91E95EF53C326C;
IL2CPP_EXTERN_C String_t* _stringLiteral66DA9442F5FF18F2F34630B0F64A95F05F711755;
IL2CPP_EXTERN_C String_t* _stringLiteral66E7E28240AB29C3409FC3DBC7531D2907A56F02;
IL2CPP_EXTERN_C String_t* _stringLiteral687D9BA523E32A2B8A4FEAD5E5EAE89D357E1420;
IL2CPP_EXTERN_C String_t* _stringLiteral68ABC5B7E2D9C8F7C46E42B01F19EFF56C819259;
IL2CPP_EXTERN_C String_t* _stringLiteral6AFE6D1F9BA73FFE9852892B0E8D97FFB7FD7676;
IL2CPP_EXTERN_C String_t* _stringLiteral6B7BA1920180F57CC65B000BBE0402C1529A16D7;
IL2CPP_EXTERN_C String_t* _stringLiteral6BD017B967376C8E1B363715F386985B52BF6EE1;
IL2CPP_EXTERN_C String_t* _stringLiteral6D8CE1651FF9BA0EDE1DE46FDEFBF913113FD949;
IL2CPP_EXTERN_C String_t* _stringLiteral6E1CB7C6103F25079BE1A6F61D75875BD47F5068;
IL2CPP_EXTERN_C String_t* _stringLiteral6F3725E84909989D3989E377719DC2A8E7AA3B63;
IL2CPP_EXTERN_C String_t* _stringLiteral71D0A6517C0D3A477013739CD5FE5F7310ACBB30;
IL2CPP_EXTERN_C String_t* _stringLiteral73BC2B8241771FC5EAF5193DADBFB9C5145DBAFB;
IL2CPP_EXTERN_C String_t* _stringLiteral742FEB59DC8AB83AAF3830BA129DBA5FFFE7F76E;
IL2CPP_EXTERN_C String_t* _stringLiteral783C37D8989ADC1EE756FE0E59E5F44AA3D5140E;
IL2CPP_EXTERN_C String_t* _stringLiteral79A646BD7D4C31240F06616D74E5E94D070AF4F6;
IL2CPP_EXTERN_C String_t* _stringLiteral7B67306E398C4403BAF521C10FBB0E9E86B3EB5E;
IL2CPP_EXTERN_C String_t* _stringLiteral7BCABA1AF0C023BD47CC57FADD9C04B9F922D6DA;
IL2CPP_EXTERN_C String_t* _stringLiteral7C0AC29A8D0286851370B7150A2AF361A5073C40;
IL2CPP_EXTERN_C String_t* _stringLiteral7EC999EEB31D4FDB96A6DB514AA0CAA86244215E;
IL2CPP_EXTERN_C String_t* _stringLiteral80C25E49528430AD5C029A6E09F8A1DC7107EC3F;
IL2CPP_EXTERN_C String_t* _stringLiteral85DECDFBCB5D8A12E0CFF931669813EFEC429128;
IL2CPP_EXTERN_C String_t* _stringLiteral869DB31C5A3D488D8DC8563CF12CB43429962FF9;
IL2CPP_EXTERN_C String_t* _stringLiteral86C6A744076C8CC32077709BDF831D565B511A86;
IL2CPP_EXTERN_C String_t* _stringLiteral87C2990C09F3D73E2C2512ABC980EEC426BC85DE;
IL2CPP_EXTERN_C String_t* _stringLiteral8943EFB8DBBDD3E994BBC72010F6830128E100A5;
IL2CPP_EXTERN_C String_t* _stringLiteral895A7C5562D2DA5DB507C3430A144F324FBA1FEC;
IL2CPP_EXTERN_C String_t* _stringLiteral8A915C1241B79502A63A19FDB5939552772053A6;
IL2CPP_EXTERN_C String_t* _stringLiteral8B97FF7B417237C9F66EA83872B0B16505D74FEC;
IL2CPP_EXTERN_C String_t* _stringLiteral8E20BA6DCE5913BB00D64A29DCCA2A82B16FEFB9;
IL2CPP_EXTERN_C String_t* _stringLiteral8EDFA4F5CE10FF860ED62FF5BB7081903CF066AD;
IL2CPP_EXTERN_C String_t* _stringLiteral8F103C685BE61397D30779FE9A4354EADD169DC5;
IL2CPP_EXTERN_C String_t* _stringLiteral921F1F9D366E611F8A551D5E3695B6EDDC58FF08;
IL2CPP_EXTERN_C String_t* _stringLiteral92CC27364F419C9FE8921E49485099EE417ADB7A;
IL2CPP_EXTERN_C String_t* _stringLiteral93861786C883B3B566EDCAD421047A5C4D006A31;
IL2CPP_EXTERN_C String_t* _stringLiteral93ADFC213A427399629E9CBF8769B349656C397F;
IL2CPP_EXTERN_C String_t* _stringLiteral95ADA1E608855E2F14812B488C44DDA8ECB96F9B;
IL2CPP_EXTERN_C String_t* _stringLiteral974FF80607D2850832F8E0C5E6BFCE5955E3113B;
IL2CPP_EXTERN_C String_t* _stringLiteral97A16F5602146D779A5D6883F92AEC125FF808D4;
IL2CPP_EXTERN_C String_t* _stringLiteral97FB36FBA00500C467DEE75B6919C6CC55520A4F;
IL2CPP_EXTERN_C String_t* _stringLiteral9836D089000B3E539DB4605A1AC504E93ABC91B8;
IL2CPP_EXTERN_C String_t* _stringLiteral984415071B027A6E300E4CA80AC0169E46972F0E;
IL2CPP_EXTERN_C String_t* _stringLiteral98EB6A098B53C71FDCD7160F6A160884F3F17266;
IL2CPP_EXTERN_C String_t* _stringLiteral9909DB5082CC8F76FB3430AF7F02F7AE0FEDF4D8;
IL2CPP_EXTERN_C String_t* _stringLiteral9AA7B8A822904E1E38F824BA930331B144AD9766;
IL2CPP_EXTERN_C String_t* _stringLiteral9F5D2C9A905003D7A11A41AAE6A62E8DABD4EB59;
IL2CPP_EXTERN_C String_t* _stringLiteralA1CE3BE2769759673A1D828C1C401647D7BCF60B;
IL2CPP_EXTERN_C String_t* _stringLiteralA3F89865E30490E81ABEFDBF0D19EBC7E69538D2;
IL2CPP_EXTERN_C String_t* _stringLiteralA48A79516820BAF044494D076B2DC4FEEAAD12CA;
IL2CPP_EXTERN_C String_t* _stringLiteralA6F0F2DD7D0A47A3F14F63868A9EB46F6CA5ACF8;
IL2CPP_EXTERN_C String_t* _stringLiteralA8F2373ADF991C37C5ABEECE00312CD475F229E9;
IL2CPP_EXTERN_C String_t* _stringLiteralAC9516655C9A985B36C0B7CE27942BD81B8908EB;
IL2CPP_EXTERN_C String_t* _stringLiteralACE00218C2787FFA96AC949D3EF70AB32F34EFE3;
IL2CPP_EXTERN_C String_t* _stringLiteralAD62C412EE5B17ED9B961E83E6C72EA9720EF2A8;
IL2CPP_EXTERN_C String_t* _stringLiteralADFF2E97BFEBE69C80BC9BC79C409BF74C970036;
IL2CPP_EXTERN_C String_t* _stringLiteralAE394F72B21FDE903D93703B0EE828B3C0A4D310;
IL2CPP_EXTERN_C String_t* _stringLiteralAE91247CABA7B5523E4506922CC38223BFBE1D3E;
IL2CPP_EXTERN_C String_t* _stringLiteralB0448E6572AD97C10FD7929B5F4958C2136F2E88;
IL2CPP_EXTERN_C String_t* _stringLiteralB0683F985BA7F461EFD91E56DF63ECCB9EB2DA87;
IL2CPP_EXTERN_C String_t* _stringLiteralB1B80710C4AAD7B9FDBDF44D1C9A0F02886AD9BB;
IL2CPP_EXTERN_C String_t* _stringLiteralB21E0B119910BC35509F196632A1FB64EC6A9970;
IL2CPP_EXTERN_C String_t* _stringLiteralB4B9884D9A091B24982C78ADD8E79B465BFA9DB8;
IL2CPP_EXTERN_C String_t* _stringLiteralB4E4E2DBFC31551BC1BD5C157680ECAE8D15E0FE;
IL2CPP_EXTERN_C String_t* _stringLiteralB895583198199E0518C92D1D5A9C95989F9A1B99;
IL2CPP_EXTERN_C String_t* _stringLiteralC11FE1AAA451A181FAC2657DD1D911A32F3955C1;
IL2CPP_EXTERN_C String_t* _stringLiteralC7C54B24F4E5A8AD8B3C11D391F6EC11BEF9A572;
IL2CPP_EXTERN_C String_t* _stringLiteralC84E4D9117775396179E79EF749351D9592346C2;
IL2CPP_EXTERN_C String_t* _stringLiteralC91E1737728E2718A6CDB4388EF6ABC1140C5EFE;
IL2CPP_EXTERN_C String_t* _stringLiteralCC1C631599A033919D014A2BA6511F93EF6A26BA;
IL2CPP_EXTERN_C String_t* _stringLiteralCC8099E3A874E4223303AF753F0B61D2807E8478;
IL2CPP_EXTERN_C String_t* _stringLiteralCD65D28A5DDFB33A67DD4A29CE5A1700A02C5904;
IL2CPP_EXTERN_C String_t* _stringLiteralCD8E37B9578BEA9485296FACF596A45CC703A39C;
IL2CPP_EXTERN_C String_t* _stringLiteralCE05A8E05C74643A5E29EF29C33FC7FEE07C45B5;
IL2CPP_EXTERN_C String_t* _stringLiteralCE163314FAA201B884DEF98401B31265D6FF4978;
IL2CPP_EXTERN_C String_t* _stringLiteralCE8A0013D4140A243026D70A506048F0B9B53C2A;
IL2CPP_EXTERN_C String_t* _stringLiteralCF059ED30A7A499E26BBB414999316984D0D7D57;
IL2CPP_EXTERN_C String_t* _stringLiteralD0922252964B4DA81D53488FD3CE7506E0B343F8;
IL2CPP_EXTERN_C String_t* _stringLiteralD1068DE67DE168DFDADD76A0088D405170ABC15E;
IL2CPP_EXTERN_C String_t* _stringLiteralD2CE79FC932055F49C0E42CD510FAAC84C4982BB;
IL2CPP_EXTERN_C String_t* _stringLiteralD2F21AFCBF1FB9CC1CDACB1C883D5FDFBD5C70A4;
IL2CPP_EXTERN_C String_t* _stringLiteralD3C428E5D5F2471B5037540743F73946D676FACE;
IL2CPP_EXTERN_C String_t* _stringLiteralD9620E76463FD7E858D4E8FF32FF28CA712A64C7;
IL2CPP_EXTERN_C String_t* _stringLiteralD9C0FFA7748008D277E391D503EDD5FEC988392A;
IL2CPP_EXTERN_C String_t* _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
IL2CPP_EXTERN_C String_t* _stringLiteralDA55D789AC0644075564A3A9118DE8B619931A28;
IL2CPP_EXTERN_C String_t* _stringLiteralDA661A147DE9D5A86D9C547E06E53270E05A1150;
IL2CPP_EXTERN_C String_t* _stringLiteralDAEBFB83D7BA5E8188D567F31243CA1CE5709FE1;
IL2CPP_EXTERN_C String_t* _stringLiteralDFE6FE7196BF4ABC4FF233F1648351E5485673A4;
IL2CPP_EXTERN_C String_t* _stringLiteralE008DD2964784D5FA9F9972A94C6214E2FF55999;
IL2CPP_EXTERN_C String_t* _stringLiteralE291D9505F6E8B8B7E44E9DA7CD4657569ABB935;
IL2CPP_EXTERN_C String_t* _stringLiteralE2F0BA1E42D98915C07B67C8F89D13676D845119;
IL2CPP_EXTERN_C String_t* _stringLiteralE2FD15AA605F64079A93F2AFE1AC6965E9466670;
IL2CPP_EXTERN_C String_t* _stringLiteralE3726E592E03F0661729947C6CB2B48360220E18;
IL2CPP_EXTERN_C String_t* _stringLiteralE6562869B1E0927E641FB9CEB82070BFEB4DBF83;
IL2CPP_EXTERN_C String_t* _stringLiteralE7D08C2FFBAEC3DC48D0A1A70AEF7807D465C0AB;
IL2CPP_EXTERN_C String_t* _stringLiteralEA058543F73E50CB1E0192DD94678A287B69370C;
IL2CPP_EXTERN_C String_t* _stringLiteralEAA8F9283296F4C0E880EE4F1A05E2FF6CCC098B;
IL2CPP_EXTERN_C String_t* _stringLiteralEC308DD32AC3ECD5859F14D8C8AFCE417C45F3DF;
IL2CPP_EXTERN_C String_t* _stringLiteralEEC7F986C7A7A979DA0887CABFB7DF21BA016E4C;
IL2CPP_EXTERN_C String_t* _stringLiteralF09EC4356FCC74D969B20E6082914C6F08BC6592;
IL2CPP_EXTERN_C String_t* _stringLiteralF32A408AE239BB49DEAB74C3B0BE763701964CA7;
IL2CPP_EXTERN_C String_t* _stringLiteralF4D5309870776863C10B078D84EE175845413FCD;
IL2CPP_EXTERN_C String_t* _stringLiteralF5B883644A591C1DAE1BDF15205EB12BF78D8A34;
IL2CPP_EXTERN_C String_t* _stringLiteralF70C6534E440D06E2EA7D6513A9E9E3AC3CA025D;
IL2CPP_EXTERN_C String_t* _stringLiteralF7418F00A0BC87B788A53131CE12EB9332E71D95;
IL2CPP_EXTERN_C String_t* _stringLiteralF93A6BBD2F32B66DCD196FEA1EB82ACBEBC67C3B;
IL2CPP_EXTERN_C String_t* _stringLiteralFBD9D6B58583FF60D71D6F28CB3BE23AC41D0834;
IL2CPP_EXTERN_C String_t* _stringLiteralFEDD4D7E4BC684807EBF7842D5983AEBFAE7AA17;
IL2CPP_EXTERN_C String_t* _stringLiteralFF4E8113C1DD42448B93F3E73007CFEF6C650CD4;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mFB5C182E24F496A866F116D3F75CBD8616A46AB3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisAudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_mCC3B7A679ECE504BFAF8C70C4EF527511F46902F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m2D99AC2081683F963C56EC738451EC0B59B5D137_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Clear_m508B72E5229FAE7042D99A04555F66F10C597C7A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CShowBannerWhenInitializedU3Ed__4_System_Collections_IEnumerator_Reset_mDDAA23D5AE0F0E33040D431742D59BEE06E7CB2B_RuntimeMethod_var;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct AudioClipU5BU5D_t9BA3E7C4B62164BA2E054F3A82CD6CE6EB273CBE;
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
struct SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77;
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
struct TextU5BU5D_t16DD1967B137EC602803C77DBB246B05B3D0275F;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tFDCAFCBB4B3431CFF2DC4D3E03FBFDF54EFF7E9A 
{
public:

public:
};


// System.Object


// System.Collections.Generic.List`1<System.Int32>
struct  List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7, ____items_1)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get__items_1() const { return ____items_1; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7_StaticFields, ____emptyArray_5)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.YieldInstruction
struct  YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
};

// AdsBanner/<ShowBannerWhenInitialized>d__4
struct  U3CShowBannerWhenInitializedU3Ed__4_t49F7781D8F81FA49B995C576008FC45EC56C152B  : public RuntimeObject
{
public:
	// System.Int32 AdsBanner/<ShowBannerWhenInitialized>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object AdsBanner/<ShowBannerWhenInitialized>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// AdsBanner AdsBanner/<ShowBannerWhenInitialized>d__4::<>4__this
	AdsBanner_tE70BD583B6D94ED4239305BA678D6CD5152F38DE * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CShowBannerWhenInitializedU3Ed__4_t49F7781D8F81FA49B995C576008FC45EC56C152B, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CShowBannerWhenInitializedU3Ed__4_t49F7781D8F81FA49B995C576008FC45EC56C152B, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CShowBannerWhenInitializedU3Ed__4_t49F7781D8F81FA49B995C576008FC45EC56C152B, ___U3CU3E4__this_2)); }
	inline AdsBanner_tE70BD583B6D94ED4239305BA678D6CD5152F38DE * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline AdsBanner_tE70BD583B6D94ED4239305BA678D6CD5152F38DE ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(AdsBanner_tE70BD583B6D94ED4239305BA678D6CD5152F38DE * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// System.Boolean
struct  Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// UnityEngine.Color
struct  Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct  Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Single
struct  Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.UI.SpriteState
struct  SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_SelectedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_HighlightedSprite_0)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HighlightedSprite_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_PressedSprite_1)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PressedSprite_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectedSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_SelectedSprite_2)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_SelectedSprite_2() const { return ___m_SelectedSprite_2; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_SelectedSprite_2() { return &___m_SelectedSprite_2; }
	inline void set_m_SelectedSprite_2(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_SelectedSprite_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectedSprite_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_3() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_DisabledSprite_3)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_DisabledSprite_3() const { return ___m_DisabledSprite_3; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_DisabledSprite_3() { return &___m_DisabledSprite_3; }
	inline void set_m_DisabledSprite_3(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_DisabledSprite_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DisabledSprite_3), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E_marshaled_pinvoke
{
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E_marshaled_com
{
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;
};

// UnityEngine.Vector4
struct  Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___zeroVector_5)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___oneVector_6)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___negativeInfinityVector_8 = value;
	}
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// UnityEngine.WaitForSeconds
struct  WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013  : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF
{
public:
	// System.Single UnityEngine.WaitForSeconds::m_Seconds
	float ___m_Seconds_0;

public:
	inline static int32_t get_offset_of_m_Seconds_0() { return static_cast<int32_t>(offsetof(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013, ___m_Seconds_0)); }
	inline float get_m_Seconds_0() const { return ___m_Seconds_0; }
	inline float* get_address_of_m_Seconds_0() { return &___m_Seconds_0; }
	inline void set_m_Seconds_0(float value)
	{
		___m_Seconds_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_marshaled_pinvoke : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
	float ___m_Seconds_0;
};
// Native definition for COM marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_marshaled_com : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
	float ___m_Seconds_0;
};

// UnityEngine.Advertisements.BannerPosition
struct  BannerPosition_t29565664AA28FC370AB42D7CC1BD2599EFA04079 
{
public:
	// System.Int32 UnityEngine.Advertisements.BannerPosition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BannerPosition_t29565664AA28FC370AB42D7CC1BD2599EFA04079, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.ColorBlock
struct  ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_SelectedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_SelectedColor_3;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_DisabledColor_4;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_5;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_6;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_NormalColor_0)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_HighlightedColor_1)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_PressedColor_2)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_SelectedColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_SelectedColor_3)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_SelectedColor_3() const { return ___m_SelectedColor_3; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_SelectedColor_3() { return &___m_SelectedColor_3; }
	inline void set_m_SelectedColor_3(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_SelectedColor_3 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_4() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_DisabledColor_4)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_DisabledColor_4() const { return ___m_DisabledColor_4; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_DisabledColor_4() { return &___m_DisabledColor_4; }
	inline void set_m_DisabledColor_4(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_DisabledColor_4 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_5() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_ColorMultiplier_5)); }
	inline float get_m_ColorMultiplier_5() const { return ___m_ColorMultiplier_5; }
	inline float* get_address_of_m_ColorMultiplier_5() { return &___m_ColorMultiplier_5; }
	inline void set_m_ColorMultiplier_5(float value)
	{
		___m_ColorMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_6() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_FadeDuration_6)); }
	inline float get_m_FadeDuration_6() const { return ___m_FadeDuration_6; }
	inline float* get_address_of_m_FadeDuration_6() { return &___m_FadeDuration_6; }
	inline void set_m_FadeDuration_6(float value)
	{
		___m_FadeDuration_6 = value;
	}
};

struct ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955_StaticFields
{
public:
	// UnityEngine.UI.ColorBlock UnityEngine.UI.ColorBlock::defaultColorBlock
	ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  ___defaultColorBlock_7;

public:
	inline static int32_t get_offset_of_defaultColorBlock_7() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955_StaticFields, ___defaultColorBlock_7)); }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  get_defaultColorBlock_7() const { return ___defaultColorBlock_7; }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 * get_address_of_defaultColorBlock_7() { return &___defaultColorBlock_7; }
	inline void set_defaultColorBlock_7(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  value)
	{
		___defaultColorBlock_7 = value;
	}
};


// UnityEngine.Coroutine
struct  Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7  : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7_marshaled_pinvoke : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7_marshaled_com : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// UnityEngine.Object
struct  Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.SystemLanguage
struct  SystemLanguage_tF8A9C86102588DE9A5041719609C2693D283B3A6 
{
public:
	// System.Int32 UnityEngine.SystemLanguage::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SystemLanguage_tF8A9C86102588DE9A5041719609C2693D283B3A6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Image/FillMethod
struct  FillMethod_tC37E5898D113A8FBF25A6AB6FBA451CC51E211E2 
{
public:
	// System.Int32 UnityEngine.UI.Image/FillMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FillMethod_tC37E5898D113A8FBF25A6AB6FBA451CC51E211E2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Image/Type
struct  Type_tDCB08AB7425CAB70C1E46CC341F877423B5A5E12 
{
public:
	// System.Int32 UnityEngine.UI.Image/Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Type_tDCB08AB7425CAB70C1E46CC341F877423B5A5E12, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Navigation/Mode
struct  Mode_t3113FDF05158BBA1DFC78D7F69E4C1D25135CB0F 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t3113FDF05158BBA1DFC78D7F69E4C1D25135CB0F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Selectable/Transition
struct  Transition_t1FC449676815A798E758D32E8BE6DC0A2511DF14 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Transition_t1FC449676815A798E758D32E8BE6DC0A2511DF14, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.AudioClip
struct  AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:
	// UnityEngine.AudioClip/PCMReaderCallback UnityEngine.AudioClip::m_PCMReaderCallback
	PCMReaderCallback_t9CA1437D36509A9FAC5EDD8FF2BC3259C24D0E0B * ___m_PCMReaderCallback_4;
	// UnityEngine.AudioClip/PCMSetPositionCallback UnityEngine.AudioClip::m_PCMSetPositionCallback
	PCMSetPositionCallback_tBDD99E7C0697687F1E7B06CDD5DE444A3709CF4C * ___m_PCMSetPositionCallback_5;

public:
	inline static int32_t get_offset_of_m_PCMReaderCallback_4() { return static_cast<int32_t>(offsetof(AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE, ___m_PCMReaderCallback_4)); }
	inline PCMReaderCallback_t9CA1437D36509A9FAC5EDD8FF2BC3259C24D0E0B * get_m_PCMReaderCallback_4() const { return ___m_PCMReaderCallback_4; }
	inline PCMReaderCallback_t9CA1437D36509A9FAC5EDD8FF2BC3259C24D0E0B ** get_address_of_m_PCMReaderCallback_4() { return &___m_PCMReaderCallback_4; }
	inline void set_m_PCMReaderCallback_4(PCMReaderCallback_t9CA1437D36509A9FAC5EDD8FF2BC3259C24D0E0B * value)
	{
		___m_PCMReaderCallback_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PCMReaderCallback_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_PCMSetPositionCallback_5() { return static_cast<int32_t>(offsetof(AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE, ___m_PCMSetPositionCallback_5)); }
	inline PCMSetPositionCallback_tBDD99E7C0697687F1E7B06CDD5DE444A3709CF4C * get_m_PCMSetPositionCallback_5() const { return ___m_PCMSetPositionCallback_5; }
	inline PCMSetPositionCallback_tBDD99E7C0697687F1E7B06CDD5DE444A3709CF4C ** get_address_of_m_PCMSetPositionCallback_5() { return &___m_PCMSetPositionCallback_5; }
	inline void set_m_PCMSetPositionCallback_5(PCMSetPositionCallback_tBDD99E7C0697687F1E7B06CDD5DE444A3709CF4C * value)
	{
		___m_PCMSetPositionCallback_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PCMSetPositionCallback_5), (void*)value);
	}
};


// UnityEngine.Component
struct  Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.GameObject
struct  GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.UI.Navigation
struct  Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// System.Boolean UnityEngine.UI.Navigation::m_WrapAround
	bool ___m_WrapAround_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_WrapAround_1() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_WrapAround_1)); }
	inline bool get_m_WrapAround_1() const { return ___m_WrapAround_1; }
	inline bool* get_address_of_m_WrapAround_1() { return &___m_WrapAround_1; }
	inline void set_m_WrapAround_1(bool value)
	{
		___m_WrapAround_1 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_2() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnUp_2)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnUp_2() const { return ___m_SelectOnUp_2; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnUp_2() { return &___m_SelectOnUp_2; }
	inline void set_m_SelectOnUp_2(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnUp_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnUp_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_3() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnDown_3)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnDown_3() const { return ___m_SelectOnDown_3; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnDown_3() { return &___m_SelectOnDown_3; }
	inline void set_m_SelectOnDown_3(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnDown_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnDown_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_4() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnLeft_4)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnLeft_4() const { return ___m_SelectOnLeft_4; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnLeft_4() { return &___m_SelectOnLeft_4; }
	inline void set_m_SelectOnLeft_4(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnLeft_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnLeft_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_5() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnRight_5)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnRight_5() const { return ___m_SelectOnRight_5; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnRight_5() { return &___m_SelectOnRight_5; }
	inline void set_m_SelectOnRight_5(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnRight_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnRight_5), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	int32_t ___m_WrapAround_1;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A_marshaled_com
{
	int32_t ___m_Mode_0;
	int32_t ___m_WrapAround_1;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;
};

// UnityEngine.Sprite
struct  Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// System.SystemException
struct  SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62  : public Exception_t
{
public:

public:
};


// UnityEngine.Behaviour
struct  Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// System.NotSupportedException
struct  NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:

public:
};


// UnityEngine.AudioBehaviour
struct  AudioBehaviour_tB44966D47AD43C50C7294AEE9B57574E55AACA4A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// AdsBanner
struct  AdsBanner_tE70BD583B6D94ED4239305BA678D6CD5152F38DE  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String AdsBanner::gameId
	String_t* ___gameId_4;
	// System.String AdsBanner::surfacingId
	String_t* ___surfacingId_5;
	// System.Boolean AdsBanner::testMode
	bool ___testMode_6;

public:
	inline static int32_t get_offset_of_gameId_4() { return static_cast<int32_t>(offsetof(AdsBanner_tE70BD583B6D94ED4239305BA678D6CD5152F38DE, ___gameId_4)); }
	inline String_t* get_gameId_4() const { return ___gameId_4; }
	inline String_t** get_address_of_gameId_4() { return &___gameId_4; }
	inline void set_gameId_4(String_t* value)
	{
		___gameId_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gameId_4), (void*)value);
	}

	inline static int32_t get_offset_of_surfacingId_5() { return static_cast<int32_t>(offsetof(AdsBanner_tE70BD583B6D94ED4239305BA678D6CD5152F38DE, ___surfacingId_5)); }
	inline String_t* get_surfacingId_5() const { return ___surfacingId_5; }
	inline String_t** get_address_of_surfacingId_5() { return &___surfacingId_5; }
	inline void set_surfacingId_5(String_t* value)
	{
		___surfacingId_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___surfacingId_5), (void*)value);
	}

	inline static int32_t get_offset_of_testMode_6() { return static_cast<int32_t>(offsetof(AdsBanner_tE70BD583B6D94ED4239305BA678D6CD5152F38DE, ___testMode_6)); }
	inline bool get_testMode_6() const { return ___testMode_6; }
	inline bool* get_address_of_testMode_6() { return &___testMode_6; }
	inline void set_testMode_6(bool value)
	{
		___testMode_6 = value;
	}
};


// UnityEngine.AudioSource
struct  AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B  : public AudioBehaviour_tB44966D47AD43C50C7294AEE9B57574E55AACA4A
{
public:

public:
};


// AwardsMenuLang
struct  AwardsMenuLang_t7929C416FB74BDCC2DC4FA1F061FAE951E4F56EE  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.UI.Text AwardsMenuLang::Text1
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___Text1_4;
	// UnityEngine.UI.Text AwardsMenuLang::Text2
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___Text2_5;
	// UnityEngine.UI.Text AwardsMenuLang::Text3
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___Text3_6;
	// UnityEngine.UI.Text AwardsMenuLang::Text4
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___Text4_7;
	// UnityEngine.UI.Text AwardsMenuLang::Text5
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___Text5_8;
	// UnityEngine.UI.Text AwardsMenuLang::Text6
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___Text6_9;
	// System.String[] AwardsMenuLang::pl
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___pl_10;
	// System.String[] AwardsMenuLang::en
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___en_11;
	// System.String[] AwardsMenuLang::de
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___de_12;
	// System.String[] AwardsMenuLang::fe
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___fe_13;
	// System.String[] AwardsMenuLang::es
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___es_14;
	// System.String[] AwardsMenuLang::ru
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___ru_15;

public:
	inline static int32_t get_offset_of_Text1_4() { return static_cast<int32_t>(offsetof(AwardsMenuLang_t7929C416FB74BDCC2DC4FA1F061FAE951E4F56EE, ___Text1_4)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_Text1_4() const { return ___Text1_4; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_Text1_4() { return &___Text1_4; }
	inline void set_Text1_4(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___Text1_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Text1_4), (void*)value);
	}

	inline static int32_t get_offset_of_Text2_5() { return static_cast<int32_t>(offsetof(AwardsMenuLang_t7929C416FB74BDCC2DC4FA1F061FAE951E4F56EE, ___Text2_5)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_Text2_5() const { return ___Text2_5; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_Text2_5() { return &___Text2_5; }
	inline void set_Text2_5(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___Text2_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Text2_5), (void*)value);
	}

	inline static int32_t get_offset_of_Text3_6() { return static_cast<int32_t>(offsetof(AwardsMenuLang_t7929C416FB74BDCC2DC4FA1F061FAE951E4F56EE, ___Text3_6)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_Text3_6() const { return ___Text3_6; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_Text3_6() { return &___Text3_6; }
	inline void set_Text3_6(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___Text3_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Text3_6), (void*)value);
	}

	inline static int32_t get_offset_of_Text4_7() { return static_cast<int32_t>(offsetof(AwardsMenuLang_t7929C416FB74BDCC2DC4FA1F061FAE951E4F56EE, ___Text4_7)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_Text4_7() const { return ___Text4_7; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_Text4_7() { return &___Text4_7; }
	inline void set_Text4_7(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___Text4_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Text4_7), (void*)value);
	}

	inline static int32_t get_offset_of_Text5_8() { return static_cast<int32_t>(offsetof(AwardsMenuLang_t7929C416FB74BDCC2DC4FA1F061FAE951E4F56EE, ___Text5_8)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_Text5_8() const { return ___Text5_8; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_Text5_8() { return &___Text5_8; }
	inline void set_Text5_8(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___Text5_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Text5_8), (void*)value);
	}

	inline static int32_t get_offset_of_Text6_9() { return static_cast<int32_t>(offsetof(AwardsMenuLang_t7929C416FB74BDCC2DC4FA1F061FAE951E4F56EE, ___Text6_9)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_Text6_9() const { return ___Text6_9; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_Text6_9() { return &___Text6_9; }
	inline void set_Text6_9(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___Text6_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Text6_9), (void*)value);
	}

	inline static int32_t get_offset_of_pl_10() { return static_cast<int32_t>(offsetof(AwardsMenuLang_t7929C416FB74BDCC2DC4FA1F061FAE951E4F56EE, ___pl_10)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_pl_10() const { return ___pl_10; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_pl_10() { return &___pl_10; }
	inline void set_pl_10(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___pl_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pl_10), (void*)value);
	}

	inline static int32_t get_offset_of_en_11() { return static_cast<int32_t>(offsetof(AwardsMenuLang_t7929C416FB74BDCC2DC4FA1F061FAE951E4F56EE, ___en_11)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_en_11() const { return ___en_11; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_en_11() { return &___en_11; }
	inline void set_en_11(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___en_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___en_11), (void*)value);
	}

	inline static int32_t get_offset_of_de_12() { return static_cast<int32_t>(offsetof(AwardsMenuLang_t7929C416FB74BDCC2DC4FA1F061FAE951E4F56EE, ___de_12)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_de_12() const { return ___de_12; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_de_12() { return &___de_12; }
	inline void set_de_12(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___de_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___de_12), (void*)value);
	}

	inline static int32_t get_offset_of_fe_13() { return static_cast<int32_t>(offsetof(AwardsMenuLang_t7929C416FB74BDCC2DC4FA1F061FAE951E4F56EE, ___fe_13)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_fe_13() const { return ___fe_13; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_fe_13() { return &___fe_13; }
	inline void set_fe_13(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___fe_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fe_13), (void*)value);
	}

	inline static int32_t get_offset_of_es_14() { return static_cast<int32_t>(offsetof(AwardsMenuLang_t7929C416FB74BDCC2DC4FA1F061FAE951E4F56EE, ___es_14)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_es_14() const { return ___es_14; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_es_14() { return &___es_14; }
	inline void set_es_14(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___es_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___es_14), (void*)value);
	}

	inline static int32_t get_offset_of_ru_15() { return static_cast<int32_t>(offsetof(AwardsMenuLang_t7929C416FB74BDCC2DC4FA1F061FAE951E4F56EE, ___ru_15)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_ru_15() const { return ___ru_15; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_ru_15() { return &___ru_15; }
	inline void set_ru_15(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___ru_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ru_15), (void*)value);
	}
};


// DisplayPoints
struct  DisplayPoints_tD2C77305910E0F73D81CED0D2E01A289C08B740E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.UI.Text DisplayPoints::txtScore
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___txtScore_4;

public:
	inline static int32_t get_offset_of_txtScore_4() { return static_cast<int32_t>(offsetof(DisplayPoints_tD2C77305910E0F73D81CED0D2E01A289C08B740E, ___txtScore_4)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_txtScore_4() const { return ___txtScore_4; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_txtScore_4() { return &___txtScore_4; }
	inline void set_txtScore_4(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___txtScore_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___txtScore_4), (void*)value);
	}
};


// EnterPIN
struct  EnterPIN_t48ADD764A3FB09C507FD777FB1A570714C54C2A0  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String EnterPIN::SetPIN1
	String_t* ___SetPIN1_4;
	// System.String EnterPIN::SetPIN
	String_t* ___SetPIN_5;
	// UnityEngine.GameObject EnterPIN::InputField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___InputField_6;
	// UnityEngine.GameObject EnterPIN::TextDisplay
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___TextDisplay_7;
	// UnityEngine.UI.Text EnterPIN::TextError
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___TextError_8;

public:
	inline static int32_t get_offset_of_SetPIN1_4() { return static_cast<int32_t>(offsetof(EnterPIN_t48ADD764A3FB09C507FD777FB1A570714C54C2A0, ___SetPIN1_4)); }
	inline String_t* get_SetPIN1_4() const { return ___SetPIN1_4; }
	inline String_t** get_address_of_SetPIN1_4() { return &___SetPIN1_4; }
	inline void set_SetPIN1_4(String_t* value)
	{
		___SetPIN1_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SetPIN1_4), (void*)value);
	}

	inline static int32_t get_offset_of_SetPIN_5() { return static_cast<int32_t>(offsetof(EnterPIN_t48ADD764A3FB09C507FD777FB1A570714C54C2A0, ___SetPIN_5)); }
	inline String_t* get_SetPIN_5() const { return ___SetPIN_5; }
	inline String_t** get_address_of_SetPIN_5() { return &___SetPIN_5; }
	inline void set_SetPIN_5(String_t* value)
	{
		___SetPIN_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SetPIN_5), (void*)value);
	}

	inline static int32_t get_offset_of_InputField_6() { return static_cast<int32_t>(offsetof(EnterPIN_t48ADD764A3FB09C507FD777FB1A570714C54C2A0, ___InputField_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_InputField_6() const { return ___InputField_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_InputField_6() { return &___InputField_6; }
	inline void set_InputField_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___InputField_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___InputField_6), (void*)value);
	}

	inline static int32_t get_offset_of_TextDisplay_7() { return static_cast<int32_t>(offsetof(EnterPIN_t48ADD764A3FB09C507FD777FB1A570714C54C2A0, ___TextDisplay_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_TextDisplay_7() const { return ___TextDisplay_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_TextDisplay_7() { return &___TextDisplay_7; }
	inline void set_TextDisplay_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___TextDisplay_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TextDisplay_7), (void*)value);
	}

	inline static int32_t get_offset_of_TextError_8() { return static_cast<int32_t>(offsetof(EnterPIN_t48ADD764A3FB09C507FD777FB1A570714C54C2A0, ___TextError_8)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_TextError_8() const { return ___TextError_8; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_TextError_8() { return &___TextError_8; }
	inline void set_TextError_8(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___TextError_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TextError_8), (void*)value);
	}
};


// EnterPINLang
struct  EnterPINLang_t7DB4141FE4619EA8936999CB62CFD95077E1B3D5  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.UI.Text EnterPINLang::Text1
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___Text1_5;
	// UnityEngine.UI.Text EnterPINLang::Text2
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___Text2_6;
	// UnityEngine.UI.Text EnterPINLang::Text3
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___Text3_7;
	// System.String[] EnterPINLang::pl
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___pl_8;
	// System.String[] EnterPINLang::en
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___en_9;
	// System.String[] EnterPINLang::de
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___de_10;
	// System.String[] EnterPINLang::fe
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___fe_11;
	// System.String[] EnterPINLang::es
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___es_12;
	// System.String[] EnterPINLang::ru
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___ru_13;

public:
	inline static int32_t get_offset_of_Text1_5() { return static_cast<int32_t>(offsetof(EnterPINLang_t7DB4141FE4619EA8936999CB62CFD95077E1B3D5, ___Text1_5)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_Text1_5() const { return ___Text1_5; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_Text1_5() { return &___Text1_5; }
	inline void set_Text1_5(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___Text1_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Text1_5), (void*)value);
	}

	inline static int32_t get_offset_of_Text2_6() { return static_cast<int32_t>(offsetof(EnterPINLang_t7DB4141FE4619EA8936999CB62CFD95077E1B3D5, ___Text2_6)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_Text2_6() const { return ___Text2_6; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_Text2_6() { return &___Text2_6; }
	inline void set_Text2_6(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___Text2_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Text2_6), (void*)value);
	}

	inline static int32_t get_offset_of_Text3_7() { return static_cast<int32_t>(offsetof(EnterPINLang_t7DB4141FE4619EA8936999CB62CFD95077E1B3D5, ___Text3_7)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_Text3_7() const { return ___Text3_7; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_Text3_7() { return &___Text3_7; }
	inline void set_Text3_7(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___Text3_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Text3_7), (void*)value);
	}

	inline static int32_t get_offset_of_pl_8() { return static_cast<int32_t>(offsetof(EnterPINLang_t7DB4141FE4619EA8936999CB62CFD95077E1B3D5, ___pl_8)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_pl_8() const { return ___pl_8; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_pl_8() { return &___pl_8; }
	inline void set_pl_8(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___pl_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pl_8), (void*)value);
	}

	inline static int32_t get_offset_of_en_9() { return static_cast<int32_t>(offsetof(EnterPINLang_t7DB4141FE4619EA8936999CB62CFD95077E1B3D5, ___en_9)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_en_9() const { return ___en_9; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_en_9() { return &___en_9; }
	inline void set_en_9(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___en_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___en_9), (void*)value);
	}

	inline static int32_t get_offset_of_de_10() { return static_cast<int32_t>(offsetof(EnterPINLang_t7DB4141FE4619EA8936999CB62CFD95077E1B3D5, ___de_10)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_de_10() const { return ___de_10; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_de_10() { return &___de_10; }
	inline void set_de_10(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___de_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___de_10), (void*)value);
	}

	inline static int32_t get_offset_of_fe_11() { return static_cast<int32_t>(offsetof(EnterPINLang_t7DB4141FE4619EA8936999CB62CFD95077E1B3D5, ___fe_11)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_fe_11() const { return ___fe_11; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_fe_11() { return &___fe_11; }
	inline void set_fe_11(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___fe_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fe_11), (void*)value);
	}

	inline static int32_t get_offset_of_es_12() { return static_cast<int32_t>(offsetof(EnterPINLang_t7DB4141FE4619EA8936999CB62CFD95077E1B3D5, ___es_12)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_es_12() const { return ___es_12; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_es_12() { return &___es_12; }
	inline void set_es_12(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___es_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___es_12), (void*)value);
	}

	inline static int32_t get_offset_of_ru_13() { return static_cast<int32_t>(offsetof(EnterPINLang_t7DB4141FE4619EA8936999CB62CFD95077E1B3D5, ___ru_13)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_ru_13() const { return ___ru_13; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_ru_13() { return &___ru_13; }
	inline void set_ru_13(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___ru_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ru_13), (void*)value);
	}
};

struct EnterPINLang_t7DB4141FE4619EA8936999CB62CFD95077E1B3D5_StaticFields
{
public:
	// EnterPINLang EnterPINLang::instance
	EnterPINLang_t7DB4141FE4619EA8936999CB62CFD95077E1B3D5 * ___instance_4;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(EnterPINLang_t7DB4141FE4619EA8936999CB62CFD95077E1B3D5_StaticFields, ___instance_4)); }
	inline EnterPINLang_t7DB4141FE4619EA8936999CB62CFD95077E1B3D5 * get_instance_4() const { return ___instance_4; }
	inline EnterPINLang_t7DB4141FE4619EA8936999CB62CFD95077E1B3D5 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(EnterPINLang_t7DB4141FE4619EA8936999CB62CFD95077E1B3D5 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instance_4), (void*)value);
	}
};


// GM
struct  GM_tFC30914A710042CAFEA7CF10E2AD01C9A529CEF0  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String GM::OneAward
	String_t* ___OneAward_5;
	// System.String GM::TwoAward
	String_t* ___TwoAward_6;
	// System.String GM::ThreeAward
	String_t* ___ThreeAward_7;
	// UnityEngine.UI.Text GM::txtScore
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___txtScore_8;
	// UnityEngine.UI.Text GM::DisplayOneAward
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___DisplayOneAward_9;
	// UnityEngine.UI.Text GM::DisplayTwoAward
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___DisplayTwoAward_10;
	// UnityEngine.UI.Text GM::DisplayThreeAward
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___DisplayThreeAward_11;
	// UnityEngine.UI.Text GM::TextError
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___TextError_12;

public:
	inline static int32_t get_offset_of_OneAward_5() { return static_cast<int32_t>(offsetof(GM_tFC30914A710042CAFEA7CF10E2AD01C9A529CEF0, ___OneAward_5)); }
	inline String_t* get_OneAward_5() const { return ___OneAward_5; }
	inline String_t** get_address_of_OneAward_5() { return &___OneAward_5; }
	inline void set_OneAward_5(String_t* value)
	{
		___OneAward_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OneAward_5), (void*)value);
	}

	inline static int32_t get_offset_of_TwoAward_6() { return static_cast<int32_t>(offsetof(GM_tFC30914A710042CAFEA7CF10E2AD01C9A529CEF0, ___TwoAward_6)); }
	inline String_t* get_TwoAward_6() const { return ___TwoAward_6; }
	inline String_t** get_address_of_TwoAward_6() { return &___TwoAward_6; }
	inline void set_TwoAward_6(String_t* value)
	{
		___TwoAward_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TwoAward_6), (void*)value);
	}

	inline static int32_t get_offset_of_ThreeAward_7() { return static_cast<int32_t>(offsetof(GM_tFC30914A710042CAFEA7CF10E2AD01C9A529CEF0, ___ThreeAward_7)); }
	inline String_t* get_ThreeAward_7() const { return ___ThreeAward_7; }
	inline String_t** get_address_of_ThreeAward_7() { return &___ThreeAward_7; }
	inline void set_ThreeAward_7(String_t* value)
	{
		___ThreeAward_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ThreeAward_7), (void*)value);
	}

	inline static int32_t get_offset_of_txtScore_8() { return static_cast<int32_t>(offsetof(GM_tFC30914A710042CAFEA7CF10E2AD01C9A529CEF0, ___txtScore_8)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_txtScore_8() const { return ___txtScore_8; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_txtScore_8() { return &___txtScore_8; }
	inline void set_txtScore_8(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___txtScore_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___txtScore_8), (void*)value);
	}

	inline static int32_t get_offset_of_DisplayOneAward_9() { return static_cast<int32_t>(offsetof(GM_tFC30914A710042CAFEA7CF10E2AD01C9A529CEF0, ___DisplayOneAward_9)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_DisplayOneAward_9() const { return ___DisplayOneAward_9; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_DisplayOneAward_9() { return &___DisplayOneAward_9; }
	inline void set_DisplayOneAward_9(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___DisplayOneAward_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DisplayOneAward_9), (void*)value);
	}

	inline static int32_t get_offset_of_DisplayTwoAward_10() { return static_cast<int32_t>(offsetof(GM_tFC30914A710042CAFEA7CF10E2AD01C9A529CEF0, ___DisplayTwoAward_10)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_DisplayTwoAward_10() const { return ___DisplayTwoAward_10; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_DisplayTwoAward_10() { return &___DisplayTwoAward_10; }
	inline void set_DisplayTwoAward_10(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___DisplayTwoAward_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DisplayTwoAward_10), (void*)value);
	}

	inline static int32_t get_offset_of_DisplayThreeAward_11() { return static_cast<int32_t>(offsetof(GM_tFC30914A710042CAFEA7CF10E2AD01C9A529CEF0, ___DisplayThreeAward_11)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_DisplayThreeAward_11() const { return ___DisplayThreeAward_11; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_DisplayThreeAward_11() { return &___DisplayThreeAward_11; }
	inline void set_DisplayThreeAward_11(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___DisplayThreeAward_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DisplayThreeAward_11), (void*)value);
	}

	inline static int32_t get_offset_of_TextError_12() { return static_cast<int32_t>(offsetof(GM_tFC30914A710042CAFEA7CF10E2AD01C9A529CEF0, ___TextError_12)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_TextError_12() const { return ___TextError_12; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_TextError_12() { return &___TextError_12; }
	inline void set_TextError_12(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___TextError_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TextError_12), (void*)value);
	}
};

struct GM_tFC30914A710042CAFEA7CF10E2AD01C9A529CEF0_StaticFields
{
public:
	// GM GM::instance
	GM_tFC30914A710042CAFEA7CF10E2AD01C9A529CEF0 * ___instance_4;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(GM_tFC30914A710042CAFEA7CF10E2AD01C9A529CEF0_StaticFields, ___instance_4)); }
	inline GM_tFC30914A710042CAFEA7CF10E2AD01C9A529CEF0 * get_instance_4() const { return ___instance_4; }
	inline GM_tFC30914A710042CAFEA7CF10E2AD01C9A529CEF0 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(GM_tFC30914A710042CAFEA7CF10E2AD01C9A529CEF0 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instance_4), (void*)value);
	}
};


// GameLang
struct  GameLang_t88B393D27387CFB1391819269B4FD02205959915  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.UI.Text GameLang::Score
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___Score_4;
	// UnityEngine.UI.Text GameLang::Start
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___Start_5;
	// UnityEngine.UI.Text GameLang::Stop
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___Stop_6;
	// UnityEngine.UI.Text GameLang::Menu
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___Menu_7;
	// System.String[] GameLang::pl
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___pl_8;
	// System.String[] GameLang::en
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___en_9;
	// System.String[] GameLang::de
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___de_10;
	// System.String[] GameLang::fe
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___fe_11;
	// System.String[] GameLang::es
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___es_12;
	// System.String[] GameLang::ru
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___ru_13;

public:
	inline static int32_t get_offset_of_Score_4() { return static_cast<int32_t>(offsetof(GameLang_t88B393D27387CFB1391819269B4FD02205959915, ___Score_4)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_Score_4() const { return ___Score_4; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_Score_4() { return &___Score_4; }
	inline void set_Score_4(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___Score_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Score_4), (void*)value);
	}

	inline static int32_t get_offset_of_Start_5() { return static_cast<int32_t>(offsetof(GameLang_t88B393D27387CFB1391819269B4FD02205959915, ___Start_5)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_Start_5() const { return ___Start_5; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_Start_5() { return &___Start_5; }
	inline void set_Start_5(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___Start_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Start_5), (void*)value);
	}

	inline static int32_t get_offset_of_Stop_6() { return static_cast<int32_t>(offsetof(GameLang_t88B393D27387CFB1391819269B4FD02205959915, ___Stop_6)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_Stop_6() const { return ___Stop_6; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_Stop_6() { return &___Stop_6; }
	inline void set_Stop_6(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___Stop_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Stop_6), (void*)value);
	}

	inline static int32_t get_offset_of_Menu_7() { return static_cast<int32_t>(offsetof(GameLang_t88B393D27387CFB1391819269B4FD02205959915, ___Menu_7)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_Menu_7() const { return ___Menu_7; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_Menu_7() { return &___Menu_7; }
	inline void set_Menu_7(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___Menu_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Menu_7), (void*)value);
	}

	inline static int32_t get_offset_of_pl_8() { return static_cast<int32_t>(offsetof(GameLang_t88B393D27387CFB1391819269B4FD02205959915, ___pl_8)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_pl_8() const { return ___pl_8; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_pl_8() { return &___pl_8; }
	inline void set_pl_8(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___pl_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pl_8), (void*)value);
	}

	inline static int32_t get_offset_of_en_9() { return static_cast<int32_t>(offsetof(GameLang_t88B393D27387CFB1391819269B4FD02205959915, ___en_9)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_en_9() const { return ___en_9; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_en_9() { return &___en_9; }
	inline void set_en_9(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___en_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___en_9), (void*)value);
	}

	inline static int32_t get_offset_of_de_10() { return static_cast<int32_t>(offsetof(GameLang_t88B393D27387CFB1391819269B4FD02205959915, ___de_10)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_de_10() const { return ___de_10; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_de_10() { return &___de_10; }
	inline void set_de_10(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___de_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___de_10), (void*)value);
	}

	inline static int32_t get_offset_of_fe_11() { return static_cast<int32_t>(offsetof(GameLang_t88B393D27387CFB1391819269B4FD02205959915, ___fe_11)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_fe_11() const { return ___fe_11; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_fe_11() { return &___fe_11; }
	inline void set_fe_11(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___fe_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fe_11), (void*)value);
	}

	inline static int32_t get_offset_of_es_12() { return static_cast<int32_t>(offsetof(GameLang_t88B393D27387CFB1391819269B4FD02205959915, ___es_12)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_es_12() const { return ___es_12; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_es_12() { return &___es_12; }
	inline void set_es_12(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___es_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___es_12), (void*)value);
	}

	inline static int32_t get_offset_of_ru_13() { return static_cast<int32_t>(offsetof(GameLang_t88B393D27387CFB1391819269B4FD02205959915, ___ru_13)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_ru_13() const { return ___ru_13; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_ru_13() { return &___ru_13; }
	inline void set_ru_13(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___ru_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ru_13), (void*)value);
	}
};


// GameMenuLang
struct  GameMenuLang_t49BEAA2ABAFC0A899B3346BEF7C050C4212D467C  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.UI.Text GameMenuLang::Score
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___Score_4;
	// UnityEngine.UI.Text GameMenuLang::Text1
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___Text1_5;
	// UnityEngine.UI.Text GameMenuLang::Text2
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___Text2_6;
	// UnityEngine.UI.Text GameMenuLang::Text3
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___Text3_7;
	// UnityEngine.UI.Image GameMenuLang::Flag
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___Flag_8;
	// UnityEngine.Sprite[] GameMenuLang::Flags
	SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77* ___Flags_9;
	// System.String[] GameMenuLang::pl
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___pl_10;
	// System.String[] GameMenuLang::en
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___en_11;
	// System.String[] GameMenuLang::de
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___de_12;
	// System.String[] GameMenuLang::fe
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___fe_13;
	// System.String[] GameMenuLang::es
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___es_14;
	// System.String[] GameMenuLang::ru
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___ru_15;

public:
	inline static int32_t get_offset_of_Score_4() { return static_cast<int32_t>(offsetof(GameMenuLang_t49BEAA2ABAFC0A899B3346BEF7C050C4212D467C, ___Score_4)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_Score_4() const { return ___Score_4; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_Score_4() { return &___Score_4; }
	inline void set_Score_4(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___Score_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Score_4), (void*)value);
	}

	inline static int32_t get_offset_of_Text1_5() { return static_cast<int32_t>(offsetof(GameMenuLang_t49BEAA2ABAFC0A899B3346BEF7C050C4212D467C, ___Text1_5)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_Text1_5() const { return ___Text1_5; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_Text1_5() { return &___Text1_5; }
	inline void set_Text1_5(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___Text1_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Text1_5), (void*)value);
	}

	inline static int32_t get_offset_of_Text2_6() { return static_cast<int32_t>(offsetof(GameMenuLang_t49BEAA2ABAFC0A899B3346BEF7C050C4212D467C, ___Text2_6)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_Text2_6() const { return ___Text2_6; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_Text2_6() { return &___Text2_6; }
	inline void set_Text2_6(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___Text2_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Text2_6), (void*)value);
	}

	inline static int32_t get_offset_of_Text3_7() { return static_cast<int32_t>(offsetof(GameMenuLang_t49BEAA2ABAFC0A899B3346BEF7C050C4212D467C, ___Text3_7)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_Text3_7() const { return ___Text3_7; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_Text3_7() { return &___Text3_7; }
	inline void set_Text3_7(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___Text3_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Text3_7), (void*)value);
	}

	inline static int32_t get_offset_of_Flag_8() { return static_cast<int32_t>(offsetof(GameMenuLang_t49BEAA2ABAFC0A899B3346BEF7C050C4212D467C, ___Flag_8)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_Flag_8() const { return ___Flag_8; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_Flag_8() { return &___Flag_8; }
	inline void set_Flag_8(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___Flag_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Flag_8), (void*)value);
	}

	inline static int32_t get_offset_of_Flags_9() { return static_cast<int32_t>(offsetof(GameMenuLang_t49BEAA2ABAFC0A899B3346BEF7C050C4212D467C, ___Flags_9)); }
	inline SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77* get_Flags_9() const { return ___Flags_9; }
	inline SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77** get_address_of_Flags_9() { return &___Flags_9; }
	inline void set_Flags_9(SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77* value)
	{
		___Flags_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Flags_9), (void*)value);
	}

	inline static int32_t get_offset_of_pl_10() { return static_cast<int32_t>(offsetof(GameMenuLang_t49BEAA2ABAFC0A899B3346BEF7C050C4212D467C, ___pl_10)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_pl_10() const { return ___pl_10; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_pl_10() { return &___pl_10; }
	inline void set_pl_10(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___pl_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pl_10), (void*)value);
	}

	inline static int32_t get_offset_of_en_11() { return static_cast<int32_t>(offsetof(GameMenuLang_t49BEAA2ABAFC0A899B3346BEF7C050C4212D467C, ___en_11)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_en_11() const { return ___en_11; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_en_11() { return &___en_11; }
	inline void set_en_11(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___en_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___en_11), (void*)value);
	}

	inline static int32_t get_offset_of_de_12() { return static_cast<int32_t>(offsetof(GameMenuLang_t49BEAA2ABAFC0A899B3346BEF7C050C4212D467C, ___de_12)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_de_12() const { return ___de_12; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_de_12() { return &___de_12; }
	inline void set_de_12(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___de_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___de_12), (void*)value);
	}

	inline static int32_t get_offset_of_fe_13() { return static_cast<int32_t>(offsetof(GameMenuLang_t49BEAA2ABAFC0A899B3346BEF7C050C4212D467C, ___fe_13)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_fe_13() const { return ___fe_13; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_fe_13() { return &___fe_13; }
	inline void set_fe_13(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___fe_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fe_13), (void*)value);
	}

	inline static int32_t get_offset_of_es_14() { return static_cast<int32_t>(offsetof(GameMenuLang_t49BEAA2ABAFC0A899B3346BEF7C050C4212D467C, ___es_14)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_es_14() const { return ___es_14; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_es_14() { return &___es_14; }
	inline void set_es_14(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___es_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___es_14), (void*)value);
	}

	inline static int32_t get_offset_of_ru_15() { return static_cast<int32_t>(offsetof(GameMenuLang_t49BEAA2ABAFC0A899B3346BEF7C050C4212D467C, ___ru_15)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_ru_15() const { return ___ru_15; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_ru_15() { return &___ru_15; }
	inline void set_ru_15(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___ru_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ru_15), (void*)value);
	}
};


// MainMenu
struct  MainMenu_tEB11F5A993C42E93B585FBB65C9E92EC91C5707C  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// MoveScenes
struct  MoveScenes_t757DBF243D1177806463417C842AF5884F8C7C17  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// MultiTable
struct  MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.AudioSource MultiTable::audios
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ___audios_5;
	// UnityEngine.AudioClip[] MultiTable::Fileaudios
	AudioClipU5BU5D_t9BA3E7C4B62164BA2E054F3A82CD6CE6EB273CBE* ___Fileaudios_6;
	// UnityEngine.UI.Button MultiTable::btnStartGame
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___btnStartGame_7;
	// UnityEngine.UI.Button MultiTable::btnStopGame
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___btnStopGame_8;
	// UnityEngine.GameObject MultiTable::Sounds
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___Sounds_9;
	// UnityEngine.GameObject MultiTable::MuteSound
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___MuteSound_10;
	// UnityEngine.UI.Text MultiTable::txtScore
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___txtScore_11;
	// UnityEngine.UI.Text MultiTable::txtTimer
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___txtTimer_12;
	// UnityEngine.UI.Text[] MultiTable::Buttons
	TextU5BU5D_t16DD1967B137EC602803C77DBB246B05B3D0275F* ___Buttons_13;
	// UnityEngine.UI.Text MultiTable::txtNum1
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___txtNum1_14;
	// UnityEngine.UI.Text MultiTable::txtNum2
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___txtNum2_15;
	// UnityEngine.UI.Text MultiTable::txtAnswer1
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___txtAnswer1_16;
	// UnityEngine.UI.Text MultiTable::txtAnswer2
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___txtAnswer2_17;
	// UnityEngine.UI.Text MultiTable::txtAnswer3
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___txtAnswer3_18;
	// UnityEngine.UI.Text MultiTable::txtResult
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___txtResult_19;
	// System.Collections.Generic.List`1<System.Int32> MultiTable::answers
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___answers_20;
	// System.Single MultiTable::timeRemaining
	float ___timeRemaining_21;
	// System.Boolean MultiTable::isStartGame
	bool ___isStartGame_22;
	// System.Int32 MultiTable::Points
	int32_t ___Points_23;
	// System.Int32 MultiTable::num1
	int32_t ___num1_24;
	// System.Int32 MultiTable::num2
	int32_t ___num2_25;
	// System.Int32 MultiTable::answer0
	int32_t ___answer0_26;
	// System.Int32 MultiTable::answer1
	int32_t ___answer1_27;
	// System.Int32 MultiTable::answer2
	int32_t ___answer2_28;
	// System.Int32 MultiTable::answer3
	int32_t ___answer3_29;
	// System.Int32 MultiTable::index
	int32_t ___index_30;

public:
	inline static int32_t get_offset_of_audios_5() { return static_cast<int32_t>(offsetof(MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206, ___audios_5)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get_audios_5() const { return ___audios_5; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of_audios_5() { return &___audios_5; }
	inline void set_audios_5(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		___audios_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___audios_5), (void*)value);
	}

	inline static int32_t get_offset_of_Fileaudios_6() { return static_cast<int32_t>(offsetof(MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206, ___Fileaudios_6)); }
	inline AudioClipU5BU5D_t9BA3E7C4B62164BA2E054F3A82CD6CE6EB273CBE* get_Fileaudios_6() const { return ___Fileaudios_6; }
	inline AudioClipU5BU5D_t9BA3E7C4B62164BA2E054F3A82CD6CE6EB273CBE** get_address_of_Fileaudios_6() { return &___Fileaudios_6; }
	inline void set_Fileaudios_6(AudioClipU5BU5D_t9BA3E7C4B62164BA2E054F3A82CD6CE6EB273CBE* value)
	{
		___Fileaudios_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Fileaudios_6), (void*)value);
	}

	inline static int32_t get_offset_of_btnStartGame_7() { return static_cast<int32_t>(offsetof(MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206, ___btnStartGame_7)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_btnStartGame_7() const { return ___btnStartGame_7; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_btnStartGame_7() { return &___btnStartGame_7; }
	inline void set_btnStartGame_7(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___btnStartGame_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___btnStartGame_7), (void*)value);
	}

	inline static int32_t get_offset_of_btnStopGame_8() { return static_cast<int32_t>(offsetof(MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206, ___btnStopGame_8)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_btnStopGame_8() const { return ___btnStopGame_8; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_btnStopGame_8() { return &___btnStopGame_8; }
	inline void set_btnStopGame_8(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___btnStopGame_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___btnStopGame_8), (void*)value);
	}

	inline static int32_t get_offset_of_Sounds_9() { return static_cast<int32_t>(offsetof(MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206, ___Sounds_9)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_Sounds_9() const { return ___Sounds_9; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_Sounds_9() { return &___Sounds_9; }
	inline void set_Sounds_9(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___Sounds_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Sounds_9), (void*)value);
	}

	inline static int32_t get_offset_of_MuteSound_10() { return static_cast<int32_t>(offsetof(MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206, ___MuteSound_10)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_MuteSound_10() const { return ___MuteSound_10; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_MuteSound_10() { return &___MuteSound_10; }
	inline void set_MuteSound_10(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___MuteSound_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MuteSound_10), (void*)value);
	}

	inline static int32_t get_offset_of_txtScore_11() { return static_cast<int32_t>(offsetof(MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206, ___txtScore_11)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_txtScore_11() const { return ___txtScore_11; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_txtScore_11() { return &___txtScore_11; }
	inline void set_txtScore_11(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___txtScore_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___txtScore_11), (void*)value);
	}

	inline static int32_t get_offset_of_txtTimer_12() { return static_cast<int32_t>(offsetof(MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206, ___txtTimer_12)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_txtTimer_12() const { return ___txtTimer_12; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_txtTimer_12() { return &___txtTimer_12; }
	inline void set_txtTimer_12(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___txtTimer_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___txtTimer_12), (void*)value);
	}

	inline static int32_t get_offset_of_Buttons_13() { return static_cast<int32_t>(offsetof(MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206, ___Buttons_13)); }
	inline TextU5BU5D_t16DD1967B137EC602803C77DBB246B05B3D0275F* get_Buttons_13() const { return ___Buttons_13; }
	inline TextU5BU5D_t16DD1967B137EC602803C77DBB246B05B3D0275F** get_address_of_Buttons_13() { return &___Buttons_13; }
	inline void set_Buttons_13(TextU5BU5D_t16DD1967B137EC602803C77DBB246B05B3D0275F* value)
	{
		___Buttons_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Buttons_13), (void*)value);
	}

	inline static int32_t get_offset_of_txtNum1_14() { return static_cast<int32_t>(offsetof(MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206, ___txtNum1_14)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_txtNum1_14() const { return ___txtNum1_14; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_txtNum1_14() { return &___txtNum1_14; }
	inline void set_txtNum1_14(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___txtNum1_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___txtNum1_14), (void*)value);
	}

	inline static int32_t get_offset_of_txtNum2_15() { return static_cast<int32_t>(offsetof(MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206, ___txtNum2_15)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_txtNum2_15() const { return ___txtNum2_15; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_txtNum2_15() { return &___txtNum2_15; }
	inline void set_txtNum2_15(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___txtNum2_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___txtNum2_15), (void*)value);
	}

	inline static int32_t get_offset_of_txtAnswer1_16() { return static_cast<int32_t>(offsetof(MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206, ___txtAnswer1_16)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_txtAnswer1_16() const { return ___txtAnswer1_16; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_txtAnswer1_16() { return &___txtAnswer1_16; }
	inline void set_txtAnswer1_16(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___txtAnswer1_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___txtAnswer1_16), (void*)value);
	}

	inline static int32_t get_offset_of_txtAnswer2_17() { return static_cast<int32_t>(offsetof(MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206, ___txtAnswer2_17)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_txtAnswer2_17() const { return ___txtAnswer2_17; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_txtAnswer2_17() { return &___txtAnswer2_17; }
	inline void set_txtAnswer2_17(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___txtAnswer2_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___txtAnswer2_17), (void*)value);
	}

	inline static int32_t get_offset_of_txtAnswer3_18() { return static_cast<int32_t>(offsetof(MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206, ___txtAnswer3_18)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_txtAnswer3_18() const { return ___txtAnswer3_18; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_txtAnswer3_18() { return &___txtAnswer3_18; }
	inline void set_txtAnswer3_18(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___txtAnswer3_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___txtAnswer3_18), (void*)value);
	}

	inline static int32_t get_offset_of_txtResult_19() { return static_cast<int32_t>(offsetof(MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206, ___txtResult_19)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_txtResult_19() const { return ___txtResult_19; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_txtResult_19() { return &___txtResult_19; }
	inline void set_txtResult_19(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___txtResult_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___txtResult_19), (void*)value);
	}

	inline static int32_t get_offset_of_answers_20() { return static_cast<int32_t>(offsetof(MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206, ___answers_20)); }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * get_answers_20() const { return ___answers_20; }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 ** get_address_of_answers_20() { return &___answers_20; }
	inline void set_answers_20(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * value)
	{
		___answers_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___answers_20), (void*)value);
	}

	inline static int32_t get_offset_of_timeRemaining_21() { return static_cast<int32_t>(offsetof(MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206, ___timeRemaining_21)); }
	inline float get_timeRemaining_21() const { return ___timeRemaining_21; }
	inline float* get_address_of_timeRemaining_21() { return &___timeRemaining_21; }
	inline void set_timeRemaining_21(float value)
	{
		___timeRemaining_21 = value;
	}

	inline static int32_t get_offset_of_isStartGame_22() { return static_cast<int32_t>(offsetof(MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206, ___isStartGame_22)); }
	inline bool get_isStartGame_22() const { return ___isStartGame_22; }
	inline bool* get_address_of_isStartGame_22() { return &___isStartGame_22; }
	inline void set_isStartGame_22(bool value)
	{
		___isStartGame_22 = value;
	}

	inline static int32_t get_offset_of_Points_23() { return static_cast<int32_t>(offsetof(MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206, ___Points_23)); }
	inline int32_t get_Points_23() const { return ___Points_23; }
	inline int32_t* get_address_of_Points_23() { return &___Points_23; }
	inline void set_Points_23(int32_t value)
	{
		___Points_23 = value;
	}

	inline static int32_t get_offset_of_num1_24() { return static_cast<int32_t>(offsetof(MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206, ___num1_24)); }
	inline int32_t get_num1_24() const { return ___num1_24; }
	inline int32_t* get_address_of_num1_24() { return &___num1_24; }
	inline void set_num1_24(int32_t value)
	{
		___num1_24 = value;
	}

	inline static int32_t get_offset_of_num2_25() { return static_cast<int32_t>(offsetof(MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206, ___num2_25)); }
	inline int32_t get_num2_25() const { return ___num2_25; }
	inline int32_t* get_address_of_num2_25() { return &___num2_25; }
	inline void set_num2_25(int32_t value)
	{
		___num2_25 = value;
	}

	inline static int32_t get_offset_of_answer0_26() { return static_cast<int32_t>(offsetof(MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206, ___answer0_26)); }
	inline int32_t get_answer0_26() const { return ___answer0_26; }
	inline int32_t* get_address_of_answer0_26() { return &___answer0_26; }
	inline void set_answer0_26(int32_t value)
	{
		___answer0_26 = value;
	}

	inline static int32_t get_offset_of_answer1_27() { return static_cast<int32_t>(offsetof(MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206, ___answer1_27)); }
	inline int32_t get_answer1_27() const { return ___answer1_27; }
	inline int32_t* get_address_of_answer1_27() { return &___answer1_27; }
	inline void set_answer1_27(int32_t value)
	{
		___answer1_27 = value;
	}

	inline static int32_t get_offset_of_answer2_28() { return static_cast<int32_t>(offsetof(MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206, ___answer2_28)); }
	inline int32_t get_answer2_28() const { return ___answer2_28; }
	inline int32_t* get_address_of_answer2_28() { return &___answer2_28; }
	inline void set_answer2_28(int32_t value)
	{
		___answer2_28 = value;
	}

	inline static int32_t get_offset_of_answer3_29() { return static_cast<int32_t>(offsetof(MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206, ___answer3_29)); }
	inline int32_t get_answer3_29() const { return ___answer3_29; }
	inline int32_t* get_address_of_answer3_29() { return &___answer3_29; }
	inline void set_answer3_29(int32_t value)
	{
		___answer3_29 = value;
	}

	inline static int32_t get_offset_of_index_30() { return static_cast<int32_t>(offsetof(MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206, ___index_30)); }
	inline int32_t get_index_30() const { return ___index_30; }
	inline int32_t* get_address_of_index_30() { return &___index_30; }
	inline void set_index_30(int32_t value)
	{
		___index_30 = value;
	}
};

struct MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206_StaticFields
{
public:
	// MultiTable MultiTable::instance
	MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206 * ___instance_4;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206_StaticFields, ___instance_4)); }
	inline MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206 * get_instance_4() const { return ___instance_4; }
	inline MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instance_4), (void*)value);
	}
};


// OneAwardLang
struct  OneAwardLang_t98E602972882C2F997D3285FD94AE10CA89D1CB0  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.UI.Text OneAwardLang::Text1
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___Text1_4;
	// UnityEngine.UI.Text OneAwardLang::Text2
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___Text2_5;
	// UnityEngine.UI.Text OneAwardLang::Text3
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___Text3_6;
	// UnityEngine.UI.Text OneAwardLang::Text4
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___Text4_7;
	// System.String[] OneAwardLang::pl
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___pl_8;
	// System.String[] OneAwardLang::en
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___en_9;
	// System.String[] OneAwardLang::de
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___de_10;
	// System.String[] OneAwardLang::fe
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___fe_11;
	// System.String[] OneAwardLang::es
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___es_12;
	// System.String[] OneAwardLang::ru
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___ru_13;

public:
	inline static int32_t get_offset_of_Text1_4() { return static_cast<int32_t>(offsetof(OneAwardLang_t98E602972882C2F997D3285FD94AE10CA89D1CB0, ___Text1_4)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_Text1_4() const { return ___Text1_4; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_Text1_4() { return &___Text1_4; }
	inline void set_Text1_4(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___Text1_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Text1_4), (void*)value);
	}

	inline static int32_t get_offset_of_Text2_5() { return static_cast<int32_t>(offsetof(OneAwardLang_t98E602972882C2F997D3285FD94AE10CA89D1CB0, ___Text2_5)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_Text2_5() const { return ___Text2_5; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_Text2_5() { return &___Text2_5; }
	inline void set_Text2_5(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___Text2_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Text2_5), (void*)value);
	}

	inline static int32_t get_offset_of_Text3_6() { return static_cast<int32_t>(offsetof(OneAwardLang_t98E602972882C2F997D3285FD94AE10CA89D1CB0, ___Text3_6)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_Text3_6() const { return ___Text3_6; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_Text3_6() { return &___Text3_6; }
	inline void set_Text3_6(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___Text3_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Text3_6), (void*)value);
	}

	inline static int32_t get_offset_of_Text4_7() { return static_cast<int32_t>(offsetof(OneAwardLang_t98E602972882C2F997D3285FD94AE10CA89D1CB0, ___Text4_7)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_Text4_7() const { return ___Text4_7; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_Text4_7() { return &___Text4_7; }
	inline void set_Text4_7(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___Text4_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Text4_7), (void*)value);
	}

	inline static int32_t get_offset_of_pl_8() { return static_cast<int32_t>(offsetof(OneAwardLang_t98E602972882C2F997D3285FD94AE10CA89D1CB0, ___pl_8)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_pl_8() const { return ___pl_8; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_pl_8() { return &___pl_8; }
	inline void set_pl_8(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___pl_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pl_8), (void*)value);
	}

	inline static int32_t get_offset_of_en_9() { return static_cast<int32_t>(offsetof(OneAwardLang_t98E602972882C2F997D3285FD94AE10CA89D1CB0, ___en_9)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_en_9() const { return ___en_9; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_en_9() { return &___en_9; }
	inline void set_en_9(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___en_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___en_9), (void*)value);
	}

	inline static int32_t get_offset_of_de_10() { return static_cast<int32_t>(offsetof(OneAwardLang_t98E602972882C2F997D3285FD94AE10CA89D1CB0, ___de_10)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_de_10() const { return ___de_10; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_de_10() { return &___de_10; }
	inline void set_de_10(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___de_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___de_10), (void*)value);
	}

	inline static int32_t get_offset_of_fe_11() { return static_cast<int32_t>(offsetof(OneAwardLang_t98E602972882C2F997D3285FD94AE10CA89D1CB0, ___fe_11)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_fe_11() const { return ___fe_11; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_fe_11() { return &___fe_11; }
	inline void set_fe_11(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___fe_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fe_11), (void*)value);
	}

	inline static int32_t get_offset_of_es_12() { return static_cast<int32_t>(offsetof(OneAwardLang_t98E602972882C2F997D3285FD94AE10CA89D1CB0, ___es_12)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_es_12() const { return ___es_12; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_es_12() { return &___es_12; }
	inline void set_es_12(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___es_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___es_12), (void*)value);
	}

	inline static int32_t get_offset_of_ru_13() { return static_cast<int32_t>(offsetof(OneAwardLang_t98E602972882C2F997D3285FD94AE10CA89D1CB0, ___ru_13)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_ru_13() const { return ___ru_13; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_ru_13() { return &___ru_13; }
	inline void set_ru_13(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___ru_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ru_13), (void*)value);
	}
};


// OperateOneAward
struct  OperateOneAward_t30BCE36800FCC2888072B07ED15106D80175AB80  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String OperateOneAward::OneAward
	String_t* ___OneAward_4;
	// UnityEngine.GameObject OperateOneAward::OneInputField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___OneInputField_5;
	// UnityEngine.GameObject OperateOneAward::TextDisplayOneAward
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___TextDisplayOneAward_6;
	// UnityEngine.UI.Button OperateOneAward::btnNext
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___btnNext_7;

public:
	inline static int32_t get_offset_of_OneAward_4() { return static_cast<int32_t>(offsetof(OperateOneAward_t30BCE36800FCC2888072B07ED15106D80175AB80, ___OneAward_4)); }
	inline String_t* get_OneAward_4() const { return ___OneAward_4; }
	inline String_t** get_address_of_OneAward_4() { return &___OneAward_4; }
	inline void set_OneAward_4(String_t* value)
	{
		___OneAward_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OneAward_4), (void*)value);
	}

	inline static int32_t get_offset_of_OneInputField_5() { return static_cast<int32_t>(offsetof(OperateOneAward_t30BCE36800FCC2888072B07ED15106D80175AB80, ___OneInputField_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_OneInputField_5() const { return ___OneInputField_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_OneInputField_5() { return &___OneInputField_5; }
	inline void set_OneInputField_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___OneInputField_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OneInputField_5), (void*)value);
	}

	inline static int32_t get_offset_of_TextDisplayOneAward_6() { return static_cast<int32_t>(offsetof(OperateOneAward_t30BCE36800FCC2888072B07ED15106D80175AB80, ___TextDisplayOneAward_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_TextDisplayOneAward_6() const { return ___TextDisplayOneAward_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_TextDisplayOneAward_6() { return &___TextDisplayOneAward_6; }
	inline void set_TextDisplayOneAward_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___TextDisplayOneAward_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TextDisplayOneAward_6), (void*)value);
	}

	inline static int32_t get_offset_of_btnNext_7() { return static_cast<int32_t>(offsetof(OperateOneAward_t30BCE36800FCC2888072B07ED15106D80175AB80, ___btnNext_7)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_btnNext_7() const { return ___btnNext_7; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_btnNext_7() { return &___btnNext_7; }
	inline void set_btnNext_7(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___btnNext_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___btnNext_7), (void*)value);
	}
};


// OperateThreeAward
struct  OperateThreeAward_t647790567BA6B786988084DF0A6C9E4E6606BC17  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String OperateThreeAward::ThreeAward
	String_t* ___ThreeAward_4;
	// UnityEngine.GameObject OperateThreeAward::ThreeInputField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___ThreeInputField_5;
	// UnityEngine.GameObject OperateThreeAward::TextDisplayThreeAward
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___TextDisplayThreeAward_6;

public:
	inline static int32_t get_offset_of_ThreeAward_4() { return static_cast<int32_t>(offsetof(OperateThreeAward_t647790567BA6B786988084DF0A6C9E4E6606BC17, ___ThreeAward_4)); }
	inline String_t* get_ThreeAward_4() const { return ___ThreeAward_4; }
	inline String_t** get_address_of_ThreeAward_4() { return &___ThreeAward_4; }
	inline void set_ThreeAward_4(String_t* value)
	{
		___ThreeAward_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ThreeAward_4), (void*)value);
	}

	inline static int32_t get_offset_of_ThreeInputField_5() { return static_cast<int32_t>(offsetof(OperateThreeAward_t647790567BA6B786988084DF0A6C9E4E6606BC17, ___ThreeInputField_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_ThreeInputField_5() const { return ___ThreeInputField_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_ThreeInputField_5() { return &___ThreeInputField_5; }
	inline void set_ThreeInputField_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___ThreeInputField_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ThreeInputField_5), (void*)value);
	}

	inline static int32_t get_offset_of_TextDisplayThreeAward_6() { return static_cast<int32_t>(offsetof(OperateThreeAward_t647790567BA6B786988084DF0A6C9E4E6606BC17, ___TextDisplayThreeAward_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_TextDisplayThreeAward_6() const { return ___TextDisplayThreeAward_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_TextDisplayThreeAward_6() { return &___TextDisplayThreeAward_6; }
	inline void set_TextDisplayThreeAward_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___TextDisplayThreeAward_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TextDisplayThreeAward_6), (void*)value);
	}
};


// OperateTwoAward
struct  OperateTwoAward_t2378CD21BAB017639BC691A4F64BEE3B42DC528D  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String OperateTwoAward::TwoAward
	String_t* ___TwoAward_4;
	// UnityEngine.GameObject OperateTwoAward::TwoInputField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___TwoInputField_5;
	// UnityEngine.GameObject OperateTwoAward::TextDisplayTwoAward
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___TextDisplayTwoAward_6;

public:
	inline static int32_t get_offset_of_TwoAward_4() { return static_cast<int32_t>(offsetof(OperateTwoAward_t2378CD21BAB017639BC691A4F64BEE3B42DC528D, ___TwoAward_4)); }
	inline String_t* get_TwoAward_4() const { return ___TwoAward_4; }
	inline String_t** get_address_of_TwoAward_4() { return &___TwoAward_4; }
	inline void set_TwoAward_4(String_t* value)
	{
		___TwoAward_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TwoAward_4), (void*)value);
	}

	inline static int32_t get_offset_of_TwoInputField_5() { return static_cast<int32_t>(offsetof(OperateTwoAward_t2378CD21BAB017639BC691A4F64BEE3B42DC528D, ___TwoInputField_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_TwoInputField_5() const { return ___TwoInputField_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_TwoInputField_5() { return &___TwoInputField_5; }
	inline void set_TwoInputField_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___TwoInputField_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TwoInputField_5), (void*)value);
	}

	inline static int32_t get_offset_of_TextDisplayTwoAward_6() { return static_cast<int32_t>(offsetof(OperateTwoAward_t2378CD21BAB017639BC691A4F64BEE3B42DC528D, ___TextDisplayTwoAward_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_TextDisplayTwoAward_6() const { return ___TextDisplayTwoAward_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_TextDisplayTwoAward_6() { return &___TextDisplayTwoAward_6; }
	inline void set_TextDisplayTwoAward_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___TextDisplayTwoAward_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TextDisplayTwoAward_6), (void*)value);
	}
};


// Settings
struct  Settings_t9AEAFFBA64B4FC9ECF724519DF460F19962F4495  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String Settings::PIN1
	String_t* ___PIN1_4;
	// System.String Settings::PIN2
	String_t* ___PIN2_5;
	// UnityEngine.GameObject Settings::PIN1InputField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___PIN1InputField_6;
	// UnityEngine.GameObject Settings::PIN2InputField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___PIN2InputField_7;
	// UnityEngine.GameObject Settings::TextDisplayPIN1
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___TextDisplayPIN1_8;
	// UnityEngine.GameObject Settings::TextDisplayPIN2
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___TextDisplayPIN2_9;
	// UnityEngine.UI.Text Settings::TextError
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___TextError_10;

public:
	inline static int32_t get_offset_of_PIN1_4() { return static_cast<int32_t>(offsetof(Settings_t9AEAFFBA64B4FC9ECF724519DF460F19962F4495, ___PIN1_4)); }
	inline String_t* get_PIN1_4() const { return ___PIN1_4; }
	inline String_t** get_address_of_PIN1_4() { return &___PIN1_4; }
	inline void set_PIN1_4(String_t* value)
	{
		___PIN1_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PIN1_4), (void*)value);
	}

	inline static int32_t get_offset_of_PIN2_5() { return static_cast<int32_t>(offsetof(Settings_t9AEAFFBA64B4FC9ECF724519DF460F19962F4495, ___PIN2_5)); }
	inline String_t* get_PIN2_5() const { return ___PIN2_5; }
	inline String_t** get_address_of_PIN2_5() { return &___PIN2_5; }
	inline void set_PIN2_5(String_t* value)
	{
		___PIN2_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PIN2_5), (void*)value);
	}

	inline static int32_t get_offset_of_PIN1InputField_6() { return static_cast<int32_t>(offsetof(Settings_t9AEAFFBA64B4FC9ECF724519DF460F19962F4495, ___PIN1InputField_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_PIN1InputField_6() const { return ___PIN1InputField_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_PIN1InputField_6() { return &___PIN1InputField_6; }
	inline void set_PIN1InputField_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___PIN1InputField_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PIN1InputField_6), (void*)value);
	}

	inline static int32_t get_offset_of_PIN2InputField_7() { return static_cast<int32_t>(offsetof(Settings_t9AEAFFBA64B4FC9ECF724519DF460F19962F4495, ___PIN2InputField_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_PIN2InputField_7() const { return ___PIN2InputField_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_PIN2InputField_7() { return &___PIN2InputField_7; }
	inline void set_PIN2InputField_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___PIN2InputField_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PIN2InputField_7), (void*)value);
	}

	inline static int32_t get_offset_of_TextDisplayPIN1_8() { return static_cast<int32_t>(offsetof(Settings_t9AEAFFBA64B4FC9ECF724519DF460F19962F4495, ___TextDisplayPIN1_8)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_TextDisplayPIN1_8() const { return ___TextDisplayPIN1_8; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_TextDisplayPIN1_8() { return &___TextDisplayPIN1_8; }
	inline void set_TextDisplayPIN1_8(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___TextDisplayPIN1_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TextDisplayPIN1_8), (void*)value);
	}

	inline static int32_t get_offset_of_TextDisplayPIN2_9() { return static_cast<int32_t>(offsetof(Settings_t9AEAFFBA64B4FC9ECF724519DF460F19962F4495, ___TextDisplayPIN2_9)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_TextDisplayPIN2_9() const { return ___TextDisplayPIN2_9; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_TextDisplayPIN2_9() { return &___TextDisplayPIN2_9; }
	inline void set_TextDisplayPIN2_9(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___TextDisplayPIN2_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TextDisplayPIN2_9), (void*)value);
	}

	inline static int32_t get_offset_of_TextError_10() { return static_cast<int32_t>(offsetof(Settings_t9AEAFFBA64B4FC9ECF724519DF460F19962F4495, ___TextError_10)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_TextError_10() const { return ___TextError_10; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_TextError_10() { return &___TextError_10; }
	inline void set_TextError_10(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___TextError_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TextError_10), (void*)value);
	}
};


// SettingsLang
struct  SettingsLang_tEC296FA53D4D04D71C9F9F8E0316113CC0DA7B62  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.UI.Text SettingsLang::Text1
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___Text1_4;
	// UnityEngine.UI.Text SettingsLang::Text2
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___Text2_5;
	// UnityEngine.UI.Text SettingsLang::Text3
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___Text3_6;
	// UnityEngine.UI.Text SettingsLang::Text4
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___Text4_7;
	// System.String[] SettingsLang::pl
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___pl_8;
	// System.String[] SettingsLang::en
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___en_9;
	// System.String[] SettingsLang::de
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___de_10;
	// System.String[] SettingsLang::fe
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___fe_11;
	// System.String[] SettingsLang::es
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___es_12;
	// System.String[] SettingsLang::ru
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___ru_13;

public:
	inline static int32_t get_offset_of_Text1_4() { return static_cast<int32_t>(offsetof(SettingsLang_tEC296FA53D4D04D71C9F9F8E0316113CC0DA7B62, ___Text1_4)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_Text1_4() const { return ___Text1_4; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_Text1_4() { return &___Text1_4; }
	inline void set_Text1_4(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___Text1_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Text1_4), (void*)value);
	}

	inline static int32_t get_offset_of_Text2_5() { return static_cast<int32_t>(offsetof(SettingsLang_tEC296FA53D4D04D71C9F9F8E0316113CC0DA7B62, ___Text2_5)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_Text2_5() const { return ___Text2_5; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_Text2_5() { return &___Text2_5; }
	inline void set_Text2_5(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___Text2_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Text2_5), (void*)value);
	}

	inline static int32_t get_offset_of_Text3_6() { return static_cast<int32_t>(offsetof(SettingsLang_tEC296FA53D4D04D71C9F9F8E0316113CC0DA7B62, ___Text3_6)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_Text3_6() const { return ___Text3_6; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_Text3_6() { return &___Text3_6; }
	inline void set_Text3_6(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___Text3_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Text3_6), (void*)value);
	}

	inline static int32_t get_offset_of_Text4_7() { return static_cast<int32_t>(offsetof(SettingsLang_tEC296FA53D4D04D71C9F9F8E0316113CC0DA7B62, ___Text4_7)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_Text4_7() const { return ___Text4_7; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_Text4_7() { return &___Text4_7; }
	inline void set_Text4_7(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___Text4_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Text4_7), (void*)value);
	}

	inline static int32_t get_offset_of_pl_8() { return static_cast<int32_t>(offsetof(SettingsLang_tEC296FA53D4D04D71C9F9F8E0316113CC0DA7B62, ___pl_8)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_pl_8() const { return ___pl_8; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_pl_8() { return &___pl_8; }
	inline void set_pl_8(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___pl_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pl_8), (void*)value);
	}

	inline static int32_t get_offset_of_en_9() { return static_cast<int32_t>(offsetof(SettingsLang_tEC296FA53D4D04D71C9F9F8E0316113CC0DA7B62, ___en_9)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_en_9() const { return ___en_9; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_en_9() { return &___en_9; }
	inline void set_en_9(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___en_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___en_9), (void*)value);
	}

	inline static int32_t get_offset_of_de_10() { return static_cast<int32_t>(offsetof(SettingsLang_tEC296FA53D4D04D71C9F9F8E0316113CC0DA7B62, ___de_10)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_de_10() const { return ___de_10; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_de_10() { return &___de_10; }
	inline void set_de_10(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___de_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___de_10), (void*)value);
	}

	inline static int32_t get_offset_of_fe_11() { return static_cast<int32_t>(offsetof(SettingsLang_tEC296FA53D4D04D71C9F9F8E0316113CC0DA7B62, ___fe_11)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_fe_11() const { return ___fe_11; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_fe_11() { return &___fe_11; }
	inline void set_fe_11(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___fe_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fe_11), (void*)value);
	}

	inline static int32_t get_offset_of_es_12() { return static_cast<int32_t>(offsetof(SettingsLang_tEC296FA53D4D04D71C9F9F8E0316113CC0DA7B62, ___es_12)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_es_12() const { return ___es_12; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_es_12() { return &___es_12; }
	inline void set_es_12(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___es_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___es_12), (void*)value);
	}

	inline static int32_t get_offset_of_ru_13() { return static_cast<int32_t>(offsetof(SettingsLang_tEC296FA53D4D04D71C9F9F8E0316113CC0DA7B62, ___ru_13)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_ru_13() const { return ___ru_13; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_ru_13() { return &___ru_13; }
	inline void set_ru_13(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___ru_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ru_13), (void*)value);
	}
};


// SettingsPINLang
struct  SettingsPINLang_tDF04F4EF7D0C7ACE1CEFBCB9722593B175BF93C7  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.UI.Text SettingsPINLang::Text1
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___Text1_4;
	// UnityEngine.UI.Text SettingsPINLang::Text2
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___Text2_5;
	// UnityEngine.UI.Text SettingsPINLang::Text3
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___Text3_6;
	// UnityEngine.UI.Text SettingsPINLang::Text4
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___Text4_7;
	// UnityEngine.UI.Text SettingsPINLang::Text5
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___Text5_8;
	// System.String[] SettingsPINLang::pl
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___pl_9;
	// System.String[] SettingsPINLang::en
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___en_10;
	// System.String[] SettingsPINLang::de
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___de_11;
	// System.String[] SettingsPINLang::fe
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___fe_12;
	// System.String[] SettingsPINLang::es
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___es_13;
	// System.String[] SettingsPINLang::ru
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___ru_14;

public:
	inline static int32_t get_offset_of_Text1_4() { return static_cast<int32_t>(offsetof(SettingsPINLang_tDF04F4EF7D0C7ACE1CEFBCB9722593B175BF93C7, ___Text1_4)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_Text1_4() const { return ___Text1_4; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_Text1_4() { return &___Text1_4; }
	inline void set_Text1_4(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___Text1_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Text1_4), (void*)value);
	}

	inline static int32_t get_offset_of_Text2_5() { return static_cast<int32_t>(offsetof(SettingsPINLang_tDF04F4EF7D0C7ACE1CEFBCB9722593B175BF93C7, ___Text2_5)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_Text2_5() const { return ___Text2_5; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_Text2_5() { return &___Text2_5; }
	inline void set_Text2_5(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___Text2_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Text2_5), (void*)value);
	}

	inline static int32_t get_offset_of_Text3_6() { return static_cast<int32_t>(offsetof(SettingsPINLang_tDF04F4EF7D0C7ACE1CEFBCB9722593B175BF93C7, ___Text3_6)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_Text3_6() const { return ___Text3_6; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_Text3_6() { return &___Text3_6; }
	inline void set_Text3_6(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___Text3_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Text3_6), (void*)value);
	}

	inline static int32_t get_offset_of_Text4_7() { return static_cast<int32_t>(offsetof(SettingsPINLang_tDF04F4EF7D0C7ACE1CEFBCB9722593B175BF93C7, ___Text4_7)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_Text4_7() const { return ___Text4_7; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_Text4_7() { return &___Text4_7; }
	inline void set_Text4_7(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___Text4_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Text4_7), (void*)value);
	}

	inline static int32_t get_offset_of_Text5_8() { return static_cast<int32_t>(offsetof(SettingsPINLang_tDF04F4EF7D0C7ACE1CEFBCB9722593B175BF93C7, ___Text5_8)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_Text5_8() const { return ___Text5_8; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_Text5_8() { return &___Text5_8; }
	inline void set_Text5_8(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___Text5_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Text5_8), (void*)value);
	}

	inline static int32_t get_offset_of_pl_9() { return static_cast<int32_t>(offsetof(SettingsPINLang_tDF04F4EF7D0C7ACE1CEFBCB9722593B175BF93C7, ___pl_9)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_pl_9() const { return ___pl_9; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_pl_9() { return &___pl_9; }
	inline void set_pl_9(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___pl_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pl_9), (void*)value);
	}

	inline static int32_t get_offset_of_en_10() { return static_cast<int32_t>(offsetof(SettingsPINLang_tDF04F4EF7D0C7ACE1CEFBCB9722593B175BF93C7, ___en_10)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_en_10() const { return ___en_10; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_en_10() { return &___en_10; }
	inline void set_en_10(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___en_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___en_10), (void*)value);
	}

	inline static int32_t get_offset_of_de_11() { return static_cast<int32_t>(offsetof(SettingsPINLang_tDF04F4EF7D0C7ACE1CEFBCB9722593B175BF93C7, ___de_11)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_de_11() const { return ___de_11; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_de_11() { return &___de_11; }
	inline void set_de_11(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___de_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___de_11), (void*)value);
	}

	inline static int32_t get_offset_of_fe_12() { return static_cast<int32_t>(offsetof(SettingsPINLang_tDF04F4EF7D0C7ACE1CEFBCB9722593B175BF93C7, ___fe_12)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_fe_12() const { return ___fe_12; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_fe_12() { return &___fe_12; }
	inline void set_fe_12(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___fe_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fe_12), (void*)value);
	}

	inline static int32_t get_offset_of_es_13() { return static_cast<int32_t>(offsetof(SettingsPINLang_tDF04F4EF7D0C7ACE1CEFBCB9722593B175BF93C7, ___es_13)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_es_13() const { return ___es_13; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_es_13() { return &___es_13; }
	inline void set_es_13(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___es_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___es_13), (void*)value);
	}

	inline static int32_t get_offset_of_ru_14() { return static_cast<int32_t>(offsetof(SettingsPINLang_tDF04F4EF7D0C7ACE1CEFBCB9722593B175BF93C7, ___ru_14)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_ru_14() const { return ___ru_14; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_ru_14() { return &___ru_14; }
	inline void set_ru_14(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___ru_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ru_14), (void*)value);
	}
};


// ThreeAwardLang
struct  ThreeAwardLang_tDDE2CB79F32FB3C2ACA78937569B61DFE012EF26  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.UI.Text ThreeAwardLang::Text1
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___Text1_4;
	// UnityEngine.UI.Text ThreeAwardLang::Text2
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___Text2_5;
	// UnityEngine.UI.Text ThreeAwardLang::Text3
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___Text3_6;
	// UnityEngine.UI.Text ThreeAwardLang::Text4
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___Text4_7;
	// System.String[] ThreeAwardLang::pl
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___pl_8;
	// System.String[] ThreeAwardLang::en
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___en_9;
	// System.String[] ThreeAwardLang::de
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___de_10;
	// System.String[] ThreeAwardLang::fe
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___fe_11;
	// System.String[] ThreeAwardLang::es
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___es_12;
	// System.String[] ThreeAwardLang::ru
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___ru_13;

public:
	inline static int32_t get_offset_of_Text1_4() { return static_cast<int32_t>(offsetof(ThreeAwardLang_tDDE2CB79F32FB3C2ACA78937569B61DFE012EF26, ___Text1_4)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_Text1_4() const { return ___Text1_4; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_Text1_4() { return &___Text1_4; }
	inline void set_Text1_4(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___Text1_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Text1_4), (void*)value);
	}

	inline static int32_t get_offset_of_Text2_5() { return static_cast<int32_t>(offsetof(ThreeAwardLang_tDDE2CB79F32FB3C2ACA78937569B61DFE012EF26, ___Text2_5)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_Text2_5() const { return ___Text2_5; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_Text2_5() { return &___Text2_5; }
	inline void set_Text2_5(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___Text2_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Text2_5), (void*)value);
	}

	inline static int32_t get_offset_of_Text3_6() { return static_cast<int32_t>(offsetof(ThreeAwardLang_tDDE2CB79F32FB3C2ACA78937569B61DFE012EF26, ___Text3_6)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_Text3_6() const { return ___Text3_6; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_Text3_6() { return &___Text3_6; }
	inline void set_Text3_6(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___Text3_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Text3_6), (void*)value);
	}

	inline static int32_t get_offset_of_Text4_7() { return static_cast<int32_t>(offsetof(ThreeAwardLang_tDDE2CB79F32FB3C2ACA78937569B61DFE012EF26, ___Text4_7)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_Text4_7() const { return ___Text4_7; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_Text4_7() { return &___Text4_7; }
	inline void set_Text4_7(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___Text4_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Text4_7), (void*)value);
	}

	inline static int32_t get_offset_of_pl_8() { return static_cast<int32_t>(offsetof(ThreeAwardLang_tDDE2CB79F32FB3C2ACA78937569B61DFE012EF26, ___pl_8)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_pl_8() const { return ___pl_8; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_pl_8() { return &___pl_8; }
	inline void set_pl_8(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___pl_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pl_8), (void*)value);
	}

	inline static int32_t get_offset_of_en_9() { return static_cast<int32_t>(offsetof(ThreeAwardLang_tDDE2CB79F32FB3C2ACA78937569B61DFE012EF26, ___en_9)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_en_9() const { return ___en_9; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_en_9() { return &___en_9; }
	inline void set_en_9(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___en_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___en_9), (void*)value);
	}

	inline static int32_t get_offset_of_de_10() { return static_cast<int32_t>(offsetof(ThreeAwardLang_tDDE2CB79F32FB3C2ACA78937569B61DFE012EF26, ___de_10)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_de_10() const { return ___de_10; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_de_10() { return &___de_10; }
	inline void set_de_10(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___de_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___de_10), (void*)value);
	}

	inline static int32_t get_offset_of_fe_11() { return static_cast<int32_t>(offsetof(ThreeAwardLang_tDDE2CB79F32FB3C2ACA78937569B61DFE012EF26, ___fe_11)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_fe_11() const { return ___fe_11; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_fe_11() { return &___fe_11; }
	inline void set_fe_11(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___fe_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fe_11), (void*)value);
	}

	inline static int32_t get_offset_of_es_12() { return static_cast<int32_t>(offsetof(ThreeAwardLang_tDDE2CB79F32FB3C2ACA78937569B61DFE012EF26, ___es_12)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_es_12() const { return ___es_12; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_es_12() { return &___es_12; }
	inline void set_es_12(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___es_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___es_12), (void*)value);
	}

	inline static int32_t get_offset_of_ru_13() { return static_cast<int32_t>(offsetof(ThreeAwardLang_tDDE2CB79F32FB3C2ACA78937569B61DFE012EF26, ___ru_13)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_ru_13() const { return ___ru_13; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_ru_13() { return &___ru_13; }
	inline void set_ru_13(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___ru_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ru_13), (void*)value);
	}
};


// TwoAwardLang
struct  TwoAwardLang_tA136F34FD58DCE4FC78BAEBF81A33966AB84DF8C  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.UI.Text TwoAwardLang::Text1
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___Text1_4;
	// UnityEngine.UI.Text TwoAwardLang::Text2
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___Text2_5;
	// UnityEngine.UI.Text TwoAwardLang::Text3
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___Text3_6;
	// UnityEngine.UI.Text TwoAwardLang::Text4
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___Text4_7;
	// System.String[] TwoAwardLang::pl
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___pl_8;
	// System.String[] TwoAwardLang::en
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___en_9;
	// System.String[] TwoAwardLang::de
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___de_10;
	// System.String[] TwoAwardLang::fe
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___fe_11;
	// System.String[] TwoAwardLang::es
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___es_12;
	// System.String[] TwoAwardLang::ru
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___ru_13;

public:
	inline static int32_t get_offset_of_Text1_4() { return static_cast<int32_t>(offsetof(TwoAwardLang_tA136F34FD58DCE4FC78BAEBF81A33966AB84DF8C, ___Text1_4)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_Text1_4() const { return ___Text1_4; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_Text1_4() { return &___Text1_4; }
	inline void set_Text1_4(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___Text1_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Text1_4), (void*)value);
	}

	inline static int32_t get_offset_of_Text2_5() { return static_cast<int32_t>(offsetof(TwoAwardLang_tA136F34FD58DCE4FC78BAEBF81A33966AB84DF8C, ___Text2_5)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_Text2_5() const { return ___Text2_5; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_Text2_5() { return &___Text2_5; }
	inline void set_Text2_5(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___Text2_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Text2_5), (void*)value);
	}

	inline static int32_t get_offset_of_Text3_6() { return static_cast<int32_t>(offsetof(TwoAwardLang_tA136F34FD58DCE4FC78BAEBF81A33966AB84DF8C, ___Text3_6)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_Text3_6() const { return ___Text3_6; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_Text3_6() { return &___Text3_6; }
	inline void set_Text3_6(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___Text3_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Text3_6), (void*)value);
	}

	inline static int32_t get_offset_of_Text4_7() { return static_cast<int32_t>(offsetof(TwoAwardLang_tA136F34FD58DCE4FC78BAEBF81A33966AB84DF8C, ___Text4_7)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_Text4_7() const { return ___Text4_7; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_Text4_7() { return &___Text4_7; }
	inline void set_Text4_7(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___Text4_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Text4_7), (void*)value);
	}

	inline static int32_t get_offset_of_pl_8() { return static_cast<int32_t>(offsetof(TwoAwardLang_tA136F34FD58DCE4FC78BAEBF81A33966AB84DF8C, ___pl_8)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_pl_8() const { return ___pl_8; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_pl_8() { return &___pl_8; }
	inline void set_pl_8(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___pl_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pl_8), (void*)value);
	}

	inline static int32_t get_offset_of_en_9() { return static_cast<int32_t>(offsetof(TwoAwardLang_tA136F34FD58DCE4FC78BAEBF81A33966AB84DF8C, ___en_9)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_en_9() const { return ___en_9; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_en_9() { return &___en_9; }
	inline void set_en_9(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___en_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___en_9), (void*)value);
	}

	inline static int32_t get_offset_of_de_10() { return static_cast<int32_t>(offsetof(TwoAwardLang_tA136F34FD58DCE4FC78BAEBF81A33966AB84DF8C, ___de_10)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_de_10() const { return ___de_10; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_de_10() { return &___de_10; }
	inline void set_de_10(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___de_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___de_10), (void*)value);
	}

	inline static int32_t get_offset_of_fe_11() { return static_cast<int32_t>(offsetof(TwoAwardLang_tA136F34FD58DCE4FC78BAEBF81A33966AB84DF8C, ___fe_11)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_fe_11() const { return ___fe_11; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_fe_11() { return &___fe_11; }
	inline void set_fe_11(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___fe_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fe_11), (void*)value);
	}

	inline static int32_t get_offset_of_es_12() { return static_cast<int32_t>(offsetof(TwoAwardLang_tA136F34FD58DCE4FC78BAEBF81A33966AB84DF8C, ___es_12)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_es_12() const { return ___es_12; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_es_12() { return &___es_12; }
	inline void set_es_12(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___es_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___es_12), (void*)value);
	}

	inline static int32_t get_offset_of_ru_13() { return static_cast<int32_t>(offsetof(TwoAwardLang_tA136F34FD58DCE4FC78BAEBF81A33966AB84DF8C, ___ru_13)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_ru_13() const { return ___ru_13; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_ru_13() { return &___ru_13; }
	inline void set_ru_13(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___ru_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ru_13), (void*)value);
	}
};


// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// UnityEngine.UI.Graphic
struct  Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipLayoutUpdate
	bool ___m_SkipLayoutUpdate_8;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipMaterialUpdate
	bool ___m_SkipMaterialUpdate_9;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_10;
	// UnityEngine.Vector4 UnityEngine.UI.Graphic::m_RaycastPadding
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___m_RaycastPadding_11;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_RectTransform_12;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * ___m_CanvasRenderer_13;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * ___m_Canvas_14;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_15;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_16;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyLayoutCallback_17;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyVertsCallback_18;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyMaterialCallback_19;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::m_CachedMesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___m_CachedMesh_22;
	// UnityEngine.Vector2[] UnityEngine.UI.Graphic::m_CachedUvs
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___m_CachedUvs_23;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * ___m_ColorTweenRunner_24;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Material_6)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Material_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Color_7)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_SkipLayoutUpdate_8() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipLayoutUpdate_8)); }
	inline bool get_m_SkipLayoutUpdate_8() const { return ___m_SkipLayoutUpdate_8; }
	inline bool* get_address_of_m_SkipLayoutUpdate_8() { return &___m_SkipLayoutUpdate_8; }
	inline void set_m_SkipLayoutUpdate_8(bool value)
	{
		___m_SkipLayoutUpdate_8 = value;
	}

	inline static int32_t get_offset_of_m_SkipMaterialUpdate_9() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipMaterialUpdate_9)); }
	inline bool get_m_SkipMaterialUpdate_9() const { return ___m_SkipMaterialUpdate_9; }
	inline bool* get_address_of_m_SkipMaterialUpdate_9() { return &___m_SkipMaterialUpdate_9; }
	inline void set_m_SkipMaterialUpdate_9(bool value)
	{
		___m_SkipMaterialUpdate_9 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_10() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastTarget_10)); }
	inline bool get_m_RaycastTarget_10() const { return ___m_RaycastTarget_10; }
	inline bool* get_address_of_m_RaycastTarget_10() { return &___m_RaycastTarget_10; }
	inline void set_m_RaycastTarget_10(bool value)
	{
		___m_RaycastTarget_10 = value;
	}

	inline static int32_t get_offset_of_m_RaycastPadding_11() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastPadding_11)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_m_RaycastPadding_11() const { return ___m_RaycastPadding_11; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_m_RaycastPadding_11() { return &___m_RaycastPadding_11; }
	inline void set_m_RaycastPadding_11(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___m_RaycastPadding_11 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_12() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RectTransform_12)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_RectTransform_12() const { return ___m_RectTransform_12; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_RectTransform_12() { return &___m_RectTransform_12; }
	inline void set_m_RectTransform_12(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_RectTransform_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RectTransform_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_13() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CanvasRenderer_13)); }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * get_m_CanvasRenderer_13() const { return ___m_CanvasRenderer_13; }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E ** get_address_of_m_CanvasRenderer_13() { return &___m_CanvasRenderer_13; }
	inline void set_m_CanvasRenderer_13(CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * value)
	{
		___m_CanvasRenderer_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasRenderer_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_Canvas_14() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Canvas_14)); }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * get_m_Canvas_14() const { return ___m_Canvas_14; }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA ** get_address_of_m_Canvas_14() { return &___m_Canvas_14; }
	inline void set_m_Canvas_14(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * value)
	{
		___m_Canvas_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Canvas_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_15() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_VertsDirty_15)); }
	inline bool get_m_VertsDirty_15() const { return ___m_VertsDirty_15; }
	inline bool* get_address_of_m_VertsDirty_15() { return &___m_VertsDirty_15; }
	inline void set_m_VertsDirty_15(bool value)
	{
		___m_VertsDirty_15 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_16() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_MaterialDirty_16)); }
	inline bool get_m_MaterialDirty_16() const { return ___m_MaterialDirty_16; }
	inline bool* get_address_of_m_MaterialDirty_16() { return &___m_MaterialDirty_16; }
	inline void set_m_MaterialDirty_16(bool value)
	{
		___m_MaterialDirty_16 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_17() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyLayoutCallback_17)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyLayoutCallback_17() const { return ___m_OnDirtyLayoutCallback_17; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyLayoutCallback_17() { return &___m_OnDirtyLayoutCallback_17; }
	inline void set_m_OnDirtyLayoutCallback_17(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyLayoutCallback_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyLayoutCallback_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_18() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyVertsCallback_18)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyVertsCallback_18() const { return ___m_OnDirtyVertsCallback_18; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyVertsCallback_18() { return &___m_OnDirtyVertsCallback_18; }
	inline void set_m_OnDirtyVertsCallback_18(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyVertsCallback_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyVertsCallback_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_19() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyMaterialCallback_19)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyMaterialCallback_19() const { return ___m_OnDirtyMaterialCallback_19; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyMaterialCallback_19() { return &___m_OnDirtyMaterialCallback_19; }
	inline void set_m_OnDirtyMaterialCallback_19(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyMaterialCallback_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyMaterialCallback_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedMesh_22() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedMesh_22)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_m_CachedMesh_22() const { return ___m_CachedMesh_22; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_m_CachedMesh_22() { return &___m_CachedMesh_22; }
	inline void set_m_CachedMesh_22(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___m_CachedMesh_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedMesh_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedUvs_23() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedUvs_23)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_m_CachedUvs_23() const { return ___m_CachedUvs_23; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_m_CachedUvs_23() { return &___m_CachedUvs_23; }
	inline void set_m_CachedUvs_23(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___m_CachedUvs_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedUvs_23), (void*)value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_24() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_ColorTweenRunner_24)); }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * get_m_ColorTweenRunner_24() const { return ___m_ColorTweenRunner_24; }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 ** get_address_of_m_ColorTweenRunner_24() { return &___m_ColorTweenRunner_24; }
	inline void set_m_ColorTweenRunner_24(TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * value)
	{
		___m_ColorTweenRunner_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ColorTweenRunner_24), (void*)value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_25(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_25 = value;
	}
};

struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___s_Mesh_20;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * ___s_VertexHelper_21;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_DefaultUI_4)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultUI_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_WhiteTexture_5), (void*)value);
	}

	inline static int32_t get_offset_of_s_Mesh_20() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_Mesh_20)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_s_Mesh_20() const { return ___s_Mesh_20; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_s_Mesh_20() { return &___s_Mesh_20; }
	inline void set_s_Mesh_20(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___s_Mesh_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Mesh_20), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_21() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_VertexHelper_21)); }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * get_s_VertexHelper_21() const { return ___s_VertexHelper_21; }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 ** get_address_of_s_VertexHelper_21() { return &___s_VertexHelper_21; }
	inline void set_s_VertexHelper_21(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * value)
	{
		___s_VertexHelper_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertexHelper_21), (void*)value);
	}
};


// UnityEngine.UI.Selectable
struct  Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// System.Boolean UnityEngine.UI.Selectable::m_EnableCalled
	bool ___m_EnableCalled_6;
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  ___m_Navigation_7;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_8;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  ___m_Colors_9;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  ___m_SpriteState_10;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * ___m_AnimationTriggers_11;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_12;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___m_TargetGraphic_13;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_14;
	// System.Int32 UnityEngine.UI.Selectable::m_CurrentIndex
	int32_t ___m_CurrentIndex_15;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_16;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_17;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_18;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * ___m_CanvasGroupCache_19;

public:
	inline static int32_t get_offset_of_m_EnableCalled_6() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_EnableCalled_6)); }
	inline bool get_m_EnableCalled_6() const { return ___m_EnableCalled_6; }
	inline bool* get_address_of_m_EnableCalled_6() { return &___m_EnableCalled_6; }
	inline void set_m_EnableCalled_6(bool value)
	{
		___m_EnableCalled_6 = value;
	}

	inline static int32_t get_offset_of_m_Navigation_7() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Navigation_7)); }
	inline Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  get_m_Navigation_7() const { return ___m_Navigation_7; }
	inline Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A * get_address_of_m_Navigation_7() { return &___m_Navigation_7; }
	inline void set_m_Navigation_7(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  value)
	{
		___m_Navigation_7 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnUp_2), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnDown_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnLeft_4), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnRight_5), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_Transition_8() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Transition_8)); }
	inline int32_t get_m_Transition_8() const { return ___m_Transition_8; }
	inline int32_t* get_address_of_m_Transition_8() { return &___m_Transition_8; }
	inline void set_m_Transition_8(int32_t value)
	{
		___m_Transition_8 = value;
	}

	inline static int32_t get_offset_of_m_Colors_9() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Colors_9)); }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  get_m_Colors_9() const { return ___m_Colors_9; }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 * get_address_of_m_Colors_9() { return &___m_Colors_9; }
	inline void set_m_Colors_9(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  value)
	{
		___m_Colors_9 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_10() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_SpriteState_10)); }
	inline SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  get_m_SpriteState_10() const { return ___m_SpriteState_10; }
	inline SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E * get_address_of_m_SpriteState_10() { return &___m_SpriteState_10; }
	inline void set_m_SpriteState_10(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  value)
	{
		___m_SpriteState_10 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_HighlightedSprite_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_PressedSprite_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_SelectedSprite_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_DisabledSprite_3), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_11() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_AnimationTriggers_11)); }
	inline AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * get_m_AnimationTriggers_11() const { return ___m_AnimationTriggers_11; }
	inline AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 ** get_address_of_m_AnimationTriggers_11() { return &___m_AnimationTriggers_11; }
	inline void set_m_AnimationTriggers_11(AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * value)
	{
		___m_AnimationTriggers_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AnimationTriggers_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_Interactable_12() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Interactable_12)); }
	inline bool get_m_Interactable_12() const { return ___m_Interactable_12; }
	inline bool* get_address_of_m_Interactable_12() { return &___m_Interactable_12; }
	inline void set_m_Interactable_12(bool value)
	{
		___m_Interactable_12 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_13() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_TargetGraphic_13)); }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * get_m_TargetGraphic_13() const { return ___m_TargetGraphic_13; }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 ** get_address_of_m_TargetGraphic_13() { return &___m_TargetGraphic_13; }
	inline void set_m_TargetGraphic_13(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * value)
	{
		___m_TargetGraphic_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TargetGraphic_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_14() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_GroupsAllowInteraction_14)); }
	inline bool get_m_GroupsAllowInteraction_14() const { return ___m_GroupsAllowInteraction_14; }
	inline bool* get_address_of_m_GroupsAllowInteraction_14() { return &___m_GroupsAllowInteraction_14; }
	inline void set_m_GroupsAllowInteraction_14(bool value)
	{
		___m_GroupsAllowInteraction_14 = value;
	}

	inline static int32_t get_offset_of_m_CurrentIndex_15() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_CurrentIndex_15)); }
	inline int32_t get_m_CurrentIndex_15() const { return ___m_CurrentIndex_15; }
	inline int32_t* get_address_of_m_CurrentIndex_15() { return &___m_CurrentIndex_15; }
	inline void set_m_CurrentIndex_15(int32_t value)
	{
		___m_CurrentIndex_15 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3CisPointerInsideU3Ek__BackingField_16)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_16() const { return ___U3CisPointerInsideU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_16() { return &___U3CisPointerInsideU3Ek__BackingField_16; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_16(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3CisPointerDownU3Ek__BackingField_17)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_17() const { return ___U3CisPointerDownU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_17() { return &___U3CisPointerDownU3Ek__BackingField_17; }
	inline void set_U3CisPointerDownU3Ek__BackingField_17(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3ChasSelectionU3Ek__BackingField_18)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_18() const { return ___U3ChasSelectionU3Ek__BackingField_18; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_18() { return &___U3ChasSelectionU3Ek__BackingField_18; }
	inline void set_U3ChasSelectionU3Ek__BackingField_18(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_19() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_CanvasGroupCache_19)); }
	inline List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * get_m_CanvasGroupCache_19() const { return ___m_CanvasGroupCache_19; }
	inline List_1_t34AA4AF4E7352129CA58045901530E41445AC16D ** get_address_of_m_CanvasGroupCache_19() { return &___m_CanvasGroupCache_19; }
	inline void set_m_CanvasGroupCache_19(List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * value)
	{
		___m_CanvasGroupCache_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasGroupCache_19), (void*)value);
	}
};

struct Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields
{
public:
	// UnityEngine.UI.Selectable[] UnityEngine.UI.Selectable::s_Selectables
	SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* ___s_Selectables_4;
	// System.Int32 UnityEngine.UI.Selectable::s_SelectableCount
	int32_t ___s_SelectableCount_5;

public:
	inline static int32_t get_offset_of_s_Selectables_4() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields, ___s_Selectables_4)); }
	inline SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* get_s_Selectables_4() const { return ___s_Selectables_4; }
	inline SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535** get_address_of_s_Selectables_4() { return &___s_Selectables_4; }
	inline void set_s_Selectables_4(SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* value)
	{
		___s_Selectables_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Selectables_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_SelectableCount_5() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields, ___s_SelectableCount_5)); }
	inline int32_t get_s_SelectableCount_5() const { return ___s_SelectableCount_5; }
	inline int32_t* get_address_of_s_SelectableCount_5() { return &___s_SelectableCount_5; }
	inline void set_s_SelectableCount_5(int32_t value)
	{
		___s_SelectableCount_5 = value;
	}
};


// UnityEngine.UI.Button
struct  Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D  : public Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD
{
public:
	// UnityEngine.UI.Button/ButtonClickedEvent UnityEngine.UI.Button::m_OnClick
	ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * ___m_OnClick_20;

public:
	inline static int32_t get_offset_of_m_OnClick_20() { return static_cast<int32_t>(offsetof(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D, ___m_OnClick_20)); }
	inline ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * get_m_OnClick_20() const { return ___m_OnClick_20; }
	inline ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F ** get_address_of_m_OnClick_20() { return &___m_OnClick_20; }
	inline void set_m_OnClick_20(ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * value)
	{
		___m_OnClick_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnClick_20), (void*)value);
	}
};


// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE  : public Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_26;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_MaskMaterial_27;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * ___m_ParentMask_28;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_29;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IsMaskingGraphic
	bool ___m_IsMaskingGraphic_30;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_31;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * ___m_OnCullStateChanged_32;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_33;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_34;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___m_Corners_35;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculateStencil_26)); }
	inline bool get_m_ShouldRecalculateStencil_26() const { return ___m_ShouldRecalculateStencil_26; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_26() { return &___m_ShouldRecalculateStencil_26; }
	inline void set_m_ShouldRecalculateStencil_26(bool value)
	{
		___m_ShouldRecalculateStencil_26 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_MaskMaterial_27)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_MaskMaterial_27() const { return ___m_MaskMaterial_27; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_MaskMaterial_27() { return &___m_MaskMaterial_27; }
	inline void set_m_MaskMaterial_27(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_MaskMaterial_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_MaskMaterial_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_ParentMask_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ParentMask_28)); }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * get_m_ParentMask_28() const { return ___m_ParentMask_28; }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 ** get_address_of_m_ParentMask_28() { return &___m_ParentMask_28; }
	inline void set_m_ParentMask_28(RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * value)
	{
		___m_ParentMask_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParentMask_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_Maskable_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Maskable_29)); }
	inline bool get_m_Maskable_29() const { return ___m_Maskable_29; }
	inline bool* get_address_of_m_Maskable_29() { return &___m_Maskable_29; }
	inline void set_m_Maskable_29(bool value)
	{
		___m_Maskable_29 = value;
	}

	inline static int32_t get_offset_of_m_IsMaskingGraphic_30() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IsMaskingGraphic_30)); }
	inline bool get_m_IsMaskingGraphic_30() const { return ___m_IsMaskingGraphic_30; }
	inline bool* get_address_of_m_IsMaskingGraphic_30() { return &___m_IsMaskingGraphic_30; }
	inline void set_m_IsMaskingGraphic_30(bool value)
	{
		___m_IsMaskingGraphic_30 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_31() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IncludeForMasking_31)); }
	inline bool get_m_IncludeForMasking_31() const { return ___m_IncludeForMasking_31; }
	inline bool* get_address_of_m_IncludeForMasking_31() { return &___m_IncludeForMasking_31; }
	inline void set_m_IncludeForMasking_31(bool value)
	{
		___m_IncludeForMasking_31 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_32() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_OnCullStateChanged_32)); }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * get_m_OnCullStateChanged_32() const { return ___m_OnCullStateChanged_32; }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 ** get_address_of_m_OnCullStateChanged_32() { return &___m_OnCullStateChanged_32; }
	inline void set_m_OnCullStateChanged_32(CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * value)
	{
		___m_OnCullStateChanged_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnCullStateChanged_32), (void*)value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_33() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculate_33)); }
	inline bool get_m_ShouldRecalculate_33() const { return ___m_ShouldRecalculate_33; }
	inline bool* get_address_of_m_ShouldRecalculate_33() { return &___m_ShouldRecalculate_33; }
	inline void set_m_ShouldRecalculate_33(bool value)
	{
		___m_ShouldRecalculate_33 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_34() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_StencilValue_34)); }
	inline int32_t get_m_StencilValue_34() const { return ___m_StencilValue_34; }
	inline int32_t* get_address_of_m_StencilValue_34() { return &___m_StencilValue_34; }
	inline void set_m_StencilValue_34(int32_t value)
	{
		___m_StencilValue_34 = value;
	}

	inline static int32_t get_offset_of_m_Corners_35() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Corners_35)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_m_Corners_35() const { return ___m_Corners_35; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_m_Corners_35() { return &___m_Corners_35; }
	inline void set_m_Corners_35(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___m_Corners_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Corners_35), (void*)value);
	}
};


// UnityEngine.UI.Image
struct  Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C  : public MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE
{
public:
	// UnityEngine.Sprite UnityEngine.UI.Image::m_Sprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_Sprite_37;
	// UnityEngine.Sprite UnityEngine.UI.Image::m_OverrideSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_OverrideSprite_38;
	// UnityEngine.UI.Image/Type UnityEngine.UI.Image::m_Type
	int32_t ___m_Type_39;
	// System.Boolean UnityEngine.UI.Image::m_PreserveAspect
	bool ___m_PreserveAspect_40;
	// System.Boolean UnityEngine.UI.Image::m_FillCenter
	bool ___m_FillCenter_41;
	// UnityEngine.UI.Image/FillMethod UnityEngine.UI.Image::m_FillMethod
	int32_t ___m_FillMethod_42;
	// System.Single UnityEngine.UI.Image::m_FillAmount
	float ___m_FillAmount_43;
	// System.Boolean UnityEngine.UI.Image::m_FillClockwise
	bool ___m_FillClockwise_44;
	// System.Int32 UnityEngine.UI.Image::m_FillOrigin
	int32_t ___m_FillOrigin_45;
	// System.Single UnityEngine.UI.Image::m_AlphaHitTestMinimumThreshold
	float ___m_AlphaHitTestMinimumThreshold_46;
	// System.Boolean UnityEngine.UI.Image::m_Tracked
	bool ___m_Tracked_47;
	// System.Boolean UnityEngine.UI.Image::m_UseSpriteMesh
	bool ___m_UseSpriteMesh_48;
	// System.Single UnityEngine.UI.Image::m_PixelsPerUnitMultiplier
	float ___m_PixelsPerUnitMultiplier_49;
	// System.Single UnityEngine.UI.Image::m_CachedReferencePixelsPerUnit
	float ___m_CachedReferencePixelsPerUnit_50;

public:
	inline static int32_t get_offset_of_m_Sprite_37() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Sprite_37)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_Sprite_37() const { return ___m_Sprite_37; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_Sprite_37() { return &___m_Sprite_37; }
	inline void set_m_Sprite_37(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_Sprite_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Sprite_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_OverrideSprite_38() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_OverrideSprite_38)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_OverrideSprite_38() const { return ___m_OverrideSprite_38; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_OverrideSprite_38() { return &___m_OverrideSprite_38; }
	inline void set_m_OverrideSprite_38(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_OverrideSprite_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OverrideSprite_38), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type_39() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Type_39)); }
	inline int32_t get_m_Type_39() const { return ___m_Type_39; }
	inline int32_t* get_address_of_m_Type_39() { return &___m_Type_39; }
	inline void set_m_Type_39(int32_t value)
	{
		___m_Type_39 = value;
	}

	inline static int32_t get_offset_of_m_PreserveAspect_40() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_PreserveAspect_40)); }
	inline bool get_m_PreserveAspect_40() const { return ___m_PreserveAspect_40; }
	inline bool* get_address_of_m_PreserveAspect_40() { return &___m_PreserveAspect_40; }
	inline void set_m_PreserveAspect_40(bool value)
	{
		___m_PreserveAspect_40 = value;
	}

	inline static int32_t get_offset_of_m_FillCenter_41() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillCenter_41)); }
	inline bool get_m_FillCenter_41() const { return ___m_FillCenter_41; }
	inline bool* get_address_of_m_FillCenter_41() { return &___m_FillCenter_41; }
	inline void set_m_FillCenter_41(bool value)
	{
		___m_FillCenter_41 = value;
	}

	inline static int32_t get_offset_of_m_FillMethod_42() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillMethod_42)); }
	inline int32_t get_m_FillMethod_42() const { return ___m_FillMethod_42; }
	inline int32_t* get_address_of_m_FillMethod_42() { return &___m_FillMethod_42; }
	inline void set_m_FillMethod_42(int32_t value)
	{
		___m_FillMethod_42 = value;
	}

	inline static int32_t get_offset_of_m_FillAmount_43() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillAmount_43)); }
	inline float get_m_FillAmount_43() const { return ___m_FillAmount_43; }
	inline float* get_address_of_m_FillAmount_43() { return &___m_FillAmount_43; }
	inline void set_m_FillAmount_43(float value)
	{
		___m_FillAmount_43 = value;
	}

	inline static int32_t get_offset_of_m_FillClockwise_44() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillClockwise_44)); }
	inline bool get_m_FillClockwise_44() const { return ___m_FillClockwise_44; }
	inline bool* get_address_of_m_FillClockwise_44() { return &___m_FillClockwise_44; }
	inline void set_m_FillClockwise_44(bool value)
	{
		___m_FillClockwise_44 = value;
	}

	inline static int32_t get_offset_of_m_FillOrigin_45() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillOrigin_45)); }
	inline int32_t get_m_FillOrigin_45() const { return ___m_FillOrigin_45; }
	inline int32_t* get_address_of_m_FillOrigin_45() { return &___m_FillOrigin_45; }
	inline void set_m_FillOrigin_45(int32_t value)
	{
		___m_FillOrigin_45 = value;
	}

	inline static int32_t get_offset_of_m_AlphaHitTestMinimumThreshold_46() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_AlphaHitTestMinimumThreshold_46)); }
	inline float get_m_AlphaHitTestMinimumThreshold_46() const { return ___m_AlphaHitTestMinimumThreshold_46; }
	inline float* get_address_of_m_AlphaHitTestMinimumThreshold_46() { return &___m_AlphaHitTestMinimumThreshold_46; }
	inline void set_m_AlphaHitTestMinimumThreshold_46(float value)
	{
		___m_AlphaHitTestMinimumThreshold_46 = value;
	}

	inline static int32_t get_offset_of_m_Tracked_47() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Tracked_47)); }
	inline bool get_m_Tracked_47() const { return ___m_Tracked_47; }
	inline bool* get_address_of_m_Tracked_47() { return &___m_Tracked_47; }
	inline void set_m_Tracked_47(bool value)
	{
		___m_Tracked_47 = value;
	}

	inline static int32_t get_offset_of_m_UseSpriteMesh_48() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_UseSpriteMesh_48)); }
	inline bool get_m_UseSpriteMesh_48() const { return ___m_UseSpriteMesh_48; }
	inline bool* get_address_of_m_UseSpriteMesh_48() { return &___m_UseSpriteMesh_48; }
	inline void set_m_UseSpriteMesh_48(bool value)
	{
		___m_UseSpriteMesh_48 = value;
	}

	inline static int32_t get_offset_of_m_PixelsPerUnitMultiplier_49() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_PixelsPerUnitMultiplier_49)); }
	inline float get_m_PixelsPerUnitMultiplier_49() const { return ___m_PixelsPerUnitMultiplier_49; }
	inline float* get_address_of_m_PixelsPerUnitMultiplier_49() { return &___m_PixelsPerUnitMultiplier_49; }
	inline void set_m_PixelsPerUnitMultiplier_49(float value)
	{
		___m_PixelsPerUnitMultiplier_49 = value;
	}

	inline static int32_t get_offset_of_m_CachedReferencePixelsPerUnit_50() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_CachedReferencePixelsPerUnit_50)); }
	inline float get_m_CachedReferencePixelsPerUnit_50() const { return ___m_CachedReferencePixelsPerUnit_50; }
	inline float* get_address_of_m_CachedReferencePixelsPerUnit_50() { return &___m_CachedReferencePixelsPerUnit_50; }
	inline void set_m_CachedReferencePixelsPerUnit_50(float value)
	{
		___m_CachedReferencePixelsPerUnit_50 = value;
	}
};

struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Image::s_ETC1DefaultUI
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_ETC1DefaultUI_36;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_VertScratch
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___s_VertScratch_51;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_UVScratch
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___s_UVScratch_52;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Xy
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___s_Xy_53;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Uv
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___s_Uv_54;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> UnityEngine.UI.Image::m_TrackedTexturelessImages
	List_1_t815A476B0A21E183042059E705F9E505478CD8AE * ___m_TrackedTexturelessImages_55;
	// System.Boolean UnityEngine.UI.Image::s_Initialized
	bool ___s_Initialized_56;

public:
	inline static int32_t get_offset_of_s_ETC1DefaultUI_36() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_ETC1DefaultUI_36)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_ETC1DefaultUI_36() const { return ___s_ETC1DefaultUI_36; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_ETC1DefaultUI_36() { return &___s_ETC1DefaultUI_36; }
	inline void set_s_ETC1DefaultUI_36(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_ETC1DefaultUI_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ETC1DefaultUI_36), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertScratch_51() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_VertScratch_51)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_s_VertScratch_51() const { return ___s_VertScratch_51; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_s_VertScratch_51() { return &___s_VertScratch_51; }
	inline void set_s_VertScratch_51(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___s_VertScratch_51 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertScratch_51), (void*)value);
	}

	inline static int32_t get_offset_of_s_UVScratch_52() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_UVScratch_52)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_s_UVScratch_52() const { return ___s_UVScratch_52; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_s_UVScratch_52() { return &___s_UVScratch_52; }
	inline void set_s_UVScratch_52(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___s_UVScratch_52 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_UVScratch_52), (void*)value);
	}

	inline static int32_t get_offset_of_s_Xy_53() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Xy_53)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_s_Xy_53() const { return ___s_Xy_53; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_s_Xy_53() { return &___s_Xy_53; }
	inline void set_s_Xy_53(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___s_Xy_53 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Xy_53), (void*)value);
	}

	inline static int32_t get_offset_of_s_Uv_54() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Uv_54)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_s_Uv_54() const { return ___s_Uv_54; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_s_Uv_54() { return &___s_Uv_54; }
	inline void set_s_Uv_54(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___s_Uv_54 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Uv_54), (void*)value);
	}

	inline static int32_t get_offset_of_m_TrackedTexturelessImages_55() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___m_TrackedTexturelessImages_55)); }
	inline List_1_t815A476B0A21E183042059E705F9E505478CD8AE * get_m_TrackedTexturelessImages_55() const { return ___m_TrackedTexturelessImages_55; }
	inline List_1_t815A476B0A21E183042059E705F9E505478CD8AE ** get_address_of_m_TrackedTexturelessImages_55() { return &___m_TrackedTexturelessImages_55; }
	inline void set_m_TrackedTexturelessImages_55(List_1_t815A476B0A21E183042059E705F9E505478CD8AE * value)
	{
		___m_TrackedTexturelessImages_55 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TrackedTexturelessImages_55), (void*)value);
	}

	inline static int32_t get_offset_of_s_Initialized_56() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Initialized_56)); }
	inline bool get_s_Initialized_56() const { return ___s_Initialized_56; }
	inline bool* get_address_of_s_Initialized_56() { return &___s_Initialized_56; }
	inline void set_s_Initialized_56(bool value)
	{
		___s_Initialized_56 = value;
	}
};


// UnityEngine.UI.Text
struct  Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1  : public MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * ___m_FontData_36;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_37;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * ___m_TextCache_38;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * ___m_TextCacheForLayout_39;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_41;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* ___m_TempVerts_42;

public:
	inline static int32_t get_offset_of_m_FontData_36() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_FontData_36)); }
	inline FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * get_m_FontData_36() const { return ___m_FontData_36; }
	inline FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 ** get_address_of_m_FontData_36() { return &___m_FontData_36; }
	inline void set_m_FontData_36(FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * value)
	{
		___m_FontData_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FontData_36), (void*)value);
	}

	inline static int32_t get_offset_of_m_Text_37() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_Text_37)); }
	inline String_t* get_m_Text_37() const { return ___m_Text_37; }
	inline String_t** get_address_of_m_Text_37() { return &___m_Text_37; }
	inline void set_m_Text_37(String_t* value)
	{
		___m_Text_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Text_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCache_38() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TextCache_38)); }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * get_m_TextCache_38() const { return ___m_TextCache_38; }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 ** get_address_of_m_TextCache_38() { return &___m_TextCache_38; }
	inline void set_m_TextCache_38(TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * value)
	{
		___m_TextCache_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCache_38), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_39() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TextCacheForLayout_39)); }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * get_m_TextCacheForLayout_39() const { return ___m_TextCacheForLayout_39; }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 ** get_address_of_m_TextCacheForLayout_39() { return &___m_TextCacheForLayout_39; }
	inline void set_m_TextCacheForLayout_39(TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * value)
	{
		___m_TextCacheForLayout_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCacheForLayout_39), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_41() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_DisableFontTextureRebuiltCallback_41)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_41() const { return ___m_DisableFontTextureRebuiltCallback_41; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_41() { return &___m_DisableFontTextureRebuiltCallback_41; }
	inline void set_m_DisableFontTextureRebuiltCallback_41(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_41 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_42() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TempVerts_42)); }
	inline UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* get_m_TempVerts_42() const { return ___m_TempVerts_42; }
	inline UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A** get_address_of_m_TempVerts_42() { return &___m_TempVerts_42; }
	inline void set_m_TempVerts_42(UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* value)
	{
		___m_TempVerts_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TempVerts_42), (void*)value);
	}
};

struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_DefaultText_40;

public:
	inline static int32_t get_offset_of_s_DefaultText_40() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_StaticFields, ___s_DefaultText_40)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_DefaultText_40() const { return ___s_DefaultText_40; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_DefaultText_40() { return &___s_DefaultText_40; }
	inline void set_s_DefaultText_40(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_DefaultText_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultText_40), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * m_Items[1];

public:
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.UI.Text[]
struct TextU5BU5D_t16DD1967B137EC602803C77DBB246B05B3D0275F  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * m_Items[1];

public:
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.AudioClip[]
struct AudioClipU5BU5D_t9BA3E7C4B62164BA2E054F3A82CD6CE6EB273CBE  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * m_Items[1];

public:
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};


// !!0 UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F_gshared (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, int32_t ___item0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponentInChildren_TisRuntimeObject_mB377B32275A969E0D1A738DBC693DE8EB3593642_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_gshared_inline (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Clear_m508B72E5229FAE7042D99A04555F66F10C597C7A_gshared (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponentInChildren<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponentInChildren_TisRuntimeObject_mC8FC6687C66150FA89090C2A7733B2EE2E1315FD_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD_gshared (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, const RuntimeMethod* method);

// System.Void UnityEngine.Advertisements.Advertisement::Initialize(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Advertisement_Initialize_m50E5DCFAC318D93B37B03C454C0B6648428A109F (String_t* ___gameId0, bool ___testMode1, const RuntimeMethod* method);
// System.Collections.IEnumerator AdsBanner::ShowBannerWhenInitialized()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* AdsBanner_ShowBannerWhenInitialized_mDE78FF5506E38562FFEF9B19F241C8DEF33A03AF (AdsBanner_tE70BD583B6D94ED4239305BA678D6CD5152F38DE * __this, const RuntimeMethod* method);
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719 (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, RuntimeObject* ___routine0, const RuntimeMethod* method);
// System.Void AdsBanner/<ShowBannerWhenInitialized>d__4::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CShowBannerWhenInitializedU3Ed__4__ctor_m8DCA04DCF4BE1E4A624C3E15F9D1D9A9A68E35BE (U3CShowBannerWhenInitializedU3Ed__4_t49F7781D8F81FA49B995C576008FC45EC56C152B * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, const RuntimeMethod* method);
// UnityEngine.SystemLanguage UnityEngine.Application::get_systemLanguage()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045 (const RuntimeMethod* method);
// System.Void AwardsMenuLang::setLang(System.String[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AwardsMenuLang_setLang_mD1FBBD124E2B37C1506481BFE485E3C7C0813D07 (AwardsMenuLang_t7929C416FB74BDCC2DC4FA1F061FAE951E4F56EE * __this, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___Leng0, int32_t ___Index1, const RuntimeMethod* method);
// System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PlayerPrefs_GetInt_m6BCF9F844298D1810A68BAF23ECBA68C6960A986 (String_t* ___key0, const RuntimeMethod* method);
// System.String System.Int32::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411 (int32_t* __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Text>()
inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * Component_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m2D99AC2081683F963C56EC738451EC0B59B5D137 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32 (Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9 * __this, bool ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.PlayerPrefs::HasKey(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PlayerPrefs_HasKey_m48BE5886380B51AB495B91C9A26115B7CB958A92 (String_t* ___key0, const RuntimeMethod* method);
// System.String UnityEngine.PlayerPrefs::GetString(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PlayerPrefs_GetString_mE7654C1031622A56CD8F248F53714B105A35A159 (String_t* ___key0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Text>()
inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// System.Boolean System.String::op_Equality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SceneManager_LoadScene_m5550E6368A6D0E37DACEDA3C5E4BA331836BC3C5 (int32_t ___sceneBuildIndex0, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_red()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  Color_get_red_m9BD55EBF7A74A515330FA5F7AC7A67C8A8913DD8 (const RuntimeMethod* method);
// System.Void EnterPINLang::setLang(System.String[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnterPINLang_setLang_m20C95F886AFB7F533F7B37F09F749D4F0D3BA165 (EnterPINLang_t7DB4141FE4619EA8936999CB62CFD95077E1B3D5 * __this, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___Leng0, int32_t ___Index1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// System.Void GM::MinusPointsOneAward(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GM_MinusPointsOneAward_m49EE269CBFE173553961DC4B1C10C71F1C76FE8E (GM_tFC30914A710042CAFEA7CF10E2AD01C9A529CEF0 * __this, int32_t ___Point0, const RuntimeMethod* method);
// System.Void GM::MinusPointsTwoAward(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GM_MinusPointsTwoAward_mE225AE18889C36C657B21FA781C3910CABF7D646 (GM_tFC30914A710042CAFEA7CF10E2AD01C9A529CEF0 * __this, int32_t ___Point0, const RuntimeMethod* method);
// System.Void GM::MinusPointsThreeAward(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GM_MinusPointsThreeAward_m9B88BC9C158273893CF784A7C0146A8526DF72D7 (GM_tFC30914A710042CAFEA7CF10E2AD01C9A529CEF0 * __this, int32_t ___Point0, const RuntimeMethod* method);
// System.Void GM::DisplayScore(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GM_DisplayScore_m9D1B41034079EE6F05346F3DFA4F69D031516C3D (GM_tFC30914A710042CAFEA7CF10E2AD01C9A529CEF0 * __this, int32_t ___Point0, const RuntimeMethod* method);
// System.Void UnityEngine.PlayerPrefs::SetInt(System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerPrefs_SetInt_m0C5C977E960B9CA8F9AB73AF4129C3DCABD067B6 (String_t* ___key0, int32_t ___value1, const RuntimeMethod* method);
// System.Void UnityEngine.PlayerPrefs::Save()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerPrefs_Save_m2C1E628FA335095CD88D0DA1CB50ACC924667EEC (const RuntimeMethod* method);
// System.Void GameLang::setLang(System.String[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameLang_setLang_mDB511190EC4318D87062125C2656C1EE9B83F8E9 (GameLang_t88B393D27387CFB1391819269B4FD02205959915 * __this, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___Leng0, int32_t ___Index1, const RuntimeMethod* method);
// System.Void UnityEngine.UI.Image::set_sprite(UnityEngine.Sprite)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Image_set_sprite_m55C50F18ABA0A98E926FC777F7D07FA18A7D04E4 (Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * __this, Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___value0, const RuntimeMethod* method);
// System.Void GameMenuLang::setLang(System.String[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameMenuLang_setLang_mA20C12C7B1D825EFD990246302FD9401A98217B9 (GameMenuLang_t49BEAA2ABAFC0A899B3346BEF7C050C4212D467C * __this, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___Leng0, int32_t ___Index1, const RuntimeMethod* method);
// System.Void UnityEngine.PlayerPrefs::DeleteKey(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerPrefs_DeleteKey_mCEF6CE08D7D7670AD4072228E261A7E746030554 (String_t* ___key0, const RuntimeMethod* method);
// System.Void UnityEngine.Application::Quit()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Application_Quit_m8D720E5092786C2EE32310D85FE61C253D3B1F2A (const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.GameObject::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * GameObject_get_gameObject_mD5FFECF7C3AC5039E847DF7A8842478539B701D6 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, bool ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.AudioSource>()
inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * Component_GetComponent_TisAudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_mCC3B7A679ECE504BFAF8C70C4EF527511F46902F (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.String System.Single::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Single_ToString_m80E7ABED4F4D73F2BE19DDB80D3D92FCD8DFA010 (float* __this, const RuntimeMethod* method);
// System.Void MultiTable::Timer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultiTable_Timer_mE67424E0638FE200B3E4917ACC7716A4DA7FAA21 (MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290 (const RuntimeMethod* method);
// System.String System.Single::ToString(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Single_ToString_m15F10F2AFF80750906CEFCFB456EBA84F9D2E8D7 (float* __this, String_t* ___format0, const RuntimeMethod* method);
// System.Void MultiTable::MinusPoints()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultiTable_MinusPoints_m1CAB936C4BB41304C0F84FFF740BDB8C90F67574 (MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206 * __this, const RuntimeMethod* method);
// System.Void MultiTable::ResetTimer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultiTable_ResetTimer_m3EDBEC0DABDB2C44DB50E1BA0494C599CD43882B (MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206 * __this, const RuntimeMethod* method);
// System.Void MultiTable::RemonteList()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultiTable_RemonteList_mCE61DCD2CD42ABEF80CCB1653A5160E4CE98842C (MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206 * __this, const RuntimeMethod* method);
// System.Void MultiTable::RandomMathOper()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultiTable_RandomMathOper_m648924AB98EC575E4E02B2E91B3DB4DC70404E20 (MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A (int32_t ___minInclusive0, int32_t ___maxExclusive1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0)
inline void List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, int32_t ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *, int32_t, const RuntimeMethod*))List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F_gshared)(__this, ___item0, method);
}
// !!0 UnityEngine.Component::GetComponentInChildren<UnityEngine.UI.Text>()
inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * Component_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mFB5C182E24F496A866F116D3F75CBD8616A46AB3 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponentInChildren_TisRuntimeObject_mB377B32275A969E0D1A738DBC693DE8EB3593642_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32)
inline int32_t List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_inline (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *, int32_t, const RuntimeMethod*))List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_gshared_inline)(__this, ___index0, method);
}
// System.Void UnityEngine.Handheld::Vibrate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Handheld_Vibrate_m521B854160443BEBA8D2D3BE63641000E1DAA82E (const RuntimeMethod* method);
// System.Void UnityEngine.AudioSource::set_mute(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AudioSource_set_mute_m69E2DFCF261D2D187ED756096B7E0DE622292C71 (AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AudioSource::set_clip(UnityEngine.AudioClip)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AudioSource_set_clip_mD1F50F7BA6EA3AF25B4922473352C5180CFF7B2B (AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * __this, AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AudioSource::Play()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AudioSource_Play_mED16664B8F8F3E4D68785C8C00FC96C4DF053AE1 (AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::Clear()
inline void List_1_Clear_m508B72E5229FAE7042D99A04555F66F10C597C7A (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *, const RuntimeMethod*))List_1_Clear_m508B72E5229FAE7042D99A04555F66F10C597C7A_gshared)(__this, method);
}
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.UI.Selectable::set_interactable(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Selectable_set_interactable_mE6F57D33A9E0484377174D0F490C4372BF7F0D40 (Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * __this, bool ___value0, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.GameObject::FindGameObjectWithTag(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * GameObject_FindGameObjectWithTag_mFC215979EDFED361F88C336BF9E187F24434C63F (String_t* ___tag0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponentInChildren<UnityEngine.UI.Text>()
inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponentInChildren_TisRuntimeObject_mC8FC6687C66150FA89090C2A7733B2EE2E1315FD_gshared)(__this, method);
}
// System.Void MultiTable::PlaySounds(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultiTable_PlaySounds_m5D0B5AB0E188B53FC4714F7BE49A960C6A5E5659 (MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206 * __this, int32_t ___IndexClip0, const RuntimeMethod* method);
// System.Void MultiTable::AddPoints()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultiTable_AddPoints_mCF0996F6154238A8000EEA211B659076B565B233 (MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206 * __this, const RuntimeMethod* method);
// System.Void MultiTable::OnVibration()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultiTable_OnVibration_m33FE08D7C4D2249464CB79DE79E9B630A64EB4B2 (MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
inline void List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *, const RuntimeMethod*))List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD_gshared)(__this, method);
}
// System.Void OneAwardLang::setLang(System.String[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OneAwardLang_setLang_m71BF319A0CFF2967CBFB7ECC05F7CF70B82182F4 (OneAwardLang_t98E602972882C2F997D3285FD94AE10CA89D1CB0 * __this, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___Leng0, int32_t ___Index1, const RuntimeMethod* method);
// System.Void UnityEngine.PlayerPrefs::SetString(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerPrefs_SetString_m94CD8FF45692553A5726DFADF74935F7E1D1C633 (String_t* ___key0, String_t* ___value1, const RuntimeMethod* method);
// System.Boolean System.String::op_Inequality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Inequality_mDDA2DDED3E7EF042987EB7180EE3E88105F0AAE2 (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// System.Void SettingsLang::setLang(System.String[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SettingsLang_setLang_mF661C7627049B8FCB2F4535F048F85C94279B563 (SettingsLang_tEC296FA53D4D04D71C9F9F8E0316113CC0DA7B62 * __this, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___Leng0, int32_t ___Index1, const RuntimeMethod* method);
// System.Void SettingsPINLang::setLang(System.String[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SettingsPINLang_setLang_m2B1B554C9BAFFD7DED5FCBBC253966C47935A989 (SettingsPINLang_tDF04F4EF7D0C7ACE1CEFBCB9722593B175BF93C7 * __this, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___Leng0, int32_t ___Index1, const RuntimeMethod* method);
// System.Void ThreeAwardLang::setLang(System.String[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThreeAwardLang_setLang_mB6B0D1EA2E97E5BF6AF62349692D278055A9322A (ThreeAwardLang_tDDE2CB79F32FB3C2ACA78937569B61DFE012EF26 * __this, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___Leng0, int32_t ___Index1, const RuntimeMethod* method);
// System.Void TwoAwardLang::setLang(System.String[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TwoAwardLang_setLang_mF511A6231AF485C0208F0D6B801569C4B0C20C6D (TwoAwardLang_tA136F34FD58DCE4FC78BAEBF81A33966AB84DF8C * __this, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___Leng0, int32_t ___Index1, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4 (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * __this, float ___seconds0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Advertisements.Advertisement::get_isInitialized()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Advertisement_get_isInitialized_mB504A790B305C52734533C91982678CA45EA9A4F (const RuntimeMethod* method);
// System.Void UnityEngine.Advertisements.Advertisement/Banner::SetPosition(UnityEngine.Advertisements.BannerPosition)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Banner_SetPosition_mB03D6FE7CBC2DA088649E8EC1DD89D352E201043 (int32_t ___position0, const RuntimeMethod* method);
// System.Void UnityEngine.Advertisements.Advertisement/Banner::Show(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Banner_Show_m3C42E55C8F73C12F90BF9AB9D5A293CA7368906E (String_t* ___placementId0, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * __this, const RuntimeMethod* method);
// System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929 (const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AdsBanner::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdsBanner_Start_m502AF0C94AF9C7230FDD68144603C7FF06421843 (AdsBanner_tE70BD583B6D94ED4239305BA678D6CD5152F38DE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Advertisement_t049C3D5BE2CA5B376B1AEC9E88408AD0B2494D84_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Advertisement.Initialize(gameId, testMode);
		String_t* L_0 = __this->get_gameId_4();
		bool L_1 = __this->get_testMode_6();
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t049C3D5BE2CA5B376B1AEC9E88408AD0B2494D84_il2cpp_TypeInfo_var);
		Advertisement_Initialize_m50E5DCFAC318D93B37B03C454C0B6648428A109F(L_0, L_1, /*hidden argument*/NULL);
		// StartCoroutine(ShowBannerWhenInitialized());
		RuntimeObject* L_2;
		L_2 = AdsBanner_ShowBannerWhenInitialized_mDE78FF5506E38562FFEF9B19F241C8DEF33A03AF(__this, /*hidden argument*/NULL);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_3;
		L_3 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Collections.IEnumerator AdsBanner::ShowBannerWhenInitialized()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* AdsBanner_ShowBannerWhenInitialized_mDE78FF5506E38562FFEF9B19F241C8DEF33A03AF (AdsBanner_tE70BD583B6D94ED4239305BA678D6CD5152F38DE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CShowBannerWhenInitializedU3Ed__4_t49F7781D8F81FA49B995C576008FC45EC56C152B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CShowBannerWhenInitializedU3Ed__4_t49F7781D8F81FA49B995C576008FC45EC56C152B * L_0 = (U3CShowBannerWhenInitializedU3Ed__4_t49F7781D8F81FA49B995C576008FC45EC56C152B *)il2cpp_codegen_object_new(U3CShowBannerWhenInitializedU3Ed__4_t49F7781D8F81FA49B995C576008FC45EC56C152B_il2cpp_TypeInfo_var);
		U3CShowBannerWhenInitializedU3Ed__4__ctor_m8DCA04DCF4BE1E4A624C3E15F9D1D9A9A68E35BE(L_0, 0, /*hidden argument*/NULL);
		U3CShowBannerWhenInitializedU3Ed__4_t49F7781D8F81FA49B995C576008FC45EC56C152B * L_1 = L_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_2(__this);
		return L_1;
	}
}
// System.Void AdsBanner::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdsBanner__ctor_m9D31B9C76AABA5141CE89CEB1D1BDD1081D85716 (AdsBanner_tE70BD583B6D94ED4239305BA678D6CD5152F38DE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCF059ED30A7A499E26BBB414999316984D0D7D57);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEEC7F986C7A7A979DA0887CABFB7DF21BA016E4C);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public string gameId = "4033827";
		__this->set_gameId_4(_stringLiteralCF059ED30A7A499E26BBB414999316984D0D7D57);
		// public string surfacingId = "banner";
		__this->set_surfacingId_5(_stringLiteralEEC7F986C7A7A979DA0887CABFB7DF21BA016E4C);
		// public bool testMode = true;
		__this->set_testMode_6((bool)1);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AwardsMenuLang::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AwardsMenuLang_Awake_m09CB9F36A702E34B62BD59FC6FCD66C362F8EF49 (AwardsMenuLang_t7929C416FB74BDCC2DC4FA1F061FAE951E4F56EE * __this, const RuntimeMethod* method)
{
	{
		// if (Application.systemLanguage == SystemLanguage.Polish)
		int32_t L_0;
		L_0 = Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)27)))))
		{
			goto IL_0017;
		}
	}
	{
		// setLang(pl, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_1 = __this->get_pl_10();
		AwardsMenuLang_setLang_mD1FBBD124E2B37C1506481BFE485E3C7C0813D07(__this, L_1, 0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0017:
	{
		// else if (Application.systemLanguage == SystemLanguage.German)
		int32_t L_2;
		L_2 = Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)15)))))
		{
			goto IL_002e;
		}
	}
	{
		// setLang(de, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_3 = __this->get_de_12();
		AwardsMenuLang_setLang_mD1FBBD124E2B37C1506481BFE485E3C7C0813D07(__this, L_3, 0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_002e:
	{
		// else if (Application.systemLanguage == SystemLanguage.French)
		int32_t L_4;
		L_4 = Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)((int32_t)14)))))
		{
			goto IL_0045;
		}
	}
	{
		// setLang(fe, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_5 = __this->get_fe_13();
		AwardsMenuLang_setLang_mD1FBBD124E2B37C1506481BFE485E3C7C0813D07(__this, L_5, 0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0045:
	{
		// else if (Application.systemLanguage == SystemLanguage.Spanish)
		int32_t L_6;
		L_6 = Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_005c;
		}
	}
	{
		// setLang(es, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_7 = __this->get_es_14();
		AwardsMenuLang_setLang_mD1FBBD124E2B37C1506481BFE485E3C7C0813D07(__this, L_7, 0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_005c:
	{
		// else if (Application.systemLanguage == SystemLanguage.Russian)
		int32_t L_8;
		L_8 = Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)30)))))
		{
			goto IL_0073;
		}
	}
	{
		// setLang(ru, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_9 = __this->get_ru_15();
		AwardsMenuLang_setLang_mD1FBBD124E2B37C1506481BFE485E3C7C0813D07(__this, L_9, 0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0073:
	{
		// setLang(en, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_10 = __this->get_en_11();
		AwardsMenuLang_setLang_mD1FBBD124E2B37C1506481BFE485E3C7C0813D07(__this, L_10, 0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AwardsMenuLang::setLang(System.String[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AwardsMenuLang_setLang_mD1FBBD124E2B37C1506481BFE485E3C7C0813D07 (AwardsMenuLang_t7929C416FB74BDCC2DC4FA1F061FAE951E4F56EE * __this, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___Leng0, int32_t ___Index1, const RuntimeMethod* method)
{
	{
		// Text1.text = Leng[Index].ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_0 = __this->get_Text1_4();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_1 = ___Leng0;
		int32_t L_2 = ___Index1;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		String_t* L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		NullCheck(L_4);
		String_t* L_5;
		L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_4);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_5);
		// Text2.text = (Leng[Index + 1]).ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_6 = __this->get_Text2_5();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_7 = ___Leng0;
		int32_t L_8 = ___Index1;
		NullCheck(L_7);
		int32_t L_9 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
		String_t* L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck(L_10);
		String_t* L_11;
		L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		NullCheck(L_6);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_6, L_11);
		// Text3.text = (Leng[Index + 2]).ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_12 = __this->get_Text3_6();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_13 = ___Leng0;
		int32_t L_14 = ___Index1;
		NullCheck(L_13);
		int32_t L_15 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)2));
		String_t* L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck(L_16);
		String_t* L_17;
		L_17 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_16);
		NullCheck(L_12);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_12, L_17);
		// Text4.text = (Leng[Index + 3]).ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_18 = __this->get_Text4_7();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_19 = ___Leng0;
		int32_t L_20 = ___Index1;
		NullCheck(L_19);
		int32_t L_21 = ((int32_t)il2cpp_codegen_add((int32_t)L_20, (int32_t)3));
		String_t* L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		NullCheck(L_22);
		String_t* L_23;
		L_23 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_22);
		NullCheck(L_18);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_18, L_23);
		// Text5.text = (Leng[Index + 4]).ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_24 = __this->get_Text5_8();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_25 = ___Leng0;
		int32_t L_26 = ___Index1;
		NullCheck(L_25);
		int32_t L_27 = ((int32_t)il2cpp_codegen_add((int32_t)L_26, (int32_t)4));
		String_t* L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_28);
		String_t* L_29;
		L_29 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_28);
		NullCheck(L_24);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_24, L_29);
		// Text6.text = (Leng[Index + 5]).ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_30 = __this->get_Text6_9();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_31 = ___Leng0;
		int32_t L_32 = ___Index1;
		NullCheck(L_31);
		int32_t L_33 = ((int32_t)il2cpp_codegen_add((int32_t)L_32, (int32_t)5));
		String_t* L_34 = (L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_33));
		NullCheck(L_34);
		String_t* L_35;
		L_35 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_34);
		NullCheck(L_30);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_30, L_35);
		// }
		return;
	}
}
// System.Void AwardsMenuLang::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AwardsMenuLang__ctor_mC09878DD4D109FBFA839BECBF168D2533787441A (AwardsMenuLang_t7929C416FB74BDCC2DC4FA1F061FAE951E4F56EE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral252402BDDD47746D72180BEB27ACE12A14C28952);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral292E99589E6C7C2BF893F091E364EBBD4F55F82A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2CBE4FC4E0A773099AE34DEE978B5DFFFB73E9BA);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral31595E973ADCB725F8CD1C8D0BE95AC5C5F177F0);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral336B029D04847DC4F36985E1B89D1A0B11D299A3);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral37675749D5041A6ED53FAA3D96E0A53C91F68B5F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3C96E1BE89CC1EF02D4EC7F1C799E7A61AD171E2);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4B2FFD21A87B4EF130038DE3D2FFF5078BB872AD);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral518A22A9F69C395D395396210805148FC85CAC3F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5ED838701824663CF4628D94757507C98B26C5ED);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5FA2A08D6D10D5421B774A13D4C4288064837A89);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6F3725E84909989D3989E377719DC2A8E7AA3B63);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral742FEB59DC8AB83AAF3830BA129DBA5FFFE7F76E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral783C37D8989ADC1EE756FE0E59E5F44AA3D5140E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral92CC27364F419C9FE8921E49485099EE417ADB7A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral97A16F5602146D779A5D6883F92AEC125FF808D4);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA3F89865E30490E81ABEFDBF0D19EBC7E69538D2);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB0683F985BA7F461EFD91E56DF63ECCB9EB2DA87);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB895583198199E0518C92D1D5A9C95989F9A1B99);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCD65D28A5DDFB33A67DD4A29CE5A1700A02C5904);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD2F21AFCBF1FB9CC1CDACB1C883D5FDFBD5C70A4);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE7D08C2FFBAEC3DC48D0A1A70AEF7807D465C0AB);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEA058543F73E50CB1E0192DD94678A287B69370C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFF4E8113C1DD42448B93F3E73007CFEF6C650CD4);
		s_Il2CppMethodInitialized = true;
	}
	{
		// string[] pl = { "Zdobyte punkty", "WYMIEŃ", "WYMIEŃ", "WYMIEŃ", "POWRÓT DO MENU", "Brak wystarczającej ilości punktów!" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_0 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)6);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_1 = L_0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, _stringLiteralB0683F985BA7F461EFD91E56DF63ECCB9EB2DA87);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteralB0683F985BA7F461EFD91E56DF63ECCB9EB2DA87);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_2 = L_1;
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, _stringLiteral3C96E1BE89CC1EF02D4EC7F1C799E7A61AD171E2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral3C96E1BE89CC1EF02D4EC7F1C799E7A61AD171E2);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_3 = L_2;
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, _stringLiteral3C96E1BE89CC1EF02D4EC7F1C799E7A61AD171E2);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral3C96E1BE89CC1EF02D4EC7F1C799E7A61AD171E2);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_4 = L_3;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteral3C96E1BE89CC1EF02D4EC7F1C799E7A61AD171E2);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral3C96E1BE89CC1EF02D4EC7F1C799E7A61AD171E2);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_5 = L_4;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, _stringLiteralB895583198199E0518C92D1D5A9C95989F9A1B99);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteralB895583198199E0518C92D1D5A9C95989F9A1B99);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_6 = L_5;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral31595E973ADCB725F8CD1C8D0BE95AC5C5F177F0);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral31595E973ADCB725F8CD1C8D0BE95AC5C5F177F0);
		__this->set_pl_10(L_6);
		// string[] en = { "Score", "REPLACE", "REPLACE", "REPLACE", "BACK TO MENU", "Not enough points!" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_7 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)6);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_8 = L_7;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, _stringLiteral336B029D04847DC4F36985E1B89D1A0B11D299A3);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral336B029D04847DC4F36985E1B89D1A0B11D299A3);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_9 = L_8;
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral5ED838701824663CF4628D94757507C98B26C5ED);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral5ED838701824663CF4628D94757507C98B26C5ED);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_10 = L_9;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral5ED838701824663CF4628D94757507C98B26C5ED);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral5ED838701824663CF4628D94757507C98B26C5ED);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_11 = L_10;
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, _stringLiteral5ED838701824663CF4628D94757507C98B26C5ED);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral5ED838701824663CF4628D94757507C98B26C5ED);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_12 = L_11;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral783C37D8989ADC1EE756FE0E59E5F44AA3D5140E);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral783C37D8989ADC1EE756FE0E59E5F44AA3D5140E);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_13 = L_12;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral92CC27364F419C9FE8921E49485099EE417ADB7A);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral92CC27364F419C9FE8921E49485099EE417ADB7A);
		__this->set_en_11(L_13);
		// string[] de = { "Punkte erhalten", "ERSETZEN", "ERSETZEN", "ERSETZEN", "ZURÜCK ZUM MENÜ", "Nicht genügend Punkte!" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_14 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)6);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_15 = L_14;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteralE7D08C2FFBAEC3DC48D0A1A70AEF7807D465C0AB);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteralE7D08C2FFBAEC3DC48D0A1A70AEF7807D465C0AB);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_16 = L_15;
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, _stringLiteralCD65D28A5DDFB33A67DD4A29CE5A1700A02C5904);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteralCD65D28A5DDFB33A67DD4A29CE5A1700A02C5904);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_17 = L_16;
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, _stringLiteralCD65D28A5DDFB33A67DD4A29CE5A1700A02C5904);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteralCD65D28A5DDFB33A67DD4A29CE5A1700A02C5904);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_18 = L_17;
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, _stringLiteralCD65D28A5DDFB33A67DD4A29CE5A1700A02C5904);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteralCD65D28A5DDFB33A67DD4A29CE5A1700A02C5904);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_19 = L_18;
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, _stringLiteralD2F21AFCBF1FB9CC1CDACB1C883D5FDFBD5C70A4);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteralD2F21AFCBF1FB9CC1CDACB1C883D5FDFBD5C70A4);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_20 = L_19;
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral4B2FFD21A87B4EF130038DE3D2FFF5078BB872AD);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral4B2FFD21A87B4EF130038DE3D2FFF5078BB872AD);
		__this->set_de_12(L_20);
		// string[] fe = { "Points gagnés", "REMPLACER", "REMPLACER", "REMPLACER", "RETOUR AU MENU", "Pas assez de points!" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_21 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)6);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_22 = L_21;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, _stringLiteral6F3725E84909989D3989E377719DC2A8E7AA3B63);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral6F3725E84909989D3989E377719DC2A8E7AA3B63);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_23 = L_22;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral2CBE4FC4E0A773099AE34DEE978B5DFFFB73E9BA);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2CBE4FC4E0A773099AE34DEE978B5DFFFB73E9BA);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_24 = L_23;
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, _stringLiteral2CBE4FC4E0A773099AE34DEE978B5DFFFB73E9BA);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral2CBE4FC4E0A773099AE34DEE978B5DFFFB73E9BA);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_25 = L_24;
		NullCheck(L_25);
		ArrayElementTypeCheck (L_25, _stringLiteral2CBE4FC4E0A773099AE34DEE978B5DFFFB73E9BA);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral2CBE4FC4E0A773099AE34DEE978B5DFFFB73E9BA);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_26 = L_25;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteralEA058543F73E50CB1E0192DD94678A287B69370C);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteralEA058543F73E50CB1E0192DD94678A287B69370C);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_27 = L_26;
		NullCheck(L_27);
		ArrayElementTypeCheck (L_27, _stringLiteral97A16F5602146D779A5D6883F92AEC125FF808D4);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral97A16F5602146D779A5D6883F92AEC125FF808D4);
		__this->set_fe_13(L_27);
		// string[] es = { "Puntos ganados", "REEMPLAZAR", "REEMPLAZAR", "REEMPLAZAR", "VOLVER AL MENÚ", "No hay suficientes puntos!" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_28 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)6);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_29 = L_28;
		NullCheck(L_29);
		ArrayElementTypeCheck (L_29, _stringLiteralFF4E8113C1DD42448B93F3E73007CFEF6C650CD4);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteralFF4E8113C1DD42448B93F3E73007CFEF6C650CD4);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_30 = L_29;
		NullCheck(L_30);
		ArrayElementTypeCheck (L_30, _stringLiteral252402BDDD47746D72180BEB27ACE12A14C28952);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral252402BDDD47746D72180BEB27ACE12A14C28952);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_31 = L_30;
		NullCheck(L_31);
		ArrayElementTypeCheck (L_31, _stringLiteral252402BDDD47746D72180BEB27ACE12A14C28952);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral252402BDDD47746D72180BEB27ACE12A14C28952);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_32 = L_31;
		NullCheck(L_32);
		ArrayElementTypeCheck (L_32, _stringLiteral252402BDDD47746D72180BEB27ACE12A14C28952);
		(L_32)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral252402BDDD47746D72180BEB27ACE12A14C28952);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_33 = L_32;
		NullCheck(L_33);
		ArrayElementTypeCheck (L_33, _stringLiteral292E99589E6C7C2BF893F091E364EBBD4F55F82A);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral292E99589E6C7C2BF893F091E364EBBD4F55F82A);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_34 = L_33;
		NullCheck(L_34);
		ArrayElementTypeCheck (L_34, _stringLiteral37675749D5041A6ED53FAA3D96E0A53C91F68B5F);
		(L_34)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral37675749D5041A6ED53FAA3D96E0A53C91F68B5F);
		__this->set_es_14(L_34);
		// string[] ru = { "Заработанные баллы", "ЗАМЕНЯТЬ", "ЗАМЕНЯТЬ", "ЗАМЕНЯТЬ", "НАЗАД К МЕНЮ", "Мало очков!" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_35 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)6);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_36 = L_35;
		NullCheck(L_36);
		ArrayElementTypeCheck (L_36, _stringLiteral5FA2A08D6D10D5421B774A13D4C4288064837A89);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral5FA2A08D6D10D5421B774A13D4C4288064837A89);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_37 = L_36;
		NullCheck(L_37);
		ArrayElementTypeCheck (L_37, _stringLiteral742FEB59DC8AB83AAF3830BA129DBA5FFFE7F76E);
		(L_37)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral742FEB59DC8AB83AAF3830BA129DBA5FFFE7F76E);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_38 = L_37;
		NullCheck(L_38);
		ArrayElementTypeCheck (L_38, _stringLiteral742FEB59DC8AB83AAF3830BA129DBA5FFFE7F76E);
		(L_38)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral742FEB59DC8AB83AAF3830BA129DBA5FFFE7F76E);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_39 = L_38;
		NullCheck(L_39);
		ArrayElementTypeCheck (L_39, _stringLiteral742FEB59DC8AB83AAF3830BA129DBA5FFFE7F76E);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral742FEB59DC8AB83AAF3830BA129DBA5FFFE7F76E);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_40 = L_39;
		NullCheck(L_40);
		ArrayElementTypeCheck (L_40, _stringLiteralA3F89865E30490E81ABEFDBF0D19EBC7E69538D2);
		(L_40)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteralA3F89865E30490E81ABEFDBF0D19EBC7E69538D2);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_41 = L_40;
		NullCheck(L_41);
		ArrayElementTypeCheck (L_41, _stringLiteral518A22A9F69C395D395396210805148FC85CAC3F);
		(L_41)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral518A22A9F69C395D395396210805148FC85CAC3F);
		__this->set_ru_15(L_41);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DisplayPoints::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisplayPoints_Awake_mF7D76B9012B2867C3010C0DD04EEEA19A23E0FB6 (DisplayPoints_tD2C77305910E0F73D81CED0D2E01A289C08B740E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1764427109D31651865A98B70E1AE56C4E9DA044);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// int Score = PlayerPrefs.GetInt("SavePoints");
		int32_t L_0;
		L_0 = PlayerPrefs_GetInt_m6BCF9F844298D1810A68BAF23ECBA68C6960A986(_stringLiteral1764427109D31651865A98B70E1AE56C4E9DA044, /*hidden argument*/NULL);
		V_0 = L_0;
		// txtScore.text = Score.ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_1 = __this->get_txtScore_4();
		String_t* L_2;
		L_2 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_0), /*hidden argument*/NULL);
		NullCheck(L_1);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, L_2);
		// }
		return;
	}
}
// System.Void DisplayPoints::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisplayPoints__ctor_mFC4D6E5BBA97990603BBABF884C5DEE78BA8DBEA (DisplayPoints_tD2C77305910E0F73D81CED0D2E01A289C08B740E * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void EnterPIN::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnterPIN_Awake_mC760E9A64B19736731A1000D8B9658A5BED5CFFD (EnterPIN_t48ADD764A3FB09C507FD777FB1A570714C54C2A0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m2D99AC2081683F963C56EC738451EC0B59B5D137_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral035A0EFF495FDDF533CE351CD95BCE3D200A3FF8);
		s_Il2CppMethodInitialized = true;
	}
	{
		// TextError.GetComponent<Text>().enabled = false;
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_0 = __this->get_TextError_8();
		NullCheck(L_0);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_1;
		L_1 = Component_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m2D99AC2081683F963C56EC738451EC0B59B5D137(L_0, /*hidden argument*/Component_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m2D99AC2081683F963C56EC738451EC0B59B5D137_RuntimeMethod_var);
		NullCheck(L_1);
		Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32(L_1, (bool)0, /*hidden argument*/NULL);
		// if (PlayerPrefs.HasKey("PIN1"))
		bool L_2;
		L_2 = PlayerPrefs_HasKey_m48BE5886380B51AB495B91C9A26115B7CB958A92(_stringLiteral035A0EFF495FDDF533CE351CD95BCE3D200A3FF8, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		// SetPIN1 = PlayerPrefs.GetString("PIN1");
		String_t* L_3;
		L_3 = PlayerPrefs_GetString_mE7654C1031622A56CD8F248F53714B105A35A159(_stringLiteral035A0EFF495FDDF533CE351CD95BCE3D200A3FF8, /*hidden argument*/NULL);
		__this->set_SetPIN1_4(L_3);
	}

IL_002d:
	{
		// }
		return;
	}
}
// System.Void EnterPIN::CheckPINOn()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnterPIN_CheckPINOn_m60B9688B15BB696B8392F919F17433876D4F3CB4 (EnterPIN_t48ADD764A3FB09C507FD777FB1A570714C54C2A0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m2D99AC2081683F963C56EC738451EC0B59B5D137_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SetPIN = InputField.GetComponent<Text>().text;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_InputField_6();
		NullCheck(L_0);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_1;
		L_1 = GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65(L_0, /*hidden argument*/GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65_RuntimeMethod_var);
		NullCheck(L_1);
		String_t* L_2;
		L_2 = VirtFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_1);
		__this->set_SetPIN_5(L_2);
		// TextDisplay.GetComponent<Text>().text = SetPIN;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3 = __this->get_TextDisplay_7();
		NullCheck(L_3);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_4;
		L_4 = GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65(L_3, /*hidden argument*/GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65_RuntimeMethod_var);
		String_t* L_5 = __this->get_SetPIN_5();
		NullCheck(L_4);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_4, L_5);
		// if (SetPIN == SetPIN1)
		String_t* L_6 = __this->get_SetPIN_5();
		String_t* L_7 = __this->get_SetPIN1_4();
		bool L_8;
		L_8 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0046;
		}
	}
	{
		// SceneManager.LoadScene(8);
		IL2CPP_RUNTIME_CLASS_INIT(SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_m5550E6368A6D0E37DACEDA3C5E4BA331836BC3C5(8, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0046:
	{
		// TextError.color = Color.red;
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_9 = __this->get_TextError_8();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_10;
		L_10 = Color_get_red_m9BD55EBF7A74A515330FA5F7AC7A67C8A8913DD8(/*hidden argument*/NULL);
		NullCheck(L_9);
		VirtActionInvoker1< Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_9, L_10);
		// TextError.GetComponent<Text>().enabled = true;
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_11 = __this->get_TextError_8();
		NullCheck(L_11);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_12;
		L_12 = Component_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m2D99AC2081683F963C56EC738451EC0B59B5D137(L_11, /*hidden argument*/Component_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m2D99AC2081683F963C56EC738451EC0B59B5D137_RuntimeMethod_var);
		NullCheck(L_12);
		Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32(L_12, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void EnterPIN::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnterPIN__ctor_mD170EF96023CA0F52FD50240FF3BD621227F3BFD (EnterPIN_t48ADD764A3FB09C507FD777FB1A570714C54C2A0 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void EnterPINLang::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnterPINLang_Awake_m6389AE7D5503D098139FC906407E0E10E87CE3EE (EnterPINLang_t7DB4141FE4619EA8936999CB62CFD95077E1B3D5 * __this, const RuntimeMethod* method)
{
	{
		// if (Application.systemLanguage == SystemLanguage.Polish)
		int32_t L_0;
		L_0 = Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)27)))))
		{
			goto IL_0017;
		}
	}
	{
		// setLang(pl, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_1 = __this->get_pl_8();
		EnterPINLang_setLang_m20C95F886AFB7F533F7B37F09F749D4F0D3BA165(__this, L_1, 0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0017:
	{
		// else if (Application.systemLanguage == SystemLanguage.German)
		int32_t L_2;
		L_2 = Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)15)))))
		{
			goto IL_002e;
		}
	}
	{
		// setLang(de, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_3 = __this->get_de_10();
		EnterPINLang_setLang_m20C95F886AFB7F533F7B37F09F749D4F0D3BA165(__this, L_3, 0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_002e:
	{
		// else if (Application.systemLanguage == SystemLanguage.French)
		int32_t L_4;
		L_4 = Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)((int32_t)14)))))
		{
			goto IL_0045;
		}
	}
	{
		// setLang(fe, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_5 = __this->get_fe_11();
		EnterPINLang_setLang_m20C95F886AFB7F533F7B37F09F749D4F0D3BA165(__this, L_5, 0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0045:
	{
		// else if (Application.systemLanguage == SystemLanguage.Spanish)
		int32_t L_6;
		L_6 = Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_005c;
		}
	}
	{
		// setLang(es, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_7 = __this->get_es_12();
		EnterPINLang_setLang_m20C95F886AFB7F533F7B37F09F749D4F0D3BA165(__this, L_7, 0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_005c:
	{
		// else if (Application.systemLanguage == SystemLanguage.Russian)
		int32_t L_8;
		L_8 = Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)30)))))
		{
			goto IL_0073;
		}
	}
	{
		// setLang(ru, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_9 = __this->get_ru_13();
		EnterPINLang_setLang_m20C95F886AFB7F533F7B37F09F749D4F0D3BA165(__this, L_9, 0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0073:
	{
		// setLang(en, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_10 = __this->get_en_9();
		EnterPINLang_setLang_m20C95F886AFB7F533F7B37F09F749D4F0D3BA165(__this, L_10, 0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void EnterPINLang::setLang(System.String[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnterPINLang_setLang_m20C95F886AFB7F533F7B37F09F749D4F0D3BA165 (EnterPINLang_t7DB4141FE4619EA8936999CB62CFD95077E1B3D5 * __this, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___Leng0, int32_t ___Index1, const RuntimeMethod* method)
{
	{
		// Text1.text = Leng[Index].ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_0 = __this->get_Text1_5();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_1 = ___Leng0;
		int32_t L_2 = ___Index1;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		String_t* L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		NullCheck(L_4);
		String_t* L_5;
		L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_4);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_5);
		// Text2.text = (Leng[Index + 1]).ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_6 = __this->get_Text2_6();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_7 = ___Leng0;
		int32_t L_8 = ___Index1;
		NullCheck(L_7);
		int32_t L_9 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
		String_t* L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck(L_10);
		String_t* L_11;
		L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		NullCheck(L_6);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_6, L_11);
		// Text3.text = (Leng[Index + 2]).ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_12 = __this->get_Text3_7();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_13 = ___Leng0;
		int32_t L_14 = ___Index1;
		NullCheck(L_13);
		int32_t L_15 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)2));
		String_t* L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck(L_16);
		String_t* L_17;
		L_17 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_16);
		NullCheck(L_12);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_12, L_17);
		// }
		return;
	}
}
// System.Void EnterPINLang::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnterPINLang__ctor_mD5FA4442D1A7B53C968E7A3E161F5CE0F8992B51 (EnterPINLang_t7DB4141FE4619EA8936999CB62CFD95077E1B3D5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral00CC5BFA1E037DB47EBF07DAAEFFCD993088112B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1AE348EAFA097AB898941EAFE912D711A407DA10);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5186B0598411520C99562D6615A80942FE52EF2A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5768436A9385EC30851400A543BFEE4F42D779D4);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral68ABC5B7E2D9C8F7C46E42B01F19EFF56C819259);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral71D0A6517C0D3A477013739CD5FE5F7310ACBB30);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7B67306E398C4403BAF521C10FBB0E9E86B3EB5E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral80C25E49528430AD5C029A6E09F8A1DC7107EC3F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral86C6A744076C8CC32077709BDF831D565B511A86);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralAC9516655C9A985B36C0B7CE27942BD81B8908EB);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCE163314FAA201B884DEF98401B31265D6FF4978);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD3C428E5D5F2471B5037540743F73946D676FACE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE6562869B1E0927E641FB9CEB82070BFEB4DBF83);
		s_Il2CppMethodInitialized = true;
	}
	{
		// string[] pl = { "WPROWADŹ PIN", "NIEPOPRAWNY PIN", "OK" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_0 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)3);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_1 = L_0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, _stringLiteralCE163314FAA201B884DEF98401B31265D6FF4978);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteralCE163314FAA201B884DEF98401B31265D6FF4978);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_2 = L_1;
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, _stringLiteral86C6A744076C8CC32077709BDF831D565B511A86);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral86C6A744076C8CC32077709BDF831D565B511A86);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_3 = L_2;
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, _stringLiteral1AE348EAFA097AB898941EAFE912D711A407DA10);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1AE348EAFA097AB898941EAFE912D711A407DA10);
		__this->set_pl_8(L_3);
		// string[] en = { "ENTER PIN", "INCORRECT PIN", "OK" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_4 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)3);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_5 = L_4;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, _stringLiteralD3C428E5D5F2471B5037540743F73946D676FACE);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteralD3C428E5D5F2471B5037540743F73946D676FACE);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_6 = L_5;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral5186B0598411520C99562D6615A80942FE52EF2A);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral5186B0598411520C99562D6615A80942FE52EF2A);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_7 = L_6;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral1AE348EAFA097AB898941EAFE912D711A407DA10);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1AE348EAFA097AB898941EAFE912D711A407DA10);
		__this->set_en_9(L_7);
		// string[] de = { "PIN EINGEBEN", "FALSCHE PIN", "OK"};
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_8 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)3);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_9 = L_8;
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral80C25E49528430AD5C029A6E09F8A1DC7107EC3F);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral80C25E49528430AD5C029A6E09F8A1DC7107EC3F);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_10 = L_9;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteralAC9516655C9A985B36C0B7CE27942BD81B8908EB);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteralAC9516655C9A985B36C0B7CE27942BD81B8908EB);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_11 = L_10;
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, _stringLiteral1AE348EAFA097AB898941EAFE912D711A407DA10);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1AE348EAFA097AB898941EAFE912D711A407DA10);
		__this->set_de_10(L_11);
		// string[] fe = { "ENTRER PIN", "PIN INCORRECT", "OK"};
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_12 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)3);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_13 = L_12;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral71D0A6517C0D3A477013739CD5FE5F7310ACBB30);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral71D0A6517C0D3A477013739CD5FE5F7310ACBB30);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_14 = L_13;
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, _stringLiteral5768436A9385EC30851400A543BFEE4F42D779D4);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral5768436A9385EC30851400A543BFEE4F42D779D4);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_15 = L_14;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral1AE348EAFA097AB898941EAFE912D711A407DA10);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1AE348EAFA097AB898941EAFE912D711A407DA10);
		__this->set_fe_11(L_15);
		// string[] es = { "INGRESE SU PIN", "PIN INCORRECTO", "OK"};
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_16 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)3);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_17 = L_16;
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, _stringLiteral68ABC5B7E2D9C8F7C46E42B01F19EFF56C819259);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral68ABC5B7E2D9C8F7C46E42B01F19EFF56C819259);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_18 = L_17;
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, _stringLiteralE6562869B1E0927E641FB9CEB82070BFEB4DBF83);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteralE6562869B1E0927E641FB9CEB82070BFEB4DBF83);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_19 = L_18;
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, _stringLiteral1AE348EAFA097AB898941EAFE912D711A407DA10);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1AE348EAFA097AB898941EAFE912D711A407DA10);
		__this->set_es_12(L_19);
		// string[] ru = { "ВВЕДИТЕ ПИН-код", "НЕПРАВИЛЬНЫЙ ПИН", "OK"};
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_20 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)3);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_21 = L_20;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, _stringLiteral7B67306E398C4403BAF521C10FBB0E9E86B3EB5E);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral7B67306E398C4403BAF521C10FBB0E9E86B3EB5E);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_22 = L_21;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, _stringLiteral00CC5BFA1E037DB47EBF07DAAEFFCD993088112B);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral00CC5BFA1E037DB47EBF07DAAEFFCD993088112B);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_23 = L_22;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral1AE348EAFA097AB898941EAFE912D711A407DA10);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1AE348EAFA097AB898941EAFE912D711A407DA10);
		__this->set_ru_13(L_23);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GM::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GM_Awake_m58CF3D467404FD735D23405BED07D28919FFD4A8 (GM_tFC30914A710042CAFEA7CF10E2AD01C9A529CEF0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m2D99AC2081683F963C56EC738451EC0B59B5D137_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GM_tFC30914A710042CAFEA7CF10E2AD01C9A529CEF0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1764427109D31651865A98B70E1AE56C4E9DA044);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4D0A51C14A2331C361C3CC21880E4CB63FEAFB6A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral639AB154A61332B41DDAE6F8231F56224448A1E2);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF7418F00A0BC87B788A53131CE12EB9332E71D95);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// TextError.GetComponent<Text>().enabled = false;
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_0 = __this->get_TextError_12();
		NullCheck(L_0);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_1;
		L_1 = Component_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m2D99AC2081683F963C56EC738451EC0B59B5D137(L_0, /*hidden argument*/Component_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m2D99AC2081683F963C56EC738451EC0B59B5D137_RuntimeMethod_var);
		NullCheck(L_1);
		Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32(L_1, (bool)0, /*hidden argument*/NULL);
		// if (instance == null)
		GM_tFC30914A710042CAFEA7CF10E2AD01C9A529CEF0 * L_2 = ((GM_tFC30914A710042CAFEA7CF10E2AD01C9A529CEF0_StaticFields*)il2cpp_codegen_static_fields_for(GM_tFC30914A710042CAFEA7CF10E2AD01C9A529CEF0_il2cpp_TypeInfo_var))->get_instance_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_2, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0024;
		}
	}
	{
		// instance = this;
		((GM_tFC30914A710042CAFEA7CF10E2AD01C9A529CEF0_StaticFields*)il2cpp_codegen_static_fields_for(GM_tFC30914A710042CAFEA7CF10E2AD01C9A529CEF0_il2cpp_TypeInfo_var))->set_instance_4(__this);
	}

IL_0024:
	{
		// if ((PlayerPrefs.HasKey("OneAward")&&PlayerPrefs.HasKey("TwoAward"))&& PlayerPrefs.HasKey("ThreeAward"))
		bool L_4;
		L_4 = PlayerPrefs_HasKey_m48BE5886380B51AB495B91C9A26115B7CB958A92(_stringLiteralF7418F00A0BC87B788A53131CE12EB9332E71D95, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_00ab;
		}
	}
	{
		bool L_5;
		L_5 = PlayerPrefs_HasKey_m48BE5886380B51AB495B91C9A26115B7CB958A92(_stringLiteral639AB154A61332B41DDAE6F8231F56224448A1E2, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_00ab;
		}
	}
	{
		bool L_6;
		L_6 = PlayerPrefs_HasKey_m48BE5886380B51AB495B91C9A26115B7CB958A92(_stringLiteral4D0A51C14A2331C361C3CC21880E4CB63FEAFB6A, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_00ab;
		}
	}
	{
		// OneAward = PlayerPrefs.GetString("OneAward");
		String_t* L_7;
		L_7 = PlayerPrefs_GetString_mE7654C1031622A56CD8F248F53714B105A35A159(_stringLiteralF7418F00A0BC87B788A53131CE12EB9332E71D95, /*hidden argument*/NULL);
		__this->set_OneAward_5(L_7);
		// TwoAward = PlayerPrefs.GetString("TwoAward");
		String_t* L_8;
		L_8 = PlayerPrefs_GetString_mE7654C1031622A56CD8F248F53714B105A35A159(_stringLiteral639AB154A61332B41DDAE6F8231F56224448A1E2, /*hidden argument*/NULL);
		__this->set_TwoAward_6(L_8);
		// ThreeAward = PlayerPrefs.GetString("ThreeAward");
		String_t* L_9;
		L_9 = PlayerPrefs_GetString_mE7654C1031622A56CD8F248F53714B105A35A159(_stringLiteral4D0A51C14A2331C361C3CC21880E4CB63FEAFB6A, /*hidden argument*/NULL);
		__this->set_ThreeAward_7(L_9);
		// DisplayOneAward.text = OneAward;
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_10 = __this->get_DisplayOneAward_9();
		String_t* L_11 = __this->get_OneAward_5();
		NullCheck(L_10);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_10, L_11);
		// DisplayTwoAward.text = TwoAward;
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_12 = __this->get_DisplayTwoAward_10();
		String_t* L_13 = __this->get_TwoAward_6();
		NullCheck(L_12);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_12, L_13);
		// DisplayThreeAward.text = ThreeAward;
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_14 = __this->get_DisplayThreeAward_11();
		String_t* L_15 = __this->get_ThreeAward_7();
		NullCheck(L_14);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_14, L_15);
	}

IL_00ab:
	{
		// int Score = PlayerPrefs.GetInt("SavePoints");
		int32_t L_16;
		L_16 = PlayerPrefs_GetInt_m6BCF9F844298D1810A68BAF23ECBA68C6960A986(_stringLiteral1764427109D31651865A98B70E1AE56C4E9DA044, /*hidden argument*/NULL);
		V_0 = L_16;
		// txtScore.text = Score.ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_17 = __this->get_txtScore_8();
		String_t* L_18;
		L_18 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_0), /*hidden argument*/NULL);
		NullCheck(L_17);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_17, L_18);
		// }
		return;
	}
}
// System.Void GM::ChangePointsOneAward()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GM_ChangePointsOneAward_m3215E6D6B990385199DF89BD488B22F4F11CCE1A (GM_tFC30914A710042CAFEA7CF10E2AD01C9A529CEF0 * __this, const RuntimeMethod* method)
{
	{
		// MinusPointsOneAward(1000);
		GM_MinusPointsOneAward_m49EE269CBFE173553961DC4B1C10C71F1C76FE8E(__this, ((int32_t)1000), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void GM::ChangePointsTwoAward()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GM_ChangePointsTwoAward_m5AEE14D3E762C6EE9BD2211432A4C1CEF83DD606 (GM_tFC30914A710042CAFEA7CF10E2AD01C9A529CEF0 * __this, const RuntimeMethod* method)
{
	{
		// MinusPointsTwoAward(2000);
		GM_MinusPointsTwoAward_mE225AE18889C36C657B21FA781C3910CABF7D646(__this, ((int32_t)2000), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void GM::ChangePointsThreeAward()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GM_ChangePointsThreeAward_m6796CE768ED25A769EC0193975C13BB80CA21682 (GM_tFC30914A710042CAFEA7CF10E2AD01C9A529CEF0 * __this, const RuntimeMethod* method)
{
	{
		// MinusPointsThreeAward(3000);
		GM_MinusPointsThreeAward_m9B88BC9C158273893CF784A7C0146A8526DF72D7(__this, ((int32_t)3000), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void GM::MinusPointsOneAward(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GM_MinusPointsOneAward_m49EE269CBFE173553961DC4B1C10C71F1C76FE8E (GM_tFC30914A710042CAFEA7CF10E2AD01C9A529CEF0 * __this, int32_t ___Point0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m2D99AC2081683F963C56EC738451EC0B59B5D137_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1764427109D31651865A98B70E1AE56C4E9DA044);
		s_Il2CppMethodInitialized = true;
	}
	{
		// int Score = PlayerPrefs.GetInt("SavePoints");
		int32_t L_0;
		L_0 = PlayerPrefs_GetInt_m6BCF9F844298D1810A68BAF23ECBA68C6960A986(_stringLiteral1764427109D31651865A98B70E1AE56C4E9DA044, /*hidden argument*/NULL);
		// if (Score < Point)
		int32_t L_1 = ___Point0;
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_001f;
		}
	}
	{
		// TextError.GetComponent<Text>().enabled = true;
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_2 = __this->get_TextError_12();
		NullCheck(L_2);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_3;
		L_3 = Component_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m2D99AC2081683F963C56EC738451EC0B59B5D137(L_2, /*hidden argument*/Component_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m2D99AC2081683F963C56EC738451EC0B59B5D137_RuntimeMethod_var);
		NullCheck(L_3);
		Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32(L_3, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}

IL_001f:
	{
		// DisplayScore(1000);
		GM_DisplayScore_m9D1B41034079EE6F05346F3DFA4F69D031516C3D(__this, ((int32_t)1000), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void GM::MinusPointsTwoAward(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GM_MinusPointsTwoAward_mE225AE18889C36C657B21FA781C3910CABF7D646 (GM_tFC30914A710042CAFEA7CF10E2AD01C9A529CEF0 * __this, int32_t ___Point0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m2D99AC2081683F963C56EC738451EC0B59B5D137_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1764427109D31651865A98B70E1AE56C4E9DA044);
		s_Il2CppMethodInitialized = true;
	}
	{
		// int Score = PlayerPrefs.GetInt("SavePoints");
		int32_t L_0;
		L_0 = PlayerPrefs_GetInt_m6BCF9F844298D1810A68BAF23ECBA68C6960A986(_stringLiteral1764427109D31651865A98B70E1AE56C4E9DA044, /*hidden argument*/NULL);
		// if (Score > Point)
		int32_t L_1 = ___Point0;
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			goto IL_0019;
		}
	}
	{
		// DisplayScore(2000);
		GM_DisplayScore_m9D1B41034079EE6F05346F3DFA4F69D031516C3D(__this, ((int32_t)2000), /*hidden argument*/NULL);
		// }
		return;
	}

IL_0019:
	{
		// TextError.GetComponent<Text>().enabled = true;
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_2 = __this->get_TextError_12();
		NullCheck(L_2);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_3;
		L_3 = Component_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m2D99AC2081683F963C56EC738451EC0B59B5D137(L_2, /*hidden argument*/Component_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m2D99AC2081683F963C56EC738451EC0B59B5D137_RuntimeMethod_var);
		NullCheck(L_3);
		Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32(L_3, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void GM::MinusPointsThreeAward(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GM_MinusPointsThreeAward_m9B88BC9C158273893CF784A7C0146A8526DF72D7 (GM_tFC30914A710042CAFEA7CF10E2AD01C9A529CEF0 * __this, int32_t ___Point0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m2D99AC2081683F963C56EC738451EC0B59B5D137_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1764427109D31651865A98B70E1AE56C4E9DA044);
		s_Il2CppMethodInitialized = true;
	}
	{
		// int Score = PlayerPrefs.GetInt("SavePoints");
		int32_t L_0;
		L_0 = PlayerPrefs_GetInt_m6BCF9F844298D1810A68BAF23ECBA68C6960A986(_stringLiteral1764427109D31651865A98B70E1AE56C4E9DA044, /*hidden argument*/NULL);
		// if (Score > Point)
		int32_t L_1 = ___Point0;
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			goto IL_0019;
		}
	}
	{
		// DisplayScore(3000);
		GM_DisplayScore_m9D1B41034079EE6F05346F3DFA4F69D031516C3D(__this, ((int32_t)3000), /*hidden argument*/NULL);
		// }
		return;
	}

IL_0019:
	{
		// TextError.GetComponent<Text>().enabled = true;
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_2 = __this->get_TextError_12();
		NullCheck(L_2);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_3;
		L_3 = Component_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m2D99AC2081683F963C56EC738451EC0B59B5D137(L_2, /*hidden argument*/Component_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m2D99AC2081683F963C56EC738451EC0B59B5D137_RuntimeMethod_var);
		NullCheck(L_3);
		Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32(L_3, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void GM::DisplayScore(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GM_DisplayScore_m9D1B41034079EE6F05346F3DFA4F69D031516C3D (GM_tFC30914A710042CAFEA7CF10E2AD01C9A529CEF0 * __this, int32_t ___Point0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1764427109D31651865A98B70E1AE56C4E9DA044);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// int Score = PlayerPrefs.GetInt("SavePoints");
		int32_t L_0;
		L_0 = PlayerPrefs_GetInt_m6BCF9F844298D1810A68BAF23ECBA68C6960A986(_stringLiteral1764427109D31651865A98B70E1AE56C4E9DA044, /*hidden argument*/NULL);
		// int Points = Score - Point;
		int32_t L_1 = ___Point0;
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_0, (int32_t)L_1));
		// PlayerPrefs.SetInt("SavePoints", Points);
		int32_t L_2 = V_0;
		PlayerPrefs_SetInt_m0C5C977E960B9CA8F9AB73AF4129C3DCABD067B6(_stringLiteral1764427109D31651865A98B70E1AE56C4E9DA044, L_2, /*hidden argument*/NULL);
		// PlayerPrefs.Save();
		PlayerPrefs_Save_m2C1E628FA335095CD88D0DA1CB50ACC924667EEC(/*hidden argument*/NULL);
		// txtScore.text = Points.ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_3 = __this->get_txtScore_8();
		String_t* L_4;
		L_4 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_0), /*hidden argument*/NULL);
		NullCheck(L_3);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_3, L_4);
		// }
		return;
	}
}
// System.Void GM::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GM__ctor_mEB1FBC9DA28AE63BEA0C4EDE9261034A5ED4932F (GM_tFC30914A710042CAFEA7CF10E2AD01C9A529CEF0 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GameLang::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameLang_Awake_m88182EB0C0AE4A78DB88095F9D2B16A2237CEC5E (GameLang_t88B393D27387CFB1391819269B4FD02205959915 * __this, const RuntimeMethod* method)
{
	{
		// if (Application.systemLanguage == SystemLanguage.Polish)
		int32_t L_0;
		L_0 = Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)27)))))
		{
			goto IL_0017;
		}
	}
	{
		// setLang(pl, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_1 = __this->get_pl_8();
		GameLang_setLang_mDB511190EC4318D87062125C2656C1EE9B83F8E9(__this, L_1, 0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0017:
	{
		// else if (Application.systemLanguage == SystemLanguage.German)
		int32_t L_2;
		L_2 = Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)15)))))
		{
			goto IL_002e;
		}
	}
	{
		// setLang(de, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_3 = __this->get_de_10();
		GameLang_setLang_mDB511190EC4318D87062125C2656C1EE9B83F8E9(__this, L_3, 0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_002e:
	{
		// else if (Application.systemLanguage == SystemLanguage.French)
		int32_t L_4;
		L_4 = Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)((int32_t)14)))))
		{
			goto IL_0045;
		}
	}
	{
		// setLang(fe, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_5 = __this->get_fe_11();
		GameLang_setLang_mDB511190EC4318D87062125C2656C1EE9B83F8E9(__this, L_5, 0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0045:
	{
		// else if (Application.systemLanguage == SystemLanguage.Spanish)
		int32_t L_6;
		L_6 = Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_005c;
		}
	}
	{
		// setLang(es, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_7 = __this->get_es_12();
		GameLang_setLang_mDB511190EC4318D87062125C2656C1EE9B83F8E9(__this, L_7, 0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_005c:
	{
		// else if (Application.systemLanguage == SystemLanguage.Russian)
		int32_t L_8;
		L_8 = Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)30)))))
		{
			goto IL_0073;
		}
	}
	{
		// setLang(ru, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_9 = __this->get_ru_13();
		GameLang_setLang_mDB511190EC4318D87062125C2656C1EE9B83F8E9(__this, L_9, 0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0073:
	{
		// setLang(en, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_10 = __this->get_en_9();
		GameLang_setLang_mDB511190EC4318D87062125C2656C1EE9B83F8E9(__this, L_10, 0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void GameLang::setLang(System.String[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameLang_setLang_mDB511190EC4318D87062125C2656C1EE9B83F8E9 (GameLang_t88B393D27387CFB1391819269B4FD02205959915 * __this, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___Leng0, int32_t ___Index1, const RuntimeMethod* method)
{
	{
		// Score.text = Leng[Index].ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_0 = __this->get_Score_4();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_1 = ___Leng0;
		int32_t L_2 = ___Index1;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		String_t* L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		NullCheck(L_4);
		String_t* L_5;
		L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_4);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_5);
		// Start.text = (Leng[Index + 1]).ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_6 = __this->get_Start_5();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_7 = ___Leng0;
		int32_t L_8 = ___Index1;
		NullCheck(L_7);
		int32_t L_9 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
		String_t* L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck(L_10);
		String_t* L_11;
		L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		NullCheck(L_6);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_6, L_11);
		// Stop.text = (Leng[Index + 2]).ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_12 = __this->get_Stop_6();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_13 = ___Leng0;
		int32_t L_14 = ___Index1;
		NullCheck(L_13);
		int32_t L_15 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)2));
		String_t* L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck(L_16);
		String_t* L_17;
		L_17 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_16);
		NullCheck(L_12);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_12, L_17);
		// Menu.text = (Leng[Index + 3]).ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_18 = __this->get_Menu_7();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_19 = ___Leng0;
		int32_t L_20 = ___Index1;
		NullCheck(L_19);
		int32_t L_21 = ((int32_t)il2cpp_codegen_add((int32_t)L_20, (int32_t)3));
		String_t* L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		NullCheck(L_22);
		String_t* L_23;
		L_23 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_22);
		NullCheck(L_18);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_18, L_23);
		// }
		return;
	}
}
// System.Void GameLang::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameLang__ctor_m144185E266545F58DB685C555BDC78E98E4C4AB3 (GameLang_t88B393D27387CFB1391819269B4FD02205959915 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral336B029D04847DC4F36985E1B89D1A0B11D299A3);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3CA684424B0F3C717BBC6239F51361B70485D4A1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5FA2A08D6D10D5421B774A13D4C4288064837A89);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral66E7E28240AB29C3409FC3DBC7531D2907A56F02);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6F3725E84909989D3989E377719DC2A8E7AA3B63);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral98EB6A098B53C71FDCD7160F6A160884F3F17266);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB0448E6572AD97C10FD7929B5F4958C2136F2E88);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB0683F985BA7F461EFD91E56DF63ECCB9EB2DA87);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD1068DE67DE168DFDADD76A0088D405170ABC15E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD2CE79FC932055F49C0E42CD510FAAC84C4982BB);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE7D08C2FFBAEC3DC48D0A1A70AEF7807D465C0AB);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFF4E8113C1DD42448B93F3E73007CFEF6C650CD4);
		s_Il2CppMethodInitialized = true;
	}
	{
		// string[] pl = { "Zdobyte punkty", "START", "STOP", "MENU" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_0 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)4);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_1 = L_0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, _stringLiteralB0683F985BA7F461EFD91E56DF63ECCB9EB2DA87);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteralB0683F985BA7F461EFD91E56DF63ECCB9EB2DA87);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_2 = L_1;
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, _stringLiteral3CA684424B0F3C717BBC6239F51361B70485D4A1);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral3CA684424B0F3C717BBC6239F51361B70485D4A1);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_3 = L_2;
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, _stringLiteralB0448E6572AD97C10FD7929B5F4958C2136F2E88);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteralB0448E6572AD97C10FD7929B5F4958C2136F2E88);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_4 = L_3;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteralD1068DE67DE168DFDADD76A0088D405170ABC15E);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteralD1068DE67DE168DFDADD76A0088D405170ABC15E);
		__this->set_pl_8(L_4);
		// string[] en = { "Score", "START", "STOP", "MENU" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_5 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)4);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_6 = L_5;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral336B029D04847DC4F36985E1B89D1A0B11D299A3);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral336B029D04847DC4F36985E1B89D1A0B11D299A3);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_7 = L_6;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral3CA684424B0F3C717BBC6239F51361B70485D4A1);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral3CA684424B0F3C717BBC6239F51361B70485D4A1);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_8 = L_7;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, _stringLiteralB0448E6572AD97C10FD7929B5F4958C2136F2E88);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteralB0448E6572AD97C10FD7929B5F4958C2136F2E88);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_9 = L_8;
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteralD1068DE67DE168DFDADD76A0088D405170ABC15E);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteralD1068DE67DE168DFDADD76A0088D405170ABC15E);
		__this->set_en_9(L_9);
		// string[] de = { "Punkte erhalten", "START", "STOP", "MENU" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_10 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)4);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_11 = L_10;
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, _stringLiteralE7D08C2FFBAEC3DC48D0A1A70AEF7807D465C0AB);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteralE7D08C2FFBAEC3DC48D0A1A70AEF7807D465C0AB);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_12 = L_11;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3CA684424B0F3C717BBC6239F51361B70485D4A1);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral3CA684424B0F3C717BBC6239F51361B70485D4A1);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_13 = L_12;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteralB0448E6572AD97C10FD7929B5F4958C2136F2E88);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteralB0448E6572AD97C10FD7929B5F4958C2136F2E88);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_14 = L_13;
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, _stringLiteralD1068DE67DE168DFDADD76A0088D405170ABC15E);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteralD1068DE67DE168DFDADD76A0088D405170ABC15E);
		__this->set_de_10(L_14);
		// string[] fe = { "Points gagnés", "START", "STOP", "MENU" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_15 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)4);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_16 = L_15;
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, _stringLiteral6F3725E84909989D3989E377719DC2A8E7AA3B63);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral6F3725E84909989D3989E377719DC2A8E7AA3B63);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_17 = L_16;
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, _stringLiteral3CA684424B0F3C717BBC6239F51361B70485D4A1);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral3CA684424B0F3C717BBC6239F51361B70485D4A1);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_18 = L_17;
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, _stringLiteralB0448E6572AD97C10FD7929B5F4958C2136F2E88);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteralB0448E6572AD97C10FD7929B5F4958C2136F2E88);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_19 = L_18;
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, _stringLiteralD1068DE67DE168DFDADD76A0088D405170ABC15E);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteralD1068DE67DE168DFDADD76A0088D405170ABC15E);
		__this->set_fe_11(L_19);
		// string[] es = { "Puntos ganados", "START", "STOP", "MENU" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_20 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)4);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_21 = L_20;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, _stringLiteralFF4E8113C1DD42448B93F3E73007CFEF6C650CD4);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteralFF4E8113C1DD42448B93F3E73007CFEF6C650CD4);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_22 = L_21;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, _stringLiteral3CA684424B0F3C717BBC6239F51361B70485D4A1);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral3CA684424B0F3C717BBC6239F51361B70485D4A1);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_23 = L_22;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteralB0448E6572AD97C10FD7929B5F4958C2136F2E88);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteralB0448E6572AD97C10FD7929B5F4958C2136F2E88);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_24 = L_23;
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, _stringLiteralD1068DE67DE168DFDADD76A0088D405170ABC15E);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteralD1068DE67DE168DFDADD76A0088D405170ABC15E);
		__this->set_es_12(L_24);
		// string[] ru = { "Заработанные баллы", "НАЧНИТЕ", "ОСТАНОВКА", "МЕНЮ" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_25 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)4);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_26 = L_25;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral5FA2A08D6D10D5421B774A13D4C4288064837A89);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral5FA2A08D6D10D5421B774A13D4C4288064837A89);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_27 = L_26;
		NullCheck(L_27);
		ArrayElementTypeCheck (L_27, _stringLiteralD2CE79FC932055F49C0E42CD510FAAC84C4982BB);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteralD2CE79FC932055F49C0E42CD510FAAC84C4982BB);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_28 = L_27;
		NullCheck(L_28);
		ArrayElementTypeCheck (L_28, _stringLiteral66E7E28240AB29C3409FC3DBC7531D2907A56F02);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral66E7E28240AB29C3409FC3DBC7531D2907A56F02);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_29 = L_28;
		NullCheck(L_29);
		ArrayElementTypeCheck (L_29, _stringLiteral98EB6A098B53C71FDCD7160F6A160884F3F17266);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral98EB6A098B53C71FDCD7160F6A160884F3F17266);
		__this->set_ru_13(L_29);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GameMenuLang::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameMenuLang_Awake_mE914B67055DF65ECD53A98153DDCFDA0C03640E9 (GameMenuLang_t49BEAA2ABAFC0A899B3346BEF7C050C4212D467C * __this, const RuntimeMethod* method)
{
	{
		// if (Application.systemLanguage == SystemLanguage.Polish)
		int32_t L_0;
		L_0 = Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)27)))))
		{
			goto IL_002a;
		}
	}
	{
		// Flag.sprite = Flags[1];
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_1 = __this->get_Flag_8();
		SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77* L_2 = __this->get_Flags_9();
		NullCheck(L_2);
		int32_t L_3 = 1;
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		NullCheck(L_1);
		Image_set_sprite_m55C50F18ABA0A98E926FC777F7D07FA18A7D04E4(L_1, L_4, /*hidden argument*/NULL);
		// setLang(pl, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_5 = __this->get_pl_10();
		GameMenuLang_setLang_mA20C12C7B1D825EFD990246302FD9401A98217B9(__this, L_5, 0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_002a:
	{
		// else if (Application.systemLanguage == SystemLanguage.German)
		int32_t L_6;
		L_6 = Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)((int32_t)15)))))
		{
			goto IL_0054;
		}
	}
	{
		// Flag.sprite = Flags[2];
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_7 = __this->get_Flag_8();
		SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77* L_8 = __this->get_Flags_9();
		NullCheck(L_8);
		int32_t L_9 = 2;
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck(L_7);
		Image_set_sprite_m55C50F18ABA0A98E926FC777F7D07FA18A7D04E4(L_7, L_10, /*hidden argument*/NULL);
		// setLang(de, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_11 = __this->get_de_12();
		GameMenuLang_setLang_mA20C12C7B1D825EFD990246302FD9401A98217B9(__this, L_11, 0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0054:
	{
		// else if (Application.systemLanguage == SystemLanguage.French)
		int32_t L_12;
		L_12 = Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_12) == ((uint32_t)((int32_t)14)))))
		{
			goto IL_007e;
		}
	}
	{
		// Flag.sprite = Flags[3];
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_13 = __this->get_Flag_8();
		SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77* L_14 = __this->get_Flags_9();
		NullCheck(L_14);
		int32_t L_15 = 3;
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck(L_13);
		Image_set_sprite_m55C50F18ABA0A98E926FC777F7D07FA18A7D04E4(L_13, L_16, /*hidden argument*/NULL);
		// setLang(fe, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_17 = __this->get_fe_13();
		GameMenuLang_setLang_mA20C12C7B1D825EFD990246302FD9401A98217B9(__this, L_17, 0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_007e:
	{
		// else if (Application.systemLanguage == SystemLanguage.Spanish)
		int32_t L_18;
		L_18 = Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_18) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_00a8;
		}
	}
	{
		// Flag.sprite = Flags[4];
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_19 = __this->get_Flag_8();
		SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77* L_20 = __this->get_Flags_9();
		NullCheck(L_20);
		int32_t L_21 = 4;
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_22 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		NullCheck(L_19);
		Image_set_sprite_m55C50F18ABA0A98E926FC777F7D07FA18A7D04E4(L_19, L_22, /*hidden argument*/NULL);
		// setLang(es, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_23 = __this->get_es_14();
		GameMenuLang_setLang_mA20C12C7B1D825EFD990246302FD9401A98217B9(__this, L_23, 0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_00a8:
	{
		// else if (Application.systemLanguage == SystemLanguage.Russian)
		int32_t L_24;
		L_24 = Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_24) == ((uint32_t)((int32_t)30)))))
		{
			goto IL_00d2;
		}
	}
	{
		// Flag.sprite = Flags[5];
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_25 = __this->get_Flag_8();
		SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77* L_26 = __this->get_Flags_9();
		NullCheck(L_26);
		int32_t L_27 = 5;
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_28 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_25);
		Image_set_sprite_m55C50F18ABA0A98E926FC777F7D07FA18A7D04E4(L_25, L_28, /*hidden argument*/NULL);
		// setLang(ru, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_29 = __this->get_ru_15();
		GameMenuLang_setLang_mA20C12C7B1D825EFD990246302FD9401A98217B9(__this, L_29, 0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_00d2:
	{
		// Flag.sprite = Flags[0];
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_30 = __this->get_Flag_8();
		SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77* L_31 = __this->get_Flags_9();
		NullCheck(L_31);
		int32_t L_32 = 0;
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_33 = (L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_30);
		Image_set_sprite_m55C50F18ABA0A98E926FC777F7D07FA18A7D04E4(L_30, L_33, /*hidden argument*/NULL);
		// setLang(en, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_34 = __this->get_en_11();
		GameMenuLang_setLang_mA20C12C7B1D825EFD990246302FD9401A98217B9(__this, L_34, 0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void GameMenuLang::setLang(System.String[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameMenuLang_setLang_mA20C12C7B1D825EFD990246302FD9401A98217B9 (GameMenuLang_t49BEAA2ABAFC0A899B3346BEF7C050C4212D467C * __this, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___Leng0, int32_t ___Index1, const RuntimeMethod* method)
{
	{
		// Score.text = Leng[Index].ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_0 = __this->get_Score_4();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_1 = ___Leng0;
		int32_t L_2 = ___Index1;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		String_t* L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		NullCheck(L_4);
		String_t* L_5;
		L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_4);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_5);
		// Text1.text = (Leng[Index+1]).ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_6 = __this->get_Text1_5();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_7 = ___Leng0;
		int32_t L_8 = ___Index1;
		NullCheck(L_7);
		int32_t L_9 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
		String_t* L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck(L_10);
		String_t* L_11;
		L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		NullCheck(L_6);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_6, L_11);
		// Text2.text = (Leng[Index+2]).ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_12 = __this->get_Text2_6();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_13 = ___Leng0;
		int32_t L_14 = ___Index1;
		NullCheck(L_13);
		int32_t L_15 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)2));
		String_t* L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck(L_16);
		String_t* L_17;
		L_17 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_16);
		NullCheck(L_12);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_12, L_17);
		// Text3.text = (Leng[Index+3]).ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_18 = __this->get_Text3_7();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_19 = ___Leng0;
		int32_t L_20 = ___Index1;
		NullCheck(L_19);
		int32_t L_21 = ((int32_t)il2cpp_codegen_add((int32_t)L_20, (int32_t)3));
		String_t* L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		NullCheck(L_22);
		String_t* L_23;
		L_23 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_22);
		NullCheck(L_18);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_18, L_23);
		// }
		return;
	}
}
// System.Void GameMenuLang::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameMenuLang__ctor_mCE79D49DE21611DF2C790014AC58D13EC1BC06B1 (GameMenuLang_t49BEAA2ABAFC0A899B3346BEF7C050C4212D467C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1DB4937A6FF134F494853D495F9912C69401B8D3);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2D189F33BC75731D52B3B0FEE15F38DDA0D3705F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2D1B3A3874C4103AEC900F038C6433A5A97FDC93);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral336B029D04847DC4F36985E1B89D1A0B11D299A3);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral347A115193887F78FC1467972A3DE60D336FCB4E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3B9BFD67DABF0DD11C843581037D06D2DD85B75E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral473A5415DB67A3D81A9EFFA2683F7A5AABDDB7E1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral530B175B7F6011707568F6CCAAA957BCEF5E641E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5FA2A08D6D10D5421B774A13D4C4288064837A89);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral687D9BA523E32A2B8A4FEAD5E5EAE89D357E1420);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6AFE6D1F9BA73FFE9852892B0E8D97FFB7FD7676);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6E1CB7C6103F25079BE1A6F61D75875BD47F5068);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6F3725E84909989D3989E377719DC2A8E7AA3B63);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral93ADFC213A427399629E9CBF8769B349656C397F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA1CE3BE2769759673A1D828C1C401647D7BCF60B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralACE00218C2787FFA96AC949D3EF70AB32F34EFE3);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB0683F985BA7F461EFD91E56DF63ECCB9EB2DA87);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC7C54B24F4E5A8AD8B3C11D391F6EC11BEF9A572);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC84E4D9117775396179E79EF749351D9592346C2);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCC8099E3A874E4223303AF753F0B61D2807E8478);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDAEBFB83D7BA5E8188D567F31243CA1CE5709FE1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE008DD2964784D5FA9F9972A94C6214E2FF55999);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE7D08C2FFBAEC3DC48D0A1A70AEF7807D465C0AB);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFF4E8113C1DD42448B93F3E73007CFEF6C650CD4);
		s_Il2CppMethodInitialized = true;
	}
	{
		// string[] pl = { "Zdobyte punkty", "Tabliczka mnożenia","Sklep","Wyjście"};
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_0 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)4);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_1 = L_0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, _stringLiteralB0683F985BA7F461EFD91E56DF63ECCB9EB2DA87);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteralB0683F985BA7F461EFD91E56DF63ECCB9EB2DA87);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_2 = L_1;
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, _stringLiteralCC8099E3A874E4223303AF753F0B61D2807E8478);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteralCC8099E3A874E4223303AF753F0B61D2807E8478);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_3 = L_2;
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, _stringLiteral6AFE6D1F9BA73FFE9852892B0E8D97FFB7FD7676);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral6AFE6D1F9BA73FFE9852892B0E8D97FFB7FD7676);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_4 = L_3;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteralACE00218C2787FFA96AC949D3EF70AB32F34EFE3);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteralACE00218C2787FFA96AC949D3EF70AB32F34EFE3);
		__this->set_pl_10(L_4);
		// string[] en = { "Score", "Multiplication table","Store","Exit"};
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_5 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)4);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_6 = L_5;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral336B029D04847DC4F36985E1B89D1A0B11D299A3);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral336B029D04847DC4F36985E1B89D1A0B11D299A3);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_7 = L_6;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteralC84E4D9117775396179E79EF749351D9592346C2);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteralC84E4D9117775396179E79EF749351D9592346C2);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_8 = L_7;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, _stringLiteral6E1CB7C6103F25079BE1A6F61D75875BD47F5068);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral6E1CB7C6103F25079BE1A6F61D75875BD47F5068);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_9 = L_8;
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral687D9BA523E32A2B8A4FEAD5E5EAE89D357E1420);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral687D9BA523E32A2B8A4FEAD5E5EAE89D357E1420);
		__this->set_en_11(L_9);
		// string[] de = { "Punkte erhalten", "Multiplikationstabelle", "Geschäft", "Ausgang" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_10 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)4);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_11 = L_10;
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, _stringLiteralE7D08C2FFBAEC3DC48D0A1A70AEF7807D465C0AB);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteralE7D08C2FFBAEC3DC48D0A1A70AEF7807D465C0AB);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_12 = L_11;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3B9BFD67DABF0DD11C843581037D06D2DD85B75E);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral3B9BFD67DABF0DD11C843581037D06D2DD85B75E);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_13 = L_12;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteralC7C54B24F4E5A8AD8B3C11D391F6EC11BEF9A572);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteralC7C54B24F4E5A8AD8B3C11D391F6EC11BEF9A572);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_14 = L_13;
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, _stringLiteral530B175B7F6011707568F6CCAAA957BCEF5E641E);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral530B175B7F6011707568F6CCAAA957BCEF5E641E);
		__this->set_de_12(L_14);
		// string[] fe = { "Points gagnés", "Table de multiplication", "Magasin", "Sortir" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_15 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)4);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_16 = L_15;
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, _stringLiteral6F3725E84909989D3989E377719DC2A8E7AA3B63);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral6F3725E84909989D3989E377719DC2A8E7AA3B63);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_17 = L_16;
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, _stringLiteral1DB4937A6FF134F494853D495F9912C69401B8D3);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral1DB4937A6FF134F494853D495F9912C69401B8D3);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_18 = L_17;
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, _stringLiteral347A115193887F78FC1467972A3DE60D336FCB4E);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral347A115193887F78FC1467972A3DE60D336FCB4E);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_19 = L_18;
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, _stringLiteralDAEBFB83D7BA5E8188D567F31243CA1CE5709FE1);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteralDAEBFB83D7BA5E8188D567F31243CA1CE5709FE1);
		__this->set_fe_13(L_19);
		// string[] es = { "Puntos ganados", "Tabla de multiplicación", "Tienda", "Salida" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_20 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)4);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_21 = L_20;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, _stringLiteralFF4E8113C1DD42448B93F3E73007CFEF6C650CD4);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteralFF4E8113C1DD42448B93F3E73007CFEF6C650CD4);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_22 = L_21;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, _stringLiteralE008DD2964784D5FA9F9972A94C6214E2FF55999);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteralE008DD2964784D5FA9F9972A94C6214E2FF55999);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_23 = L_22;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteralA1CE3BE2769759673A1D828C1C401647D7BCF60B);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteralA1CE3BE2769759673A1D828C1C401647D7BCF60B);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_24 = L_23;
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, _stringLiteral473A5415DB67A3D81A9EFFA2683F7A5AABDDB7E1);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral473A5415DB67A3D81A9EFFA2683F7A5AABDDB7E1);
		__this->set_es_14(L_24);
		// string[] ru = { "Заработанные баллы", "Таблица умножения", "Магазин", "Выход" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_25 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)4);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_26 = L_25;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral5FA2A08D6D10D5421B774A13D4C4288064837A89);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral5FA2A08D6D10D5421B774A13D4C4288064837A89);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_27 = L_26;
		NullCheck(L_27);
		ArrayElementTypeCheck (L_27, _stringLiteral2D1B3A3874C4103AEC900F038C6433A5A97FDC93);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2D1B3A3874C4103AEC900F038C6433A5A97FDC93);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_28 = L_27;
		NullCheck(L_28);
		ArrayElementTypeCheck (L_28, _stringLiteral2D189F33BC75731D52B3B0FEE15F38DDA0D3705F);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral2D189F33BC75731D52B3B0FEE15F38DDA0D3705F);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_29 = L_28;
		NullCheck(L_29);
		ArrayElementTypeCheck (L_29, _stringLiteral93ADFC213A427399629E9CBF8769B349656C397F);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral93ADFC213A427399629E9CBF8769B349656C397F);
		__this->set_ru_15(L_29);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MainMenu::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_Start_m3B552CE289B1D7E5343961C8461C484EA61DB621 (MainMenu_tEB11F5A993C42E93B585FBB65C9E92EC91C5707C * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void MainMenu::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_Update_m2DE1F2570AFBF09401821F2F89779639CD0BBC76 (MainMenu_tEB11F5A993C42E93B585FBB65C9E92EC91C5707C * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void MainMenu::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu__ctor_m4D77CEC8F91682A2D9492AE815F89B178BF9717D (MainMenu_tEB11F5A993C42E93B585FBB65C9E92EC91C5707C * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoveScenes::ResetPlayer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MoveScenes_ResetPlayer_m4DD51BA8E2F6512E076ADF834248E979BBECDD59 (MoveScenes_t757DBF243D1177806463417C842AF5884F8C7C17 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral035A0EFF495FDDF533CE351CD95BCE3D200A3FF8);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1764427109D31651865A98B70E1AE56C4E9DA044);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8F103C685BE61397D30779FE9A4354EADD169DC5);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF7418F00A0BC87B788A53131CE12EB9332E71D95);
		s_Il2CppMethodInitialized = true;
	}
	{
		// PlayerPrefs.DeleteKey("OneAward");
		PlayerPrefs_DeleteKey_mCEF6CE08D7D7670AD4072228E261A7E746030554(_stringLiteralF7418F00A0BC87B788A53131CE12EB9332E71D95, /*hidden argument*/NULL);
		// PlayerPrefs.DeleteKey("PIN1");
		PlayerPrefs_DeleteKey_mCEF6CE08D7D7670AD4072228E261A7E746030554(_stringLiteral035A0EFF495FDDF533CE351CD95BCE3D200A3FF8, /*hidden argument*/NULL);
		// PlayerPrefs.DeleteKey("PIN2");
		PlayerPrefs_DeleteKey_mCEF6CE08D7D7670AD4072228E261A7E746030554(_stringLiteral8F103C685BE61397D30779FE9A4354EADD169DC5, /*hidden argument*/NULL);
		// PlayerPrefs.SetInt("SavePoints", 5);
		PlayerPrefs_SetInt_m0C5C977E960B9CA8F9AB73AF4129C3DCABD067B6(_stringLiteral1764427109D31651865A98B70E1AE56C4E9DA044, 5, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoveScenes::GoToGame()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MoveScenes_GoToGame_m9E78314FA9457A016EBE6BE4B3BF4C7BA389FBFA (MoveScenes_t757DBF243D1177806463417C842AF5884F8C7C17 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene(1);
		IL2CPP_RUNTIME_CLASS_INIT(SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_m5550E6368A6D0E37DACEDA3C5E4BA331836BC3C5(1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoveScenes::GoToMenu()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MoveScenes_GoToMenu_m16E09AE612DDF55500163AC9EB55C3B533C207CA (MoveScenes_t757DBF243D1177806463417C842AF5884F8C7C17 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene(0);
		IL2CPP_RUNTIME_CLASS_INIT(SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_m5550E6368A6D0E37DACEDA3C5E4BA331836BC3C5(0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoveScenes::GoToLeng()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MoveScenes_GoToLeng_m155008EC70257E0A33D13FDC54DDBE3335783176 (MoveScenes_t757DBF243D1177806463417C842AF5884F8C7C17 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene(6);
		IL2CPP_RUNTIME_CLASS_INIT(SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_m5550E6368A6D0E37DACEDA3C5E4BA331836BC3C5(6, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoveScenes::GoToSettings()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MoveScenes_GoToSettings_m0997DD73155D917CD357EA3601D2124ACE72D7DD (MoveScenes_t757DBF243D1177806463417C842AF5884F8C7C17 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral035A0EFF495FDDF533CE351CD95BCE3D200A3FF8);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!PlayerPrefs.HasKey("PIN1"))
		bool L_0;
		L_0 = PlayerPrefs_HasKey_m48BE5886380B51AB495B91C9A26115B7CB958A92(_stringLiteral035A0EFF495FDDF533CE351CD95BCE3D200A3FF8, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		// SceneManager.LoadScene(7);
		IL2CPP_RUNTIME_CLASS_INIT(SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_m5550E6368A6D0E37DACEDA3C5E4BA331836BC3C5(7, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0013:
	{
		// SceneManager.LoadScene(9);
		IL2CPP_RUNTIME_CLASS_INIT(SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_m5550E6368A6D0E37DACEDA3C5E4BA331836BC3C5(((int32_t)9), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoveScenes::GoToAwardsNew()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MoveScenes_GoToAwardsNew_mDAABC2F77881CE5FBC7608F853413D621771357E (MoveScenes_t757DBF243D1177806463417C842AF5884F8C7C17 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene(2);
		IL2CPP_RUNTIME_CLASS_INIT(SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_m5550E6368A6D0E37DACEDA3C5E4BA331836BC3C5(2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoveScenes::GoToAwards()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MoveScenes_GoToAwards_m8B4511572CEFF307714385C813C4CCFB67E6E7CB (MoveScenes_t757DBF243D1177806463417C842AF5884F8C7C17 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4D0A51C14A2331C361C3CC21880E4CB63FEAFB6A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral639AB154A61332B41DDAE6F8231F56224448A1E2);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF7418F00A0BC87B788A53131CE12EB9332E71D95);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if ((PlayerPrefs.HasKey("OneAward"))&& (PlayerPrefs.HasKey("TwoAward"))&& (PlayerPrefs.HasKey("ThreeAward")))
		bool L_0;
		L_0 = PlayerPrefs_HasKey_m48BE5886380B51AB495B91C9A26115B7CB958A92(_stringLiteralF7418F00A0BC87B788A53131CE12EB9332E71D95, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_002b;
		}
	}
	{
		bool L_1;
		L_1 = PlayerPrefs_HasKey_m48BE5886380B51AB495B91C9A26115B7CB958A92(_stringLiteral639AB154A61332B41DDAE6F8231F56224448A1E2, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002b;
		}
	}
	{
		bool L_2;
		L_2 = PlayerPrefs_HasKey_m48BE5886380B51AB495B91C9A26115B7CB958A92(_stringLiteral4D0A51C14A2331C361C3CC21880E4CB63FEAFB6A, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002b;
		}
	}
	{
		// SceneManager.LoadScene(5);
		IL2CPP_RUNTIME_CLASS_INIT(SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_m5550E6368A6D0E37DACEDA3C5E4BA331836BC3C5(5, /*hidden argument*/NULL);
		// }
		return;
	}

IL_002b:
	{
		// SceneManager.LoadScene(2);
		IL2CPP_RUNTIME_CLASS_INIT(SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_m5550E6368A6D0E37DACEDA3C5E4BA331836BC3C5(2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoveScenes::Exit()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MoveScenes_Exit_m25FCCD9471FB39841AE8BFA5794841886A1A5677 (MoveScenes_t757DBF243D1177806463417C842AF5884F8C7C17 * __this, const RuntimeMethod* method)
{
	{
		// Application.Quit();
		Application_Quit_m8D720E5092786C2EE32310D85FE61C253D3B1F2A(/*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoveScenes::ChangePIN()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MoveScenes_ChangePIN_m81FA7929492E38C20ABBE3CFF6F1A20A9BD85FE4 (MoveScenes_t757DBF243D1177806463417C842AF5884F8C7C17 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene(7);
		IL2CPP_RUNTIME_CLASS_INIT(SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_m5550E6368A6D0E37DACEDA3C5E4BA331836BC3C5(7, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoveScenes::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MoveScenes__ctor_m7FF737B394D13C1AF24F3DA8145F6C987E554168 (MoveScenes_t757DBF243D1177806463417C842AF5884F8C7C17 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MultiTable::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultiTable_Awake_mD4ACC8EEC340DEED112FD459D2219102221A8F47 (MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisAudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_mCC3B7A679ECE504BFAF8C70C4EF527511F46902F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1764427109D31651865A98B70E1AE56C4E9DA044);
		s_Il2CppMethodInitialized = true;
	}
	{
		// MuteSound.gameObject.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_MuteSound_10();
		NullCheck(L_0);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1;
		L_1 = GameObject_get_gameObject_mD5FFECF7C3AC5039E847DF7A8842478539B701D6(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_1, (bool)0, /*hidden argument*/NULL);
		// audios = GetComponent<AudioSource>();
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_2;
		L_2 = Component_GetComponent_TisAudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_mCC3B7A679ECE504BFAF8C70C4EF527511F46902F(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_mCC3B7A679ECE504BFAF8C70C4EF527511F46902F_RuntimeMethod_var);
		__this->set_audios_5(L_2);
		// if (instance == null)
		MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206 * L_3 = ((MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206_StaticFields*)il2cpp_codegen_static_fields_for(MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206_il2cpp_TypeInfo_var))->get_instance_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_4;
		L_4 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_3, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0030;
		}
	}
	{
		// instance = this;
		((MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206_StaticFields*)il2cpp_codegen_static_fields_for(MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206_il2cpp_TypeInfo_var))->set_instance_4(__this);
	}

IL_0030:
	{
		// isStartGame = false;
		__this->set_isStartGame_22((bool)0);
		// Points = PlayerPrefs.GetInt("SavePoints");
		int32_t L_5;
		L_5 = PlayerPrefs_GetInt_m6BCF9F844298D1810A68BAF23ECBA68C6960A986(_stringLiteral1764427109D31651865A98B70E1AE56C4E9DA044, /*hidden argument*/NULL);
		__this->set_Points_23(L_5);
		// txtScore.text = Points.ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_6 = __this->get_txtScore_11();
		int32_t* L_7 = __this->get_address_of_Points_23();
		String_t* L_8;
		L_8 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_6, L_8);
		// txtTimer.text = timeRemaining.ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_9 = __this->get_txtTimer_12();
		float* L_10 = __this->get_address_of_timeRemaining_21();
		String_t* L_11;
		L_11 = Single_ToString_m80E7ABED4F4D73F2BE19DDB80D3D92FCD8DFA010((float*)L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_9, L_11);
		// }
		return;
	}
}
// System.Void MultiTable::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultiTable_Update_m321C153556B65CD94A61E9C6DD70091F0CD04E3D (MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206 * __this, const RuntimeMethod* method)
{
	{
		// if (isStartGame)
		bool L_0 = __this->get_isStartGame_22();
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		// Timer();
		MultiTable_Timer_mE67424E0638FE200B3E4917ACC7716A4DA7FAA21(__this, /*hidden argument*/NULL);
	}

IL_000e:
	{
		// }
		return;
	}
}
// System.Void MultiTable::Timer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultiTable_Timer_mE67424E0638FE200B3E4917ACC7716A4DA7FAA21 (MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCD8E37B9578BEA9485296FACF596A45CC703A39C);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (timeRemaining > 0)
		float L_0 = __this->get_timeRemaining_21();
		if ((!(((float)L_0) > ((float)(0.0f)))))
		{
			goto IL_003b;
		}
	}
	{
		// timeRemaining -= Time.deltaTime;
		float L_1 = __this->get_timeRemaining_21();
		float L_2;
		L_2 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		__this->set_timeRemaining_21(((float)il2cpp_codegen_subtract((float)L_1, (float)L_2)));
		// txtTimer.text = timeRemaining.ToString("f0");
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_3 = __this->get_txtTimer_12();
		float* L_4 = __this->get_address_of_timeRemaining_21();
		String_t* L_5;
		L_5 = Single_ToString_m15F10F2AFF80750906CEFCFB456EBA84F9D2E8D7((float*)L_4, _stringLiteralCD8E37B9578BEA9485296FACF596A45CC703A39C, /*hidden argument*/NULL);
		NullCheck(L_3);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_3, L_5);
		// }
		return;
	}

IL_003b:
	{
		// else if (timeRemaining <= 0)
		float L_6 = __this->get_timeRemaining_21();
		if ((!(((float)L_6) <= ((float)(0.0f)))))
		{
			goto IL_0060;
		}
	}
	{
		// MinusPoints();
		MultiTable_MinusPoints_m1CAB936C4BB41304C0F84FFF740BDB8C90F67574(__this, /*hidden argument*/NULL);
		// ResetTimer();
		MultiTable_ResetTimer_m3EDBEC0DABDB2C44DB50E1BA0494C599CD43882B(__this, /*hidden argument*/NULL);
		// RemonteList();
		MultiTable_RemonteList_mCE61DCD2CD42ABEF80CCB1653A5160E4CE98842C(__this, /*hidden argument*/NULL);
		// RandomMathOper();
		MultiTable_RandomMathOper_m648924AB98EC575E4E02B2E91B3DB4DC70404E20(__this, /*hidden argument*/NULL);
	}

IL_0060:
	{
		// }
		return;
	}
}
// System.Void MultiTable::RandomMathOper()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultiTable_RandomMathOper_m648924AB98EC575E4E02B2E91B3DB4DC70404E20 (MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mFB5C182E24F496A866F116D3F75CBD8616A46AB3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	{
		// int num1 = Random.Range(0, 11);
		int32_t L_0;
		L_0 = Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A(0, ((int32_t)11), /*hidden argument*/NULL);
		V_0 = L_0;
		// int num2 = Random.Range(0, 11);
		int32_t L_1;
		L_1 = Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A(0, ((int32_t)11), /*hidden argument*/NULL);
		V_1 = L_1;
		// txtNum1.text = num1.ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_2 = __this->get_txtNum1_14();
		String_t* L_3;
		L_3 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_0), /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, L_3);
		// txtNum2.text = num2.ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_4 = __this->get_txtNum2_15();
		String_t* L_5;
		L_5 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_1), /*hidden argument*/NULL);
		NullCheck(L_4);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_4, L_5);
		// int result = num1 * num2;
		int32_t L_6 = V_0;
		int32_t L_7 = V_1;
		V_2 = ((int32_t)il2cpp_codegen_multiply((int32_t)L_6, (int32_t)L_7));
		// int answer1 = Random.Range(0, 101);
		int32_t L_8;
		L_8 = Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A(0, ((int32_t)101), /*hidden argument*/NULL);
		V_3 = L_8;
		// int answer2 = Random.Range(0, 101);
		int32_t L_9;
		L_9 = Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A(0, ((int32_t)101), /*hidden argument*/NULL);
		V_4 = L_9;
		// int answer3 = Random.Range(0, 101);
		int32_t L_10;
		L_10 = Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A(0, ((int32_t)101), /*hidden argument*/NULL);
		V_5 = L_10;
		// answers.Add(result);
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_11 = __this->get_answers_20();
		int32_t L_12 = V_2;
		NullCheck(L_11);
		List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F(L_11, L_12, /*hidden argument*/List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F_RuntimeMethod_var);
		// answers.Add(answer1);
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_13 = __this->get_answers_20();
		int32_t L_14 = V_3;
		NullCheck(L_13);
		List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F(L_13, L_14, /*hidden argument*/List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F_RuntimeMethod_var);
		// answers.Add(answer2);
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_15 = __this->get_answers_20();
		int32_t L_16 = V_4;
		NullCheck(L_15);
		List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F(L_15, L_16, /*hidden argument*/List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F_RuntimeMethod_var);
		// answers.Add(answer3);
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_17 = __this->get_answers_20();
		int32_t L_18 = V_5;
		NullCheck(L_17);
		List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F(L_17, L_18, /*hidden argument*/List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F_RuntimeMethod_var);
		// int rand = Random.Range(0, 4);
		int32_t L_19;
		L_19 = Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A(0, 4, /*hidden argument*/NULL);
		V_6 = L_19;
		// Buttons[rand].GetComponentInChildren<Text>().text = answers[0].ToString();
		TextU5BU5D_t16DD1967B137EC602803C77DBB246B05B3D0275F* L_20 = __this->get_Buttons_13();
		int32_t L_21 = V_6;
		NullCheck(L_20);
		int32_t L_22 = L_21;
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_23 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		NullCheck(L_23);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_24;
		L_24 = Component_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mFB5C182E24F496A866F116D3F75CBD8616A46AB3(L_23, /*hidden argument*/Component_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mFB5C182E24F496A866F116D3F75CBD8616A46AB3_RuntimeMethod_var);
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_25 = __this->get_answers_20();
		NullCheck(L_25);
		int32_t L_26;
		L_26 = List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_inline(L_25, 0, /*hidden argument*/List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var);
		V_7 = L_26;
		String_t* L_27;
		L_27 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_7), /*hidden argument*/NULL);
		NullCheck(L_24);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_24, L_27);
		// if (rand == 0)
		int32_t L_28 = V_6;
		if (L_28)
		{
			goto IL_013d;
		}
	}
	{
		// Buttons[rand + 1].GetComponentInChildren<Text>().text = answers[1].ToString();
		TextU5BU5D_t16DD1967B137EC602803C77DBB246B05B3D0275F* L_29 = __this->get_Buttons_13();
		int32_t L_30 = V_6;
		NullCheck(L_29);
		int32_t L_31 = ((int32_t)il2cpp_codegen_add((int32_t)L_30, (int32_t)1));
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_32 = (L_29)->GetAt(static_cast<il2cpp_array_size_t>(L_31));
		NullCheck(L_32);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_33;
		L_33 = Component_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mFB5C182E24F496A866F116D3F75CBD8616A46AB3(L_32, /*hidden argument*/Component_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mFB5C182E24F496A866F116D3F75CBD8616A46AB3_RuntimeMethod_var);
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_34 = __this->get_answers_20();
		NullCheck(L_34);
		int32_t L_35;
		L_35 = List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_inline(L_34, 1, /*hidden argument*/List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var);
		V_7 = L_35;
		String_t* L_36;
		L_36 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_7), /*hidden argument*/NULL);
		NullCheck(L_33);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_33, L_36);
		// Buttons[rand + 2].GetComponentInChildren<Text>().text = answers[2].ToString();
		TextU5BU5D_t16DD1967B137EC602803C77DBB246B05B3D0275F* L_37 = __this->get_Buttons_13();
		int32_t L_38 = V_6;
		NullCheck(L_37);
		int32_t L_39 = ((int32_t)il2cpp_codegen_add((int32_t)L_38, (int32_t)2));
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_40 = (L_37)->GetAt(static_cast<il2cpp_array_size_t>(L_39));
		NullCheck(L_40);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_41;
		L_41 = Component_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mFB5C182E24F496A866F116D3F75CBD8616A46AB3(L_40, /*hidden argument*/Component_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mFB5C182E24F496A866F116D3F75CBD8616A46AB3_RuntimeMethod_var);
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_42 = __this->get_answers_20();
		NullCheck(L_42);
		int32_t L_43;
		L_43 = List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_inline(L_42, 2, /*hidden argument*/List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var);
		V_7 = L_43;
		String_t* L_44;
		L_44 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_7), /*hidden argument*/NULL);
		NullCheck(L_41);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_41, L_44);
		// Buttons[rand + 3].GetComponentInChildren<Text>().text = answers[3].ToString();
		TextU5BU5D_t16DD1967B137EC602803C77DBB246B05B3D0275F* L_45 = __this->get_Buttons_13();
		int32_t L_46 = V_6;
		NullCheck(L_45);
		int32_t L_47 = ((int32_t)il2cpp_codegen_add((int32_t)L_46, (int32_t)3));
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		NullCheck(L_48);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_49;
		L_49 = Component_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mFB5C182E24F496A866F116D3F75CBD8616A46AB3(L_48, /*hidden argument*/Component_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mFB5C182E24F496A866F116D3F75CBD8616A46AB3_RuntimeMethod_var);
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_50 = __this->get_answers_20();
		NullCheck(L_50);
		int32_t L_51;
		L_51 = List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_inline(L_50, 3, /*hidden argument*/List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var);
		V_7 = L_51;
		String_t* L_52;
		L_52 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_7), /*hidden argument*/NULL);
		NullCheck(L_49);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_49, L_52);
		// }
		return;
	}

IL_013d:
	{
		// else if (rand == 1)
		int32_t L_53 = V_6;
		if ((!(((uint32_t)L_53) == ((uint32_t)1))))
		{
			goto IL_01c1;
		}
	}
	{
		// Buttons[rand - 1].GetComponentInChildren<Text>().text = answers[2].ToString();
		TextU5BU5D_t16DD1967B137EC602803C77DBB246B05B3D0275F* L_54 = __this->get_Buttons_13();
		int32_t L_55 = V_6;
		NullCheck(L_54);
		int32_t L_56 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_55, (int32_t)1));
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_57 = (L_54)->GetAt(static_cast<il2cpp_array_size_t>(L_56));
		NullCheck(L_57);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_58;
		L_58 = Component_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mFB5C182E24F496A866F116D3F75CBD8616A46AB3(L_57, /*hidden argument*/Component_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mFB5C182E24F496A866F116D3F75CBD8616A46AB3_RuntimeMethod_var);
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_59 = __this->get_answers_20();
		NullCheck(L_59);
		int32_t L_60;
		L_60 = List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_inline(L_59, 2, /*hidden argument*/List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var);
		V_7 = L_60;
		String_t* L_61;
		L_61 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_7), /*hidden argument*/NULL);
		NullCheck(L_58);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_58, L_61);
		// Buttons[rand + 1].GetComponentInChildren<Text>().text = answers[1].ToString();
		TextU5BU5D_t16DD1967B137EC602803C77DBB246B05B3D0275F* L_62 = __this->get_Buttons_13();
		int32_t L_63 = V_6;
		NullCheck(L_62);
		int32_t L_64 = ((int32_t)il2cpp_codegen_add((int32_t)L_63, (int32_t)1));
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_65 = (L_62)->GetAt(static_cast<il2cpp_array_size_t>(L_64));
		NullCheck(L_65);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_66;
		L_66 = Component_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mFB5C182E24F496A866F116D3F75CBD8616A46AB3(L_65, /*hidden argument*/Component_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mFB5C182E24F496A866F116D3F75CBD8616A46AB3_RuntimeMethod_var);
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_67 = __this->get_answers_20();
		NullCheck(L_67);
		int32_t L_68;
		L_68 = List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_inline(L_67, 1, /*hidden argument*/List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var);
		V_7 = L_68;
		String_t* L_69;
		L_69 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_7), /*hidden argument*/NULL);
		NullCheck(L_66);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_66, L_69);
		// Buttons[rand + 2].GetComponentInChildren<Text>().text = answers[3].ToString();
		TextU5BU5D_t16DD1967B137EC602803C77DBB246B05B3D0275F* L_70 = __this->get_Buttons_13();
		int32_t L_71 = V_6;
		NullCheck(L_70);
		int32_t L_72 = ((int32_t)il2cpp_codegen_add((int32_t)L_71, (int32_t)2));
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_73 = (L_70)->GetAt(static_cast<il2cpp_array_size_t>(L_72));
		NullCheck(L_73);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_74;
		L_74 = Component_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mFB5C182E24F496A866F116D3F75CBD8616A46AB3(L_73, /*hidden argument*/Component_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mFB5C182E24F496A866F116D3F75CBD8616A46AB3_RuntimeMethod_var);
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_75 = __this->get_answers_20();
		NullCheck(L_75);
		int32_t L_76;
		L_76 = List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_inline(L_75, 3, /*hidden argument*/List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var);
		V_7 = L_76;
		String_t* L_77;
		L_77 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_7), /*hidden argument*/NULL);
		NullCheck(L_74);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_74, L_77);
		// }
		return;
	}

IL_01c1:
	{
		// else if (rand == 2)
		int32_t L_78 = V_6;
		if ((!(((uint32_t)L_78) == ((uint32_t)2))))
		{
			goto IL_0245;
		}
	}
	{
		// Buttons[rand - 2].GetComponentInChildren<Text>().text = answers[3].ToString();
		TextU5BU5D_t16DD1967B137EC602803C77DBB246B05B3D0275F* L_79 = __this->get_Buttons_13();
		int32_t L_80 = V_6;
		NullCheck(L_79);
		int32_t L_81 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_80, (int32_t)2));
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_82 = (L_79)->GetAt(static_cast<il2cpp_array_size_t>(L_81));
		NullCheck(L_82);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_83;
		L_83 = Component_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mFB5C182E24F496A866F116D3F75CBD8616A46AB3(L_82, /*hidden argument*/Component_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mFB5C182E24F496A866F116D3F75CBD8616A46AB3_RuntimeMethod_var);
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_84 = __this->get_answers_20();
		NullCheck(L_84);
		int32_t L_85;
		L_85 = List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_inline(L_84, 3, /*hidden argument*/List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var);
		V_7 = L_85;
		String_t* L_86;
		L_86 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_7), /*hidden argument*/NULL);
		NullCheck(L_83);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_83, L_86);
		// Buttons[rand - 1].GetComponentInChildren<Text>().text = answers[1].ToString();
		TextU5BU5D_t16DD1967B137EC602803C77DBB246B05B3D0275F* L_87 = __this->get_Buttons_13();
		int32_t L_88 = V_6;
		NullCheck(L_87);
		int32_t L_89 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_88, (int32_t)1));
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_90 = (L_87)->GetAt(static_cast<il2cpp_array_size_t>(L_89));
		NullCheck(L_90);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_91;
		L_91 = Component_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mFB5C182E24F496A866F116D3F75CBD8616A46AB3(L_90, /*hidden argument*/Component_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mFB5C182E24F496A866F116D3F75CBD8616A46AB3_RuntimeMethod_var);
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_92 = __this->get_answers_20();
		NullCheck(L_92);
		int32_t L_93;
		L_93 = List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_inline(L_92, 1, /*hidden argument*/List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var);
		V_7 = L_93;
		String_t* L_94;
		L_94 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_7), /*hidden argument*/NULL);
		NullCheck(L_91);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_91, L_94);
		// Buttons[rand + 1].GetComponentInChildren<Text>().text = answers[2].ToString();
		TextU5BU5D_t16DD1967B137EC602803C77DBB246B05B3D0275F* L_95 = __this->get_Buttons_13();
		int32_t L_96 = V_6;
		NullCheck(L_95);
		int32_t L_97 = ((int32_t)il2cpp_codegen_add((int32_t)L_96, (int32_t)1));
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_98 = (L_95)->GetAt(static_cast<il2cpp_array_size_t>(L_97));
		NullCheck(L_98);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_99;
		L_99 = Component_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mFB5C182E24F496A866F116D3F75CBD8616A46AB3(L_98, /*hidden argument*/Component_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mFB5C182E24F496A866F116D3F75CBD8616A46AB3_RuntimeMethod_var);
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_100 = __this->get_answers_20();
		NullCheck(L_100);
		int32_t L_101;
		L_101 = List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_inline(L_100, 2, /*hidden argument*/List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var);
		V_7 = L_101;
		String_t* L_102;
		L_102 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_7), /*hidden argument*/NULL);
		NullCheck(L_99);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_99, L_102);
		// }
		return;
	}

IL_0245:
	{
		// Buttons[rand - 3].GetComponentInChildren<Text>().text = answers[1].ToString();
		TextU5BU5D_t16DD1967B137EC602803C77DBB246B05B3D0275F* L_103 = __this->get_Buttons_13();
		int32_t L_104 = V_6;
		NullCheck(L_103);
		int32_t L_105 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_104, (int32_t)3));
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_106 = (L_103)->GetAt(static_cast<il2cpp_array_size_t>(L_105));
		NullCheck(L_106);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_107;
		L_107 = Component_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mFB5C182E24F496A866F116D3F75CBD8616A46AB3(L_106, /*hidden argument*/Component_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mFB5C182E24F496A866F116D3F75CBD8616A46AB3_RuntimeMethod_var);
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_108 = __this->get_answers_20();
		NullCheck(L_108);
		int32_t L_109;
		L_109 = List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_inline(L_108, 1, /*hidden argument*/List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var);
		V_7 = L_109;
		String_t* L_110;
		L_110 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_7), /*hidden argument*/NULL);
		NullCheck(L_107);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_107, L_110);
		// Buttons[rand - 2].GetComponentInChildren<Text>().text = answers[2].ToString();
		TextU5BU5D_t16DD1967B137EC602803C77DBB246B05B3D0275F* L_111 = __this->get_Buttons_13();
		int32_t L_112 = V_6;
		NullCheck(L_111);
		int32_t L_113 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_112, (int32_t)2));
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_114 = (L_111)->GetAt(static_cast<il2cpp_array_size_t>(L_113));
		NullCheck(L_114);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_115;
		L_115 = Component_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mFB5C182E24F496A866F116D3F75CBD8616A46AB3(L_114, /*hidden argument*/Component_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mFB5C182E24F496A866F116D3F75CBD8616A46AB3_RuntimeMethod_var);
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_116 = __this->get_answers_20();
		NullCheck(L_116);
		int32_t L_117;
		L_117 = List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_inline(L_116, 2, /*hidden argument*/List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var);
		V_7 = L_117;
		String_t* L_118;
		L_118 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_7), /*hidden argument*/NULL);
		NullCheck(L_115);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_115, L_118);
		// Buttons[rand - 1].GetComponentInChildren<Text>().text = answers[3].ToString();
		TextU5BU5D_t16DD1967B137EC602803C77DBB246B05B3D0275F* L_119 = __this->get_Buttons_13();
		int32_t L_120 = V_6;
		NullCheck(L_119);
		int32_t L_121 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_120, (int32_t)1));
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_122 = (L_119)->GetAt(static_cast<il2cpp_array_size_t>(L_121));
		NullCheck(L_122);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_123;
		L_123 = Component_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mFB5C182E24F496A866F116D3F75CBD8616A46AB3(L_122, /*hidden argument*/Component_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mFB5C182E24F496A866F116D3F75CBD8616A46AB3_RuntimeMethod_var);
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_124 = __this->get_answers_20();
		NullCheck(L_124);
		int32_t L_125;
		L_125 = List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_inline(L_124, 3, /*hidden argument*/List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var);
		V_7 = L_125;
		String_t* L_126;
		L_126 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_7), /*hidden argument*/NULL);
		NullCheck(L_123);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_123, L_126);
		// }
		return;
	}
}
// System.Void MultiTable::OnVibration()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultiTable_OnVibration_m33FE08D7C4D2249464CB79DE79E9B630A64EB4B2 (MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206 * __this, const RuntimeMethod* method)
{
	{
		// Handheld.Vibrate();
		Handheld_Vibrate_m521B854160443BEBA8D2D3BE63641000E1DAA82E(/*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MultiTable::MuteSounds()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultiTable_MuteSounds_mBBC5CEED95E798A9A3E0A0BA22B854FE232B50BC (MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206 * __this, const RuntimeMethod* method)
{
	{
		// MuteSound.gameObject.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_MuteSound_10();
		NullCheck(L_0);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1;
		L_1 = GameObject_get_gameObject_mD5FFECF7C3AC5039E847DF7A8842478539B701D6(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_1, (bool)1, /*hidden argument*/NULL);
		// audios.mute = true;
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_2 = __this->get_audios_5();
		NullCheck(L_2);
		AudioSource_set_mute_m69E2DFCF261D2D187ED756096B7E0DE622292C71(L_2, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MultiTable::OnSounds()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultiTable_OnSounds_m51705407D34611622E9AE2FDAD02AC1B4EA4EA23 (MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206 * __this, const RuntimeMethod* method)
{
	{
		// MuteSound.gameObject.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_MuteSound_10();
		NullCheck(L_0);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1;
		L_1 = GameObject_get_gameObject_mD5FFECF7C3AC5039E847DF7A8842478539B701D6(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_1, (bool)0, /*hidden argument*/NULL);
		// audios.mute = false;
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_2 = __this->get_audios_5();
		NullCheck(L_2);
		AudioSource_set_mute_m69E2DFCF261D2D187ED756096B7E0DE622292C71(L_2, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MultiTable::PlaySounds(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultiTable_PlaySounds_m5D0B5AB0E188B53FC4714F7BE49A960C6A5E5659 (MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206 * __this, int32_t ___IndexClip0, const RuntimeMethod* method)
{
	{
		// audios.clip = Fileaudios[IndexClip];
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_0 = __this->get_audios_5();
		AudioClipU5BU5D_t9BA3E7C4B62164BA2E054F3A82CD6CE6EB273CBE* L_1 = __this->get_Fileaudios_6();
		int32_t L_2 = ___IndexClip0;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		NullCheck(L_0);
		AudioSource_set_clip_mD1F50F7BA6EA3AF25B4922473352C5180CFF7B2B(L_0, L_4, /*hidden argument*/NULL);
		// audios.Play();
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_5 = __this->get_audios_5();
		NullCheck(L_5);
		AudioSource_Play_mED16664B8F8F3E4D68785C8C00FC96C4DF053AE1(L_5, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MultiTable::RemonteList()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultiTable_RemonteList_mCE61DCD2CD42ABEF80CCB1653A5160E4CE98842C (MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Clear_m508B72E5229FAE7042D99A04555F66F10C597C7A_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// answers.Clear();
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_0 = __this->get_answers_20();
		NullCheck(L_0);
		List_1_Clear_m508B72E5229FAE7042D99A04555F66F10C597C7A(L_0, /*hidden argument*/List_1_Clear_m508B72E5229FAE7042D99A04555F66F10C597C7A_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void MultiTable::AddPoints()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultiTable_AddPoints_mCF0996F6154238A8000EEA211B659076B565B233 (MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206 * __this, const RuntimeMethod* method)
{
	{
		// Points++;
		int32_t L_0 = __this->get_Points_23();
		__this->set_Points_23(((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)1)));
		// txtScore.text = Points.ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_1 = __this->get_txtScore_11();
		int32_t* L_2 = __this->get_address_of_Points_23();
		String_t* L_3;
		L_3 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, L_3);
		// }
		return;
	}
}
// System.Void MultiTable::MinusPoints()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultiTable_MinusPoints_m1CAB936C4BB41304C0F84FFF740BDB8C90F67574 (MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206 * __this, const RuntimeMethod* method)
{
	{
		// Points--;
		int32_t L_0 = __this->get_Points_23();
		__this->set_Points_23(((int32_t)il2cpp_codegen_subtract((int32_t)L_0, (int32_t)1)));
		// txtScore.text = Points.ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_1 = __this->get_txtScore_11();
		int32_t* L_2 = __this->get_address_of_Points_23();
		String_t* L_3;
		L_3 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, L_3);
		// }
		return;
	}
}
// System.Void MultiTable::StopGame()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultiTable_StopGame_mDC42C69626996D613140DF940005822E9395CCCB (MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1764427109D31651865A98B70E1AE56C4E9DA044);
		s_Il2CppMethodInitialized = true;
	}
	{
		// isStartGame = false;
		__this->set_isStartGame_22((bool)0);
		// btnStopGame.gameObject.SetActive(false);
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_0 = __this->get_btnStopGame_8();
		NullCheck(L_0);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1;
		L_1 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_1, (bool)0, /*hidden argument*/NULL);
		// btnStartGame.gameObject.SetActive(true);
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_2 = __this->get_btnStartGame_7();
		NullCheck(L_2);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3;
		L_3 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_3, (bool)1, /*hidden argument*/NULL);
		// btnStartGame.interactable = true;
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_4 = __this->get_btnStartGame_7();
		NullCheck(L_4);
		Selectable_set_interactable_mE6F57D33A9E0484377174D0F490C4372BF7F0D40(L_4, (bool)1, /*hidden argument*/NULL);
		// PlayerPrefs.SetInt("SavePoints", Points);
		int32_t L_5 = __this->get_Points_23();
		PlayerPrefs_SetInt_m0C5C977E960B9CA8F9AB73AF4129C3DCABD067B6(_stringLiteral1764427109D31651865A98B70E1AE56C4E9DA044, L_5, /*hidden argument*/NULL);
		// PlayerPrefs.Save();
		PlayerPrefs_Save_m2C1E628FA335095CD88D0DA1CB50ACC924667EEC(/*hidden argument*/NULL);
		// SceneManager.LoadScene(1);
		IL2CPP_RUNTIME_CLASS_INIT(SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_m5550E6368A6D0E37DACEDA3C5E4BA331836BC3C5(1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MultiTable::StartGame()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultiTable_StartGame_m530950033857B8D5459D26A44F6B06DD60BF137F (MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206 * __this, const RuntimeMethod* method)
{
	{
		// RandomMathOper();
		MultiTable_RandomMathOper_m648924AB98EC575E4E02B2E91B3DB4DC70404E20(__this, /*hidden argument*/NULL);
		// txtTimer.text = timeRemaining.ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_0 = __this->get_txtTimer_12();
		float* L_1 = __this->get_address_of_timeRemaining_21();
		String_t* L_2;
		L_2 = Single_ToString_m80E7ABED4F4D73F2BE19DDB80D3D92FCD8DFA010((float*)L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_2);
		// isStartGame = true;
		__this->set_isStartGame_22((bool)1);
		// btnStartGame.gameObject.SetActive(false);
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_3 = __this->get_btnStartGame_7();
		NullCheck(L_3);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4;
		L_4 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_4, (bool)0, /*hidden argument*/NULL);
		// btnStopGame.gameObject.SetActive(true);
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_5 = __this->get_btnStopGame_8();
		NullCheck(L_5);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_6;
		L_6 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_6, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MultiTable::ResetTimer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultiTable_ResetTimer_m3EDBEC0DABDB2C44DB50E1BA0494C599CD43882B (MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206 * __this, const RuntimeMethod* method)
{
	{
		// timeRemaining = 5f;
		__this->set_timeRemaining_21((5.0f));
		// txtTimer.text = timeRemaining.ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_0 = __this->get_txtTimer_12();
		float* L_1 = __this->get_address_of_timeRemaining_21();
		String_t* L_2;
		L_2 = Single_ToString_m80E7ABED4F4D73F2BE19DDB80D3D92FCD8DFA010((float*)L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_2);
		// }
		return;
	}
}
// System.Void MultiTable::CheckBtnZero()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultiTable_CheckBtnZero_m87E1A39FF161E5691B43DD3411E15CBA45D997DD (MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral653A457CFCABD3E7BD6C53A71BFDDFB52801E206);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	{
		// string answer0 = GameObject.FindGameObjectWithTag("btn0").GetComponentInChildren<Text>().text;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = GameObject_FindGameObjectWithTag_mFC215979EDFED361F88C336BF9E187F24434C63F(_stringLiteral653A457CFCABD3E7BD6C53A71BFDDFB52801E206, /*hidden argument*/NULL);
		NullCheck(L_0);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_1;
		L_1 = GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881(L_0, /*hidden argument*/GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881_RuntimeMethod_var);
		NullCheck(L_1);
		String_t* L_2;
		L_2 = VirtFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_1);
		V_0 = L_2;
		// if (answers[0].ToString() == answer0.ToString())
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_3 = __this->get_answers_20();
		NullCheck(L_3);
		int32_t L_4;
		L_4 = List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_inline(L_3, 0, /*hidden argument*/List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var);
		V_1 = L_4;
		String_t* L_5;
		L_5 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_1), /*hidden argument*/NULL);
		String_t* L_6 = V_0;
		NullCheck(L_6);
		String_t* L_7;
		L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_6);
		bool L_8;
		L_8 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_5, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0056;
		}
	}
	{
		// PlaySounds(1);
		MultiTable_PlaySounds_m5D0B5AB0E188B53FC4714F7BE49A960C6A5E5659(__this, 1, /*hidden argument*/NULL);
		// AddPoints();
		MultiTable_AddPoints_mCF0996F6154238A8000EEA211B659076B565B233(__this, /*hidden argument*/NULL);
		// ResetTimer();
		MultiTable_ResetTimer_m3EDBEC0DABDB2C44DB50E1BA0494C599CD43882B(__this, /*hidden argument*/NULL);
		// RemonteList();
		MultiTable_RemonteList_mCE61DCD2CD42ABEF80CCB1653A5160E4CE98842C(__this, /*hidden argument*/NULL);
		// RandomMathOper();
		MultiTable_RandomMathOper_m648924AB98EC575E4E02B2E91B3DB4DC70404E20(__this, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0056:
	{
		// PlaySounds(0);
		MultiTable_PlaySounds_m5D0B5AB0E188B53FC4714F7BE49A960C6A5E5659(__this, 0, /*hidden argument*/NULL);
		// OnVibration();
		MultiTable_OnVibration_m33FE08D7C4D2249464CB79DE79E9B630A64EB4B2(__this, /*hidden argument*/NULL);
		// MinusPoints();
		MultiTable_MinusPoints_m1CAB936C4BB41304C0F84FFF740BDB8C90F67574(__this, /*hidden argument*/NULL);
		// ResetTimer();
		MultiTable_ResetTimer_m3EDBEC0DABDB2C44DB50E1BA0494C599CD43882B(__this, /*hidden argument*/NULL);
		// RemonteList();
		MultiTable_RemonteList_mCE61DCD2CD42ABEF80CCB1653A5160E4CE98842C(__this, /*hidden argument*/NULL);
		// RandomMathOper();
		MultiTable_RandomMathOper_m648924AB98EC575E4E02B2E91B3DB4DC70404E20(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MultiTable::CheckBtnOne()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultiTable_CheckBtnOne_m21B5A5564BCB5815C171453F21C436AFBFCB7644 (MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral974FF80607D2850832F8E0C5E6BFCE5955E3113B);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	{
		// string answer1 = GameObject.FindGameObjectWithTag("btn1").GetComponentInChildren<Text>().text;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = GameObject_FindGameObjectWithTag_mFC215979EDFED361F88C336BF9E187F24434C63F(_stringLiteral974FF80607D2850832F8E0C5E6BFCE5955E3113B, /*hidden argument*/NULL);
		NullCheck(L_0);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_1;
		L_1 = GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881(L_0, /*hidden argument*/GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881_RuntimeMethod_var);
		NullCheck(L_1);
		String_t* L_2;
		L_2 = VirtFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_1);
		V_0 = L_2;
		// if (answers[0].ToString() == answer1.ToString())
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_3 = __this->get_answers_20();
		NullCheck(L_3);
		int32_t L_4;
		L_4 = List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_inline(L_3, 0, /*hidden argument*/List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var);
		V_1 = L_4;
		String_t* L_5;
		L_5 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_1), /*hidden argument*/NULL);
		String_t* L_6 = V_0;
		NullCheck(L_6);
		String_t* L_7;
		L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_6);
		bool L_8;
		L_8 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_5, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0056;
		}
	}
	{
		// PlaySounds(1);
		MultiTable_PlaySounds_m5D0B5AB0E188B53FC4714F7BE49A960C6A5E5659(__this, 1, /*hidden argument*/NULL);
		// AddPoints();
		MultiTable_AddPoints_mCF0996F6154238A8000EEA211B659076B565B233(__this, /*hidden argument*/NULL);
		// ResetTimer();
		MultiTable_ResetTimer_m3EDBEC0DABDB2C44DB50E1BA0494C599CD43882B(__this, /*hidden argument*/NULL);
		// RemonteList();
		MultiTable_RemonteList_mCE61DCD2CD42ABEF80CCB1653A5160E4CE98842C(__this, /*hidden argument*/NULL);
		// RandomMathOper();
		MultiTable_RandomMathOper_m648924AB98EC575E4E02B2E91B3DB4DC70404E20(__this, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0056:
	{
		// PlaySounds(0);
		MultiTable_PlaySounds_m5D0B5AB0E188B53FC4714F7BE49A960C6A5E5659(__this, 0, /*hidden argument*/NULL);
		// OnVibration();
		MultiTable_OnVibration_m33FE08D7C4D2249464CB79DE79E9B630A64EB4B2(__this, /*hidden argument*/NULL);
		// MinusPoints();
		MultiTable_MinusPoints_m1CAB936C4BB41304C0F84FFF740BDB8C90F67574(__this, /*hidden argument*/NULL);
		// ResetTimer();
		MultiTable_ResetTimer_m3EDBEC0DABDB2C44DB50E1BA0494C599CD43882B(__this, /*hidden argument*/NULL);
		// RemonteList();
		MultiTable_RemonteList_mCE61DCD2CD42ABEF80CCB1653A5160E4CE98842C(__this, /*hidden argument*/NULL);
		// RandomMathOper();
		MultiTable_RandomMathOper_m648924AB98EC575E4E02B2E91B3DB4DC70404E20(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MultiTable::CheckBtnTwo()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultiTable_CheckBtnTwo_m27A5A4575EE3408DEFAA05A848B081B4762F7ADC (MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1E00E5176988AB5EB370C9793515E4691A169055);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	{
		// string answer2 = GameObject.FindGameObjectWithTag("btn2").GetComponentInChildren<Text>().text;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = GameObject_FindGameObjectWithTag_mFC215979EDFED361F88C336BF9E187F24434C63F(_stringLiteral1E00E5176988AB5EB370C9793515E4691A169055, /*hidden argument*/NULL);
		NullCheck(L_0);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_1;
		L_1 = GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881(L_0, /*hidden argument*/GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881_RuntimeMethod_var);
		NullCheck(L_1);
		String_t* L_2;
		L_2 = VirtFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_1);
		V_0 = L_2;
		// if (answers[0].ToString() == answer2.ToString())
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_3 = __this->get_answers_20();
		NullCheck(L_3);
		int32_t L_4;
		L_4 = List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_inline(L_3, 0, /*hidden argument*/List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var);
		V_1 = L_4;
		String_t* L_5;
		L_5 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_1), /*hidden argument*/NULL);
		String_t* L_6 = V_0;
		NullCheck(L_6);
		String_t* L_7;
		L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_6);
		bool L_8;
		L_8 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_5, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0056;
		}
	}
	{
		// PlaySounds(1);
		MultiTable_PlaySounds_m5D0B5AB0E188B53FC4714F7BE49A960C6A5E5659(__this, 1, /*hidden argument*/NULL);
		// AddPoints();
		MultiTable_AddPoints_mCF0996F6154238A8000EEA211B659076B565B233(__this, /*hidden argument*/NULL);
		// ResetTimer();
		MultiTable_ResetTimer_m3EDBEC0DABDB2C44DB50E1BA0494C599CD43882B(__this, /*hidden argument*/NULL);
		// RemonteList();
		MultiTable_RemonteList_mCE61DCD2CD42ABEF80CCB1653A5160E4CE98842C(__this, /*hidden argument*/NULL);
		// RandomMathOper();
		MultiTable_RandomMathOper_m648924AB98EC575E4E02B2E91B3DB4DC70404E20(__this, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0056:
	{
		// PlaySounds(0);
		MultiTable_PlaySounds_m5D0B5AB0E188B53FC4714F7BE49A960C6A5E5659(__this, 0, /*hidden argument*/NULL);
		// OnVibration();
		MultiTable_OnVibration_m33FE08D7C4D2249464CB79DE79E9B630A64EB4B2(__this, /*hidden argument*/NULL);
		// MinusPoints();
		MultiTable_MinusPoints_m1CAB936C4BB41304C0F84FFF740BDB8C90F67574(__this, /*hidden argument*/NULL);
		// ResetTimer();
		MultiTable_ResetTimer_m3EDBEC0DABDB2C44DB50E1BA0494C599CD43882B(__this, /*hidden argument*/NULL);
		// RemonteList();
		MultiTable_RemonteList_mCE61DCD2CD42ABEF80CCB1653A5160E4CE98842C(__this, /*hidden argument*/NULL);
		// RandomMathOper();
		MultiTable_RandomMathOper_m648924AB98EC575E4E02B2E91B3DB4DC70404E20(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MultiTable::CheckBtnThree()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultiTable_CheckBtnThree_mE92B07328B3CB4241BAC7EFAE9BBA7AB0F869442 (MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEAA8F9283296F4C0E880EE4F1A05E2FF6CCC098B);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	{
		// string answer3 = GameObject.FindGameObjectWithTag("btn3").GetComponentInChildren<Text>().text;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = GameObject_FindGameObjectWithTag_mFC215979EDFED361F88C336BF9E187F24434C63F(_stringLiteralEAA8F9283296F4C0E880EE4F1A05E2FF6CCC098B, /*hidden argument*/NULL);
		NullCheck(L_0);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_1;
		L_1 = GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881(L_0, /*hidden argument*/GameObject_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m7CAECD44A40B4306F85A45E96E9486068B89D881_RuntimeMethod_var);
		NullCheck(L_1);
		String_t* L_2;
		L_2 = VirtFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_1);
		V_0 = L_2;
		// if (answers[0].ToString() == answer3.ToString())
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_3 = __this->get_answers_20();
		NullCheck(L_3);
		int32_t L_4;
		L_4 = List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_inline(L_3, 0, /*hidden argument*/List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var);
		V_1 = L_4;
		String_t* L_5;
		L_5 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_1), /*hidden argument*/NULL);
		String_t* L_6 = V_0;
		NullCheck(L_6);
		String_t* L_7;
		L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_6);
		bool L_8;
		L_8 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_5, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0056;
		}
	}
	{
		// PlaySounds(1);
		MultiTable_PlaySounds_m5D0B5AB0E188B53FC4714F7BE49A960C6A5E5659(__this, 1, /*hidden argument*/NULL);
		// AddPoints();
		MultiTable_AddPoints_mCF0996F6154238A8000EEA211B659076B565B233(__this, /*hidden argument*/NULL);
		// ResetTimer();
		MultiTable_ResetTimer_m3EDBEC0DABDB2C44DB50E1BA0494C599CD43882B(__this, /*hidden argument*/NULL);
		// RemonteList();
		MultiTable_RemonteList_mCE61DCD2CD42ABEF80CCB1653A5160E4CE98842C(__this, /*hidden argument*/NULL);
		// RandomMathOper();
		MultiTable_RandomMathOper_m648924AB98EC575E4E02B2E91B3DB4DC70404E20(__this, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0056:
	{
		// PlaySounds(0);
		MultiTable_PlaySounds_m5D0B5AB0E188B53FC4714F7BE49A960C6A5E5659(__this, 0, /*hidden argument*/NULL);
		// OnVibration();
		MultiTable_OnVibration_m33FE08D7C4D2249464CB79DE79E9B630A64EB4B2(__this, /*hidden argument*/NULL);
		// MinusPoints();
		MultiTable_MinusPoints_m1CAB936C4BB41304C0F84FFF740BDB8C90F67574(__this, /*hidden argument*/NULL);
		// ResetTimer();
		MultiTable_ResetTimer_m3EDBEC0DABDB2C44DB50E1BA0494C599CD43882B(__this, /*hidden argument*/NULL);
		// RemonteList();
		MultiTable_RemonteList_mCE61DCD2CD42ABEF80CCB1653A5160E4CE98842C(__this, /*hidden argument*/NULL);
		// RandomMathOper();
		MultiTable_RandomMathOper_m648924AB98EC575E4E02B2E91B3DB4DC70404E20(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MultiTable::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultiTable__ctor_mC74F963BA5AD318F968DC7C7FDED3CC73963E724 (MultiTable_tE3E42DCC121E190493FB4ECE790F4BBDD29C0206 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public List<int> answers = new List<int>();
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_0 = (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)il2cpp_codegen_object_new(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7_il2cpp_TypeInfo_var);
		List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD(L_0, /*hidden argument*/List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD_RuntimeMethod_var);
		__this->set_answers_20(L_0);
		// float timeRemaining = 5f;
		__this->set_timeRemaining_21((5.0f));
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void OneAwardLang::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OneAwardLang_Awake_mCFE94B300C3AD48B4287E6FEBE5A86F861545762 (OneAwardLang_t98E602972882C2F997D3285FD94AE10CA89D1CB0 * __this, const RuntimeMethod* method)
{
	{
		// if (Application.systemLanguage == SystemLanguage.Polish)
		int32_t L_0;
		L_0 = Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)27)))))
		{
			goto IL_0017;
		}
	}
	{
		// setLang(pl, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_1 = __this->get_pl_8();
		OneAwardLang_setLang_m71BF319A0CFF2967CBFB7ECC05F7CF70B82182F4(__this, L_1, 0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0017:
	{
		// else if (Application.systemLanguage == SystemLanguage.German)
		int32_t L_2;
		L_2 = Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)15)))))
		{
			goto IL_002e;
		}
	}
	{
		// setLang(de, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_3 = __this->get_de_10();
		OneAwardLang_setLang_m71BF319A0CFF2967CBFB7ECC05F7CF70B82182F4(__this, L_3, 0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_002e:
	{
		// else if (Application.systemLanguage == SystemLanguage.French)
		int32_t L_4;
		L_4 = Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)((int32_t)14)))))
		{
			goto IL_0045;
		}
	}
	{
		// setLang(fe, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_5 = __this->get_fe_11();
		OneAwardLang_setLang_m71BF319A0CFF2967CBFB7ECC05F7CF70B82182F4(__this, L_5, 0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0045:
	{
		// else if (Application.systemLanguage == SystemLanguage.Spanish)
		int32_t L_6;
		L_6 = Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_005c;
		}
	}
	{
		// setLang(es, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_7 = __this->get_es_12();
		OneAwardLang_setLang_m71BF319A0CFF2967CBFB7ECC05F7CF70B82182F4(__this, L_7, 0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_005c:
	{
		// else if (Application.systemLanguage == SystemLanguage.Russian)
		int32_t L_8;
		L_8 = Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)30)))))
		{
			goto IL_0073;
		}
	}
	{
		// setLang(ru, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_9 = __this->get_ru_13();
		OneAwardLang_setLang_m71BF319A0CFF2967CBFB7ECC05F7CF70B82182F4(__this, L_9, 0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0073:
	{
		// setLang(en, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_10 = __this->get_en_9();
		OneAwardLang_setLang_m71BF319A0CFF2967CBFB7ECC05F7CF70B82182F4(__this, L_10, 0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void OneAwardLang::setLang(System.String[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OneAwardLang_setLang_m71BF319A0CFF2967CBFB7ECC05F7CF70B82182F4 (OneAwardLang_t98E602972882C2F997D3285FD94AE10CA89D1CB0 * __this, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___Leng0, int32_t ___Index1, const RuntimeMethod* method)
{
	{
		// Text1.text = Leng[Index].ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_0 = __this->get_Text1_4();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_1 = ___Leng0;
		int32_t L_2 = ___Index1;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		String_t* L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		NullCheck(L_4);
		String_t* L_5;
		L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_4);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_5);
		// Text2.text = (Leng[Index + 1]).ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_6 = __this->get_Text2_5();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_7 = ___Leng0;
		int32_t L_8 = ___Index1;
		NullCheck(L_7);
		int32_t L_9 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
		String_t* L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck(L_10);
		String_t* L_11;
		L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		NullCheck(L_6);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_6, L_11);
		// Text3.text = (Leng[Index + 2]).ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_12 = __this->get_Text3_6();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_13 = ___Leng0;
		int32_t L_14 = ___Index1;
		NullCheck(L_13);
		int32_t L_15 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)2));
		String_t* L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck(L_16);
		String_t* L_17;
		L_17 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_16);
		NullCheck(L_12);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_12, L_17);
		// Text4.text = (Leng[Index + 3]).ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_18 = __this->get_Text4_7();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_19 = ___Leng0;
		int32_t L_20 = ___Index1;
		NullCheck(L_19);
		int32_t L_21 = ((int32_t)il2cpp_codegen_add((int32_t)L_20, (int32_t)3));
		String_t* L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		NullCheck(L_22);
		String_t* L_23;
		L_23 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_22);
		NullCheck(L_18);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_18, L_23);
		// }
		return;
	}
}
// System.Void OneAwardLang::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OneAwardLang__ctor_m7109BA9634E6974ADB66C1EBBF2FDE22589897C2 (OneAwardLang_t98E602972882C2F997D3285FD94AE10CA89D1CB0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1BE450AF172FA36120A25FDEE8A898E902B2AD73);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral39994065AEF5B81B168A2450D5CB126C9CA8D3EE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4096248D5E896770D10C3198F6CA42764F6065B5);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral440ADF0B63952180E519C4C8C2B9B099EEF2FA6A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6B7BA1920180F57CC65B000BBE0402C1529A16D7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7BCABA1AF0C023BD47CC57FADD9C04B9F922D6DA);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7C0AC29A8D0286851370B7150A2AF361A5073C40);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8A915C1241B79502A63A19FDB5939552772053A6);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8B97FF7B417237C9F66EA83872B0B16505D74FEC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral921F1F9D366E611F8A551D5E3695B6EDDC58FF08);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral95ADA1E608855E2F14812B488C44DDA8ECB96F9B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA8F2373ADF991C37C5ABEECE00312CD475F229E9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB4E4E2DBFC31551BC1BD5C157680ECAE8D15E0FE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC11FE1AAA451A181FAC2657DD1D911A32F3955C1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCE8A0013D4140A243026D70A506048F0B9B53C2A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA661A147DE9D5A86D9C547E06E53270E05A1150);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE291D9505F6E8B8B7E44E9DA7CD4657569ABB935);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEC308DD32AC3ECD5859F14D8C8AFCE417C45F3DF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF09EC4356FCC74D969B20E6082914C6F08BC6592);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF93A6BBD2F32B66DCD196FEA1EB82ACBEBC67C3B);
		s_Il2CppMethodInitialized = true;
	}
	{
		// string[] pl = { "Dodaj pierwszą nagrodę", "Nagroda za 1000p", "Wpisz pierwszą nagrodę. np.Pizza", "NEXT" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_0 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)4);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_1 = L_0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, _stringLiteralDA661A147DE9D5A86D9C547E06E53270E05A1150);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteralDA661A147DE9D5A86D9C547E06E53270E05A1150);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_2 = L_1;
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, _stringLiteralF09EC4356FCC74D969B20E6082914C6F08BC6592);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteralF09EC4356FCC74D969B20E6082914C6F08BC6592);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_3 = L_2;
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, _stringLiteralF93A6BBD2F32B66DCD196FEA1EB82ACBEBC67C3B);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteralF93A6BBD2F32B66DCD196FEA1EB82ACBEBC67C3B);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_4 = L_3;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteral7C0AC29A8D0286851370B7150A2AF361A5073C40);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral7C0AC29A8D0286851370B7150A2AF361A5073C40);
		__this->set_pl_8(L_4);
		// string[] en = { "Add the first prize", "Reward for 1000p", "Enter the first award. e.g. Pizza", "NEXT" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_5 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)4);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_6 = L_5;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteralE291D9505F6E8B8B7E44E9DA7CD4657569ABB935);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteralE291D9505F6E8B8B7E44E9DA7CD4657569ABB935);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_7 = L_6;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral8A915C1241B79502A63A19FDB5939552772053A6);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral8A915C1241B79502A63A19FDB5939552772053A6);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_8 = L_7;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, _stringLiteral4096248D5E896770D10C3198F6CA42764F6065B5);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral4096248D5E896770D10C3198F6CA42764F6065B5);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_9 = L_8;
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral7C0AC29A8D0286851370B7150A2AF361A5073C40);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral7C0AC29A8D0286851370B7150A2AF361A5073C40);
		__this->set_en_9(L_9);
		// string[] de = { "Fügen Sie den ersten Preis hinzu", "Belohnung für 1000p", "Geben Sie den ersten Preis ein. z.B. Pizza", "NEXT" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_10 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)4);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_11 = L_10;
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, _stringLiteral7BCABA1AF0C023BD47CC57FADD9C04B9F922D6DA);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral7BCABA1AF0C023BD47CC57FADD9C04B9F922D6DA);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_12 = L_11;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteralCE8A0013D4140A243026D70A506048F0B9B53C2A);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteralCE8A0013D4140A243026D70A506048F0B9B53C2A);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_13 = L_12;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral39994065AEF5B81B168A2450D5CB126C9CA8D3EE);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral39994065AEF5B81B168A2450D5CB126C9CA8D3EE);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_14 = L_13;
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, _stringLiteral7C0AC29A8D0286851370B7150A2AF361A5073C40);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral7C0AC29A8D0286851370B7150A2AF361A5073C40);
		__this->set_de_10(L_14);
		// string[] fe = { "Ajouter le premier prix", "Récompense pour 1000p", "Entrez le premier prix. p. ex. pizza", "NEXT" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_15 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)4);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_16 = L_15;
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, _stringLiteral8B97FF7B417237C9F66EA83872B0B16505D74FEC);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral8B97FF7B417237C9F66EA83872B0B16505D74FEC);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_17 = L_16;
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, _stringLiteral6B7BA1920180F57CC65B000BBE0402C1529A16D7);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral6B7BA1920180F57CC65B000BBE0402C1529A16D7);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_18 = L_17;
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, _stringLiteral95ADA1E608855E2F14812B488C44DDA8ECB96F9B);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral95ADA1E608855E2F14812B488C44DDA8ECB96F9B);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_19 = L_18;
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, _stringLiteral7C0AC29A8D0286851370B7150A2AF361A5073C40);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral7C0AC29A8D0286851370B7150A2AF361A5073C40);
		__this->set_fe_11(L_19);
		// string[] es = { "Agrega el primer premio", "Recompensa por 1000p", "Ingrese el primer premio. p. ej., pizza", "NEXT" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_20 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)4);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_21 = L_20;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, _stringLiteralB4E4E2DBFC31551BC1BD5C157680ECAE8D15E0FE);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteralB4E4E2DBFC31551BC1BD5C157680ECAE8D15E0FE);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_22 = L_21;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, _stringLiteral1BE450AF172FA36120A25FDEE8A898E902B2AD73);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral1BE450AF172FA36120A25FDEE8A898E902B2AD73);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_23 = L_22;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteralC11FE1AAA451A181FAC2657DD1D911A32F3955C1);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteralC11FE1AAA451A181FAC2657DD1D911A32F3955C1);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_24 = L_23;
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, _stringLiteral7C0AC29A8D0286851370B7150A2AF361A5073C40);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral7C0AC29A8D0286851370B7150A2AF361A5073C40);
		__this->set_es_12(L_24);
		// string[] ru = { "Добавить первый приз", "Приз за 1000p", "Войдите в первый приз. например, пицца", "СЛЕДУЮЩИЙ" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_25 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)4);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_26 = L_25;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral440ADF0B63952180E519C4C8C2B9B099EEF2FA6A);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral440ADF0B63952180E519C4C8C2B9B099EEF2FA6A);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_27 = L_26;
		NullCheck(L_27);
		ArrayElementTypeCheck (L_27, _stringLiteralA8F2373ADF991C37C5ABEECE00312CD475F229E9);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteralA8F2373ADF991C37C5ABEECE00312CD475F229E9);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_28 = L_27;
		NullCheck(L_28);
		ArrayElementTypeCheck (L_28, _stringLiteral921F1F9D366E611F8A551D5E3695B6EDDC58FF08);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral921F1F9D366E611F8A551D5E3695B6EDDC58FF08);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_29 = L_28;
		NullCheck(L_29);
		ArrayElementTypeCheck (L_29, _stringLiteralEC308DD32AC3ECD5859F14D8C8AFCE417C45F3DF);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteralEC308DD32AC3ECD5859F14D8C8AFCE417C45F3DF);
		__this->set_ru_13(L_29);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void OperateOneAward::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OperateOneAward_Awake_m0743FF891291370783D4078645D1F71BB76FFFA7 (OperateOneAward_t30BCE36800FCC2888072B07ED15106D80175AB80 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF7418F00A0BC87B788A53131CE12EB9332E71D95);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (PlayerPrefs.HasKey("OneAward"))
		bool L_0;
		L_0 = PlayerPrefs_HasKey_m48BE5886380B51AB495B91C9A26115B7CB958A92(_stringLiteralF7418F00A0BC87B788A53131CE12EB9332E71D95, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0032;
		}
	}
	{
		// OneAward = PlayerPrefs.GetString("OneAward");
		String_t* L_1;
		L_1 = PlayerPrefs_GetString_mE7654C1031622A56CD8F248F53714B105A35A159(_stringLiteralF7418F00A0BC87B788A53131CE12EB9332E71D95, /*hidden argument*/NULL);
		__this->set_OneAward_4(L_1);
		// TextDisplayOneAward.GetComponent<Text>().text = OneAward;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = __this->get_TextDisplayOneAward_6();
		NullCheck(L_2);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_3;
		L_3 = GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65(L_2, /*hidden argument*/GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65_RuntimeMethod_var);
		String_t* L_4 = __this->get_OneAward_4();
		NullCheck(L_3);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_3, L_4);
	}

IL_0032:
	{
		// }
		return;
	}
}
// System.Void OperateOneAward::GoToTwoSceneAwards()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OperateOneAward_GoToTwoSceneAwards_mA847ADAE8A1141CE1A8C9189089E9B8EB39246FD (OperateOneAward_t30BCE36800FCC2888072B07ED15106D80175AB80 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF7418F00A0BC87B788A53131CE12EB9332E71D95);
		s_Il2CppMethodInitialized = true;
	}
	{
		// OneAward = PlayerPrefs.GetString("OneAward");
		String_t* L_0;
		L_0 = PlayerPrefs_GetString_mE7654C1031622A56CD8F248F53714B105A35A159(_stringLiteralF7418F00A0BC87B788A53131CE12EB9332E71D95, /*hidden argument*/NULL);
		__this->set_OneAward_4(L_0);
		// OneAward = OneInputField.GetComponent<Text>().text;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1 = __this->get_OneInputField_5();
		NullCheck(L_1);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_2;
		L_2 = GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65(L_1, /*hidden argument*/GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65_RuntimeMethod_var);
		NullCheck(L_2);
		String_t* L_3;
		L_3 = VirtFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_2);
		__this->set_OneAward_4(L_3);
		// TextDisplayOneAward.GetComponent<Text>().text = OneAward;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4 = __this->get_TextDisplayOneAward_6();
		NullCheck(L_4);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_5;
		L_5 = GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65(L_4, /*hidden argument*/GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65_RuntimeMethod_var);
		String_t* L_6 = __this->get_OneAward_4();
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, L_6);
		// PlayerPrefs.SetString("OneAward", OneAward);
		String_t* L_7 = __this->get_OneAward_4();
		PlayerPrefs_SetString_m94CD8FF45692553A5726DFADF74935F7E1D1C633(_stringLiteralF7418F00A0BC87B788A53131CE12EB9332E71D95, L_7, /*hidden argument*/NULL);
		// PlayerPrefs.Save();
		PlayerPrefs_Save_m2C1E628FA335095CD88D0DA1CB50ACC924667EEC(/*hidden argument*/NULL);
		// SceneManager.LoadScene(3);
		IL2CPP_RUNTIME_CLASS_INIT(SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_m5550E6368A6D0E37DACEDA3C5E4BA331836BC3C5(3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void OperateOneAward::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OperateOneAward__ctor_m369F55CDB2FD39015CB82205FD13D2439A433D7D (OperateOneAward_t30BCE36800FCC2888072B07ED15106D80175AB80 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void OperateThreeAward::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OperateThreeAward_Awake_mD63E1DC17E3ECB9E7EAAF23A07925FFE090AB41B (OperateThreeAward_t647790567BA6B786988084DF0A6C9E4E6606BC17 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4D0A51C14A2331C361C3CC21880E4CB63FEAFB6A);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (PlayerPrefs.HasKey("ThreeAward"))
		bool L_0;
		L_0 = PlayerPrefs_HasKey_m48BE5886380B51AB495B91C9A26115B7CB958A92(_stringLiteral4D0A51C14A2331C361C3CC21880E4CB63FEAFB6A, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0032;
		}
	}
	{
		// ThreeAward = PlayerPrefs.GetString("ThreeAward");
		String_t* L_1;
		L_1 = PlayerPrefs_GetString_mE7654C1031622A56CD8F248F53714B105A35A159(_stringLiteral4D0A51C14A2331C361C3CC21880E4CB63FEAFB6A, /*hidden argument*/NULL);
		__this->set_ThreeAward_4(L_1);
		// TextDisplayThreeAward.GetComponent<Text>().text = ThreeAward;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = __this->get_TextDisplayThreeAward_6();
		NullCheck(L_2);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_3;
		L_3 = GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65(L_2, /*hidden argument*/GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65_RuntimeMethod_var);
		String_t* L_4 = __this->get_ThreeAward_4();
		NullCheck(L_3);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_3, L_4);
	}

IL_0032:
	{
		// }
		return;
	}
}
// System.Void OperateThreeAward::GoToSceneAwards()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OperateThreeAward_GoToSceneAwards_m4165BD3BD54B69C55386F4A98B5A5DF3C4A58209 (OperateThreeAward_t647790567BA6B786988084DF0A6C9E4E6606BC17 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4D0A51C14A2331C361C3CC21880E4CB63FEAFB6A);
		s_Il2CppMethodInitialized = true;
	}
	{
		// ThreeAward = PlayerPrefs.GetString("ThreeAward");
		String_t* L_0;
		L_0 = PlayerPrefs_GetString_mE7654C1031622A56CD8F248F53714B105A35A159(_stringLiteral4D0A51C14A2331C361C3CC21880E4CB63FEAFB6A, /*hidden argument*/NULL);
		__this->set_ThreeAward_4(L_0);
		// ThreeAward = ThreeInputField.GetComponent<Text>().text;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1 = __this->get_ThreeInputField_5();
		NullCheck(L_1);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_2;
		L_2 = GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65(L_1, /*hidden argument*/GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65_RuntimeMethod_var);
		NullCheck(L_2);
		String_t* L_3;
		L_3 = VirtFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_2);
		__this->set_ThreeAward_4(L_3);
		// TextDisplayThreeAward.GetComponent<Text>().text = ThreeAward;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4 = __this->get_TextDisplayThreeAward_6();
		NullCheck(L_4);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_5;
		L_5 = GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65(L_4, /*hidden argument*/GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65_RuntimeMethod_var);
		String_t* L_6 = __this->get_ThreeAward_4();
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, L_6);
		// PlayerPrefs.SetString("ThreeAward", ThreeAward);
		String_t* L_7 = __this->get_ThreeAward_4();
		PlayerPrefs_SetString_m94CD8FF45692553A5726DFADF74935F7E1D1C633(_stringLiteral4D0A51C14A2331C361C3CC21880E4CB63FEAFB6A, L_7, /*hidden argument*/NULL);
		// PlayerPrefs.Save();
		PlayerPrefs_Save_m2C1E628FA335095CD88D0DA1CB50ACC924667EEC(/*hidden argument*/NULL);
		// SceneManager.LoadScene(5);
		IL2CPP_RUNTIME_CLASS_INIT(SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_m5550E6368A6D0E37DACEDA3C5E4BA331836BC3C5(5, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void OperateThreeAward::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OperateThreeAward__ctor_mEFF96D2D36F84D3E1E71725167F1235035D575DF (OperateThreeAward_t647790567BA6B786988084DF0A6C9E4E6606BC17 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void OperateTwoAward::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OperateTwoAward_Awake_m477F7E004001CC13273D6E49CF8CC2A10852F606 (OperateTwoAward_t2378CD21BAB017639BC691A4F64BEE3B42DC528D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral639AB154A61332B41DDAE6F8231F56224448A1E2);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (PlayerPrefs.HasKey("TwoAward"))
		bool L_0;
		L_0 = PlayerPrefs_HasKey_m48BE5886380B51AB495B91C9A26115B7CB958A92(_stringLiteral639AB154A61332B41DDAE6F8231F56224448A1E2, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0032;
		}
	}
	{
		// TwoAward = PlayerPrefs.GetString("TwoAward");
		String_t* L_1;
		L_1 = PlayerPrefs_GetString_mE7654C1031622A56CD8F248F53714B105A35A159(_stringLiteral639AB154A61332B41DDAE6F8231F56224448A1E2, /*hidden argument*/NULL);
		__this->set_TwoAward_4(L_1);
		// TextDisplayTwoAward.GetComponent<Text>().text = TwoAward;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = __this->get_TextDisplayTwoAward_6();
		NullCheck(L_2);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_3;
		L_3 = GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65(L_2, /*hidden argument*/GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65_RuntimeMethod_var);
		String_t* L_4 = __this->get_TwoAward_4();
		NullCheck(L_3);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_3, L_4);
	}

IL_0032:
	{
		// }
		return;
	}
}
// System.Void OperateTwoAward::GoToThreeSceneAwards()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OperateTwoAward_GoToThreeSceneAwards_m9A31B66CF6B81ED057BE9FF44D6CD33AE36610E3 (OperateTwoAward_t2378CD21BAB017639BC691A4F64BEE3B42DC528D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral639AB154A61332B41DDAE6F8231F56224448A1E2);
		s_Il2CppMethodInitialized = true;
	}
	{
		// TwoAward = PlayerPrefs.GetString("TwoAward");
		String_t* L_0;
		L_0 = PlayerPrefs_GetString_mE7654C1031622A56CD8F248F53714B105A35A159(_stringLiteral639AB154A61332B41DDAE6F8231F56224448A1E2, /*hidden argument*/NULL);
		__this->set_TwoAward_4(L_0);
		// TwoAward = TwoInputField.GetComponent<Text>().text;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1 = __this->get_TwoInputField_5();
		NullCheck(L_1);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_2;
		L_2 = GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65(L_1, /*hidden argument*/GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65_RuntimeMethod_var);
		NullCheck(L_2);
		String_t* L_3;
		L_3 = VirtFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_2);
		__this->set_TwoAward_4(L_3);
		// TextDisplayTwoAward.GetComponent<Text>().text = TwoAward;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4 = __this->get_TextDisplayTwoAward_6();
		NullCheck(L_4);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_5;
		L_5 = GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65(L_4, /*hidden argument*/GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65_RuntimeMethod_var);
		String_t* L_6 = __this->get_TwoAward_4();
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, L_6);
		// PlayerPrefs.SetString("TwoAward", TwoAward);
		String_t* L_7 = __this->get_TwoAward_4();
		PlayerPrefs_SetString_m94CD8FF45692553A5726DFADF74935F7E1D1C633(_stringLiteral639AB154A61332B41DDAE6F8231F56224448A1E2, L_7, /*hidden argument*/NULL);
		// PlayerPrefs.Save();
		PlayerPrefs_Save_m2C1E628FA335095CD88D0DA1CB50ACC924667EEC(/*hidden argument*/NULL);
		// SceneManager.LoadScene(4);
		IL2CPP_RUNTIME_CLASS_INIT(SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_m5550E6368A6D0E37DACEDA3C5E4BA331836BC3C5(4, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void OperateTwoAward::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OperateTwoAward__ctor_m857F2C283020A1B6BD5D26A572D2743F683D7E73 (OperateTwoAward_t2378CD21BAB017639BC691A4F64BEE3B42DC528D * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Settings::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Settings_Awake_mBA445DDD366FCA51F1223575885A10ABDF812A21 (Settings_t9AEAFFBA64B4FC9ECF724519DF460F19962F4495 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m2D99AC2081683F963C56EC738451EC0B59B5D137_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// TextError.GetComponent<Text>().enabled = false;
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_0 = __this->get_TextError_10();
		NullCheck(L_0);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_1;
		L_1 = Component_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m2D99AC2081683F963C56EC738451EC0B59B5D137(L_0, /*hidden argument*/Component_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m2D99AC2081683F963C56EC738451EC0B59B5D137_RuntimeMethod_var);
		NullCheck(L_1);
		Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32(L_1, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Settings::CheckPIN()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Settings_CheckPIN_m9240031C817801CBB76A55EF476BABEED6960A4B (Settings_t9AEAFFBA64B4FC9ECF724519DF460F19962F4495 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m2D99AC2081683F963C56EC738451EC0B59B5D137_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral035A0EFF495FDDF533CE351CD95BCE3D200A3FF8);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8F103C685BE61397D30779FE9A4354EADD169DC5);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		s_Il2CppMethodInitialized = true;
	}
	{
		// PIN1 = PIN1InputField.GetComponent<Text>().text;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_PIN1InputField_6();
		NullCheck(L_0);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_1;
		L_1 = GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65(L_0, /*hidden argument*/GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65_RuntimeMethod_var);
		NullCheck(L_1);
		String_t* L_2;
		L_2 = VirtFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_1);
		__this->set_PIN1_4(L_2);
		// PIN2 = PIN2InputField.GetComponent<Text>().text;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3 = __this->get_PIN2InputField_7();
		NullCheck(L_3);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_4;
		L_4 = GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65(L_3, /*hidden argument*/GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65_RuntimeMethod_var);
		NullCheck(L_4);
		String_t* L_5;
		L_5 = VirtFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_4);
		__this->set_PIN2_5(L_5);
		// TextDisplayPIN1.GetComponent<Text>().text = PIN1;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_6 = __this->get_TextDisplayPIN1_8();
		NullCheck(L_6);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_7;
		L_7 = GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65(L_6, /*hidden argument*/GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65_RuntimeMethod_var);
		String_t* L_8 = __this->get_PIN1_4();
		NullCheck(L_7);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_7, L_8);
		// TextDisplayPIN2.GetComponent<Text>().text = PIN2;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_9 = __this->get_TextDisplayPIN2_9();
		NullCheck(L_9);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_10;
		L_10 = GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65(L_9, /*hidden argument*/GameObject_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mD98876EFC776CB8D02A1394AE6A72DC47F271C65_RuntimeMethod_var);
		String_t* L_11 = __this->get_PIN2_5();
		NullCheck(L_10);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_10, L_11);
		// if (PIN1 == "" ||  PIN2 == "" || PIN1 != PIN2)
		String_t* L_12 = __this->get_PIN1_4();
		bool L_13;
		L_13 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_12, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_008f;
		}
	}
	{
		String_t* L_14 = __this->get_PIN2_5();
		bool L_15;
		L_15 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_14, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_008f;
		}
	}
	{
		String_t* L_16 = __this->get_PIN1_4();
		String_t* L_17 = __this->get_PIN2_5();
		bool L_18;
		L_18 = String_op_Inequality_mDDA2DDED3E7EF042987EB7180EE3E88105F0AAE2(L_16, L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00b1;
		}
	}

IL_008f:
	{
		// TextError.color = Color.red;
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_19 = __this->get_TextError_10();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_20;
		L_20 = Color_get_red_m9BD55EBF7A74A515330FA5F7AC7A67C8A8913DD8(/*hidden argument*/NULL);
		NullCheck(L_19);
		VirtActionInvoker1< Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_19, L_20);
		// TextError.GetComponent<Text>().enabled = true;
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_21 = __this->get_TextError_10();
		NullCheck(L_21);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_22;
		L_22 = Component_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m2D99AC2081683F963C56EC738451EC0B59B5D137(L_21, /*hidden argument*/Component_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m2D99AC2081683F963C56EC738451EC0B59B5D137_RuntimeMethod_var);
		NullCheck(L_22);
		Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32(L_22, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}

IL_00b1:
	{
		// PlayerPrefs.SetString("PIN1", PIN1);
		String_t* L_23 = __this->get_PIN1_4();
		PlayerPrefs_SetString_m94CD8FF45692553A5726DFADF74935F7E1D1C633(_stringLiteral035A0EFF495FDDF533CE351CD95BCE3D200A3FF8, L_23, /*hidden argument*/NULL);
		// PlayerPrefs.SetString("PIN2", PIN2);
		String_t* L_24 = __this->get_PIN2_5();
		PlayerPrefs_SetString_m94CD8FF45692553A5726DFADF74935F7E1D1C633(_stringLiteral8F103C685BE61397D30779FE9A4354EADD169DC5, L_24, /*hidden argument*/NULL);
		// SceneManager.LoadScene(8);
		IL2CPP_RUNTIME_CLASS_INIT(SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_m5550E6368A6D0E37DACEDA3C5E4BA331836BC3C5(8, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Settings::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Settings__ctor_m2E00E4F8E213532A3051E24E75CE65368C0BB661 (Settings_t9AEAFFBA64B4FC9ECF724519DF460F19962F4495 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SettingsLang::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SettingsLang_Awake_m4651456ED167577745ECA89FB1F3F4FF06CC7C68 (SettingsLang_tEC296FA53D4D04D71C9F9F8E0316113CC0DA7B62 * __this, const RuntimeMethod* method)
{
	{
		// if (Application.systemLanguage == SystemLanguage.Polish)
		int32_t L_0;
		L_0 = Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)27)))))
		{
			goto IL_0017;
		}
	}
	{
		// setLang(pl, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_1 = __this->get_pl_8();
		SettingsLang_setLang_mF661C7627049B8FCB2F4535F048F85C94279B563(__this, L_1, 0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0017:
	{
		// else if (Application.systemLanguage == SystemLanguage.German)
		int32_t L_2;
		L_2 = Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)15)))))
		{
			goto IL_002e;
		}
	}
	{
		// setLang(de, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_3 = __this->get_de_10();
		SettingsLang_setLang_mF661C7627049B8FCB2F4535F048F85C94279B563(__this, L_3, 0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_002e:
	{
		// else if (Application.systemLanguage == SystemLanguage.French)
		int32_t L_4;
		L_4 = Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)((int32_t)14)))))
		{
			goto IL_0045;
		}
	}
	{
		// setLang(fe, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_5 = __this->get_fe_11();
		SettingsLang_setLang_mF661C7627049B8FCB2F4535F048F85C94279B563(__this, L_5, 0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0045:
	{
		// else if (Application.systemLanguage == SystemLanguage.Spanish)
		int32_t L_6;
		L_6 = Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_005c;
		}
	}
	{
		// setLang(es, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_7 = __this->get_es_12();
		SettingsLang_setLang_mF661C7627049B8FCB2F4535F048F85C94279B563(__this, L_7, 0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_005c:
	{
		// else if (Application.systemLanguage == SystemLanguage.Russian)
		int32_t L_8;
		L_8 = Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)30)))))
		{
			goto IL_0073;
		}
	}
	{
		// setLang(ru, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_9 = __this->get_ru_13();
		SettingsLang_setLang_mF661C7627049B8FCB2F4535F048F85C94279B563(__this, L_9, 0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0073:
	{
		// setLang(en, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_10 = __this->get_en_9();
		SettingsLang_setLang_mF661C7627049B8FCB2F4535F048F85C94279B563(__this, L_10, 0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void SettingsLang::setLang(System.String[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SettingsLang_setLang_mF661C7627049B8FCB2F4535F048F85C94279B563 (SettingsLang_tEC296FA53D4D04D71C9F9F8E0316113CC0DA7B62 * __this, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___Leng0, int32_t ___Index1, const RuntimeMethod* method)
{
	{
		// Text1.text = Leng[Index].ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_0 = __this->get_Text1_4();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_1 = ___Leng0;
		int32_t L_2 = ___Index1;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		String_t* L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		NullCheck(L_4);
		String_t* L_5;
		L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_4);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_5);
		// Text2.text = (Leng[Index + 1]).ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_6 = __this->get_Text2_5();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_7 = ___Leng0;
		int32_t L_8 = ___Index1;
		NullCheck(L_7);
		int32_t L_9 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
		String_t* L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck(L_10);
		String_t* L_11;
		L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		NullCheck(L_6);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_6, L_11);
		// Text3.text = (Leng[Index + 2]).ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_12 = __this->get_Text3_6();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_13 = ___Leng0;
		int32_t L_14 = ___Index1;
		NullCheck(L_13);
		int32_t L_15 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)2));
		String_t* L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck(L_16);
		String_t* L_17;
		L_17 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_16);
		NullCheck(L_12);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_12, L_17);
		// }
		return;
	}
}
// System.Void SettingsLang::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SettingsLang__ctor_m02AF41BCD4C92388A17F0117F6E42F6813513C4A (SettingsLang_tEC296FA53D4D04D71C9F9F8E0316113CC0DA7B62 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral04D9F7A73DAB5872453B01CA9006EA04E5D4AF1C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral18545C029F8273DF3E13B4D472D6E44246691335);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral24F81026436D7EF86549AA058D3F541EED7A6974);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral24FF8FA7BBD419257374764826B5F3463CB2BF7A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral292E99589E6C7C2BF893F091E364EBBD4F55F82A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral29AE05BA7260E645D628C1A847FF86E97893857C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral305609DE0BD9A461509B7BA6EAF308D9CE516DB1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral329914E10919B589345B44E12B94F381C92412C3);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4314D0BCBBB70D26FE2C01FA02E57C8C10120C29);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4F96043EB67DE62B0944AC570CD83249C52E7DB7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral52D443779C3B4BD379537021CDDE739770D7F448);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral62F9586420589E0E9C123EA0FB9858B4A9ACCFC5);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6BD017B967376C8E1B363715F386985B52BF6EE1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9AA7B8A822904E1E38F824BA930331B144AD9766);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA3F89865E30490E81ABEFDBF0D19EBC7E69538D2);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB21E0B119910BC35509F196632A1FB64EC6A9970);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB895583198199E0518C92D1D5A9C95989F9A1B99);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCE05A8E05C74643A5E29EF29C33FC7FEE07C45B5);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD2F21AFCBF1FB9CC1CDACB1C883D5FDFBD5C70A4);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEA058543F73E50CB1E0192DD94678A287B69370C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF32A408AE239BB49DEAB74C3B0BE763701964CA7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF5B883644A591C1DAE1BDF15205EB12BF78D8A34);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF70C6534E440D06E2EA7D6513A9E9E3AC3CA025D);
		s_Il2CppMethodInitialized = true;
	}
	{
		// string[] pl = { "USTAWIENIA", "ZMIEŃ NAGRODY", "ZMIEŃ PIN", "POWRÓT DO MENU" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_0 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)4);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_1 = L_0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, _stringLiteralCE05A8E05C74643A5E29EF29C33FC7FEE07C45B5);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteralCE05A8E05C74643A5E29EF29C33FC7FEE07C45B5);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_2 = L_1;
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, _stringLiteral62F9586420589E0E9C123EA0FB9858B4A9ACCFC5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral62F9586420589E0E9C123EA0FB9858B4A9ACCFC5);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_3 = L_2;
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, _stringLiteral24FF8FA7BBD419257374764826B5F3463CB2BF7A);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral24FF8FA7BBD419257374764826B5F3463CB2BF7A);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_4 = L_3;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteralB895583198199E0518C92D1D5A9C95989F9A1B99);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteralB895583198199E0518C92D1D5A9C95989F9A1B99);
		__this->set_pl_8(L_4);
		// string[] en = { "SETTINGS", "CHANGE THE REWARDS", "CHANGE PIN", "GO TO MENU" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_5 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)4);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_6 = L_5;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral4F96043EB67DE62B0944AC570CD83249C52E7DB7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral4F96043EB67DE62B0944AC570CD83249C52E7DB7);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_7 = L_6;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral18545C029F8273DF3E13B4D472D6E44246691335);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral18545C029F8273DF3E13B4D472D6E44246691335);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_8 = L_7;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, _stringLiteral29AE05BA7260E645D628C1A847FF86E97893857C);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral29AE05BA7260E645D628C1A847FF86E97893857C);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_9 = L_8;
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral04D9F7A73DAB5872453B01CA9006EA04E5D4AF1C);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral04D9F7A73DAB5872453B01CA9006EA04E5D4AF1C);
		__this->set_en_9(L_9);
		// string[] de = { "DIE EINSTELLUNGEN", "ÄNDERN SIE DIE BELOHNUNGEN", "ZURÜCK ZUM MENÜ" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_10 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)3);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_11 = L_10;
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, _stringLiteral52D443779C3B4BD379537021CDDE739770D7F448);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral52D443779C3B4BD379537021CDDE739770D7F448);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_12 = L_11;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteralF5B883644A591C1DAE1BDF15205EB12BF78D8A34);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteralF5B883644A591C1DAE1BDF15205EB12BF78D8A34);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_13 = L_12;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteralD2F21AFCBF1FB9CC1CDACB1C883D5FDFBD5C70A4);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteralD2F21AFCBF1FB9CC1CDACB1C883D5FDFBD5C70A4);
		__this->set_de_10(L_13);
		// string[] fe = { "RÉGLAGES", "CHANGER LES RÉCOMPENSES", "CHANGER LE PIN", "RETOUR AU MENU" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_14 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)4);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_15 = L_14;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteralB21E0B119910BC35509F196632A1FB64EC6A9970);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteralB21E0B119910BC35509F196632A1FB64EC6A9970);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_16 = L_15;
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, _stringLiteral329914E10919B589345B44E12B94F381C92412C3);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral329914E10919B589345B44E12B94F381C92412C3);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_17 = L_16;
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, _stringLiteral9AA7B8A822904E1E38F824BA930331B144AD9766);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral9AA7B8A822904E1E38F824BA930331B144AD9766);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_18 = L_17;
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, _stringLiteralEA058543F73E50CB1E0192DD94678A287B69370C);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteralEA058543F73E50CB1E0192DD94678A287B69370C);
		__this->set_fe_11(L_18);
		// string[] es = { "AJUSTES", "CAMBIAR LAS RECOMPENSAS", "CAMBIAR PIN", "VOLVER AL MENÚ" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_19 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)4);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_20 = L_19;
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral305609DE0BD9A461509B7BA6EAF308D9CE516DB1);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral305609DE0BD9A461509B7BA6EAF308D9CE516DB1);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_21 = L_20;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, _stringLiteral4314D0BCBBB70D26FE2C01FA02E57C8C10120C29);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral4314D0BCBBB70D26FE2C01FA02E57C8C10120C29);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_22 = L_21;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, _stringLiteralF32A408AE239BB49DEAB74C3B0BE763701964CA7);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteralF32A408AE239BB49DEAB74C3B0BE763701964CA7);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_23 = L_22;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral292E99589E6C7C2BF893F091E364EBBD4F55F82A);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral292E99589E6C7C2BF893F091E364EBBD4F55F82A);
		__this->set_es_12(L_23);
		// string[] ru = { "НАСТРОЙКИ", "ИЗМЕНИТЬ НАГРАДЫ", "ИЗМЕНИТЬ ПИН", "НАЗАД К МЕНЮ" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_24 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)4);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_25 = L_24;
		NullCheck(L_25);
		ArrayElementTypeCheck (L_25, _stringLiteralF70C6534E440D06E2EA7D6513A9E9E3AC3CA025D);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteralF70C6534E440D06E2EA7D6513A9E9E3AC3CA025D);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_26 = L_25;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral24F81026436D7EF86549AA058D3F541EED7A6974);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral24F81026436D7EF86549AA058D3F541EED7A6974);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_27 = L_26;
		NullCheck(L_27);
		ArrayElementTypeCheck (L_27, _stringLiteral6BD017B967376C8E1B363715F386985B52BF6EE1);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral6BD017B967376C8E1B363715F386985B52BF6EE1);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_28 = L_27;
		NullCheck(L_28);
		ArrayElementTypeCheck (L_28, _stringLiteralA3F89865E30490E81ABEFDBF0D19EBC7E69538D2);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteralA3F89865E30490E81ABEFDBF0D19EBC7E69538D2);
		__this->set_ru_13(L_28);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SettingsPINLang::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SettingsPINLang_Awake_m382EFB07AAC18FE069FC4C64B0572986FD6CC6DD (SettingsPINLang_tDF04F4EF7D0C7ACE1CEFBCB9722593B175BF93C7 * __this, const RuntimeMethod* method)
{
	{
		// if (Application.systemLanguage == SystemLanguage.Polish)
		int32_t L_0;
		L_0 = Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)27)))))
		{
			goto IL_0017;
		}
	}
	{
		// setLang(pl, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_1 = __this->get_pl_9();
		SettingsPINLang_setLang_m2B1B554C9BAFFD7DED5FCBBC253966C47935A989(__this, L_1, 0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0017:
	{
		// else if (Application.systemLanguage == SystemLanguage.German)
		int32_t L_2;
		L_2 = Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)15)))))
		{
			goto IL_002e;
		}
	}
	{
		// setLang(de, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_3 = __this->get_de_11();
		SettingsPINLang_setLang_m2B1B554C9BAFFD7DED5FCBBC253966C47935A989(__this, L_3, 0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_002e:
	{
		// else if (Application.systemLanguage == SystemLanguage.French)
		int32_t L_4;
		L_4 = Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)((int32_t)14)))))
		{
			goto IL_0045;
		}
	}
	{
		// setLang(fe, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_5 = __this->get_fe_12();
		SettingsPINLang_setLang_m2B1B554C9BAFFD7DED5FCBBC253966C47935A989(__this, L_5, 0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0045:
	{
		// else if (Application.systemLanguage == SystemLanguage.Spanish)
		int32_t L_6;
		L_6 = Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_005c;
		}
	}
	{
		// setLang(es, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_7 = __this->get_es_13();
		SettingsPINLang_setLang_m2B1B554C9BAFFD7DED5FCBBC253966C47935A989(__this, L_7, 0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_005c:
	{
		// else if (Application.systemLanguage == SystemLanguage.Russian)
		int32_t L_8;
		L_8 = Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)30)))))
		{
			goto IL_0073;
		}
	}
	{
		// setLang(ru, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_9 = __this->get_ru_14();
		SettingsPINLang_setLang_m2B1B554C9BAFFD7DED5FCBBC253966C47935A989(__this, L_9, 0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0073:
	{
		// setLang(en, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_10 = __this->get_en_10();
		SettingsPINLang_setLang_m2B1B554C9BAFFD7DED5FCBBC253966C47935A989(__this, L_10, 0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void SettingsPINLang::setLang(System.String[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SettingsPINLang_setLang_m2B1B554C9BAFFD7DED5FCBBC253966C47935A989 (SettingsPINLang_tDF04F4EF7D0C7ACE1CEFBCB9722593B175BF93C7 * __this, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___Leng0, int32_t ___Index1, const RuntimeMethod* method)
{
	{
		// Text1.text = Leng[Index].ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_0 = __this->get_Text1_4();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_1 = ___Leng0;
		int32_t L_2 = ___Index1;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		String_t* L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		NullCheck(L_4);
		String_t* L_5;
		L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_4);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_5);
		// Text2.text = (Leng[Index + 1]).ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_6 = __this->get_Text2_5();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_7 = ___Leng0;
		int32_t L_8 = ___Index1;
		NullCheck(L_7);
		int32_t L_9 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
		String_t* L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck(L_10);
		String_t* L_11;
		L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		NullCheck(L_6);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_6, L_11);
		// Text3.text = (Leng[Index + 2]).ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_12 = __this->get_Text3_6();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_13 = ___Leng0;
		int32_t L_14 = ___Index1;
		NullCheck(L_13);
		int32_t L_15 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)2));
		String_t* L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck(L_16);
		String_t* L_17;
		L_17 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_16);
		NullCheck(L_12);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_12, L_17);
		// Text4.text = (Leng[Index + 3]).ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_18 = __this->get_Text4_7();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_19 = ___Leng0;
		int32_t L_20 = ___Index1;
		NullCheck(L_19);
		int32_t L_21 = ((int32_t)il2cpp_codegen_add((int32_t)L_20, (int32_t)3));
		String_t* L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		NullCheck(L_22);
		String_t* L_23;
		L_23 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_22);
		NullCheck(L_18);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_18, L_23);
		// Text5.text = (Leng[Index + 4]).ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_24 = __this->get_Text5_8();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_25 = ___Leng0;
		int32_t L_26 = ___Index1;
		NullCheck(L_25);
		int32_t L_27 = ((int32_t)il2cpp_codegen_add((int32_t)L_26, (int32_t)4));
		String_t* L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_28);
		String_t* L_29;
		L_29 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_28);
		NullCheck(L_24);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_24, L_29);
		// }
		return;
	}
}
// System.Void SettingsPINLang::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SettingsPINLang__ctor_m76902D98A8B491489AA91ED7D0F4D3042A39A4CD (SettingsPINLang_tDF04F4EF7D0C7ACE1CEFBCB9722593B175BF93C7 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral021DFDEE24FD0D1AA24E28572F9B12B32ED7470C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1B4B0C8D886688ABA7D856B296240347916533C8);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral29AE05BA7260E645D628C1A847FF86E97893857C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2A9CFA9E1CCC77FD6688571E47F9B7FB42108B32);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2D04724A9557A62A9E96AFFEAB1585D081CF98B5);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3B7950727169090BB5C52667A23D97449FCA4DB0);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5AC7460DE1EBD9758F16E22EFCD6C591008ED7E5);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6371A78739DD9B8B6C667985882072A75525C424);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral66AE76C32C3B18489B51BA40CF91E95EF53C326C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral68ABC5B7E2D9C8F7C46E42B01F19EFF56C819259);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6D8CE1651FF9BA0EDE1DE46FDEFBF913113FD949);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral71D0A6517C0D3A477013739CD5FE5F7310ACBB30);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral73BC2B8241771FC5EAF5193DADBFB9C5145DBAFB);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral79A646BD7D4C31240F06616D74E5E94D070AF4F6);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7B67306E398C4403BAF521C10FBB0E9E86B3EB5E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral80C25E49528430AD5C029A6E09F8A1DC7107EC3F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral85DECDFBCB5D8A12E0CFF931669813EFEC429128);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8943EFB8DBBDD3E994BBC72010F6830128E100A5);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral93861786C883B3B566EDCAD421047A5C4D006A31);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral97FB36FBA00500C467DEE75B6919C6CC55520A4F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral984415071B027A6E300E4CA80AC0169E46972F0E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9F5D2C9A905003D7A11A41AAE6A62E8DABD4EB59);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralADFF2E97BFEBE69C80BC9BC79C409BF74C970036);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCC1C631599A033919D014A2BA6511F93EF6A26BA);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD3C428E5D5F2471B5037540743F73946D676FACE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD9620E76463FD7E858D4E8FF32FF28CA712A64C7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD9C0FFA7748008D277E391D503EDD5FEC988392A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE2F0BA1E42D98915C07B67C8F89D13676D845119);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF4D5309870776863C10B078D84EE175845413FCD);
		s_Il2CppMethodInitialized = true;
	}
	{
		// string[] pl = { "USTAW PIN","WPISZ PIN", "POTWIERDŹ PIN","WPROWADZONE PINY SĄ NIEZGODNE","ZAPISZ" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_0 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_1 = L_0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, _stringLiteral2D04724A9557A62A9E96AFFEAB1585D081CF98B5);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2D04724A9557A62A9E96AFFEAB1585D081CF98B5);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_2 = L_1;
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, _stringLiteral73BC2B8241771FC5EAF5193DADBFB9C5145DBAFB);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral73BC2B8241771FC5EAF5193DADBFB9C5145DBAFB);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_3 = L_2;
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, _stringLiteralE2F0BA1E42D98915C07B67C8F89D13676D845119);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteralE2F0BA1E42D98915C07B67C8F89D13676D845119);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_4 = L_3;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteral66AE76C32C3B18489B51BA40CF91E95EF53C326C);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral66AE76C32C3B18489B51BA40CF91E95EF53C326C);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_5 = L_4;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, _stringLiteral5AC7460DE1EBD9758F16E22EFCD6C591008ED7E5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral5AC7460DE1EBD9758F16E22EFCD6C591008ED7E5);
		__this->set_pl_9(L_5);
		// string[] en = { "SET PIN", "ENTER PIN", "CHANGE PIN", "THE ENTERED PINS ARE INCOMPATIBLE", "SAVE" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_6 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_7 = L_6;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteralD9620E76463FD7E858D4E8FF32FF28CA712A64C7);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteralD9620E76463FD7E858D4E8FF32FF28CA712A64C7);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_8 = L_7;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, _stringLiteralD3C428E5D5F2471B5037540743F73946D676FACE);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteralD3C428E5D5F2471B5037540743F73946D676FACE);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_9 = L_8;
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral29AE05BA7260E645D628C1A847FF86E97893857C);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral29AE05BA7260E645D628C1A847FF86E97893857C);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_10 = L_9;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral6371A78739DD9B8B6C667985882072A75525C424);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral6371A78739DD9B8B6C667985882072A75525C424);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_11 = L_10;
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, _stringLiteral79A646BD7D4C31240F06616D74E5E94D070AF4F6);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral79A646BD7D4C31240F06616D74E5E94D070AF4F6);
		__this->set_en_10(L_11);
		// string[] de = { "PIN EINSTELLEN", "PIN EINGEBEN", "PIN BESTÄTIGEN", "Die eingegebenen Pins sind nicht kompatibel", "SPEICHERN" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_12 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_13 = L_12;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteralD9C0FFA7748008D277E391D503EDD5FEC988392A);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteralD9C0FFA7748008D277E391D503EDD5FEC988392A);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_14 = L_13;
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, _stringLiteral80C25E49528430AD5C029A6E09F8A1DC7107EC3F);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral80C25E49528430AD5C029A6E09F8A1DC7107EC3F);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_15 = L_14;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteralF4D5309870776863C10B078D84EE175845413FCD);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteralF4D5309870776863C10B078D84EE175845413FCD);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_16 = L_15;
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, _stringLiteral93861786C883B3B566EDCAD421047A5C4D006A31);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral93861786C883B3B566EDCAD421047A5C4D006A31);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_17 = L_16;
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, _stringLiteral6D8CE1651FF9BA0EDE1DE46FDEFBF913113FD949);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral6D8CE1651FF9BA0EDE1DE46FDEFBF913113FD949);
		__this->set_de_11(L_17);
		// string[] fe = { "SET PIN", "ENTRER PIN", "CONFIRMER LE NIP", "LES BROCHES ENTRÉES SONT INCOMPATIBLES", "SAUVEGARDER" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_18 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_19 = L_18;
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, _stringLiteralD9620E76463FD7E858D4E8FF32FF28CA712A64C7);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteralD9620E76463FD7E858D4E8FF32FF28CA712A64C7);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_20 = L_19;
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral71D0A6517C0D3A477013739CD5FE5F7310ACBB30);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral71D0A6517C0D3A477013739CD5FE5F7310ACBB30);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_21 = L_20;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, _stringLiteralADFF2E97BFEBE69C80BC9BC79C409BF74C970036);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteralADFF2E97BFEBE69C80BC9BC79C409BF74C970036);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_22 = L_21;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, _stringLiteral97FB36FBA00500C467DEE75B6919C6CC55520A4F);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral97FB36FBA00500C467DEE75B6919C6CC55520A4F);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_23 = L_22;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral8943EFB8DBBDD3E994BBC72010F6830128E100A5);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral8943EFB8DBBDD3E994BBC72010F6830128E100A5);
		__this->set_fe_12(L_23);
		// string[] es = { "ESTABLECER PIN", "INGRESE SU PIN", "CONFIRMAR PIN", "LOS PINES INTRODUCIDOS SON INCOMPATIBLES", "SALVAR" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_24 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_25 = L_24;
		NullCheck(L_25);
		ArrayElementTypeCheck (L_25, _stringLiteral9F5D2C9A905003D7A11A41AAE6A62E8DABD4EB59);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral9F5D2C9A905003D7A11A41AAE6A62E8DABD4EB59);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_26 = L_25;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral68ABC5B7E2D9C8F7C46E42B01F19EFF56C819259);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral68ABC5B7E2D9C8F7C46E42B01F19EFF56C819259);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_27 = L_26;
		NullCheck(L_27);
		ArrayElementTypeCheck (L_27, _stringLiteral1B4B0C8D886688ABA7D856B296240347916533C8);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1B4B0C8D886688ABA7D856B296240347916533C8);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_28 = L_27;
		NullCheck(L_28);
		ArrayElementTypeCheck (L_28, _stringLiteral3B7950727169090BB5C52667A23D97449FCA4DB0);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral3B7950727169090BB5C52667A23D97449FCA4DB0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_29 = L_28;
		NullCheck(L_29);
		ArrayElementTypeCheck (L_29, _stringLiteral85DECDFBCB5D8A12E0CFF931669813EFEC429128);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral85DECDFBCB5D8A12E0CFF931669813EFEC429128);
		__this->set_es_13(L_29);
		// string[] ru = { "УСТАНОВИТЬ ПИН-код", "ВВЕДИТЕ ПИН-код", "ПОДТВЕРДИТЬ PIN", "Введенные PIN-коды несовместимы", "СПАСТИ" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_30 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_31 = L_30;
		NullCheck(L_31);
		ArrayElementTypeCheck (L_31, _stringLiteral2A9CFA9E1CCC77FD6688571E47F9B7FB42108B32);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2A9CFA9E1CCC77FD6688571E47F9B7FB42108B32);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_32 = L_31;
		NullCheck(L_32);
		ArrayElementTypeCheck (L_32, _stringLiteral7B67306E398C4403BAF521C10FBB0E9E86B3EB5E);
		(L_32)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral7B67306E398C4403BAF521C10FBB0E9E86B3EB5E);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_33 = L_32;
		NullCheck(L_33);
		ArrayElementTypeCheck (L_33, _stringLiteral984415071B027A6E300E4CA80AC0169E46972F0E);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral984415071B027A6E300E4CA80AC0169E46972F0E);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_34 = L_33;
		NullCheck(L_34);
		ArrayElementTypeCheck (L_34, _stringLiteral021DFDEE24FD0D1AA24E28572F9B12B32ED7470C);
		(L_34)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral021DFDEE24FD0D1AA24E28572F9B12B32ED7470C);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_35 = L_34;
		NullCheck(L_35);
		ArrayElementTypeCheck (L_35, _stringLiteralCC1C631599A033919D014A2BA6511F93EF6A26BA);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteralCC1C631599A033919D014A2BA6511F93EF6A26BA);
		__this->set_ru_14(L_35);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ThreeAwardLang::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThreeAwardLang_Awake_mFE9E4E41EF1A0F665F145E4E610E64AE18E16FD5 (ThreeAwardLang_tDDE2CB79F32FB3C2ACA78937569B61DFE012EF26 * __this, const RuntimeMethod* method)
{
	{
		// if (Application.systemLanguage == SystemLanguage.Polish)
		int32_t L_0;
		L_0 = Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)27)))))
		{
			goto IL_0017;
		}
	}
	{
		// setLang(pl, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_1 = __this->get_pl_8();
		ThreeAwardLang_setLang_mB6B0D1EA2E97E5BF6AF62349692D278055A9322A(__this, L_1, 0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0017:
	{
		// else if (Application.systemLanguage == SystemLanguage.German)
		int32_t L_2;
		L_2 = Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)15)))))
		{
			goto IL_002e;
		}
	}
	{
		// setLang(de, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_3 = __this->get_de_10();
		ThreeAwardLang_setLang_mB6B0D1EA2E97E5BF6AF62349692D278055A9322A(__this, L_3, 0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_002e:
	{
		// else if (Application.systemLanguage == SystemLanguage.French)
		int32_t L_4;
		L_4 = Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)((int32_t)14)))))
		{
			goto IL_0045;
		}
	}
	{
		// setLang(fe, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_5 = __this->get_fe_11();
		ThreeAwardLang_setLang_mB6B0D1EA2E97E5BF6AF62349692D278055A9322A(__this, L_5, 0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0045:
	{
		// else if (Application.systemLanguage == SystemLanguage.Spanish)
		int32_t L_6;
		L_6 = Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_005c;
		}
	}
	{
		// setLang(es, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_7 = __this->get_es_12();
		ThreeAwardLang_setLang_mB6B0D1EA2E97E5BF6AF62349692D278055A9322A(__this, L_7, 0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_005c:
	{
		// else if (Application.systemLanguage == SystemLanguage.Russian)
		int32_t L_8;
		L_8 = Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)30)))))
		{
			goto IL_0073;
		}
	}
	{
		// setLang(ru, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_9 = __this->get_ru_13();
		ThreeAwardLang_setLang_mB6B0D1EA2E97E5BF6AF62349692D278055A9322A(__this, L_9, 0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0073:
	{
		// setLang(en, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_10 = __this->get_en_9();
		ThreeAwardLang_setLang_mB6B0D1EA2E97E5BF6AF62349692D278055A9322A(__this, L_10, 0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ThreeAwardLang::setLang(System.String[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThreeAwardLang_setLang_mB6B0D1EA2E97E5BF6AF62349692D278055A9322A (ThreeAwardLang_tDDE2CB79F32FB3C2ACA78937569B61DFE012EF26 * __this, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___Leng0, int32_t ___Index1, const RuntimeMethod* method)
{
	{
		// Text1.text = Leng[Index].ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_0 = __this->get_Text1_4();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_1 = ___Leng0;
		int32_t L_2 = ___Index1;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		String_t* L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		NullCheck(L_4);
		String_t* L_5;
		L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_4);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_5);
		// Text2.text = (Leng[Index + 1]).ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_6 = __this->get_Text2_5();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_7 = ___Leng0;
		int32_t L_8 = ___Index1;
		NullCheck(L_7);
		int32_t L_9 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
		String_t* L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck(L_10);
		String_t* L_11;
		L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		NullCheck(L_6);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_6, L_11);
		// Text3.text = (Leng[Index + 2]).ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_12 = __this->get_Text3_6();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_13 = ___Leng0;
		int32_t L_14 = ___Index1;
		NullCheck(L_13);
		int32_t L_15 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)2));
		String_t* L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck(L_16);
		String_t* L_17;
		L_17 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_16);
		NullCheck(L_12);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_12, L_17);
		// Text4.text = (Leng[Index + 3]).ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_18 = __this->get_Text4_7();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_19 = ___Leng0;
		int32_t L_20 = ___Index1;
		NullCheck(L_19);
		int32_t L_21 = ((int32_t)il2cpp_codegen_add((int32_t)L_20, (int32_t)3));
		String_t* L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		NullCheck(L_22);
		String_t* L_23;
		L_23 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_22);
		NullCheck(L_18);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_18, L_23);
		// }
		return;
	}
}
// System.Void ThreeAwardLang::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThreeAwardLang__ctor_m5EC5F4A4055221D27A5D67F3214A701918D42566 (ThreeAwardLang_tDDE2CB79F32FB3C2ACA78937569B61DFE012EF26 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1297894ED887548D0C5F68D62D8FEE6DD439DCAF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1378668D80AB356B40A2895FBA87A6ED36D93325);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral29D2BAAB6C1DADC3F02DBC611FE1F33D1316C96E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral459128DFEC7AEB59A3931020D880E9BB40929AC4);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4A474656ED5A1176BC10BE769CABB0A46172AFD3);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4F16F1667495C8DAA5142C398B5AFD69A30C4871);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral508746C9AC40E3F9CF297A5FBE2CDCEA56348D2E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5A9E55471716F3AEEA71B91E41E453A38C9CBE7F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5AC7460DE1EBD9758F16E22EFCD6C591008ED7E5);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6D8CE1651FF9BA0EDE1DE46FDEFBF913113FD949);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral79A646BD7D4C31240F06616D74E5E94D070AF4F6);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7EC999EEB31D4FDB96A6DB514AA0CAA86244215E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral85DECDFBCB5D8A12E0CFF931669813EFEC429128);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral87C2990C09F3D73E2C2512ABC980EEC426BC85DE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8943EFB8DBBDD3E994BBC72010F6830128E100A5);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8E20BA6DCE5913BB00D64A29DCCA2A82B16FEFB9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8EDFA4F5CE10FF860ED62FF5BB7081903CF066AD);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA6F0F2DD7D0A47A3F14F63868A9EB46F6CA5ACF8);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralAD62C412EE5B17ED9B961E83E6C72EA9720EF2A8);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralAE394F72B21FDE903D93703B0EE828B3C0A4D310);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCC1C631599A033919D014A2BA6511F93EF6A26BA);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD0922252964B4DA81D53488FD3CE7506E0B343F8);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDFE6FE7196BF4ABC4FF233F1648351E5485673A4);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFEDD4D7E4BC684807EBF7842D5983AEBFAE7AA17);
		s_Il2CppMethodInitialized = true;
	}
	{
		// string[] pl = { "Dodaj trzecią nagrodę", "Nagroda za 3000p", "Wpisz trzecią nagrodę. np.Dodatkowa godzina grania na konsoli", "ZAPISZ" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_0 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)4);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_1 = L_0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, _stringLiteral8E20BA6DCE5913BB00D64A29DCCA2A82B16FEFB9);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral8E20BA6DCE5913BB00D64A29DCCA2A82B16FEFB9);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_2 = L_1;
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, _stringLiteral7EC999EEB31D4FDB96A6DB514AA0CAA86244215E);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral7EC999EEB31D4FDB96A6DB514AA0CAA86244215E);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_3 = L_2;
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, _stringLiteral4A474656ED5A1176BC10BE769CABB0A46172AFD3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral4A474656ED5A1176BC10BE769CABB0A46172AFD3);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_4 = L_3;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteral5AC7460DE1EBD9758F16E22EFCD6C591008ED7E5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral5AC7460DE1EBD9758F16E22EFCD6C591008ED7E5);
		__this->set_pl_8(L_4);
		// string[] en = { "Add a third prize", "Reward for 3000p", "Enter the third award. e.g. An extra hour of playing on the console", "SAVE" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_5 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)4);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_6 = L_5;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteralAD62C412EE5B17ED9B961E83E6C72EA9720EF2A8);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteralAD62C412EE5B17ED9B961E83E6C72EA9720EF2A8);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_7 = L_6;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteralA6F0F2DD7D0A47A3F14F63868A9EB46F6CA5ACF8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteralA6F0F2DD7D0A47A3F14F63868A9EB46F6CA5ACF8);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_8 = L_7;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, _stringLiteralFEDD4D7E4BC684807EBF7842D5983AEBFAE7AA17);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteralFEDD4D7E4BC684807EBF7842D5983AEBFAE7AA17);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_9 = L_8;
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral79A646BD7D4C31240F06616D74E5E94D070AF4F6);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral79A646BD7D4C31240F06616D74E5E94D070AF4F6);
		__this->set_en_9(L_9);
		// string[] de = { "Fügen Sie einen dritten Preis hinzu", "Belohnung für 3000p", "Geben Sie den dritten Preis ein. B. eine zusätzliche Stunde auf der Konsole spielen", "SPEICHERN" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_10 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)4);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_11 = L_10;
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, _stringLiteral508746C9AC40E3F9CF297A5FBE2CDCEA56348D2E);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral508746C9AC40E3F9CF297A5FBE2CDCEA56348D2E);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_12 = L_11;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral87C2990C09F3D73E2C2512ABC980EEC426BC85DE);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral87C2990C09F3D73E2C2512ABC980EEC426BC85DE);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_13 = L_12;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral1297894ED887548D0C5F68D62D8FEE6DD439DCAF);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1297894ED887548D0C5F68D62D8FEE6DD439DCAF);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_14 = L_13;
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, _stringLiteral6D8CE1651FF9BA0EDE1DE46FDEFBF913113FD949);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral6D8CE1651FF9BA0EDE1DE46FDEFBF913113FD949);
		__this->set_de_10(L_14);
		// string[] fe = { "Ajouter un troisième prix", "Récompense pour 3000p", "Entrez le troisième prix. par exemple une heure supplémentaire de lecture sur la console", "SAUVEGARDER" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_15 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)4);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_16 = L_15;
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, _stringLiteralD0922252964B4DA81D53488FD3CE7506E0B343F8);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteralD0922252964B4DA81D53488FD3CE7506E0B343F8);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_17 = L_16;
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, _stringLiteral8EDFA4F5CE10FF860ED62FF5BB7081903CF066AD);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral8EDFA4F5CE10FF860ED62FF5BB7081903CF066AD);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_18 = L_17;
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, _stringLiteralDFE6FE7196BF4ABC4FF233F1648351E5485673A4);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteralDFE6FE7196BF4ABC4FF233F1648351E5485673A4);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_19 = L_18;
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, _stringLiteral8943EFB8DBBDD3E994BBC72010F6830128E100A5);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral8943EFB8DBBDD3E994BBC72010F6830128E100A5);
		__this->set_fe_11(L_19);
		// string[] es = { "Agrega un tercer premio", "Recompensa por 3000p", "Ingrese el tercer premio. por ejemplo, una hora extra de juego en la consola", "SALVAR" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_20 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)4);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_21 = L_20;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, _stringLiteral5A9E55471716F3AEEA71B91E41E453A38C9CBE7F);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral5A9E55471716F3AEEA71B91E41E453A38C9CBE7F);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_22 = L_21;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, _stringLiteralAE394F72B21FDE903D93703B0EE828B3C0A4D310);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteralAE394F72B21FDE903D93703B0EE828B3C0A4D310);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_23 = L_22;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral1378668D80AB356B40A2895FBA87A6ED36D93325);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1378668D80AB356B40A2895FBA87A6ED36D93325);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_24 = L_23;
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, _stringLiteral85DECDFBCB5D8A12E0CFF931669813EFEC429128);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral85DECDFBCB5D8A12E0CFF931669813EFEC429128);
		__this->set_es_12(L_24);
		// string[] ru = { "Добавить третий приз", "Награда за 3000p", "Получите третий приз. например, дополнительный час игры на консоли", "СПАСТИ" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_25 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)4);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_26 = L_25;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral459128DFEC7AEB59A3931020D880E9BB40929AC4);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral459128DFEC7AEB59A3931020D880E9BB40929AC4);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_27 = L_26;
		NullCheck(L_27);
		ArrayElementTypeCheck (L_27, _stringLiteral4F16F1667495C8DAA5142C398B5AFD69A30C4871);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral4F16F1667495C8DAA5142C398B5AFD69A30C4871);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_28 = L_27;
		NullCheck(L_28);
		ArrayElementTypeCheck (L_28, _stringLiteral29D2BAAB6C1DADC3F02DBC611FE1F33D1316C96E);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral29D2BAAB6C1DADC3F02DBC611FE1F33D1316C96E);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_29 = L_28;
		NullCheck(L_29);
		ArrayElementTypeCheck (L_29, _stringLiteralCC1C631599A033919D014A2BA6511F93EF6A26BA);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteralCC1C631599A033919D014A2BA6511F93EF6A26BA);
		__this->set_ru_13(L_29);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TwoAwardLang::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TwoAwardLang_Awake_mDCE3F5388A42670FC8C22230DD373326F14E3CF0 (TwoAwardLang_tA136F34FD58DCE4FC78BAEBF81A33966AB84DF8C * __this, const RuntimeMethod* method)
{
	{
		// if (Application.systemLanguage == SystemLanguage.Polish)
		int32_t L_0;
		L_0 = Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)27)))))
		{
			goto IL_0017;
		}
	}
	{
		// setLang(pl, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_1 = __this->get_pl_8();
		TwoAwardLang_setLang_mF511A6231AF485C0208F0D6B801569C4B0C20C6D(__this, L_1, 0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0017:
	{
		// else if (Application.systemLanguage == SystemLanguage.German)
		int32_t L_2;
		L_2 = Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)15)))))
		{
			goto IL_002e;
		}
	}
	{
		// setLang(de, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_3 = __this->get_de_10();
		TwoAwardLang_setLang_mF511A6231AF485C0208F0D6B801569C4B0C20C6D(__this, L_3, 0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_002e:
	{
		// else if (Application.systemLanguage == SystemLanguage.French)
		int32_t L_4;
		L_4 = Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)((int32_t)14)))))
		{
			goto IL_0045;
		}
	}
	{
		// setLang(fe, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_5 = __this->get_fe_11();
		TwoAwardLang_setLang_mF511A6231AF485C0208F0D6B801569C4B0C20C6D(__this, L_5, 0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0045:
	{
		// else if (Application.systemLanguage == SystemLanguage.Spanish)
		int32_t L_6;
		L_6 = Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_005c;
		}
	}
	{
		// setLang(es, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_7 = __this->get_es_12();
		TwoAwardLang_setLang_mF511A6231AF485C0208F0D6B801569C4B0C20C6D(__this, L_7, 0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_005c:
	{
		// else if (Application.systemLanguage == SystemLanguage.Russian)
		int32_t L_8;
		L_8 = Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)30)))))
		{
			goto IL_0073;
		}
	}
	{
		// setLang(ru, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_9 = __this->get_ru_13();
		TwoAwardLang_setLang_mF511A6231AF485C0208F0D6B801569C4B0C20C6D(__this, L_9, 0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0073:
	{
		// setLang(en, 0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_10 = __this->get_en_9();
		TwoAwardLang_setLang_mF511A6231AF485C0208F0D6B801569C4B0C20C6D(__this, L_10, 0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void TwoAwardLang::setLang(System.String[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TwoAwardLang_setLang_mF511A6231AF485C0208F0D6B801569C4B0C20C6D (TwoAwardLang_tA136F34FD58DCE4FC78BAEBF81A33966AB84DF8C * __this, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___Leng0, int32_t ___Index1, const RuntimeMethod* method)
{
	{
		// Text1.text = Leng[Index].ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_0 = __this->get_Text1_4();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_1 = ___Leng0;
		int32_t L_2 = ___Index1;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		String_t* L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		NullCheck(L_4);
		String_t* L_5;
		L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_4);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_5);
		// Text2.text = (Leng[Index + 1]).ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_6 = __this->get_Text2_5();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_7 = ___Leng0;
		int32_t L_8 = ___Index1;
		NullCheck(L_7);
		int32_t L_9 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
		String_t* L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck(L_10);
		String_t* L_11;
		L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		NullCheck(L_6);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_6, L_11);
		// Text3.text = (Leng[Index + 2]).ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_12 = __this->get_Text3_6();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_13 = ___Leng0;
		int32_t L_14 = ___Index1;
		NullCheck(L_13);
		int32_t L_15 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)2));
		String_t* L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck(L_16);
		String_t* L_17;
		L_17 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_16);
		NullCheck(L_12);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_12, L_17);
		// Text4.text = (Leng[Index + 3]).ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_18 = __this->get_Text4_7();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_19 = ___Leng0;
		int32_t L_20 = ___Index1;
		NullCheck(L_19);
		int32_t L_21 = ((int32_t)il2cpp_codegen_add((int32_t)L_20, (int32_t)3));
		String_t* L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		NullCheck(L_22);
		String_t* L_23;
		L_23 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_22);
		NullCheck(L_18);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_18, L_23);
		// }
		return;
	}
}
// System.Void TwoAwardLang::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TwoAwardLang__ctor_mCF71BEEEE0C2FEA7CD21261F0DD5E30BCE849C78 (TwoAwardLang_tA136F34FD58DCE4FC78BAEBF81A33966AB84DF8C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral31A1366D20C9DFB407937096ACBB38DBCB891917);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral36A3B7109E60582E8F76195C95C760FB2B03D883);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral47782EC37002A09A7750757091F216DF542806E0);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5C8680FB0D26638685FA8EC75DF5799F2B3FD582);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral66DA9442F5FF18F2F34630B0F64A95F05F711755);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7C0AC29A8D0286851370B7150A2AF361A5073C40);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral869DB31C5A3D488D8DC8563CF12CB43429962FF9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral895A7C5562D2DA5DB507C3430A144F324FBA1FEC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9836D089000B3E539DB4605A1AC504E93ABC91B8);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9909DB5082CC8F76FB3430AF7F02F7AE0FEDF4D8);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA48A79516820BAF044494D076B2DC4FEEAAD12CA);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralAE91247CABA7B5523E4506922CC38223BFBE1D3E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB1B80710C4AAD7B9FDBDF44D1C9A0F02886AD9BB);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB4B9884D9A091B24982C78ADD8E79B465BFA9DB8);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC91E1737728E2718A6CDB4388EF6ABC1140C5EFE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA55D789AC0644075564A3A9118DE8B619931A28);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE2FD15AA605F64079A93F2AFE1AC6965E9466670);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE3726E592E03F0661729947C6CB2B48360220E18);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFBD9D6B58583FF60D71D6F28CB3BE23AC41D0834);
		s_Il2CppMethodInitialized = true;
	}
	{
		// string[] pl = { "Dodaj drugą nagrodę", "Nagroda za 2000p", "Wpisz drugą nagrodę. np.Wyście do kina", "NEXT" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_0 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)4);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_1 = L_0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, _stringLiteralDA55D789AC0644075564A3A9118DE8B619931A28);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteralDA55D789AC0644075564A3A9118DE8B619931A28);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_2 = L_1;
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, _stringLiteral869DB31C5A3D488D8DC8563CF12CB43429962FF9);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral869DB31C5A3D488D8DC8563CF12CB43429962FF9);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_3 = L_2;
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, _stringLiteral66DA9442F5FF18F2F34630B0F64A95F05F711755);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral66DA9442F5FF18F2F34630B0F64A95F05F711755);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_4 = L_3;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteral7C0AC29A8D0286851370B7150A2AF361A5073C40);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral7C0AC29A8D0286851370B7150A2AF361A5073C40);
		__this->set_pl_8(L_4);
		// string[] en = { "Add a second award", "Reward for 2000p", "Enter the second award. e.g. Going to the cinema", "NEXT" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_5 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)4);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_6 = L_5;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral31A1366D20C9DFB407937096ACBB38DBCB891917);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral31A1366D20C9DFB407937096ACBB38DBCB891917);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_7 = L_6;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral9909DB5082CC8F76FB3430AF7F02F7AE0FEDF4D8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral9909DB5082CC8F76FB3430AF7F02F7AE0FEDF4D8);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_8 = L_7;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, _stringLiteral47782EC37002A09A7750757091F216DF542806E0);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral47782EC37002A09A7750757091F216DF542806E0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_9 = L_8;
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral7C0AC29A8D0286851370B7150A2AF361A5073C40);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral7C0AC29A8D0286851370B7150A2AF361A5073C40);
		__this->set_en_9(L_9);
		// string[] de = { "Fügen Sie einen zweiten Preis hinzu", "Auszeichnung für 2000p", "Geben Sie den zweiten Preis ein. B. ins Kino gehen", "NEXT" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_10 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)4);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_11 = L_10;
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, _stringLiteral5C8680FB0D26638685FA8EC75DF5799F2B3FD582);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral5C8680FB0D26638685FA8EC75DF5799F2B3FD582);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_12 = L_11;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteralAE91247CABA7B5523E4506922CC38223BFBE1D3E);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteralAE91247CABA7B5523E4506922CC38223BFBE1D3E);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_13 = L_12;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteralC91E1737728E2718A6CDB4388EF6ABC1140C5EFE);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteralC91E1737728E2718A6CDB4388EF6ABC1140C5EFE);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_14 = L_13;
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, _stringLiteral7C0AC29A8D0286851370B7150A2AF361A5073C40);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral7C0AC29A8D0286851370B7150A2AF361A5073C40);
		__this->set_de_10(L_14);
		// string[] fe = { "Ajouter un deuxième prix", "Prix ​​pour 2000p", "Entrez le deuxième prix. par exemple aller au cinéma", "NEXT" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_15 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)4);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_16 = L_15;
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, _stringLiteralA48A79516820BAF044494D076B2DC4FEEAAD12CA);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteralA48A79516820BAF044494D076B2DC4FEEAAD12CA);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_17 = L_16;
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, _stringLiteralE3726E592E03F0661729947C6CB2B48360220E18);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteralE3726E592E03F0661729947C6CB2B48360220E18);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_18 = L_17;
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, _stringLiteralE2FD15AA605F64079A93F2AFE1AC6965E9466670);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteralE2FD15AA605F64079A93F2AFE1AC6965E9466670);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_19 = L_18;
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, _stringLiteral7C0AC29A8D0286851370B7150A2AF361A5073C40);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral7C0AC29A8D0286851370B7150A2AF361A5073C40);
		__this->set_fe_11(L_19);
		// string[] es = { "Agrega un segundo premio", "Premio por 2000p", "Ingrese el segundo premio. por ejemplo, ir al cine", "NEXT" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_20 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)4);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_21 = L_20;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, _stringLiteralFBD9D6B58583FF60D71D6F28CB3BE23AC41D0834);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteralFBD9D6B58583FF60D71D6F28CB3BE23AC41D0834);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_22 = L_21;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, _stringLiteral9836D089000B3E539DB4605A1AC504E93ABC91B8);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral9836D089000B3E539DB4605A1AC504E93ABC91B8);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_23 = L_22;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteralB1B80710C4AAD7B9FDBDF44D1C9A0F02886AD9BB);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteralB1B80710C4AAD7B9FDBDF44D1C9A0F02886AD9BB);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_24 = L_23;
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, _stringLiteral7C0AC29A8D0286851370B7150A2AF361A5073C40);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral7C0AC29A8D0286851370B7150A2AF361A5073C40);
		__this->set_es_12(L_24);
		// string[] ru = { "Добавить второй приз", "Премия за 2000p", "Получите второй приз. например, пойти в кино", "NEXT" };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_25 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)4);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_26 = L_25;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral895A7C5562D2DA5DB507C3430A144F324FBA1FEC);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral895A7C5562D2DA5DB507C3430A144F324FBA1FEC);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_27 = L_26;
		NullCheck(L_27);
		ArrayElementTypeCheck (L_27, _stringLiteral36A3B7109E60582E8F76195C95C760FB2B03D883);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral36A3B7109E60582E8F76195C95C760FB2B03D883);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_28 = L_27;
		NullCheck(L_28);
		ArrayElementTypeCheck (L_28, _stringLiteralB4B9884D9A091B24982C78ADD8E79B465BFA9DB8);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteralB4B9884D9A091B24982C78ADD8E79B465BFA9DB8);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_29 = L_28;
		NullCheck(L_29);
		ArrayElementTypeCheck (L_29, _stringLiteral7C0AC29A8D0286851370B7150A2AF361A5073C40);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral7C0AC29A8D0286851370B7150A2AF361A5073C40);
		__this->set_ru_13(L_29);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AdsBanner/<ShowBannerWhenInitialized>d__4::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CShowBannerWhenInitializedU3Ed__4__ctor_m8DCA04DCF4BE1E4A624C3E15F9D1D9A9A68E35BE (U3CShowBannerWhenInitializedU3Ed__4_t49F7781D8F81FA49B995C576008FC45EC56C152B * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void AdsBanner/<ShowBannerWhenInitialized>d__4::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CShowBannerWhenInitializedU3Ed__4_System_IDisposable_Dispose_mD55CAADBA6A2A4D90D2D9ED73ED3204E52ED6443 (U3CShowBannerWhenInitializedU3Ed__4_t49F7781D8F81FA49B995C576008FC45EC56C152B * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean AdsBanner/<ShowBannerWhenInitialized>d__4::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CShowBannerWhenInitializedU3Ed__4_MoveNext_m9E5EBEBE43E8B1ABE5D9C19983995D650E154AEA (U3CShowBannerWhenInitializedU3Ed__4_t49F7781D8F81FA49B995C576008FC45EC56C152B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Advertisement_t049C3D5BE2CA5B376B1AEC9E88408AD0B2494D84_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	AdsBanner_tE70BD583B6D94ED4239305BA678D6CD5152F38DE * V_1 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		AdsBanner_tE70BD583B6D94ED4239305BA678D6CD5152F38DE * L_1 = __this->get_U3CU3E4__this_2();
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0039;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->set_U3CU3E1__state_0((-1));
		goto IL_0040;
	}

IL_0020:
	{
		// yield return new WaitForSeconds(0.1f);
		WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * L_4 = (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 *)il2cpp_codegen_object_new(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4(L_4, (0.100000001f), /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_4);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0039:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_0040:
	{
		// while (!Advertisement.isInitialized)
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t049C3D5BE2CA5B376B1AEC9E88408AD0B2494D84_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Advertisement_get_isInitialized_mB504A790B305C52734533C91982678CA45EA9A4F(/*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0020;
		}
	}
	{
		// Advertisement.Banner.SetPosition(BannerPosition.BOTTOM_CENTER);
		Banner_SetPosition_mB03D6FE7CBC2DA088649E8EC1DD89D352E201043(4, /*hidden argument*/NULL);
		// Advertisement.Banner.Show(surfacingId);
		AdsBanner_tE70BD583B6D94ED4239305BA678D6CD5152F38DE * L_6 = V_1;
		NullCheck(L_6);
		String_t* L_7 = L_6->get_surfacingId_5();
		Banner_Show_m3C42E55C8F73C12F90BF9AB9D5A293CA7368906E(L_7, /*hidden argument*/NULL);
		// }
		return (bool)0;
	}
}
// System.Object AdsBanner/<ShowBannerWhenInitialized>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CShowBannerWhenInitializedU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m85649261B8CBA0ADFC4624AF146BD1F735BDD6F3 (U3CShowBannerWhenInitializedU3Ed__4_t49F7781D8F81FA49B995C576008FC45EC56C152B * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void AdsBanner/<ShowBannerWhenInitialized>d__4::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CShowBannerWhenInitializedU3Ed__4_System_Collections_IEnumerator_Reset_mDDAA23D5AE0F0E33040D431742D59BEE06E7CB2B (U3CShowBannerWhenInitializedU3Ed__4_t49F7781D8F81FA49B995C576008FC45EC56C152B * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CShowBannerWhenInitializedU3Ed__4_System_Collections_IEnumerator_Reset_mDDAA23D5AE0F0E33040D431742D59BEE06E7CB2B_RuntimeMethod_var)));
	}
}
// System.Object AdsBanner/<ShowBannerWhenInitialized>d__4::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CShowBannerWhenInitializedU3Ed__4_System_Collections_IEnumerator_get_Current_m9DD5569CEF9113FAA0420420EA3015085FEA420A (U3CShowBannerWhenInitializedU3Ed__4_t49F7781D8F81FA49B995C576008FC45EC56C152B * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_gshared_inline (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929(/*hidden argument*/NULL);
	}

IL_000e:
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_2 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)__this->get__items_1();
		int32_t L_3 = ___index0;
		int32_t L_4;
		L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)L_2, (int32_t)L_3);
		return (int32_t)L_4;
	}
}
